﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testCube : MonoBehaviour
{
    float moveX = 0;
    public float speed = 1;
    //private void Awake()
    //{
    //    Application.targetFrameRate = 30;
    //}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        moveX += speed * Time.deltaTime;
        if(moveX > 10)
        {
            moveX = -10;
        }
        transform.position = new Vector3(moveX, 0, 0);
    }
}
