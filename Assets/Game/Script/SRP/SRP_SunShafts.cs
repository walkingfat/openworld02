﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using WalkingFat;

public class SRP_SunShafts : ScriptableRendererFeature
{

    class SunShaftsPass : ScriptableRenderPass
    {
        const string k_RenderSunShaftsPassTag = "SunShaftsPass";

        public RenderTargetIdentifier m_ColorSource;
        public RenderTargetIdentifier m_DepthSource;
        //private RenderTargetHandle m_TempTarget;
        private Material m_Mat;
        RenderTargetHandle m_CopyColor;
        RenderTargetHandle m_CopyDepth;
        RenderTargetHandle m_TempRT0;
        RenderTargetHandle m_TempRT1;
        //RenderTargetHandle m_ScreenCopyId;
        public int m_DownSample;

        public void Setup(RenderTargetIdentifier colorSource, RenderTargetIdentifier depthSource, Material mat, int downSample)
        {
            m_ColorSource = colorSource;
            m_DepthSource = depthSource;
            m_Mat = mat;
            m_DownSample = downSample;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            RenderTextureDescriptor opaqueDesc = cameraTextureDescriptor;
            m_CopyColor.id = Shader.PropertyToID("_CopyColor");
            m_CopyDepth.id = Shader.PropertyToID("_CopyDepth");
            cmd.GetTemporaryRT(m_CopyColor.id, opaqueDesc, FilterMode.Bilinear);
            cmd.GetTemporaryRT(m_CopyDepth.id, opaqueDesc, FilterMode.Bilinear);

            // get two smaller RTs
            RenderTextureDescriptor opaqueDescDS = cameraTextureDescriptor;
            //opaqueDescDS.width /= m_DownSample;
            //opaqueDescDS.height /= m_DownSample;
            opaqueDescDS.graphicsFormat = UnityEngine.Experimental.Rendering.GraphicsFormat.R16G16_SFloat;

            m_TempRT0.id = Shader.PropertyToID("_TempRT0");
            m_TempRT1.id = Shader.PropertyToID("_TempRT1");
            cmd.GetTemporaryRT(m_TempRT0.id, opaqueDescDS, FilterMode.Bilinear);
            cmd.GetTemporaryRT(m_TempRT1.id, opaqueDescDS, FilterMode.Bilinear);
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get(k_RenderSunShaftsPassTag);

            Vector3 lightScreenPos = Camera.main.WorldToScreenPoint(Shader.GetGlobalVector("_SunPos"));
            Vector2 posSS = new Vector2(lightScreenPos.x / Camera.main.pixelWidth, lightScreenPos.y / Camera.main.pixelHeight);

            if (lightScreenPos.z > 0 && posSS.x > -0.5f && posSS.x < 1.5f && posSS.y > -0.5f && posSS.y < 1.5f)
            {
                float edgeFade = Mathf.Clamp01((posSS.x + 0.5f) * (1.5f - posSS.x) * (posSS.y + 0.5f) * (1.5f - posSS.y) * 1);

                Vector4 screenSunPos = new Vector4(posSS.x, posSS.y, edgeFade, 0);

                m_Mat.SetVector("_ScreenSunPos", screenSunPos);

                using (new ProfilingSample(cmd, k_RenderSunShaftsPassTag))
                {
                    // copy screen into temporary RT
                    cmd.Blit(m_ColorSource, m_CopyColor.id);
                    cmd.SetGlobalTexture("_ColorTex", m_CopyColor.id);
                    cmd.Blit(m_DepthSource, m_CopyDepth.id);
                    cmd.SetGlobalTexture("_DepthTex", m_CopyDepth.id);

                    // setup
                    Blit(cmd, m_CopyDepth.id, m_TempRT0.id, m_Mat, 0);

                    // radial blur
                    Blit(cmd, m_TempRT0.id, m_TempRT1.id, m_Mat, 1);
                    Blit(cmd, m_TempRT1.id, m_TempRT0.id, m_Mat, 1);
                    Blit(cmd, m_TempRT0.id, m_TempRT1.id, m_Mat, 1);
                    Blit(cmd, m_TempRT1.id, m_TempRT0.id, m_Mat, 1);

                    // blend scene
                    cmd.SetGlobalTexture("_SunShaftsTex", m_TempRT0.id);
                    Blit(cmd, m_TempRT0.id, m_ColorSource, m_Mat, 2);

                }
            }

            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();
            CommandBufferPool.Release(cmd);
        }

        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            cmd.ReleaseTemporaryRT(m_CopyColor.id);
            cmd.ReleaseTemporaryRT(m_CopyDepth.id);
            cmd.ReleaseTemporaryRT(m_TempRT0.id);
            cmd.ReleaseTemporaryRT(m_TempRT1.id);
        }
    }

    [System.Serializable]
    public class EnvScanObjectsSettings
    {
        public bool isActive = true;
        [Range(1, 4)]
        public int downSample = 2;
        public Material mat;
        public RenderPassEvent Event = RenderPassEvent.AfterRenderingOpaques;

    }

    public EnvScanObjectsSettings settings = new EnvScanObjectsSettings();
    SunShaftsPass sunShaftsPass;

    public override void Create()
    {
        sunShaftsPass = new SunShaftsPass();
        sunShaftsPass.renderPassEvent = settings.Event;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        sunShaftsPass.Setup(renderer.cameraColorTarget, renderer.cameraDepthTarget, settings.mat, settings.downSample);
        if (settings.isActive == true)
            renderer.EnqueuePass(sunShaftsPass);
    }
}


