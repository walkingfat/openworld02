﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using WalkingFat;

public class SRP_ColorfulFog : ScriptableRendererFeature
{

    class ColorfulFogPass : ScriptableRenderPass
    {
        const string k_RenderColorfulFogPassTag = "ColorfulFogPass";

        public RenderTargetIdentifier m_ColorSource;
        public RenderTargetIdentifier m_DepthSource;
        private RenderTargetHandle m_TempRT0;
        private Material m_Mat;

        public void Setup(RenderTargetIdentifier colorSource, RenderTargetIdentifier depthSource, Material mat)
        {
            m_ColorSource = colorSource;
            m_DepthSource = depthSource;
            m_Mat = mat;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            RenderTextureDescriptor opaqueDesc = cameraTextureDescriptor;

            //m_TempRT0.id = Shader.PropertyToID("_SceneTex_ColorfulFog");
            cmd.GetTemporaryRT(m_TempRT0.id, opaqueDesc, FilterMode.Bilinear);
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get(k_RenderColorfulFogPassTag);

            using (new ProfilingSample(cmd, k_RenderColorfulFogPassTag))
            {
                // copy screen into temporary RT
                cmd.Blit(m_ColorSource, m_TempRT0.id);
                cmd.SetGlobalTexture("_ColorTex", m_TempRT0.id);
                cmd.SetGlobalTexture("_DepthTex", m_DepthSource);

                // Setup commands
                cmd.Blit(m_TempRT0.id, m_ColorSource, m_Mat);
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            cmd.ReleaseTemporaryRT(m_TempRT0.id);
            //cmd.ReleaseTemporaryRT(m_TempTarget2.id);
        }
    }

    [System.Serializable]
    public class EnvScanObjectsSettings
    {
        public bool isActive = true;
        public Material mat;
        public RenderPassEvent Event = RenderPassEvent.AfterRenderingOpaques;

    }

    public EnvScanObjectsSettings settings = new EnvScanObjectsSettings();
    ColorfulFogPass colorfulFogPass;

    public override void Create()
    {
        colorfulFogPass = new ColorfulFogPass();
        colorfulFogPass.renderPassEvent = settings.Event;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        colorfulFogPass.Setup(renderer.cameraColorTarget, renderer.cameraDepthTarget, settings.mat);
        if (settings.isActive == true)
            renderer.EnqueuePass(colorfulFogPass);
    }
}


