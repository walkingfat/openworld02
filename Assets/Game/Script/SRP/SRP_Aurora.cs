﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering.Universal;

public class SRP_Aurora : ScriptableRendererFeature
{
    public class AuroraPass : ScriptableRenderPass
    {
        const string k_RenderAuroraPassTag = "AuroraPass";

        public RenderTargetIdentifier m_ColorSource;
        public RenderTargetIdentifier m_DepthSource;
        private Material m_AuroraMat;
        private Mesh m_skyMesh;
        RenderTargetHandle m_CopyColor;
        //RenderTargetHandle m_CopyDepth;
        RenderTargetHandle m_TempRT0;
        RenderTargetHandle m_TempRT1;
        public int m_DownSample;
        private Vector2 m_TexSize;
        public float m_Height;
        public float m_Offset;
        public float m_RadialBlurH0;
        public float m_RadialBlurH1;

        public void Setup(RenderTargetIdentifier colorSource, Mesh skyMesh, Material auroraMat, float offset, float height, int downSample)
        {
            m_ColorSource = colorSource;
            m_skyMesh = skyMesh;
            m_AuroraMat = auroraMat;
            m_DownSample = downSample;
            m_Height = height * 0.01f;
            m_Offset = offset;
            m_RadialBlurH0 = m_Height / 3.14159f;
            m_RadialBlurH1 = m_RadialBlurH0 / 3.14159f;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            RenderTextureDescriptor opaqueDesc = cameraTextureDescriptor;
            m_CopyColor.id = Shader.PropertyToID("_CopyColor");
            //m_CopyDepth.id = Shader.PropertyToID("_CopyDepth");
            cmd.GetTemporaryRT(m_CopyColor.id, opaqueDesc, FilterMode.Bilinear);
            //cmd.GetTemporaryRT(m_CopyDepth.id, opaqueDesc, FilterMode.Bilinear);

            // get two smaller RTs
            RenderTextureDescriptor opaqueDescDS = cameraTextureDescriptor;
            opaqueDescDS.width /= 2;
            opaqueDescDS.height /= 2;
            //opaqueDescDS.graphicsFormat = UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8_SNorm;

            m_TempRT0.id = Shader.PropertyToID("_TempRT0");
            m_TempRT1.id = Shader.PropertyToID("_TempRT1");
            cmd.GetTemporaryRT(m_TempRT0.id, opaqueDescDS, FilterMode.Bilinear);
            cmd.GetTemporaryRT(m_TempRT1.id, opaqueDescDS, FilterMode.Bilinear);

            m_TexSize = new Vector2(opaqueDescDS.width, opaqueDescDS.height);
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get(k_RenderAuroraPassTag);

            Vector3 topPos = Camera.main.WorldToScreenPoint(new Vector3(0,999999,0));
            Vector2 posSS = new Vector2(topPos.x / Camera.main.pixelWidth, topPos.y / Camera.main.pixelHeight);

            posSS.y = Mathf.Clamp(posSS.y, 0, 10);
            //Debug.LogError("posSS = " + posSS);

            Vector4 screenTopPos = new Vector4(posSS.x, posSS.y, 0, 0);
            m_AuroraMat.SetVector("_ScreenTopPos", screenTopPos);
            Vector4 blurParams = new Vector4(1.0f / (float)m_TexSize.x, 1.0f / (float)m_TexSize.y, 10f, 0f);
            m_AuroraMat.SetVector("_BlurParams", blurParams);
            m_AuroraMat.SetFloat("_AuroraOffset", m_Offset);

            using (new ProfilingSample(cmd, k_RenderAuroraPassTag))
            {
                //context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
                cmd.SetRenderTarget(m_TempRT0.id);
                cmd.ClearRenderTarget(false, true, Color.black);

                Matrix4x4 matrix = Matrix4x4.identity;
                cmd.DrawMesh(m_skyMesh, matrix, m_AuroraMat, 0, 0);

                //cmd.Blit(m_TempRT0.id, m_ColorSource);
                
                cmd.SetGlobalFloat("_AuroraHeight", m_Height);
                cmd.Blit(m_TempRT0.id, m_TempRT1.id, m_AuroraMat, 1);

                cmd.SetGlobalFloat("_AuroraHeight", m_RadialBlurH0);
                cmd.Blit(m_TempRT1.id, m_TempRT0.id, m_AuroraMat, 2);
                cmd.SetGlobalFloat("_AuroraHeight", m_RadialBlurH1);
                cmd.Blit(m_TempRT0.id, m_TempRT1.id, m_AuroraMat, 2);

                cmd.Blit(m_TempRT1.id, m_TempRT0.id, m_AuroraMat, 3);
                
                cmd.SetGlobalTexture("_AuroraTex", m_TempRT0.id);
                //cmd.SetRenderTarget(m_ColorSource);
                //cmd.SetGlobalTexture("_AuroraTex", m_TempRT0.id);
                
            }
            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();
            CommandBufferPool.Release(cmd);
        }
        
        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            cmd.ReleaseTemporaryRT(m_CopyColor.id);
            //cmd.ReleaseTemporaryRT(m_CopyDepth.id);
            cmd.ReleaseTemporaryRT(m_TempRT0.id);
            cmd.ReleaseTemporaryRT(m_TempRT1.id);
        }
    }

    [System.Serializable]
    public class AuroraSettings
    {
        public bool isActive = true;
        [Range(1, 4)]
        public int downSample = 2;
        public float height = 10f;
        public Material auroraMat;
        public float auroraOffset;
        public Mesh skyMesh;
        public RenderPassEvent Event = RenderPassEvent.BeforeRenderingOpaques;
    }

    public AuroraSettings settings = new AuroraSettings();
    AuroraPass auroraPass;

    public override void Create()
    {
        auroraPass = new AuroraPass();
        auroraPass.renderPassEvent = settings.Event;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        auroraPass.Setup(renderer.cameraColorTarget, settings.skyMesh, settings.auroraMat, settings.auroraOffset, settings.height, settings.downSample);
        if (settings.isActive == true)
            renderer.EnqueuePass(auroraPass);
    }
}


