﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using WalkingFat;

public class SRP_EnvScan : ScriptableRendererFeature
{

    class EnvScanPass : ScriptableRenderPass
    {
        const string k_RenderEnvScanPassTag = "EnvScanPass";

        public RenderTargetIdentifier m_ColorSource;

        private RenderTargetHandle m_TempTarget1;
        private RenderTargetHandle m_TempTarget2;
        private Material m_LinesMat;
        private Material m_OutlinesMat;
        private Material m_PointMarksMat;

        public void Setup(RenderTargetIdentifier colorSource, Material linesMat, Material outlinesMat, Material pointMarksMat)
        {
            m_ColorSource = colorSource;
            m_LinesMat = linesMat;
            m_OutlinesMat = outlinesMat;
            m_PointMarksMat = pointMarksMat;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            RenderTextureDescriptor opaqueDesc = cameraTextureDescriptor;
            cmd.GetTemporaryRT(m_TempTarget1.id, opaqueDesc, FilterMode.Bilinear);
            cmd.GetTemporaryRT(m_TempTarget2.id, opaqueDesc, FilterMode.Bilinear);
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get(k_RenderEnvScanPassTag);

            using (new ProfilingSample(cmd, k_RenderEnvScanPassTag))
            {
                // copy screen into temporary RT
                Blit(cmd, m_ColorSource, m_TempTarget1.Identifier());

                // Setup blur commands
                cmd.Blit(m_TempTarget1.id, m_TempTarget2.id, m_OutlinesMat);


                // Set texture id so we can use it later
                //cmd.SetGlobalTexture("_GrabBlurTexture", m_BlurTemp1.id);


                Blit(cmd, m_TempTarget2.id, m_ColorSource);
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            cmd.ReleaseTemporaryRT(m_TempTarget1.id);
            cmd.ReleaseTemporaryRT(m_TempTarget2.id);
        }
    }

    [System.Serializable]
    public class EnvScanObjectsSettings
    {
        public bool isActive = true;
        public Material linesMat;
        public Material outlinesMat;
        public Material pointMarksMat;
    }

    public EnvScanObjectsSettings settings = new EnvScanObjectsSettings();
    EnvScanPass envScanPass;

    public override void Create()
    {
        envScanPass = new EnvScanPass();
        envScanPass.renderPassEvent = RenderPassEvent.AfterRenderingOpaques;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        envScanPass.Setup(renderer.cameraColorTarget, settings.linesMat, settings.outlinesMat, settings.pointMarksMat);
        if (settings.isActive == true)
            renderer.EnqueuePass(envScanPass);
    }
}


