﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Experimental.Rendering.Universal;

public class SRP_SSGI : ScriptableRendererFeature
{
    public class SSGIPass : ScriptableRenderPass
    {
        const string k_RenderSSGIPassTag = "SSGIPass";

        public RenderTargetIdentifier m_ColorSource;
        public RenderTargetIdentifier m_DepthSource;
        private Material m_SSGIMat;
        RenderTexture m_HiZRT;
        RenderTexture m_HiZRT_Backup;
        RenderTargetHandle m_CopyColor;
        //RenderTargetHandle m_CopyDepth;
        RenderTexture m_NormalWS;
        RenderTexture m_TraceRT;
        RenderTexture m_PrevTraceRT;
        RenderTexture m_BlurRT;
        //RenderTexture m_BlurRT1;

        int m_HiZlevelCount;
        int m_RayCount;
        int m_TraceSteps;
        int m_TraceMaxLevel;
        int m_TraceStartLevel;
        int m_TraceEndLevel;
        float m_Thickness;
        float m_TemporalScale;
        float m_TemporalWeight;
        bool m_ResolverAABB;
        bool m_IsDenoise;
        float m_SSGIIntensity;

        Vector4 m_TraceRes;
        Vector4 m_ScreenRes;
        //Vector4 m_NoiseRes;

        Matrix4x4 m_PrevMatrixVP;

        int m_TraceSampleIndex = 0;

        public void Setup(
            RenderTargetIdentifier colorSource, RenderTargetIdentifier depthSource, 
            Material ssgiMat, Vector4 screenRes, int rayCount, int traceSteps, 
            int traceMaxLevel, int traceStartlevel, int traceEndLevel,
            float thickness, float temporalScale, float temporalWeight, bool resolverAABB, bool isDenoise, float ssgiIntensity)
        {
            m_ColorSource = colorSource;
            m_DepthSource = depthSource;
            m_SSGIMat = ssgiMat;
            m_ScreenRes = screenRes;
            m_RayCount = rayCount;
            m_TraceSteps = traceSteps;
            m_TraceMaxLevel = traceMaxLevel;
            m_TraceStartLevel = traceStartlevel;
            m_TraceEndLevel = traceEndLevel;
            m_Thickness = thickness;
            m_TemporalScale = temporalScale;
            m_TemporalWeight = temporalWeight;
            m_ResolverAABB = resolverAABB;
            m_IsDenoise = isDenoise;
            m_SSGIIntensity = ssgiIntensity;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            RenderTextureDescriptor opaqueDesc = cameraTextureDescriptor;
            cmd.GetTemporaryRT(m_CopyColor.id, opaqueDesc, FilterMode.Bilinear);

            RenderTexture.ReleaseTemporary(m_HiZRT);
            m_HiZRT = RenderTexture.GetTemporary((int)m_ScreenRes.x, (int)m_ScreenRes.y, 0, RenderTextureFormat.RFloat, RenderTextureReadWrite.Linear);
            m_HiZRT.filterMode = FilterMode.Point;
            m_HiZRT.useMipMap = true;
            m_HiZRT.autoGenerateMips = false;

            RenderTexture.ReleaseTemporary(m_HiZRT_Backup);
            m_HiZRT_Backup = RenderTexture.GetTemporary((int)m_ScreenRes.x, (int)m_ScreenRes.y, 0, RenderTextureFormat.RFloat, RenderTextureReadWrite.Linear);
            m_HiZRT_Backup.filterMode = FilterMode.Point;
            m_HiZRT_Backup.useMipMap = true;
            m_HiZRT_Backup.autoGenerateMips = false;

            m_HiZlevelCount = Mathf.Min(m_HiZRT.mipmapCount, m_HiZRT_Backup.mipmapCount);

            RenderTexture.ReleaseTemporary(m_NormalWS);
            m_NormalWS = RenderTexture.GetTemporary((int)m_ScreenRes.x, (int)m_ScreenRes.y, 0, RenderTextureFormat.ARGBHalf);
            m_NormalWS.filterMode = FilterMode.Point;

            RenderTexture.ReleaseTemporary(m_TraceRT);
            m_TraceRT = RenderTexture.GetTemporary((int)m_ScreenRes.x, (int)m_ScreenRes.y, 0, RenderTextureFormat.ARGBHalf);
            m_TraceRT.filterMode = FilterMode.Bilinear;

            RenderTexture.ReleaseTemporary(m_PrevTraceRT);
            m_PrevTraceRT = RenderTexture.GetTemporary((int)m_ScreenRes.x, (int)m_ScreenRes.y, 0, RenderTextureFormat.ARGBHalf);
            m_PrevTraceRT.filterMode = FilterMode.Bilinear;

            RenderTexture.ReleaseTemporary(m_BlurRT);
            m_BlurRT = RenderTexture.GetTemporary((int)m_ScreenRes.x, (int)m_ScreenRes.y, 0, RenderTextureFormat.ARGBHalf);
            m_BlurRT.filterMode = FilterMode.Bilinear;

            m_TraceRes = new Vector4(
                m_ScreenRes.x / 1024.0f,
                m_ScreenRes.y / 1024.0f,
                0,0
                );

        }

        private float GetHaltonValue(int index, int radix)
        {
            float result = 0f;
            float fraction = 1f / radix;

            while (index > 0)
            {
                result += (index % radix) * fraction;
                index /= radix;
                fraction /= radix;
            }
            return result;
        }         

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get(k_RenderSSGIPassTag);

            Vector2 jitter = new Vector2(GetHaltonValue(m_TraceSampleIndex & 1023, 2), GetHaltonValue(m_TraceSampleIndex & 1023, 3));
            m_TraceSampleIndex += 1;
            if (m_TraceSampleIndex >= 64) m_TraceSampleIndex = 0;

            m_TraceRes = new Vector4(
                m_TraceRes.x, m_TraceRes.y,
                jitter.x, jitter.y
                );

            m_SSGIMat.SetVector("_ScreenRes", m_ScreenRes);
            m_SSGIMat.SetVector("_TraceRes", m_TraceRes);
            m_SSGIMat.SetFloat("_RayCount", m_RayCount);
            m_SSGIMat.SetFloat("_TraceSteps", m_TraceSteps);
            m_SSGIMat.SetFloat("_TraceMaxLevel", m_TraceMaxLevel);
            m_SSGIMat.SetFloat("_TraceStartLevel", m_TraceStartLevel);
            m_SSGIMat.SetFloat("_TraceEndLevel", m_TraceEndLevel);
            m_SSGIMat.SetFloat("_Thickness", m_Thickness);
            m_SSGIMat.SetFloat("_TemporalScale", m_TemporalScale);
            m_SSGIMat.SetFloat("_TemporalWeight", m_TemporalWeight);
            m_SSGIMat.SetFloat("_SSGIIntensity", m_SSGIIntensity);

            Matrix4x4 matrix_V = renderingData.cameraData.camera.worldToCameraMatrix;
            Matrix4x4 matrix_P = GL.GetGPUProjectionMatrix(renderingData.cameraData.camera.projectionMatrix, false);
            Matrix4x4 matrix_VP = matrix_P * matrix_V;
            cmd.SetGlobalMatrix("_Matrix_V", matrix_V);
            cmd.SetGlobalMatrix("_Matrix_IV", matrix_V.inverse);
            cmd.SetGlobalMatrix("_Matrix_P", matrix_P);
            cmd.SetGlobalMatrix("_Matrix_IP", matrix_P.inverse);
            cmd.SetGlobalMatrix("_Matrix_VP", matrix_VP);
            cmd.SetGlobalMatrix("_Matrix_IVP", matrix_VP.inverse);

            cmd.SetGlobalMatrix("_Matrix_VP_Prev", m_PrevMatrixVP);
            m_PrevMatrixVP = matrix_VP;

            using (new ProfilingSample(cmd, k_RenderSSGIPassTag))
            {
                // copy screen into temporary RT
                cmd.Blit(m_ColorSource, m_CopyColor.id);
                //cmd.Blit(m_DepthSource, m_CopyDepth.id);
                // cmd.SetGlobalTexture("_SSGI_DepthTex", m_CopyDepth.id);

                cmd.Blit(m_DepthSource, m_HiZRT, m_SSGIMat, 0);
                //cmd.SetRenderTarget(m_HiZRT, 0);
                //cmd.SetViewProjectionMatrices(Matrix4x4.identity, Matrix4x4.identity);
                //cmd.DrawMesh(RenderingUtils.fullscreenMesh, Matrix4x4.identity, m_SSGIMat, 0, 0);
                cmd.GenerateMips(m_HiZRT);
                cmd.SetGlobalTexture("_HiZRT", m_HiZRT);
                
                // get Hi-Z RT
                for (int i = 1; i < m_HiZlevelCount; ++i)
                {
                    cmd.SetGlobalInt("_HiZ_PrevLevel", i - 1);
                    cmd.SetRenderTarget(m_HiZRT_Backup, i);
                    cmd.SetViewProjectionMatrices(Matrix4x4.identity, Matrix4x4.identity);
                    cmd.DrawMesh(RenderingUtils.fullscreenMesh, Matrix4x4.identity, m_SSGIMat, 0, 1);
                    cmd.CopyTexture(m_HiZRT_Backup, 0, i, m_HiZRT, 0, i);
                }
                cmd.SetGlobalTexture("_HiZRT", m_HiZRT);

                // get world normal RT
                cmd.Blit(m_DepthSource, m_NormalWS, m_SSGIMat, 2);
                //cmd.SetRenderTarget(m_NormalWS);
                //cmd.SetViewProjectionMatrices(Matrix4x4.identity, Matrix4x4.identity);
                //cmd.DrawMesh(RenderingUtils.fullscreenMesh, Matrix4x4.identity, m_SSGIMat, 0, 2);
                cmd.SetGlobalTexture("_NormalWS", m_NormalWS);

                // ray trace and Temporal filter
                if(m_ResolverAABB == true)
                {
                    cmd.Blit(m_CopyColor.id, m_TraceRT, m_SSGIMat, 3);

                    cmd.SetGlobalTexture("_PrevSSGITraceRT", m_PrevTraceRT);
                    // Temporal filter
                    cmd.Blit(m_TraceRT, m_BlurRT, m_SSGIMat, 4);
                    cmd.Blit(m_BlurRT, m_PrevTraceRT);

                    // denoise
                    if (m_IsDenoise)
                    {
                        cmd.Blit(m_BlurRT, m_TraceRT, m_SSGIMat, 5);
                        cmd.Blit(m_TraceRT, m_BlurRT, m_SSGIMat, 6);
                    }
                    cmd.SetGlobalTexture("_SSGIRT", m_BlurRT);
                }
                else
                {
                    cmd.SetGlobalTexture("_PrevSSGITraceRT", m_PrevTraceRT);
                    cmd.Blit(m_CopyColor.id, m_TraceRT, m_SSGIMat, 8);
                    cmd.Blit(m_TraceRT, m_PrevTraceRT);

                    // denoise
                    if (m_IsDenoise)
                    {
                        cmd.Blit(m_TraceRT, m_BlurRT, m_SSGIMat, 5);
                        cmd.Blit(m_BlurRT, m_TraceRT, m_SSGIMat, 6);
                    }
                    cmd.SetGlobalTexture("_SSGIRT", m_TraceRT);
                }
                
                //cmd.Blit(m_TraceRT, m_ColorSource, m_SSGIMat, 7);
                cmd.Blit(m_CopyColor.id, m_ColorSource, m_SSGIMat, 7);

            }
            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();
            CommandBufferPool.Release(cmd);
        }

        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            cmd.ReleaseTemporaryRT(m_CopyColor.id);
            //cmd.ReleaseTemporaryRT(m_CopyDepth.id);
            
            RenderTexture.ReleaseTemporary(m_TraceRT);
            RenderTexture.ReleaseTemporary(m_PrevTraceRT);
            
            RenderTexture.ReleaseTemporary(m_HiZRT);
            RenderTexture.ReleaseTemporary(m_HiZRT_Backup);
            RenderTexture.ReleaseTemporary(m_NormalWS);
            RenderTexture.ReleaseTemporary(m_BlurRT);
            //RenderTexture.ReleaseTemporary(m_BlurRT1);

        }
    }

    [System.Serializable]
    public class SSGISettings
    {
        public bool isActive = true;
        public bool isHalfRes = false;
        public bool isDenoise = false;
        public bool resolverAABB = false;
        public Material SSGIMat;
        [Range(1, 64)]
        public int rayCount;
        [Range(16, 256)]
        public int traceSteps;
        [Range(0.0f, 10.0f)]
        public float thickness;
        [Range(0, 10)]
        public int traceMaxLevel;
        [Range(0, 3)]
        public int traceStartLevel;
        [Range(0, 2)]
        public int traceEndLevel; 
        [Range(0.1f, 10.0f)]
        public float temporalScale;
        [Range(0.1f, 0.99f)]
        public float temporalWeight;
        [Range(0.0f, 2.0f)]
        public float ssgiIntensity;

        public RenderPassEvent Event = RenderPassEvent.AfterRenderingTransparents;
    }

    public SSGISettings settings = new SSGISettings();
    SSGIPass ssgiPass;

    public override void Create()
    {
        ssgiPass = new SSGIPass();
        ssgiPass.renderPassEvent = settings.Event;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        Vector4 screenRes = new Vector4(
                renderingData.cameraData.camera.pixelWidth,
                renderingData.cameraData.camera.pixelHeight,
                1.0f / (float)renderingData.cameraData.camera.pixelWidth,
                1.0f / (float)renderingData.cameraData.camera.pixelHeight
                );
        if (settings.isHalfRes)
        {
            screenRes = new Vector4(
                renderingData.cameraData.camera.pixelWidth * 0.5f,
                renderingData.cameraData.camera.pixelHeight * 0.5f,
                2.0f / (float)renderingData.cameraData.camera.pixelWidth,
                2.0f / (float)renderingData.cameraData.camera.pixelHeight
                );
        }

        ssgiPass.Setup(
            renderer.cameraColorTarget, renderer.cameraDepthTarget, settings.SSGIMat, screenRes, 
            settings.rayCount, settings.traceSteps,settings.traceMaxLevel, settings.traceStartLevel, settings.traceEndLevel,
            settings.thickness, settings.temporalScale, settings.temporalWeight, settings.resolverAABB, settings.isDenoise, settings.ssgiIntensity
            );
        if (settings.isActive == true)
            renderer.EnqueuePass(ssgiPass);
    }
}


