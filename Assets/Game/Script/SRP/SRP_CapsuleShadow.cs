﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using WalkingFat;

public class SRP_CapsuleShadow : ScriptableRendererFeature
{

    class CapsuleShadowPass : ScriptableRenderPass
    {
        const string k_RenderCapsuleShadowPassTag = "CapsuleShadowPass";

        public RenderTargetIdentifier m_ColorSource;
        public RenderTargetIdentifier m_DepthSource;
        private RenderTargetHandle m_TempRT;
        private Material m_Mat;
        public int m_DownSample;

        public void Setup(RenderTargetIdentifier colorSource, RenderTargetIdentifier depthSource, Material mat, int downSample)
        {
            m_ColorSource = colorSource;
            m_DepthSource = depthSource;
            m_Mat = mat;
            m_DownSample = downSample;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            // get two smaller RTs
            RenderTextureDescriptor opaqueDesc = cameraTextureDescriptor;
            opaqueDesc.width /= m_DownSample;
            opaqueDesc.height /= m_DownSample;
            //opaqueDesc.width = 256;
            //opaqueDesc.height = 256;
            //opaqueDesc.graphicsFormat = UnityEngine.Experimental.Rendering.GraphicsFormat.R8_SRGB;

            m_TempRT.id = Shader.PropertyToID("_TempRT");
            cmd.GetTemporaryRT(m_TempRT.id, opaqueDesc, FilterMode.Bilinear);
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get(k_RenderCapsuleShadowPassTag);

            cmd.SetGlobalTexture("_ColorTex", m_ColorSource);
            cmd.SetGlobalTexture("_DepthTex", m_DepthSource);

            using (new ProfilingSample(cmd, k_RenderCapsuleShadowPassTag))
            {
                //cmd.Blit(m_DepthSource, m_ColorSource, m_Mat, 0);

                cmd.Blit(m_DepthSource, m_TempRT.id, m_Mat, 0);
                cmd.Blit(m_TempRT.id, m_ColorSource, m_Mat, 1);
                //cmd.SetGlobalTexture("_CapsuleShadowMap", m_TempRT.id);
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        public override void FrameCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            cmd.ReleaseTemporaryRT(m_TempRT.id);
        }
    }

    [System.Serializable]
    public class SSRPSettings
    {
        public bool isActive = true;
        [Range(1,8)]
        public int downSample = 2;
        public Material mat;
        public RenderPassEvent Event = RenderPassEvent.AfterRenderingOpaques;

    }

    public SSRPSettings settings = new SSRPSettings();
    private CapsuleShadowPass capsuleShadowPass;

    public override void Create()
    {
        capsuleShadowPass = new CapsuleShadowPass();
        capsuleShadowPass.renderPassEvent = settings.Event;
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        capsuleShadowPass.Setup(renderer.cameraColorTarget, renderer.cameraDepthTarget, settings.mat, settings.downSample);
        if (settings.isActive == true)
            renderer.EnqueuePass(capsuleShadowPass);
    }
}


