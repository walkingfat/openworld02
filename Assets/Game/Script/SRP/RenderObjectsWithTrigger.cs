﻿using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.Scripting.APIUpdating;
using UnityEngine.Experimental.Rendering.Universal;

[MovedFrom("UnityEngine.Experimental.Rendering.LWRP")]
public class RenderObjectsWithTrigger : ScriptableRendererFeature
{
    [System.Serializable]
    public class RenderObjectsSettingsWithTrigger
    {
        public string passTag = "RenderObjectsFeature";
        public bool isActive = true;
        public RenderPassEvent Event = RenderPassEvent.AfterRenderingOpaques;

        public RenderObjects.FilterSettings filterSettings = new RenderObjects.FilterSettings();

        public Material overrideMaterial = null;
        public int overrideMaterialPassIndex = 0;

        public bool overrideDepthState = false;
        public CompareFunction depthCompareFunction = CompareFunction.LessEqual;
        public bool enableWrite = true;

        public StencilStateData stencilSettings = new StencilStateData();

        public RenderObjects.CustomCameraSettings cameraSettings = new RenderObjects.CustomCameraSettings();
    }

    public RenderObjectsSettingsWithTrigger settings = new RenderObjectsSettingsWithTrigger();

    RenderObjectsPass renderObjectsPass;

    public override void Create()
    {
        RenderObjects.FilterSettings filter = settings.filterSettings;
        renderObjectsPass = new RenderObjectsPass(settings.passTag, settings.Event, filter.PassNames,
            filter.RenderQueueType, filter.LayerMask, settings.cameraSettings);

        renderObjectsPass.overrideMaterial = settings.overrideMaterial;
        renderObjectsPass.overrideMaterialPassIndex = settings.overrideMaterialPassIndex;

        if (settings.overrideDepthState)
            renderObjectsPass.SetDetphState(settings.enableWrite, settings.depthCompareFunction);

        if (settings.stencilSettings.overrideStencilState)
            renderObjectsPass.SetStencilState(settings.stencilSettings.stencilReference,
                settings.stencilSettings.stencilCompareFunction, settings.stencilSettings.passOperation,
                settings.stencilSettings.failOperation, settings.stencilSettings.zFailOperation);
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        if(settings.isActive == true)
            renderer.EnqueuePass(renderObjectsPass);
    }
}

