﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat
{
    public class DynamicGrassInfo
    {
        public int id;
        public Vector3 pos;
        public Quaternion rotation;
        public Vector3 scale;
    }

    public class GrassGridInfo
    {
        public int startId;
        public int length;
    }

    public class DynamicGrass : MonoBehaviour
    {
        // grass property
        public Mesh mesh;
        public Material mat;
        public int densityChannel = 0;
        public float density = 5f; // grass count per 1 world unit
        public float aloneTerrainRate = 1.0f;
        public float heightOffset = 0.0f;
        public float randomRotateY;
        public float randomRotateXZ;
        public float scaleRateXZ = 1.0f;
        public float scaleRateYMin = 0.0f;
        public float scaleRateYMax = 1.0f;
        public float scaleMin = 0.85f;
        public float scaleMax = 1.25f;
        public float scaleWithDensity = 1.0f;
        public bool scatteringWithDensity = false;
        public int slpitCount = 20;
        public float nearClipDist = 10f;
        public float farClipDist = 10f;

        // draw mesh instance
        public Camera camera;
        public GameObject targetObj;
        private Matrix4x4 matrix;
        private Matrix4x4[] matrices;
        private MaterialPropertyBlock property;
        private int drawLayer;
        public bool castShadows = false;
        public bool recieveShadows = false;
        public bool useGpuInstancing = false;

        public Texture2D heightMap;
        public Texture2D densityMap;

        public int terrainSize = 256;
        public int terrainHeight = 256;

        private List<DynamicGrassInfo> grassInfoList = new List<DynamicGrassInfo>();
        private List<DynamicGrassInfo> currentGrassInfoList = new List<DynamicGrassInfo>();
        private List<int> gridIdList = new List<int>();
        private List<GrassGridInfo> grassGridInfoList  = new List<GrassGridInfo>();

        // common value
        private float terrainSizeReciprocal;
        private float terrainHeightReciprocal;
        private float gridPosMinY;
        //private int currentX = 0;
        //private int currentY = 0;
        private float gridSize;
        private float gridSizeReciprocal;
        private float slpitCountReciprocal;
        private int gridGrassCount;
        private float posOffset;
        private float grassInterval;

        private GameObject obj;
        private GameObject tempCon;
        private GameObject tempObj;
        private int gridId;
        private Vector3 gridCenterPos;
        private GrassGridInfo ggi;
        private DynamicGrassInfo dgi;
        private Vector3 gridStartPos;
        private Vector3 grassScale;
        private Vector3 grassPos;
        private Vector2 uvPos;
        private float scaleFromMap;
        private int grassCount;
        private float mapHeightOffsetX;
        private float mapHeightOffsetY;
        private float mapRotateY;
        private float mapRotateX;
        private Quaternion grassRotation;
        private float mapHeight;
        private float grassPosY;


        // Start is called before the first frame update
        void Start()
        {
            // common value
            terrainSizeReciprocal = 1.0f / terrainSize;
            terrainHeightReciprocal = 1.0f / terrainHeight;
            gridSize = (float)terrainSize / (float)slpitCount;
            gridSizeReciprocal = 1.0f / gridSize;
            slpitCountReciprocal = 1.0f / (float)slpitCount;
            gridGrassCount = Mathf.FloorToInt(gridSize * density);
            gridPosMinY = terrainHeight;
            grassInterval = (float)gridSize / (float)gridGrassCount;
            posOffset = grassInterval * 0.4f;

            obj = new GameObject();
            tempCon = Instantiate(obj, transform);
            tempObj = Instantiate(obj, tempCon.transform);
            Destroy(obj);

            GenerateGrassData();

            SetMatData();

            drawLayer = gameObject.layer;
        }

        public void SetMatData ()
        {
            // fade in
            float fadeInDist = nearClipDist;
            mat.SetFloat("_FadeInDist", fadeInDist);
            // fade out
            float fadeOutDist = farClipDist - gridSize * 0.7f;
            mat.SetFloat("_FadeOutDist", fadeOutDist);
        }

        void Update()
        {

            GetGrassList();

            DrawGrassInstancing();
        }

        public void GenerateGrassData()
        {
            int listStartId = 0;

            for (int h = 0; h < slpitCount; h++)
            {
                for (int v = 0; v < slpitCount; v++)
                {
                    gridId = h + v * slpitCount;
                    gridStartPos = new Vector3(h * gridSize, 0, v * gridSize);

                    grassCount = 0;
                    for (int w = 0; w < gridGrassCount; w++)
                    {
                        for (int l = 0; l < gridGrassCount; l++)
                        {
                            grassPos = gridStartPos + new Vector3(w * grassInterval, 0, l * grassInterval);
                            grassPos += new Vector3(Random.Range(-posOffset, posOffset), 0, Random.Range(-posOffset, posOffset));

                            // get density map
                            uvPos = new Vector2(grassPos.x * terrainSizeReciprocal, grassPos.z * terrainSizeReciprocal);

                            // scale
                            switch(densityChannel)
                            {
                                case 0:
                                    scaleFromMap = densityMap.GetPixelBilinear(uvPos.x, uvPos.y).r;
                                    break;
                                case 1:
                                    scaleFromMap = densityMap.GetPixelBilinear(uvPos.x, uvPos.y).g;
                                    break;
                                case 2:
                                    scaleFromMap = densityMap.GetPixelBilinear(uvPos.x, uvPos.y).b;
                                    break;
                                case 3:
                                    scaleFromMap = densityMap.GetPixelBilinear(uvPos.x, uvPos.y).a;
                                    break;
                                default:
                                    scaleFromMap = densityMap.GetPixelBilinear(uvPos.x, uvPos.y).r;
                                    break;
                            }

                            float clip = 0.2f;
                            if (scatteringWithDensity)
                                clip = Random.Range(0.2f, Mathf.Lerp(1, 0.2f, scaleFromMap));
                            
                            if (scaleFromMap > clip)
                            {
                                scaleFromMap = Mathf.Lerp(1.0f, scaleFromMap, scaleWithDensity);
                                float scale1 = Random.Range(scaleMin, scaleMax) * scaleFromMap;
                                grassScale = new Vector3(1, 0, 1) * Mathf.Lerp(1.0f, scale1, scaleRateXZ);
                                grassScale += new Vector3(0, 1, 0) * Random.Range(scaleRateYMin, scaleRateYMax);

                                // get terrain normal
                                tempObj.transform.localEulerAngles = new Vector3(0, Random.Range(0f, randomRotateY), 0);

                                mapHeightOffsetX = heightMap.GetPixelBilinear(uvPos.x + 0.01f, uvPos.y).r - heightMap.GetPixelBilinear(uvPos.x - 0.01f, uvPos.y).r;
                                mapHeightOffsetY = heightMap.GetPixelBilinear(uvPos.x, uvPos.y - 0.01f).r - heightMap.GetPixelBilinear(uvPos.x, uvPos.y + 0.01f).r;
                                mapRotateY = Mathf.Atan2(mapHeightOffsetX * terrainHeight * aloneTerrainRate, 0.02f * terrainSize) * Mathf.Rad2Deg;
                                mapRotateX = Mathf.Atan2(mapHeightOffsetY * terrainHeight * aloneTerrainRate, 0.02f * terrainSize) * Mathf.Rad2Deg;

                                tempCon.transform.localEulerAngles = new Vector3(mapRotateX, 0, mapRotateY);
                                tempCon.transform.localEulerAngles += new Vector3(
                                    Random.Range(-randomRotateXZ, randomRotateXZ),
                                    0,
                                    Random.Range(-randomRotateXZ, randomRotateXZ)
                                    );

                                grassRotation = tempObj.transform.rotation;

                                // get height map
                                mapHeight = heightMap.GetPixelBilinear(uvPos.x, uvPos.y).r;
                                grassPosY = (mapHeight - 0.5f) * terrainHeight;
                                grassPos += new Vector3(0, grassPosY, 0);
                                grassPos += tempCon.transform.up * heightOffset;

                            
                                dgi = new DynamicGrassInfo();
                                dgi.id = gridId;
                                dgi.pos = grassPos;
                                dgi.rotation = grassRotation;
                                dgi.scale = grassScale;

                                grassInfoList.Add(dgi);
                                
                                grassCount += 1;
                            }
                        }
                    }

                    ggi = new GrassGridInfo();
                    ggi.startId = listStartId;
                    ggi.length = grassCount;
                    grassGridInfoList.Add(ggi);

                    listStartId += grassCount;
                }
            }
        }

        void GetGrassList()
        {
            for (int h = 0; h < slpitCount; h++)
            {
                for (int v = 0; v < slpitCount; v++)
                {
                    float posY = heightMap.GetPixelBilinear((h + 0.5f) * slpitCountReciprocal, (v + 0.5f) * slpitCountReciprocal).r * terrainHeight;
                    gridCenterPos = new Vector3((h + 0.5f) * gridSize, posY - terrainHeight * 0.5f, (v + 0.5f) * gridSize);

                    int gridId = v + h * slpitCount;

                    float gridDist = Vector3.Distance(gridCenterPos, targetObj.transform.position);
                    if (gridDist < farClipDist && gridDist > nearClipDist)
                    {
                        if (gridIdList.Contains(gridId) == false)
                        {
                            // add new grid
                            gridIdList.Add(gridId);
                        }
                    }
                    else
                    {
                        if (gridIdList.Contains(gridId))
                        {
                            // remove far grid
                            gridIdList.Remove(gridId);
                        }

                    }
                }
            }
        }

        void DrawGrassInstancing()
        {
            foreach (int g in gridIdList)
            {
                
                if (grassGridInfoList[g].length > 1023)
                {
                    matrices = new Matrix4x4[1023];
                }
                else
                {
                    matrices = new Matrix4x4[grassGridInfoList[g].length];
                }

                //print("g = " + g + "   matrices length = " + matrices.Length);

                for (int n = 0; n < matrices.Length; n++)
                {
                    int id = grassGridInfoList[g].startId + n;
                    matrix = Matrix4x4.TRS(grassInfoList[id].pos, grassInfoList[id].rotation, grassInfoList[id].scale);

                    if (useGpuInstancing)
                    {
                        matrices[n] = matrix;
                    }
                    else
                    {
                        Graphics.DrawMesh(mesh, matrix, mat, drawLayer, camera, 0, property, castShadows, recieveShadows, false); // otherwise just draw it now
                    }
                }

                if (useGpuInstancing)
                {
                    UnityEngine.Rendering.ShadowCastingMode castShadowMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    if (castShadows)
                        castShadowMode = UnityEngine.Rendering.ShadowCastingMode.On;

                    Graphics.DrawMeshInstanced(mesh, 0, mat, matrices, matrices.Length, property, castShadowMode, recieveShadows, drawLayer, camera);

                }
            }
        }
    }
}

