﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat
{ 
    public class DynamicEnvManager : MonoBehaviour
    {
        public float grassEdgeDist = 100f;
        public Transform[] obstacles;
        private Vector4[] obstaclePositions = new Vector4[100];
        
        void Start()
        {

        }

        void Update()
        {
            // send data to grass shader
            for (int n = 0; n < obstacles.Length; n++)
            {
                obstaclePositions[n] = new Vector4(
                    obstacles[n].position.x,
                    obstacles[n].position.y,
                    obstacles[n].position.z,
                    obstacles[n].GetComponent<CharacterController>().radius * 4
                    );
            }

            Shader.SetGlobalFloat("_grassEdgeDist", grassEdgeDist);
            Shader.SetGlobalFloat("_ObstacleCount", obstacles.Length);
            Shader.SetGlobalVectorArray("_ObstacleParams", obstaclePositions);

        }
    }
}
