﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO; // for json
using UnityEditor; // for inspector button
using WalkingFat;

namespace WalkingFat
{
    // setting data
    [System.Serializable]
    public class WfSkySystemColors
    {
        public float timePoint;
        public Gradient skyColor;
        public Gradient cloudColor;
        public Color haloColor;
        public Color mainLightColor;
        public float haloRadius;
        //public Color fogColor;
        //public Vector2 fogDistance; // start & end
    }

    // json save data
    [System.Serializable]
    public class SkySystemInfo
    {
        public float timePoint;
        // sky color
        public float skyColor0x;
        public float skyColor0y;
        public float skyColor0z;
        public float skyColor1x;
        public float skyColor1y;
        public float skyColor1z;
        public float skyColor2x;
        public float skyColor2y;
        public float skyColor2z;
        public float skyColorMPoint;
        // cloud color
        public float cloudColor0x;
        public float cloudColor0y;
        public float cloudColor0z;
        public float cloudColor1x;
        public float cloudColor1y;
        public float cloudColor1z;
        public float cloudColor2x;
        public float cloudColor2y;
        public float cloudColor2z;
        public float cloudColorMPoint;
        // sun halo color
        public float haloColorX;
        public float haloColorY;
        public float haloColorZ;
        // main light color
        public float mainLightColorX;
        public float mainLightColorY;
        public float mainLightColorZ;
        public float mainLightColorW;

        public float haloRadius;
        // fog
        //public float fogColorX;
        //public float fogColorY;
        //public float fogColorZ;
        //public float fogStart;
        //public float fogEnd;
        public float fogDensity;
    }

    public class DynamicSky : MonoBehaviour
    {
        public float timecycle = 9f; // 0-24 hours
        public float timeSpeed = 2;

        public float cloudyRate = 0.5f;
        public float rainRate = 0.5f;

        public float windAngle = 0f;
        private float currentWindAngle = 0f;
        private float currentWindRadian = 0f;
        public float windStrength = 0f;
        private Vector2 targetWindDir = new Vector2(0, 0);
        private Vector2 currentWindDir = new Vector2(0, 0);
        private Vector2 dirVelocity = new Vector2(0, 0);
        private Vector2 currentCloudUV = new Vector2(0, 0);
        private float angleVelocity = 0;

        public GameObject camera;
        public GameObject hero;
        public Transform sun;
        public float sunRadius = 0.2f;
        public Transform dayTimeAxis;
        private float screenRatio;
        //public GameObject moon;
        public GameObject mainLight;
        private Light mainDirLight;
        //private LensFlare mainLightFlare;

        public GameObject raindropObj;
        private ParticleSystem raindropParticle;

        public WfSkySystemColors[] skyColorCloudy;
        public WfSkySystemColors[] skyColorList;

        private float[] timecycleIntervals;
        private int currentTimecyclePoint;
        private int nextTimecyclePoint;
        //private PostProcessingProfile ppp;

        // Start is called before the first frame update
        void Start()
        {
            LoadSkySystemData();

            //  check time point
            timecycleIntervals = new float[skyColorList.Length];
            for (int n = 0; n < timecycleIntervals.Length; n++)
            {
                if (skyColorList[n].timePoint >= 24f)
                    Debug.LogError("Time point of skyColorList [ " + n + " ] is bigger than 24:00!");

                if (n != timecycleIntervals.Length - 1)
                {
                    if (skyColorList[n + 1].timePoint <= skyColorList[n].timePoint)
                        Debug.LogError("Time point of skyColorList [ " + (n + 1) + " ] is less than before!");

                    timecycleIntervals[n] = skyColorList[n + 1].timePoint - skyColorList[n].timePoint;
                }
                else
                {
                    timecycleIntervals[n] = 24 - skyColorList[n].timePoint;
                }
            }

            screenRatio = (float)Screen.width / (float)Screen.height;
            currentWindAngle = windAngle;
            currentWindRadian = currentWindAngle * Mathf.Deg2Rad;
            targetWindDir = GetWindDir(currentWindRadian);

            mainDirLight = mainLight.GetComponent<Light>() as Light;
            //mainLightFlare = mainLight.GetComponent<LensFlare>() as LensFlare;

            raindropParticle = raindropObj.GetComponent<ParticleSystem>() as ParticleSystem;
        }

        // Update is called once per frame
        void Update()
        {

            Shader.SetGlobalFloat("_RainRate", rainRate);

            //transform.position = hero.transform.position;// new Vector3(hero.transform.position.x, cloud.transform.position.y, hero.transform.position.z);

            // Time cycle =============================================
            timecycle += Time.deltaTime * timeSpeed;
            if (timecycle < 0) timecycle += 24;
            if (timecycle > 24) timecycle -= 24;
            float normalizeTimecycle = timecycle / 24f;
            Shader.SetGlobalFloat("_Timecycle", normalizeTimecycle);

            for (int n = 0; n < timecycleIntervals.Length; n++)
            {
                if (n != timecycleIntervals.Length - 1)
                {
                    if (timecycle >= skyColorList[n].timePoint && timecycle < skyColorList[n + 1].timePoint)
                    {
                        currentTimecyclePoint = n;
                        nextTimecyclePoint = n + 1;
                        //print("currentTimecyclePoint = " + currentTimecyclePoint);
                    }
                }
                else
                {
                    if (timecycle >= skyColorList[n].timePoint)
                    {
                        currentTimecyclePoint = n;
                        nextTimecyclePoint = 0;
                    }
                }
            }

            // wind direction ===========================================
            if (windAngle != currentWindAngle)
            {
                currentWindAngle = windAngle;
                currentWindRadian = currentWindAngle * Mathf.Deg2Rad;
                targetWindDir = GetWindDir(currentWindRadian);
            }

            currentWindDir = targetWindDir;// Vector2.SmoothDamp(currentWindDir, targetWindDir, ref dirVelocity, 5f, 10f);
            currentCloudUV += currentWindDir * windStrength  * 0.001f * Time.deltaTime;
            currentCloudUV = new Vector2(currentCloudUV.x % 1.0f, currentCloudUV.y % 1.0f);

            // rain particle
            float rainForceMax = windStrength * 40f + 20f;
            float rainForceMin = windStrength * -40f - 20f;
            var fol = raindropParticle.forceOverLifetime;
            fol.x = new ParticleSystem.MinMaxCurve(rainForceMin, rainForceMax);
            fol.z = new ParticleSystem.MinMaxCurve(rainForceMin, rainForceMax);
            raindropObj.transform.position = camera.transform.position + camera.transform.forward * 20;
            raindropObj.transform.eulerAngles = new Vector3(
                windStrength * 50f + 90f,
                90 - currentWindAngle,
                0f
                );
            var ems = raindropParticle.emission;
            ems.rateOverTime = rainRate * 1000f;


            // set shader global data
            Shader.SetGlobalVector("_CloudParams", new Vector4(currentCloudUV.x, currentCloudUV.y, 0, 0));
            Shader.SetGlobalVector("_WindParams", new Vector4(currentWindDir.x, currentWindDir.y, windStrength, currentWindRadian)); // x = windDirX， y = windDirZ, z = windStrength, w = windRadian
            Shader.SetGlobalVector("_SunPos", new Vector4(sun.position.x, sun.position.y, sun.position.z, 1));
            Shader.SetGlobalFloat("_ScreenRatio", screenRatio);

            // day Time period
            float correctTimecycle = timecycle + 6;
            if (correctTimecycle < 0) correctTimecycle += 24;
            if (correctTimecycle > 24) correctTimecycle -= 24;
            float periodTime = (correctTimecycle / 12) % 1;
            float periodAngle = Mathf.Lerp(200, -20, periodTime);
            dayTimeAxis.localEulerAngles = new Vector3(periodAngle, 0, 0);
            // print("periodTime = " + periodTime + "    periodAngle = " + periodAngle + "   correctTimecycle = " + correctTimecycle);

            // check day and night
            if (timecycle > 6 && timecycle < 18)
            {
                // day
                //moon.SetActive(false);
                //mainLightFlare.brightness = (6f - Mathf.Abs(12f - timecycle)) * Mathf.Clamp01((1 - cloudyRate) * 2f - 0.6f) * 0.4f;
                Shader.SetGlobalFloat("_SunRadius", sunRadius);
            
            }
            else
            {
                // night
                //moon.SetActive(true);
                //mainLightFlare.brightness = 0;
                Shader.SetGlobalFloat("_SunRadius", 0f);
            }
            float dayNightTime = 1 - Mathf.Clamp((Mathf.Clamp01(Mathf.Abs(timecycle - 12f) / 12f) - 0.3f) * 5f, 0f, 1f);
            float darkTimeForCloudy = 1 - Mathf.Clamp01(Mathf.Abs(timecycle % 12f - 6f));
            float hasSunMoon = 0;
            if (timecycle > 6 && timecycle < 18) hasSunMoon = 1;

            float litRateTime = Mathf.Abs((timecycle % 12) - 6); // 0-6 range
            Shader.SetGlobalFloat("_DayNightTime", dayNightTime);
            Shader.SetGlobalFloat("_LitRateTime", litRateTime);
            Shader.SetGlobalFloat("_HasSunMoon", hasSunMoon);

            // color ================================================
            float colorLerpTime = (timecycle - skyColorList[currentTimecyclePoint].timePoint) / timecycleIntervals[currentTimecyclePoint];
            float correctCloudyRate = Mathf.Pow(cloudyRate, 2f);// Mathf.Clamp01(cloudyRate - 0.5f) * 2f;

            cloudyRate = Mathf.Clamp01(cloudyRate);
            Shader.SetGlobalFloat("_CloudyRate", cloudyRate);
            // sky color ------------------
            // normal
            Color normalSkyColor0 = Color.Lerp(skyColorList[currentTimecyclePoint].skyColor.colorKeys[0].color, skyColorList[nextTimecyclePoint].skyColor.colorKeys[0].color, colorLerpTime);
            Color normalSkyColor1 = Color.Lerp(skyColorList[currentTimecyclePoint].skyColor.colorKeys[1].color, skyColorList[nextTimecyclePoint].skyColor.colorKeys[1].color, colorLerpTime);
            Color normalSkyColor2 = Color.Lerp(skyColorList[currentTimecyclePoint].skyColor.colorKeys[2].color, skyColorList[nextTimecyclePoint].skyColor.colorKeys[2].color, colorLerpTime);
            float normalSkyColorMTime = Mathf.Lerp(skyColorList[currentTimecyclePoint].skyColor.colorKeys[1].time, skyColorList[nextTimecyclePoint].skyColor.colorKeys[1].time, colorLerpTime);
            // cloudy
            Color cloudySkyColor0 = Color.Lerp(skyColorCloudy[0].skyColor.colorKeys[0].color, skyColorCloudy[1].skyColor.colorKeys[0].color, dayNightTime);
            Color cloudySkyColor1 = Color.Lerp(skyColorCloudy[0].skyColor.colorKeys[1].color, skyColorCloudy[1].skyColor.colorKeys[1].color, dayNightTime);
            Color cloudySkyColor2 = Color.Lerp(skyColorCloudy[0].skyColor.colorKeys[2].color, skyColorCloudy[1].skyColor.colorKeys[2].color, dayNightTime);
            float cloudySkyColorMTime = Mathf.Lerp(skyColorCloudy[0].skyColor.colorKeys[1].time, skyColorCloudy[1].skyColor.colorKeys[1].time, dayNightTime);
            // final sky color
            Color currentSkyColor0 = Color.Lerp(normalSkyColor0, cloudySkyColor0, correctCloudyRate);
            Color currentSkyColor1 = Color.Lerp(normalSkyColor1, cloudySkyColor1, correctCloudyRate);
            Color currentSkyColor2 = Color.Lerp(normalSkyColor2, cloudySkyColor2, correctCloudyRate);
            float currentSkyColorMTime = Mathf.Lerp(normalSkyColorMTime, cloudySkyColorMTime, correctCloudyRate);

            Shader.SetGlobalVector("_SkyColor0", currentSkyColor0);
            Shader.SetGlobalVector("_SkyColor1", currentSkyColor1);
            Shader.SetGlobalVector("_SkyColor2", currentSkyColor2);
            Shader.SetGlobalFloat("_SkyColorMTime", currentSkyColorMTime);

            //camera.GetComponent<Camera>().backgroundColor = currentSkyColor1;

            // cloud color --------------------
            // normal
            Color normalCloudColor0 = Color.Lerp(skyColorList[currentTimecyclePoint].cloudColor.colorKeys[0].color, skyColorList[nextTimecyclePoint].cloudColor.colorKeys[0].color, colorLerpTime);
            Color normalCloudColor1 = Color.Lerp(skyColorList[currentTimecyclePoint].cloudColor.colorKeys[1].color, skyColorList[nextTimecyclePoint].cloudColor.colorKeys[1].color, colorLerpTime);
            Color normalCloudColor2 = Color.Lerp(skyColorList[currentTimecyclePoint].cloudColor.colorKeys[2].color, skyColorList[nextTimecyclePoint].cloudColor.colorKeys[2].color, colorLerpTime);
            float normalColorMTime = Mathf.Lerp(skyColorList[currentTimecyclePoint].cloudColor.colorKeys[1].time, skyColorList[nextTimecyclePoint].cloudColor.colorKeys[1].time, colorLerpTime);
            // cloudy
            Color cloudyCloudColor0 = Color.Lerp(skyColorCloudy[0].cloudColor.colorKeys[0].color, skyColorCloudy[1].cloudColor.colorKeys[0].color, dayNightTime);
            Color cloudyCloudColor1 = Color.Lerp(skyColorCloudy[0].cloudColor.colorKeys[1].color, skyColorCloudy[1].cloudColor.colorKeys[1].color, dayNightTime);
            Color cloudyCloudColor2 = Color.Lerp(skyColorCloudy[0].cloudColor.colorKeys[2].color, skyColorCloudy[1].cloudColor.colorKeys[2].color, dayNightTime);
            float cloudyColorMTime = Mathf.Lerp(skyColorCloudy[0].cloudColor.colorKeys[1].time, skyColorCloudy[1].cloudColor.colorKeys[1].time, dayNightTime);
            // final cloud color
            Color currentCloudColor0 = Color.Lerp(normalCloudColor0, cloudyCloudColor0, correctCloudyRate);
            Color currentCloudColor1 = Color.Lerp(normalCloudColor1, cloudyCloudColor1, correctCloudyRate);
            Color currentCloudColor2 = Color.Lerp(normalCloudColor2, cloudyCloudColor2, correctCloudyRate);
            float cloudColorMTime = Mathf.Lerp(normalColorMTime, cloudyColorMTime, correctCloudyRate);

            Shader.SetGlobalVector("_CloudColor0", currentCloudColor0);
            Shader.SetGlobalVector("_CloudColor1", currentCloudColor1);
            Shader.SetGlobalVector("_CloudColor2", currentCloudColor2);
            Shader.SetGlobalFloat("_CloudColorMTime", cloudColorMTime);
            Shader.SetGlobalVector("_CloudEdgeColor", currentSkyColor1);

            // sun halo color

            Color normalHaloColor = Color.Lerp(skyColorList[currentTimecyclePoint].haloColor, skyColorList[nextTimecyclePoint].haloColor, colorLerpTime);
            Color cloudyHaloColor = Color.Lerp(skyColorCloudy[0].haloColor, skyColorCloudy[1].haloColor, dayNightTime);
            Color currentHaloColor = Color.Lerp(normalHaloColor, cloudyHaloColor, correctCloudyRate);
            Shader.SetGlobalVector("_HaloColor", currentHaloColor);

            // sun light color
            Color normalMainLightColor = Color.Lerp(skyColorList[currentTimecyclePoint].mainLightColor, skyColorList[nextTimecyclePoint].mainLightColor, colorLerpTime);
            Color cloudyMainLightColor = Color.Lerp(skyColorCloudy[0].mainLightColor, skyColorCloudy[1].mainLightColor, dayNightTime);
            Color currentMainLightColor = Color.Lerp(normalMainLightColor, cloudyMainLightColor, Mathf.Max(0, correctCloudyRate - 0.3f) * 1.667f);

            mainDirLight.color = new Color(currentMainLightColor.r, currentMainLightColor.g, currentMainLightColor.b, 1);
            mainDirLight.intensity = currentMainLightColor.a;
            Shader.SetGlobalFloat("_MainLitStrength", currentMainLightColor.a);

            // sun halo radius
            float normalHaloRadius = Mathf.Lerp(skyColorList[currentTimecyclePoint].haloRadius, skyColorList[nextTimecyclePoint].haloRadius, colorLerpTime);
            float cloudyHaloRadius = Mathf.Lerp(skyColorCloudy[0].haloRadius, skyColorCloudy[1].haloRadius, dayNightTime);
            float currentHaloRadius = Mathf.Lerp(normalHaloRadius, cloudyHaloRadius, correctCloudyRate);
            Shader.SetGlobalFloat("_HaloRadius", currentHaloRadius);

            // fog
            //Color normalFogColor = Color.Lerp(skyColorList[currentTimecyclePoint].fogColor, skyColorList[nextTimecyclePoint].fogColor, colorLerpTime);
            //Color cloudyFogColor = Color.Lerp(skyColorCloudy[0].fogColor, skyColorCloudy[1].fogColor, dayNightTime);
            //Color currentFogColor = Color.Lerp(normalFogColor, cloudyFogColor, correctCloudyRate);

            //Vector2 normalFogDistance = Vector2.Lerp(skyColorList[currentTimecyclePoint].fogDistance, skyColorList[nextTimecyclePoint].fogDistance, colorLerpTime);
            //Vector2 cloudyFogDistance = Vector2.Lerp(skyColorCloudy[0].fogDistance, skyColorCloudy[1].fogDistance, dayNightTime);
            //Vector2 currentFogDistance = Vector2.Lerp(normalFogDistance, cloudyFogDistance, correctCloudyRate);

            Color currentFogColor = Color.Lerp(currentSkyColor0, currentSkyColor1, 0.4f) * 0.8f;//(1f + correctCloudyRate * 0.6f);
            Color currentAmbientColor = Color.Lerp(currentFogColor, currentMainLightColor, 0.5f) * 0.8f;
            currentAmbientColor.a = 1;
            float fogStartDist = 10 + (1 - correctCloudyRate) * 50;
            float fogEndDist = 200 + (1 - correctCloudyRate) * 500;
            float fogDensity = 0.003f + correctCloudyRate * 0.005f + rainRate * 0.01f;

            RenderSettings.fogColor = currentFogColor;
            RenderSettings.fogStartDistance = fogStartDist;
            RenderSettings.fogEndDistance = fogEndDist;
            RenderSettings.ambientLight = currentAmbientColor;
            RenderSettings.ambientEquatorColor = currentAmbientColor * 0.5f;
            RenderSettings.ambientGroundColor = currentAmbientColor * 0.2f;

            Shader.SetGlobalVector("_AmbientColor", currentAmbientColor);
            //RenderSettings.fogDensity = fogDensity;

            //RenderSettings.fogStartDistance = currentFogDistance.x;
            //RenderSettings.fogEndDistance = currentFogDistance.y;

            //print("time : " + timecycle + "   slope = " + ppp.colorGrading.settings.colorWheels.log.slope + "   colorLerpTime = " + colorLerpTime);
        }



        Vector2 GetWindDir(float radian)
        {
            Vector2 dir = new Vector2(0, 0);
            dir = new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
            dir = dir.normalized;
            return dir;
        }

        public void SaveSkySystemData()
        {
            // timecycle ==================================================================
            List<SkySystemInfo> skySystemTimecycleData = new List<SkySystemInfo>();

            for (int n = 0; n < skyColorList.Length; n++)
            {
                SkySystemInfo ssInfo = new SkySystemInfo();
                // set value--------------------------------------------
                ssInfo.timePoint = skyColorList[n].timePoint;
                // sky color
                ssInfo.skyColor0x = skyColorList[n].skyColor.colorKeys[0].color.r;
                ssInfo.skyColor0y = skyColorList[n].skyColor.colorKeys[0].color.g;
                ssInfo.skyColor0z = skyColorList[n].skyColor.colorKeys[0].color.b;
                ssInfo.skyColor1x = skyColorList[n].skyColor.colorKeys[1].color.r;
                ssInfo.skyColor1y = skyColorList[n].skyColor.colorKeys[1].color.g;
                ssInfo.skyColor1z = skyColorList[n].skyColor.colorKeys[1].color.b;
                ssInfo.skyColor2x = skyColorList[n].skyColor.colorKeys[2].color.r;
                ssInfo.skyColor2y = skyColorList[n].skyColor.colorKeys[2].color.g;
                ssInfo.skyColor2z = skyColorList[n].skyColor.colorKeys[2].color.b;
                ssInfo.skyColorMPoint = skyColorList[n].skyColor.colorKeys[1].time;
                // cloud color
                ssInfo.cloudColor0x = skyColorList[n].cloudColor.colorKeys[0].color.r;
                ssInfo.cloudColor0y = skyColorList[n].cloudColor.colorKeys[0].color.g;
                ssInfo.cloudColor0z = skyColorList[n].cloudColor.colorKeys[0].color.b;
                ssInfo.cloudColor1x = skyColorList[n].cloudColor.colorKeys[1].color.r;
                ssInfo.cloudColor1y = skyColorList[n].cloudColor.colorKeys[1].color.g;
                ssInfo.cloudColor1z = skyColorList[n].cloudColor.colorKeys[1].color.b;
                ssInfo.cloudColor2x = skyColorList[n].cloudColor.colorKeys[2].color.r;
                ssInfo.cloudColor2y = skyColorList[n].cloudColor.colorKeys[2].color.g;
                ssInfo.cloudColor2z = skyColorList[n].cloudColor.colorKeys[2].color.b;
                ssInfo.cloudColorMPoint = skyColorList[n].cloudColor.colorKeys[1].time;
                // sun halo color
                ssInfo.haloColorX = skyColorList[n].haloColor.r;
                ssInfo.haloColorY = skyColorList[n].haloColor.g;
                ssInfo.haloColorZ = skyColorList[n].haloColor.b;
                // main light color
                ssInfo.mainLightColorX = skyColorList[n].mainLightColor.r;
                ssInfo.mainLightColorY = skyColorList[n].mainLightColor.g;
                ssInfo.mainLightColorZ = skyColorList[n].mainLightColor.b;
                ssInfo.mainLightColorW = skyColorList[n].mainLightColor.a;

                ssInfo.haloRadius = skyColorList[n].haloRadius;
                // fog
                //ssInfo.fogStart = skyColorList[n].fogDistance.x;
                //ssInfo.fogEnd = skyColorList[n].fogDistance.y;

                // set into list ---------------------------------
                skySystemTimecycleData.Add(ssInfo);
            }

            // write data to local file
            string timecycleDataPath = System.IO.Path.Combine(Application.dataPath, "Resources/Data/skySystemTimecycleData.txt");
            StreamWriter timecycleSW = new StreamWriter(timecycleDataPath);
            for (int n = 0; n < skyColorList.Length; n++)
            {
                string json = JsonUtility.ToJson(skySystemTimecycleData[n]);
                timecycleSW.WriteLine(json);
            }
            timecycleSW.Close();

            // cloudy ================================================================
            List<SkySystemInfo> skySystemCloudyData = new List<SkySystemInfo>();

            for (int n = 0; n < skyColorCloudy.Length; n++)
            {
                SkySystemInfo ssInfo = new SkySystemInfo();
                // set value--------------------------------------------
                ssInfo.timePoint = skyColorCloudy[n].timePoint;
                // sky color
                ssInfo.skyColor0x = skyColorCloudy[n].skyColor.colorKeys[0].color.r;
                ssInfo.skyColor0y = skyColorCloudy[n].skyColor.colorKeys[0].color.g;
                ssInfo.skyColor0z = skyColorCloudy[n].skyColor.colorKeys[0].color.b;
                ssInfo.skyColor1x = skyColorCloudy[n].skyColor.colorKeys[1].color.r;
                ssInfo.skyColor1y = skyColorCloudy[n].skyColor.colorKeys[1].color.g;
                ssInfo.skyColor1z = skyColorCloudy[n].skyColor.colorKeys[1].color.b;
                ssInfo.skyColor2x = skyColorCloudy[n].skyColor.colorKeys[2].color.r;
                ssInfo.skyColor2y = skyColorCloudy[n].skyColor.colorKeys[2].color.g;
                ssInfo.skyColor2z = skyColorCloudy[n].skyColor.colorKeys[2].color.b;
                ssInfo.skyColorMPoint = skyColorCloudy[n].skyColor.colorKeys[1].time;
                // cloud color
                ssInfo.cloudColor0x = skyColorCloudy[n].cloudColor.colorKeys[0].color.r;
                ssInfo.cloudColor0y = skyColorCloudy[n].cloudColor.colorKeys[0].color.g;
                ssInfo.cloudColor0z = skyColorCloudy[n].cloudColor.colorKeys[0].color.b;
                ssInfo.cloudColor1x = skyColorCloudy[n].cloudColor.colorKeys[1].color.r;
                ssInfo.cloudColor1y = skyColorCloudy[n].cloudColor.colorKeys[1].color.g;
                ssInfo.cloudColor1z = skyColorCloudy[n].cloudColor.colorKeys[1].color.b;
                ssInfo.cloudColor2x = skyColorCloudy[n].cloudColor.colorKeys[2].color.r;
                ssInfo.cloudColor2y = skyColorCloudy[n].cloudColor.colorKeys[2].color.g;
                ssInfo.cloudColor2z = skyColorCloudy[n].cloudColor.colorKeys[2].color.b;
                ssInfo.cloudColorMPoint = skyColorCloudy[n].cloudColor.colorKeys[1].time;
                // sun halo color
                ssInfo.haloColorX = skyColorCloudy[n].haloColor.r;
                ssInfo.haloColorY = skyColorCloudy[n].haloColor.g;
                ssInfo.haloColorZ = skyColorCloudy[n].haloColor.b;
                // main light color
                ssInfo.mainLightColorX = skyColorCloudy[n].mainLightColor.r;
                ssInfo.mainLightColorY = skyColorCloudy[n].mainLightColor.g;
                ssInfo.mainLightColorZ = skyColorCloudy[n].mainLightColor.b;
                ssInfo.mainLightColorW = skyColorCloudy[n].mainLightColor.a;

                ssInfo.haloRadius = skyColorCloudy[n].haloRadius;
                // fog
                //ssInfo.fogStart = skyColorCloudy[n].fogDistance.x;
                //ssInfo.fogEnd = skyColorCloudy[n].fogDistance.y;

                // set into list ---------------------------------
                skySystemCloudyData.Add(ssInfo);
            }
            // write data to local file
            string cloudyDataPath = System.IO.Path.Combine(Application.dataPath, "Resources/Data/skySystemCloudyData.txt");
            StreamWriter cloudySW = new StreamWriter(cloudyDataPath);
            for (int n = 0; n < skyColorCloudy.Length; n++)
            {
                string json = JsonUtility.ToJson(skySystemCloudyData[n]);
                cloudySW.WriteLine(json);
            }
            cloudySW.Close();
        }

        public void LoadSkySystemData()
        {
            // timecycle data =============================================
            List<SkySystemInfo> skySystemTimecycleData = new List<SkySystemInfo>();

            TextAsset ta = Resources.Load<TextAsset>("Data/skySystemTimecycleData");
            string[] tas = ta.text.Split('\n');
            foreach (string line in tas)
            {
                SkySystemInfo tempSsi = JsonUtility.FromJson<SkySystemInfo>(line);
                if (tempSsi != null)
                {
                    skySystemTimecycleData.Add(JsonUtility.FromJson<SkySystemInfo>(line));
                }
            }

            skyColorList = new WfSkySystemColors[skySystemTimecycleData.Count];

            int timecycleCount = 0;
            foreach (var ssInfo in skySystemTimecycleData)
            {
                WfSkySystemColors ssColors = new WfSkySystemColors();
                // set value--------------------------------------------
                ssColors.timePoint = ssInfo.timePoint;
                // sky color
                GradientColorKey[] ckey = new GradientColorKey[3];
                ckey[0].color = new Color(ssInfo.skyColor0x, ssInfo.skyColor0y, ssInfo.skyColor0z, 1);
                ckey[0].time = 0f;
                ckey[1].color = new Color(ssInfo.skyColor1x, ssInfo.skyColor1y, ssInfo.skyColor1z, 1);
                ckey[1].time = ssInfo.skyColorMPoint;
                ckey[2].color = new Color(ssInfo.skyColor2x, ssInfo.skyColor2y, ssInfo.skyColor2z, 1);
                ckey[2].time = 1f;
                Gradient cGradient = new Gradient();
                cGradient.colorKeys = ckey;
                ssColors.skyColor = cGradient;
                // cloud color
                ckey = new GradientColorKey[3];
                ckey[0].color = new Color(ssInfo.cloudColor0x, ssInfo.cloudColor0y, ssInfo.cloudColor0z, 1);
                ckey[0].time = 0f;
                ckey[1].color = new Color(ssInfo.cloudColor1x, ssInfo.cloudColor1y, ssInfo.cloudColor1z, 1);
                ckey[1].time = ssInfo.cloudColorMPoint;
                ckey[2].color = new Color(ssInfo.cloudColor2x, ssInfo.cloudColor2y, ssInfo.cloudColor2z, 1);
                ckey[2].time = 1f;
                cGradient = new Gradient();
                cGradient.colorKeys = ckey;
                ssColors.cloudColor = cGradient;
                // sun halo color
                ssColors.haloColor = new Color(ssInfo.haloColorX, ssInfo.haloColorY, ssInfo.haloColorZ, 1);
                // main light color
                ssColors.mainLightColor = new Color(ssInfo.mainLightColorX, ssInfo.mainLightColorY, ssInfo.mainLightColorZ, ssInfo.mainLightColorW);

                ssColors.haloRadius = ssInfo.haloRadius;

                // fog
                //ssColors.fogDistance = new Vector2(ssInfo.fogStart, ssInfo.fogEnd);

                // set colors --------------------------
                skyColorList[timecycleCount] = ssColors;
                timecycleCount += 1;
            }

            // cloudy data =============================================
            List<SkySystemInfo> skySystemCloudyData = new List<SkySystemInfo>();
            
            ta = Resources.Load<TextAsset>("Data/skySystemCloudyData");
            tas = ta.text.Split('\n');
            foreach (string line in tas)
            {
                SkySystemInfo tempSsi = JsonUtility.FromJson<SkySystemInfo>(line);
                if (tempSsi != null)
                {
                    skySystemCloudyData.Add(JsonUtility.FromJson<SkySystemInfo>(line));
                }
            }

            skyColorCloudy = new WfSkySystemColors[skySystemCloudyData.Count];

            int cloudyCount = 0;
            foreach (var ssInfo in skySystemCloudyData)
            {
                WfSkySystemColors ssColors = new WfSkySystemColors();
                // set value--------------------------------------------
                ssColors.timePoint = ssInfo.timePoint;
                // sky color
                GradientColorKey[] ckey = new GradientColorKey[3];
                ckey[0].color = new Color(ssInfo.skyColor0x, ssInfo.skyColor0y, ssInfo.skyColor0z, 1);
                ckey[0].time = 0f;
                ckey[1].color = new Color(ssInfo.skyColor1x, ssInfo.skyColor1y, ssInfo.skyColor1z, 1);
                ckey[1].time = ssInfo.skyColorMPoint;
                ckey[2].color = new Color(ssInfo.skyColor2x, ssInfo.skyColor2y, ssInfo.skyColor2z, 1);
                ckey[2].time = 1f;
                Gradient cGradient = new Gradient();
                cGradient.colorKeys = ckey;
                ssColors.skyColor = cGradient;
                // cloud color
                ckey = new GradientColorKey[3];
                ckey[0].color = new Color(ssInfo.cloudColor0x, ssInfo.cloudColor0y, ssInfo.cloudColor0z, 1);
                ckey[0].time = 0f;
                ckey[1].color = new Color(ssInfo.cloudColor1x, ssInfo.cloudColor1y, ssInfo.cloudColor1z, 1);
                ckey[1].time = ssInfo.cloudColorMPoint;
                ckey[2].color = new Color(ssInfo.cloudColor2x, ssInfo.cloudColor2y, ssInfo.cloudColor2z, 1);
                ckey[2].time = 1f;
                cGradient = new Gradient();
                cGradient.colorKeys = ckey;
                ssColors.cloudColor = cGradient;
                // sun halo color
                ssColors.haloColor = new Color(ssInfo.haloColorX, ssInfo.haloColorY, ssInfo.haloColorZ, 1);
                // main light color
                ssColors.mainLightColor = new Color(ssInfo.mainLightColorX, ssInfo.mainLightColorY, ssInfo.mainLightColorZ, ssInfo.mainLightColorW);

                ssColors.haloRadius = ssInfo.haloRadius;

                // fog
                //ssColors.fogDistance = new Vector2(ssInfo.fogStart, ssInfo.fogEnd);

                // set colors --------------------------
                skyColorCloudy[cloudyCount] = ssColors;
                cloudyCount += 1;
            }
        }
    }

    #if UNITY_EDITOR
    [CustomEditor(typeof(DynamicSky))]
    public class DynamicSkyEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            DynamicSky myScript = (DynamicSky)target;
            if (GUILayout.Button("Save sky system data"))
            {
                myScript.SaveSkySystemData();
                AssetDatabase.Refresh();
            }
            if (GUILayout.Button("Load sky system data"))
            {
                myScript.LoadSkySystemData();
            }
        }
    }
    #endif
}