﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using WalkingFat;

namespace WalkingFat
{
    public class UI_SSGI : MonoBehaviour
    {
        public SRP_SSGI srpSsgi;
        public Text ssgiSwitchTxt;
        public Text resolutionTxt;
        public Text rayCountTxt;
        public Text traceStepTxt;
        public Text traceStartLevelTxt;
        public Text temporalWeightTxt;
        public Text denoiseTxt;
        public Text resolverAABBTxt;
        public Text intensityTxt;

        // Start is called before the first frame update
        void Awake()
        {
            srpSsgi.settings.isActive = true;
            ssgiSwitchTxt.text = "SSGI On";

            srpSsgi.settings.isHalfRes = true;
            resolutionTxt.text = "Half Res";

            srpSsgi.settings.isDenoise = true;
            denoiseTxt.text = "Denoise On";

            srpSsgi.settings.resolverAABB = true;
            resolverAABBTxt.text = "ResolverAABB On";

            srpSsgi.settings.rayCount = 8;
            rayCountTxt.text = "Ray Count: " + srpSsgi.settings.rayCount;

            srpSsgi.settings.traceSteps = 24;
            traceStepTxt.text = "Trace Step: " + srpSsgi.settings.traceSteps;

            srpSsgi.settings.traceStartLevel = 1;
            traceStartLevelTxt.text = "Trace Start Level: " + srpSsgi.settings.traceStartLevel;

            srpSsgi.settings.temporalWeight = 0.9f;
            temporalWeightTxt.text = "Temporal Weight: " + GetDisplayValue(srpSsgi.settings.temporalWeight);

            srpSsgi.settings.ssgiIntensity = 1.0f;
            intensityTxt.text = "Intensity: " + GetDisplayValue(srpSsgi.settings.ssgiIntensity);
        }
        
        private string GetDisplayValue(float value)
        {
            float v = Mathf.FloorToInt(value * 10) * 0.1f;
            return v.ToString();
        }

        public void SwitchSSGI()
        {
            srpSsgi.settings.isActive = !srpSsgi.settings.isActive;
            if(srpSsgi.settings.isActive)
                ssgiSwitchTxt.text = "SSGI On";
            else
                ssgiSwitchTxt.text = "SSGI Off";
        }

        public void SetResolution()
        {
            srpSsgi.settings.isHalfRes = !srpSsgi.settings.isHalfRes;
            if(srpSsgi.settings.isHalfRes)
                resolutionTxt.text = "Half Res";
            else
                resolutionTxt.text = "Full Res";
        }

        public void SwitchResolverAABB()
        {
            srpSsgi.settings.resolverAABB = !srpSsgi.settings.resolverAABB;
            if(srpSsgi.settings.resolverAABB == true)
                resolverAABBTxt.text = "ResolverAABB On";
            else
                resolverAABBTxt.text = "ResolverAABB Off";
        }

        public void SwitchDenoise()
        {
            srpSsgi.settings.isDenoise = !srpSsgi.settings.isDenoise;
            if (srpSsgi.settings.isDenoise)
                denoiseTxt.text = "Denoise On";
            else
                denoiseTxt.text = "Denoise Off";
        }

        public void SetRayCount()
        {
            srpSsgi.settings.rayCount += 4;
            if (srpSsgi.settings.rayCount > 32)
                srpSsgi.settings.rayCount = 4;

            rayCountTxt.text = "Ray Count: " + srpSsgi.settings.rayCount;
        }

        public void SetTraceSteps()
        {
            srpSsgi.settings.traceSteps += 8;
            if (srpSsgi.settings.traceSteps > 64)
                srpSsgi.settings.traceSteps = 16;

            traceStepTxt.text = "Trace Step: " + srpSsgi.settings.traceSteps;
        }

        public void SetTraceStartLevel()
        {
            srpSsgi.settings.traceStartLevel += 1;
            if (srpSsgi.settings.traceStartLevel > 3)
                srpSsgi.settings.traceStartLevel = 0;

            traceStartLevelTxt.text = "Trace Start Level: " + srpSsgi.settings.traceStartLevel;
        }

        public void SetTemporalWeight()
        {
            srpSsgi.settings.temporalWeight += 0.1f;
            if (srpSsgi.settings.temporalWeight > 0.99f)
                srpSsgi.settings.temporalWeight = 0.3f;

            temporalWeightTxt.text = "Temporal Weight: " + GetDisplayValue(srpSsgi.settings.temporalWeight);
        }

        public void SetSsgiIntensity()
        {
            srpSsgi.settings.ssgiIntensity += 0.5f;
            if (srpSsgi.settings.ssgiIntensity > 5.1f)
                srpSsgi.settings.ssgiIntensity = 0.5f;

            intensityTxt.text = "Intensity: " + GetDisplayValue(srpSsgi.settings.ssgiIntensity);
        }
    }
}