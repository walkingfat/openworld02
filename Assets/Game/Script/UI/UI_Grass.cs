﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WalkingFat;

namespace WalkingFat
{
    public class UI_Grass : MonoBehaviour
    {
        public GameObject[] grassList;
        public Text[] grassTriggerTxtList;
        public Text[] recieveShadowTxtList;
        private DynamicGrass[] dgList = new DynamicGrass[3];

        // Start is called before the first frame update
        void Start()
        {
            for (int n = 0; n < grassList.Length; n++)
            {
                dgList[n] = grassList[n].GetComponent<DynamicGrass>() as DynamicGrass;

                grassList[n].SetActive(true);
                grassTriggerTxtList[n].text = "Grass " + (n + 1).ToString() + " : true";

                dgList[n].recieveShadows = true;
                recieveShadowTxtList[n].text = "Recieve Shadow " + (n + 1).ToString() + " : true";
            }
        }
        
        public void GrassTrigger(int id)
        {
            grassList[id].SetActive(!grassList[id].activeSelf);
            grassTriggerTxtList[id].text = "Grass " + (id + 1).ToString() + " : " + grassList[id].activeSelf;
        }

        public void RecieveShadowTrigger(int id)
        {
            bool rs = !dgList[id].recieveShadows;
            dgList[id].recieveShadows = rs;
            recieveShadowTxtList[id].text = "Recieve Shadow " + (id + 1).ToString() + " : " + rs;
        }
    }
}