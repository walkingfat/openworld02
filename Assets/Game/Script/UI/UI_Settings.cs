﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.Rendering;

enum UIStatus
{
    closed = 0,
    list = 1,
    menu = 2,
}

public class UI_Settings : MonoBehaviour
{
    public GameObject ConHeroAction;

    public GameObject btnSetting;
    public GameObject btnBack;
    public GameObject ConSettingList;

    public GameObject uiOptions;
    public GameObject uiSkySystem;
    public GameObject fx;
    public GameObject uiGrass;
    public GameObject uiSSGI;
    //public SRPBatcherProfiler srpBatcherProfiler;

    private UIStatus status = UIStatus.closed;

    // Start is called before the first frame update
    void Start()
    {
        btnSetting.SetActive(true);
        btnBack.SetActive(false);
        ConSettingList.SetActive(false);
        uiOptions.SetActive(false);
        uiSkySystem.SetActive(false);
        fx.SetActive(false);
        uiGrass.SetActive(false);
        uiSSGI.SetActive(false);
        status = UIStatus.closed;
        ConHeroAction.SetActive(true);
    }
    
    public void OpenSettingList()
    {
        btnSetting.SetActive(false);
        btnBack.SetActive(true);
        ConSettingList.SetActive(true);
        uiOptions.SetActive(false);
        uiSkySystem.SetActive(false);
        fx.SetActive(false);
        uiGrass.SetActive(false);
        uiSSGI.SetActive(false);
        status = UIStatus.list;
        ConHeroAction.SetActive(false);
    }

    public void EnterOptions()
    {
        btnSetting.SetActive(false);
        btnBack.SetActive(true);
        ConSettingList.SetActive(false);
        uiOptions.SetActive(true);
        uiSkySystem.SetActive(false);
        fx.SetActive(false);
        uiGrass.SetActive(false);
        uiSSGI.SetActive(false);
        status = UIStatus.menu;
    }

    public void EnterSkySystem ()
    {
        btnSetting.SetActive(false);
        btnBack.SetActive(true);
        ConSettingList.SetActive(false);
        uiOptions.SetActive(false);
        uiSkySystem.SetActive(true);
        fx.SetActive(false);
        uiGrass.SetActive(false);
        uiSSGI.SetActive(false);
        status = UIStatus.menu;
    }

    public void EnterFX()
    {
        btnSetting.SetActive(false);
        btnBack.SetActive(true);
        ConSettingList.SetActive(false);
        uiOptions.SetActive(false);
        uiSkySystem.SetActive(false);
        fx.SetActive(true);
        uiGrass.SetActive(false);
        uiSSGI.SetActive(false);
        status = UIStatus.menu;
    }

    public void EnterGrass()
    {
        btnSetting.SetActive(false);
        btnBack.SetActive(true);
        ConSettingList.SetActive(false);
        uiOptions.SetActive(false);
        uiSkySystem.SetActive(false);
        fx.SetActive(false);
        uiGrass.SetActive(true);
        uiSSGI.SetActive(false);
        status = UIStatus.menu;
    }

    public void EnterSSGI()
    {
        btnSetting.SetActive(false);
        btnBack.SetActive(true);
        ConSettingList.SetActive(false);
        uiOptions.SetActive(false);
        uiSkySystem.SetActive(false);
        fx.SetActive(false);
        uiGrass.SetActive(false);
        uiSSGI.SetActive(true);
        status = UIStatus.menu;
    }

    /*
    public void SRPBatcherProfilerTrigger()
    {
        srpBatcherProfiler.m_Enable = !srpBatcherProfiler.m_Enable;
    }
    */

    public void BackBtn ()
    {
        if (status == UIStatus.list)
        {
            btnSetting.SetActive(true);
            btnBack.SetActive(false);
            ConSettingList.SetActive(false);
            uiOptions.SetActive(false);
            uiSkySystem.SetActive(false);
            fx.SetActive(false);
            uiGrass.SetActive(false);
            uiSSGI.SetActive(false);
            status = UIStatus.closed;

            ConHeroAction.SetActive(true);
        }
        else if(status == UIStatus.menu)
        {
            btnSetting.SetActive(false);
            btnBack.SetActive(true);
            ConSettingList.SetActive(true);
            uiOptions.SetActive(false);
            uiSkySystem.SetActive(false);
            fx.SetActive(false);
            uiGrass.SetActive(false);
            uiSSGI.SetActive(false);
            status = UIStatus.list;
        }
    }
}
