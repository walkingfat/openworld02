﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WalkingFat;

namespace WalkingFat
{

    public class UI_SkySystem : MonoBehaviour
    {
        public Slider timecycleSlider;
        public Slider timeSpeedSlider;
        public Slider cloudySlider;
        public Slider windDirSlider;
        public Slider windStrengthSlider;
        public Slider rainSlider;

        public Text timecycleValueTxt;
        public Text timecycleSpeedValueTxt;
        public Text cloudyValueTxt;
        public Text windDirValueTxt;
        public Text windStrengthValueTxt;
        public Text rainValueTxt;

        public DynamicSky dynamicSky;

        // Start is called before the first frame update
        void Awake()
        {
            timeSpeedSlider.value = dynamicSky.timeSpeed;
            cloudySlider.value = dynamicSky.cloudyRate;
            windDirSlider.value = dynamicSky.windAngle;
            windStrengthSlider.value = dynamicSky.windStrength;

            timecycleSpeedValueTxt.text = (Mathf.FloorToInt(dynamicSky.timeSpeed * 10f) * 0.1f).ToString();
            cloudyValueTxt.text = (Mathf.FloorToInt(dynamicSky.cloudyRate * 10f) * 0.1f).ToString();
            windDirValueTxt.text = (Mathf.FloorToInt(dynamicSky.windAngle * 10f) * 0.1f).ToString();
            windStrengthValueTxt.text = (Mathf.FloorToInt(dynamicSky.windStrength * 10f) * 0.1f).ToString();

            Shader.SetGlobalFloat("_RainRate", rainSlider.value);
            rainValueTxt.text = (Mathf.FloorToInt(rainSlider.value * 10f) * 0.1f).ToString();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            timecycleSlider.value = dynamicSky.timecycle;
            timecycleValueTxt.text = (Mathf.FloorToInt(dynamicSky.timecycle * 10f) * 0.1f).ToString();
        }

        public void SetTimecycle()
        {
            dynamicSky.timecycle = timecycleSlider.value;
        }
        public void SetTimeSpeed()
        {
            dynamicSky.timeSpeed = Mathf.RoundToInt(timeSpeedSlider.value * 10f) * 0.1f;
            timecycleSpeedValueTxt.text = dynamicSky.timeSpeed.ToString();
        }
        public void SetCloudyRate()
        {
            dynamicSky.cloudyRate = cloudySlider.value;
            cloudyValueTxt.text = (Mathf.FloorToInt(dynamicSky.cloudyRate * 10f) * 0.1f).ToString();
        }
        public void SetWindDir()
        {
            dynamicSky.windAngle = windDirSlider.value;
            windDirValueTxt.text = (Mathf.FloorToInt(dynamicSky.windAngle * 10f) * 0.1f).ToString();
        }
        public void SetWindStrength()
        {
            dynamicSky.windStrength = windStrengthSlider.value;
            windStrengthValueTxt.text = (Mathf.FloorToInt(dynamicSky.windStrength * 10f) * 0.1f).ToString();
        }
        public void SetRainRate()
        {
            dynamicSky.rainRate = rainSlider.value;
            rainValueTxt.text = (Mathf.FloorToInt(rainSlider.value * 10f) * 0.1f).ToString();
        }
    }
}