﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using WalkingFat;

namespace WalkingFat
{
    public class UI_FX : MonoBehaviour
    {
        public Eft_EnvScan envScan;
        public Eft_NoiseHologram noiseHologram;
        public Text noiseHologramTxt;
        public SRP_ColorfulFog srpFog;
        public Text fogTxt;
        public SRP_SunShafts srpSunShafts;
        public Text sunShaftsTxt;
        public Volume volume;
        private Bloom bloom;
        public Text bloomTxt;


        // Start is called before the first frame update
        void Awake()
        {
            noiseHologramTxt.text = "NoiseHologram Off";
            fogTxt.text = srpFog.isActive == true ? "Fog On" : "Fog Off";
            sunShaftsTxt.text = srpSunShafts.isActive == true ? "Sun Shafts On" : "Sun Shafts Off";

            volume.profile.TryGet(out bloom);

            bloomTxt.text = bloom.IsActive() == true ? "Bloom On" : "Bloom Off";
        }
        
        public void EnvScanTrigger()
        {
            envScan.envScanTrigger = true;
        }

        public void NoiseHologramTrigger()
        {
            noiseHologram.trigger = !noiseHologram.trigger;
            noiseHologramTxt.text = noiseHologram.trigger == true ? "NoiseHologram On" : "NoiseHologram Off";
        }
        public void FogTrigger()
        {
            srpFog.settings.isActive = !srpFog.settings.isActive;
            fogTxt.text = srpFog.settings.isActive == true ? "Fog On" : "Fog Off";
        }
        public void SunShaftTrigger()
        {
            srpSunShafts.settings.isActive = !srpSunShafts.settings.isActive;
            sunShaftsTxt.text = srpSunShafts.settings.isActive == true ? "Sun Shafts On" : "Sun Shafts Off";
        }
        public void BloomTrigger()
        {
            bloom.active = !bloom.active;
            bloomTxt.text = bloom.active == true ? "Bloom On" : "Bloom Off";
        }
    }
}