﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;
using WalkingFat;

namespace WalkingFat
{
    public class UI_Options : MonoBehaviour
    {
        public Text targetFpsTxt;
        public Text resolutionTxt;

        private int[] resList = {480, 640, 720, 960, 1080, 1280};
        private int resId = 2;
        private float ratio;

        // Start is called before the first frame update
        void Awake()
        {
            ratio = (float)Screen.width / (float)Screen.height;

            Application.targetFrameRate = 60;
            targetFpsTxt.text = "target FPS: " + Application.targetFrameRate;

            if (SystemInfo.deviceType == DeviceType.Handheld)
            {
                int height = resList[resId];
                Screen.SetResolution(Mathf.FloorToInt(ratio * height), height, true);

                resolutionTxt.text = "Res: " + height;
            }

        }

        public void SetTragetFPS()
        {
            if (Application.targetFrameRate == 60)
                Application.targetFrameRate = 30;
            else
                Application.targetFrameRate = 60;

            targetFpsTxt.text = "target FPS: " + Application.targetFrameRate;
        }

        public void SetResolution()
        {
            if (SystemInfo.deviceType == DeviceType.Handheld)
            {
                resId += 1;
                if (resId >= resList.Length)
                    resId = 0;
                int height = resList[resId];
                Screen.SetResolution(Mathf.FloorToInt(ratio * height), height, true);

                resolutionTxt.text = "Res: " + height;
            }
        }

    }
}