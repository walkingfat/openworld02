﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat
{
    [ExecuteInEditMode]
    public class Tool_CameraDepthTrigger : MonoBehaviour
    {
        void Start()
        {
            Camera cam = GetComponent<Camera>() as Camera;
            cam.depthTextureMode = DepthTextureMode.Depth;
        }
    }
}