﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat{

	public class Tool_GetSet : MonoBehaviour {



		public static Transform[] GetTransformsFromObjs (GameObject[] objList) {
			Transform[] transformList = new Transform[objList.Length];
			for (int n=0; n<objList.Length; n++) {
				transformList [n] = objList [n].transform;
			}

			return transformList;
		}

		public static void SetObjsEnabled (GameObject[] objList, bool isEnabled) {
			for (int n=0; n<objList.Length; n++) {
				objList [n].SetActive (isEnabled);
			}
		}

		public static Rigidbody GetRbFormObj (GameObject obj) {
			Rigidbody Rb = obj.GetComponent<Rigidbody>();

			return Rb;
		}
	}
}