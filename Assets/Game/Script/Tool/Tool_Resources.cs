﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat{

	public class Tool_Resources : MonoBehaviour {



		public static GameObject LoadObjAsset (string name) {
			GameObject obj = Resources.Load (name) as GameObject;
			return obj;
		}
	}
}
