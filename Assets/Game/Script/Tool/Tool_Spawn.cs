﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat{
	public class Tool_Spawn : MonoBehaviour {

		public static GameObject SpawnObj (bool isEnabled, GameObject prefab, GameObject parent, string name, Vector3 pos, Vector3 dir, Vector3 scale, bool isLocal) {
			GameObject obj = Instantiate (prefab, pos, Quaternion.identity) as GameObject;
			obj.name = name;
			// set parent
			if (parent != null)
				obj.transform.SetParent (parent.transform);
			// set pos and angle
			if (isLocal) {
				obj.transform.localPosition = pos;
				obj.transform.localEulerAngles = dir;
			} else {
				obj.transform.eulerAngles = dir;
			}
            obj.transform.localScale = scale;
            obj.SetActive (isEnabled);
			return obj;
		}

		public static GameObject[] SpawnObjs (bool isEnabled, GameObject prefab, int count, GameObject parent, string name, Vector3[] posList, Vector3[] dirList, bool isLocal) {
			GameObject[] objs = new GameObject[count];
			for (int n = 0; n < count; n++) {
				objs [n] = Instantiate (prefab, posList [n], Quaternion.identity) as GameObject;
				objs [n].name = name + "_" + n;
				// set parent
				if (parent != null)
					objs [n].transform.SetParent (parent.transform);
				// set pos and angle
				if (isLocal) {
					objs [n].transform.localPosition = posList [n];
					objs [n].transform.localEulerAngles = dirList [n];
				} else {
					objs [n].transform.eulerAngles = dirList [n];
				}
				objs [n].SetActive (isEnabled);
			}
			return objs;
		}

		public static GameObject[] SpawnObjsInParents (bool isEnabled, GameObject prefab, int count, GameObject[] parent, string name, Vector3[] posList, Vector3[] dirList, bool isLocal) {
			GameObject[] objs = new GameObject[count];
			for (int n = 0; n < count; n++) {
				objs [n] = Instantiate (prefab, posList [n], Quaternion.identity) as GameObject;
				objs [n].name = name + "_" + n;
				// set parent
				if (parent [n] != null)
					objs [n].transform.SetParent (parent [n].transform);
				// set pos and angle
				if (isLocal) {
					objs [n].transform.localPosition = posList [n];
					objs [n].transform.localEulerAngles = dirList [n];
				} else {
					objs [n].transform.eulerAngles = dirList [n];
				}
				objs [n].SetActive (isEnabled);
			}
			return objs;
		}

		public static GameObject SpawnUi (GameObject prefab, Transform parent) {
			GameObject ui = Instantiate (prefab, parent, false) as GameObject;
			//ui.transform.SetParent (parent);
			return ui;
		}

		public static GameObject SpawnEmptyObj (bool isEnabled, GameObject parent, string name, Vector3 pos, Vector3 dir, bool isLocal) {
			GameObject t = new GameObject ();
			GameObject obj = Instantiate (t, pos, Quaternion.identity) as GameObject;
			obj.name = name;
			// set parent
			if (parent != null)
				obj.transform.SetParent (parent.transform);
			// set pos and angle
			if (isLocal) {
				obj.transform.localPosition = pos;
				obj.transform.localEulerAngles = dir;
			} else {
				obj.transform.eulerAngles = dir;
			}
			obj.SetActive (isEnabled);
			return obj;
		}
	}
}
