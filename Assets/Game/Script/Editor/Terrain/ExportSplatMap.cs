﻿using UnityEditor;
using UnityEngine;
using System.IO;

public class WizardExportSplatMap : ScriptableWizard
{
    public Terrain terrain;
    public Object targetFolder;
    public int id;
    private string targetFolderPath = "";

    WizardExportSplatMap()
    {
        targetFolder = null;
        terrain = null;
        id = 0;
    }

    [MenuItem("Terrain/Export Splat Map")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<WizardExportSplatMap>("Export Splat Map", "Export with layer ID", "Export all");
    }

    void OnWizardUpdate()
    {
        helpString = "\n1) Drag the terrain to the 'Terrain' box.\n2) Then drag the folder from Project to the 'Target Folder' box.\n3) Input the terrain layer number you want to export.";

        if (AssetDatabase.GetAssetPath(targetFolder).Length > 0)
        {

            if (AssetDatabase.IsValidFolder(AssetDatabase.GetAssetPath(targetFolder)) == true)
            {
                targetFolderPath = AssetDatabase.GetAssetPath(targetFolder);
                helpString += "\n\nTarget path: " + AssetDatabase.GetAssetPath(targetFolder);
                errorString = "";
            }
            else
            {
                targetFolderPath = "";
                errorString = "Please select a folder in Project!";
            }

        }
        else
        {
            targetFolderPath = "";
            errorString = "Please select a folder in Project!";
        }

        isValid = (targetFolderPath != "" && terrain != null);
    }

    void OnWizardCreate()
    {
        Texture2D tex = terrain.terrainData.GetAlphamapTexture(id);
        byte[] tgaData = tex.EncodeToTGA();
        if (tgaData != null)
        {
            File.WriteAllBytes(targetFolderPath + "/" + tex.name + ".tga", tgaData);
        }
        else
        {
            Debug.Log("Could not convert " + tex.name + " to tga. Skipping saving texture.");
        }
    }

    void OnWizardOtherButton()
    {
        int alphaMapsCount = terrain.terrainData.alphamapTextureCount;

        for (int i = 0; i < alphaMapsCount; i++)
        {
            Texture2D tex = terrain.terrainData.GetAlphamapTexture(i);
            byte[] tgaData = tex.EncodeToTGA();
            if (tgaData != null)
            {
                File.WriteAllBytes(targetFolderPath + "/" + tex.name + ".tga", tgaData);
            }
            else
            {
                Debug.Log("Could not convert " + tex.name + " to tga. Skipping saving texture.");
            }
        }
    }
}