﻿using UnityEditor;
using UnityEngine;

public class WizardImportSlpatMap : ScriptableWizard
{
    public Terrain terrain;
    public Texture2D[] splatMaps;

    WizardImportSlpatMap()
    {
        terrain = null;
    }

    [MenuItem("Terrain/Import Splat Map")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<WizardImportSlpatMap>("Import Splat Map", "Import all");
    }

    void OnWizardUpdate()
    {
        helpString = "\n1) Drag the terrain to the 'Terrain' box.\n2) Drag Textures to the 'Splat Maps' list.";

        bool canMapRead = true;
        if (splatMaps != null) 
        {
            for (int i = 0; i < splatMaps.Length; i++)
            {
                if(splatMaps[i] != null)
                {
                    if (splatMaps[i].isReadable == false)
                    {
                        canMapRead = false;
                    }
                }
            }
        }
        if (canMapRead == false)
            errorString = "Please toggle on the 'Read/Write Enabled' option of splat map.";
        else
            errorString = "";

        isValid = (terrain != null && canMapRead == true);
    }

    void OnWizardCreate()
    {
        int w = terrain.terrainData.alphamapResolution;
        float[,,] mapData = terrain.terrainData.GetAlphamaps(0, 0, w, w);

        int channelCount = 4;

        for (int i = 0; i < splatMaps.Length; i++)
        {
            Color[] mapColors = splatMaps[i].GetPixels();

            if (i == splatMaps.Length - 1) channelCount = terrain.terrainData.alphamapLayers % 4;

            for (int z = 0; z < channelCount; z++)
            {
                for (int y = 0; y < w; y++)
                {
                    for (int x = 0; x < w; x++)
                    {
                        mapData[x, y, z + i * 4] = mapColors[x  * w + y][z];
                    }
                }
            }
            terrain.terrainData.SetAlphamaps(0, 0, mapData);
        }
    }
}