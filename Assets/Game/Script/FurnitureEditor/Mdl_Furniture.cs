﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat.Furniture;

namespace WalkingFat.Furniture
{
    [System.Serializable]
    public class FurnitrueData
    {
        // basic size
        public float width;
        public float length;
        public float height;
        public float leftSize;
        public float rightSize;
        public float upSize;
        public float downSize;
        public float frontSize;
        public float backSize;

        public List<FurnitureParts> parts;
    }

    [System.Serializable]
    public enum SizeProperty
    {
        width,
        length,
        height,
        leftSize,
        rightSize,
        upSize,
        downSize,
        frontSize,
        backSize
    }

    [System.Serializable]
    public class ActivateDepends
    {
        public SizeProperty depends = SizeProperty.width;
        public float value = 0;
        public DependsCondition condition = DependsCondition.greater;
    }

    [System.Serializable]
    public enum DependsCondition
    {
        equal,
        greater,
        less
    }

    [System.Serializable]
    public class FurnitureParts
    {
        public Mesh mesh;
        public DependsSize posX;
        public DependsSize posY;
        public DependsSize posZ;
        public DependsSize SizeX;
        public DependsSize SizeY;
        public DependsSize SizeZ;

        public bool isActivate;
        public List<ActivateDepends> activateDepends;
    }

    [System.Serializable]
    public class DependsSize
    {
        public SizeProperty depends = SizeProperty.width;
        public float offset = 0;
    }

    public class Mdl_Furniture : MonoBehaviour
    {
        public FurnitrueData furnitrueData;

        void Start()
        {
            Ctlr_Furniture ctlr = gameObject.AddComponent<Ctlr_Furniture>();
        }
    }
}
