﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat.Furniture;

namespace WalkingFat.Furniture
{
    public class Ctlr_Furniture : MonoBehaviour
    {
        private Mdl_Furniture mdl;

        private List<GameObject> parts;

        private void Awake()
        {
            mdl = gameObject.GetComponent<Mdl_Furniture>();

        }

        void Start()
        {
            GameObject obj = new GameObject();

            for (int n = 0; n < mdl.furnitrueData.parts.Count; n++)
            {
                GameObject partObj = Instantiate(obj);

                MeshFilter mf = partObj.AddComponent<MeshFilter>();
                MeshRenderer mr = partObj.AddComponent<MeshRenderer>();

                mf.mesh = mdl.furnitrueData.parts[n].mesh;

                //partObj.position.x = mdl.furnitrueData.parts[n].posX.depends

                parts.Add(partObj);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
