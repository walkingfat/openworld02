﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering.Universal;
using UnityEngine;

public class LitIllumination : MonoBehaviour
{
    private Camera cam;
    private Camera illuminationCam;

    public LayerMask drawLayer;
    public RenderTexture illuminationRT;

    
    void Start()
    {
        cam = GetComponent<Camera>();

        // create illumination camera
        GameObject newCamObj = new GameObject("Illumination Camera id" + GetInstanceID() + " for " + cam.GetInstanceID(), typeof(Camera));

        newCamObj.transform.parent = gameObject.transform;
        newCamObj.transform.SetPositionAndRotation(transform.position, transform.rotation);
        newCamObj.hideFlags = HideFlags.DontSave;

        illuminationCam = newCamObj.GetComponent<Camera>();

        illuminationCam.allowMSAA = false;
        illuminationCam.depth = -10;
        illuminationCam.cullingMask = drawLayer;
        illuminationCam.clearFlags = CameraClearFlags.SolidColor;
        illuminationCam.backgroundColor = new Color(0, 0, 0, 0);
        illuminationCam.allowHDR = true;

        UniversalAdditionalCameraData urpCamData = newCamObj.AddComponent(typeof(UniversalAdditionalCameraData)) as UniversalAdditionalCameraData;
        urpCamData.renderShadows = false; // turn off shadows for the reflection camera
        urpCamData.requiresColorOption = CameraOverrideOption.Off;
        urpCamData.requiresDepthOption = CameraOverrideOption.Off;

        illuminationCam.targetTexture = illuminationRT;

    }

    void Update()
    {
        illuminationCam.projectionMatrix = cam.projectionMatrix;
        illuminationCam.fieldOfView = cam.fieldOfView;
    }

}
