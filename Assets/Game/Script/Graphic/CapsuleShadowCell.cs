﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat
{
    [ExecuteInEditMode]
    public class CapsuleShadowCell : MonoBehaviour
    {
        public float length = 1.0f;
        public float radiusA = 0.3f;
        public float radiusB = 0.2f;
        public Vector3 positionA;
        public Vector3 positionB;

        private void Update()
        {
            positionA = transform.position;
            positionB = transform.TransformPoint(0, length, 0);
        }
    }
}