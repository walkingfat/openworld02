﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using WalkingFat;

namespace WalkingFat
{
    [ExecuteInEditMode]
    public class CapsuleShadowManage : MonoBehaviour
    {
        public bool isEditorMode = false;
        public CapsuleShadowCell[] capsuleShadowCells;

        public Material mat;
        public Mesh shapeMesh;
        //public Mesh cylinderMesh;
        public bool showSphereTrigger = true;
        public Color capsuleColor;

        private Vector4[] capsuleDatas;

        private void Update()
        {
            capsuleDatas = new Vector4[capsuleShadowCells.Length * 2];

            for (int n = 0; n < capsuleShadowCells.Length; n++)
            {
                int id = n * 2;
                capsuleDatas[id] = new Vector4(capsuleShadowCells[n].positionA.x, capsuleShadowCells[n].positionA.y, capsuleShadowCells[n].positionA.z, capsuleShadowCells[n].radiusA);
                capsuleDatas[id + 1] = new Vector4(capsuleShadowCells[n].positionB.x, capsuleShadowCells[n].positionB.y, capsuleShadowCells[n].positionB.z, capsuleShadowCells[n].radiusB);

                //capsuleDatas[id] = new Vector4(1, 1, 1, 0.2f);
                //capsuleDatas[id + 1] = new Vector4(1, 1, 1, 0.6f);
            }

            Shader.SetGlobalVectorArray("_CapsuleData", capsuleDatas);

            //Debug.LogError("capsuleDatas.Length = " + capsuleDatas.Length);

            /*
            if (isEditorMode)
            {
                Camera curCam = SceneView.lastActiveSceneView.camera;
                Matrix4x4 camInvProjection = curCam.projectionMatrix.inverse;
                Shader.SetGlobalMatrix("_CameraInvProjection", camInvProjection);
                Debug.LogError("curCam.pos = " + curCam.transform.position);
            }
            else
            {
                Matrix4x4 camInvProjection = Camera.main.projectionMatrix.inverse;
                Shader.SetGlobalMatrix("_CameraInvProjection", camInvProjection);
                Debug.LogError("can not get scene view camera!");
            }
            */
        }

#if UNITY_EDITOR
        private void OnRenderObject()
        {
            Shader shader = Shader.Find("WalkingFat/RendererFeather/CapsuleShadowEditor");
            if (shapeMesh != null && mat != null)
            {
                for (int n = 0; n < capsuleShadowCells.Length; n++)
                {
                    if(capsuleShadowCells[n] != null && showSphereTrigger == true)
                    {
                        //mat.SetPass(0);
                        //Matrix4x4 matrix = Matrix4x4.TRS(capsuleShadowCells[n].positionA, Quaternion.identity, Vector3.one * capsuleShadowCells[n].radiusA * 2);
                        //Graphics.DrawMeshNow(sphereMesh, matrix);
                        //matrix = Matrix4x4.TRS(capsuleShadowCells[n].positionB, Quaternion.identity, Vector3.one * capsuleShadowCells[n].radiusB * 2);
                        //Graphics.DrawMeshNow(sphereMesh, matrix);

                        /*
                        // create new capsule mesh
                        Mesh newCylinderMesh = new Mesh();
                        Vector3[] newVertices = new Vector3[cylinderMesh.vertexCount];

                        for (int v = 0; v < newVertices.Length; v++)
                        {
                            float radius = Mathf.Lerp(capsuleShadowCells[n].radiusA, capsuleShadowCells[n].radiusB, cylinderMesh.colors[n].r) * 2f;
                            //radius = 0.1f;

                            newVertices[v] = new Vector3(cylinderMesh.vertices[v].x * radius, cylinderMesh.vertices[v].y, cylinderMesh.vertices[v].z * radius);
                            //Debug.LogError(" cylinderMesh.colors[n].r = " + cylinderMesh.colors[n].r + "   radius = " + radius + "    newVertices[v] = " + newVertices[v]);
                        }
                        newVertices[0] = cylinderMesh.vertices[0] * Mathf.Lerp(capsuleShadowCells[n].radiusA, capsuleShadowCells[n].radiusB, 0);

                        newCylinderMesh.vertices = newVertices;
                        //newCylinderMesh.normals = cylinderMesh.normals;
                        newCylinderMesh.uv = cylinderMesh.uv;
                        newCylinderMesh.triangles = cylinderMesh.triangles;

                        //newCylinderMesh.RecalculateBounds();
                        newCylinderMesh.RecalculateNormals();
                        */

                        Material tempMat = new Material(shader);
                        tempMat.SetColor("_Color", capsuleColor);
                        tempMat.SetFloat("_RadiusA", capsuleShadowCells[n].radiusA);
                        tempMat.SetFloat("_RadiusB", capsuleShadowCells[n].radiusB);
                        tempMat.SetFloat("_Length", capsuleShadowCells[n].length);
                        tempMat.SetPass(0);
                        Matrix4x4 matrix = Matrix4x4.TRS(capsuleShadowCells[n].positionA, capsuleShadowCells[n].transform.rotation, Vector3.one);
                        Graphics.DrawMeshNow(shapeMesh, matrix);

                        /*
                        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
                        mpb.SetFloat("_RadiusA", capsuleShadowCells[n].radiusA);
                        mpb.SetFloat("_RadiusB", capsuleShadowCells[n].radiusB);

                        matrix = Matrix4x4.TRS(capsuleShadowCells[n].positionA, capsuleShadowCells[n].transform.rotation, new Vector3(1, capsuleShadowCells[n].length, 1));
                        Graphics.DrawMesh(cylinderMesh, matrix, mat, 0, Camera.main, 0, mpb, false);
                        */
                    }
                }
            }
            else
            {
                Debug.LogError("sphereMesh = " + (shapeMesh != null) + "   mat = " + (mat != null));
            }
        }
# endif
    }
}