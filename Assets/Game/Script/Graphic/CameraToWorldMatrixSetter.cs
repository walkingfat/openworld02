﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraToWorldMatrixSetter : MonoBehaviour
{
    private Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Shader.SetGlobalMatrix("_ViewToWorldMaterx", cam.cameraToWorldMatrix);

        // Inverse projection matrices, plumbed through GetGPUProjectionMatrix to compensate for render texture
        Matrix4x4 clipToViewMatrix = GL.GetGPUProjectionMatrix(cam.projectionMatrix, true).inverse;
        // Negate [1,1] to reflect Unity's CBuffer state
        clipToViewMatrix[1, 1] *= -1;
        Shader.SetGlobalMatrix("_ClipToViewMaterx", clipToViewMatrix);
    }
}
