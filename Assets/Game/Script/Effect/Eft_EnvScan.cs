﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity​Engine.Rendering.Universal;
using UnityEngine.Experimental.Rendering.Universal;
using WalkingFat;

namespace WalkingFat
{
    public class Eft_EnvScan : MonoBehaviour
    {
        public bool envScanTrigger = false;
        public float scanDistance = 100f;
        public float scanSpeed = 10f;
        private float scanTime = 0f;
        private float scanPeriod;
        public Transform scanPivot;
        public Material[] scanMats;
        //public RenderObjectsWithTrigger linesRenderFeather;
        //public RenderObjectsWithTrigger outlinesRenderFeather;

        public bool pointMarkTrigger = false;
        public float pointMarkDistance = 50f;
        public float pointMarkSpeed = 10f;
        private float pointMarkTime = 0f;
        private float pointMarkPeriod;
        private float pointMarkTotalPeriod;
        public int pointMarkLoopTimes;
        public Material pointMarkMat;
        //public RenderObjectsWithTrigger pointMarkFeather;

        private Vector3 pivotPos;

        void Start()
        {
            scanPeriod = scanDistance * 2;
            foreach(Material m in scanMats)
            {
                m.SetFloat("_ScanDist", scanDistance);
            }

            pointMarkPeriod = pointMarkDistance * 6;
            pointMarkTotalPeriod = pointMarkPeriod * pointMarkLoopTimes;
            pointMarkMat.SetFloat("_PointMarkDist", pointMarkDistance);

            
            //linesRenderFeather.settings.isActive = false;
            //outlinesRenderFeather.settings.isActive = false;
            //pointMarkFeather.settings.isActive = false;
            
        }

        void Update()
        {
            // input
            if (Input.GetKeyDown(KeyCode.K))
            {
                //print("press k!");
                LuanchScan();
            }

            // scan
            if (envScanTrigger == true)
            {
                if (scanTime < scanPeriod)
                {
                    //if (aaa.)
                    scanTime += Time.deltaTime * scanSpeed;
                    Vector4 scanParams = new Vector4(pivotPos.x, pivotPos.y, pivotPos.z, scanTime);
                    Shader.SetGlobalVector("_ScanParams", scanParams);
                }
                else
                {
                    envScanTrigger = false;
                    //linesRenderFeather.settings.isActive = envScanTrigger;
                    //outlinesRenderFeather.settings.isActive = envScanTrigger;
                }
            }

            // scan
            if (pointMarkTrigger == true)
            {
                if (pointMarkTime < pointMarkTotalPeriod)
                {
                    pointMarkTime += Time.deltaTime * pointMarkSpeed;

                    float t = pointMarkTime % pointMarkPeriod;

                    Vector4 pointMarkParams = new Vector4(pivotPos.x, pivotPos.y, pivotPos.z, t);
                    Shader.SetGlobalVector("_PointMarkParams", pointMarkParams);
                }
                else
                {
                    pointMarkTrigger = false;
                    //pointMarkFeather.settings.isActive = pointMarkTrigger;
                }

                //print("pointMarkTime = " + pointMarkTime + "   pointMarkTrigger = " + pointMarkTrigger);
            }
        }

        void LuanchScan()
        {
            if(envScanTrigger == false)
            {
                envScanTrigger = true;
                //linesRenderFeather.settings.isActive = envScanTrigger;
                //outlinesRenderFeather.settings.isActive = envScanTrigger;
                scanTime = 0f;

                pointMarkTrigger = true;
                //pointMarkFeather.settings.isActive = pointMarkTrigger;
                pointMarkTime = 0f;
                pivotPos = scanPivot.position;
            }
        }

    }
}