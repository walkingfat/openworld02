﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat
{
    public class Eft_NoiseHologram : MonoBehaviour
    {
        public Material[] mats;
        private float fallDown;
        public bool trigger = false;
        public float fallDownSpeed = 1.0f;

        // Start is called before the first frame update
        void Start()
        {
            trigger = false;
            fallDown = 0;
        }

        // Update is called once per frame
        void Update()
        {
            if(trigger == true)
            {
                if(fallDown < 1.0f)
                {
                    fallDown += Time.deltaTime * fallDownSpeed;
                    fallDown = Mathf.Clamp01(fallDown);
                    foreach (Material mat in mats)
                    {
                        mat.SetFloat("_FallDown", fallDown);
                    }
                }   
            } 
            else
            {
                if (fallDown > 0.0f)
                {
                    fallDown -= Time.deltaTime * fallDownSpeed;
                    fallDown = Mathf.Clamp01(fallDown);
                    foreach (Material mat in mats)
                    {
                        mat.SetFloat("_FallDown", fallDown);
                    }
                }
            }
        }
    }
}
