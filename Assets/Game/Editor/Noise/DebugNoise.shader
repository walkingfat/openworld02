﻿Shader "Custom/DebugNoise"
{
	Properties
	{
		[MaterialToggle] _isToggled("Use 3D Noise", Float) = 0
		_2DNoise ("2D Noise", 2D) = "white" {}
		_3DNoise ("3D Noise", 3D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Transparent" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			//ZWrite On

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 uv : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float _isToggled;
			CBUFFER_END

			TEXTURE2D(_2DNoise);						SAMPLER(sampler_2DNoise);
			TEXTURE3D(_3DNoise);						SAMPLER(sampler_3DNoise);

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

				output.positionCS = vertexInput.positionCS;
				output.uv = float4(input.positionOS.xyz * 0.5 + 0.5, 0);

				return output;
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 col;
				if(_isToggled) 
					col = SAMPLE_TEXTURE3D(_3DNoise, sampler_3DNoise, input.uv.xyz); //tex3D(_3DNoise, i.uv);
				else 
					col = SAMPLE_TEXTURE2D(_2DNoise, sampler_2DNoise, input.uv.xy); //tex2D(_2DNoise, i.uv.xy);
				return col;
			}
			ENDHLSL
		}
	}
}
