﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat{
	
	public class Ctrlr_Joystick : MonoBehaviour {


		public RectTransform AxisTransform;

		private GameObject joystick;
		private UI_Hud_Joystick joystickView;
        private Mdl_Joystick mdl;
        private Vector2 joystickCenter;
		private int joystickFingerId;
		private float joystickMoveDist;
		private float joystickRadius;
		private float joystickInnerRadius; // not move if stick distance < inner radius
		private float canvasToScreenRate = 1f;
		private float screenToCanvasRate = 1f;
		private bool hasJoystick = false;
		private Rect joystickArea = new Rect (0,0,0,0);
        private float lastScreenRes;
        
		private Vector2 joystickDir = Vector2.zero;
		private float joystickAngel = 0f;
		private float joystickForce = 0f;

        private void Start()
        {
            mdl = gameObject.GetComponent<Mdl_Joystick>() as Mdl_Joystick;

            SetResParams();
        }

        void Update()
        {
            if (lastScreenRes != Screen.height)
            {
                SetResParams();
            }
        }

        void SetResParams () 
        {
            canvasToScreenRate = (float)Screen.height / (float)mdl.uiHeight;
            screenToCanvasRate = (float)mdl.uiHeight / (float)Screen.height;
            
            canvasToScreenRate = (float)Screen.height / (float)mdl.uiHeight;
            joystickArea = new Rect(
                mdl.joystickAreaImg.rectTransform.position.x,
                mdl.joystickAreaImg.rectTransform.position.y,
                mdl.joystickAreaImg.rectTransform.rect.width * canvasToScreenRate,
                mdl.joystickAreaImg.rectTransform.rect.height * canvasToScreenRate
                );

            //print("joystickArea = " + joystickArea);

            lastScreenRes = Screen.height;
        }

        public void SpawnJoystick (GameObject prefab, Transform parent)
        {
//			joystick = Instantiate (prefab) as GameObject;
//			joystick.transform.SetParent (parent, false);
			joystick = Tool_Spawn.SpawnUi (prefab, parent);
			joystickView = joystick.GetComponent<UI_Hud_Joystick> () as UI_Hud_Joystick;

			joystickRadius = joystickView.joystickRadius * canvasToScreenRate;
			joystickInnerRadius = joystickView.joystickInnerRadius * canvasToScreenRate;
		}

		public Vector4 GetJoystickValue ()
        {
			foreach (Touch touch in Input.touches)
            {
                // Start
                if (touch.phase == TouchPhase.Began)
                {
                    if (joystickArea.Contains(touch.position) && hasJoystick == false) 
                    {
                        joystickCenter = touch.position;
                        joystickFingerId = touch.fingerId;
                        SetViewStatusId(JoystickViewStatus.show);
                        SetViewForceValue(0f);
                        Vector2 tempPos = touch.position * screenToCanvasRate;
                        //print("T-pos/J-pos: " + touch.position + " / " + tempPos + "   rate: " + screenToCanvasRate + "   Screen H: " + Screen.height);
                        SetAxisPos(tempPos);
                        SetStickPos(tempPos);
                        //						joystickDir = Vector2.zero;
                        //						joystickAngel = 0f;
                        joystickForce = 0f;
                        hasJoystick = true;
                    }
                }
				// End
				if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {
					if (joystickFingerId == touch.fingerId)
                    {
						SetViewStatusId (JoystickViewStatus.hide);
						SetViewForceValue (0f);
//						joystickDir = Vector2.zero;
//						joystickAngel = 0f;
						joystickForce = 0f;
                        hasJoystick = false;
                    }
				}
				// Move
				if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                {
					if (joystickFingerId == touch.fingerId && hasJoystick == true)
                    {
						joystickMoveDist = Vector2.Distance (joystickCenter, touch.position);

						// stick move
						Vector2 stickPos = Vector2.zero;
						if (joystickMoveDist <= joystickRadius)
                        {
							stickPos = touch.position;
						}
                        else
                        {
							stickPos = ((touch.position - joystickCenter) * (joystickRadius / joystickMoveDist) + joystickCenter);
						}

						SetStickPos (stickPos * screenToCanvasRate);

						if (joystickMoveDist > joystickInnerRadius)
                        {
							joystickDir = Vector3.Normalize (new Vector3 (touch.position.x - joystickCenter.x, touch.position.y - joystickCenter.y, 0f));

							joystickForce = Vector2.Distance(joystickCenter, stickPos) / joystickRadius;

							float tempAngle = Vector2.Angle (Vector2.right, joystickDir);
							if (joystickDir.y >= 0)
								joystickAngel = tempAngle;
							else
								joystickAngel = 360 - tempAngle;
						}
                        else
                        {
							joystickDir = Vector3.zero;
//							joystickAngel = 0;
							joystickForce = 0f;
						}
					}
				}
			}

			return new Vector4 (joystickDir.x, joystickDir.y, joystickAngel, joystickForce);

		}

		public void JoystickLocked ()
        {
			foreach (Touch touch in Input.touches)
            {
				// Start
				if (touch.phase == TouchPhase.Began)
                {
					if (joystickArea.Contains (touch.position))
                    {
						joystickCenter = touch.position;
						joystickFingerId = touch.fingerId;
						SetViewStatusId (JoystickViewStatus.locked);
						SetViewForceValue (0f);
						AxisTransform.anchoredPosition = touch.position * screenToCanvasRate;
					}
				}
			}
		}

		public void SetViewStatusId (JoystickViewStatus id)
        {
			if (joystickView)
				joystickView.SetAnimStatusId (id);
			else
				Debug.LogError ("Can not find UI_Joystick instance.");
		}

		public void SetViewForceValue (float force)
        {
			if (joystickView)
				joystickView.SetAnimForceValue (force);
			else
				Debug.LogError ("Can not find UI_Joystick instance.");
		}

		void SetStickPos (Vector2 pos)
        {
			if (joystickView)
				joystickView.SetStickPos (pos);
			else
				Debug.LogError ("Can not find UI_Joystick instance.");
		}

		void SetAxisPos (Vector2 pos)
        {
			if (joystickView)
				joystickView.SetAxisPos (pos);
			else
				Debug.LogError ("Can not find UI_Joystick instance.");
		}
	}
}