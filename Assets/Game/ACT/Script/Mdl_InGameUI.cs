﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat {

	public enum InGameBtStatus {
		none = 0,
		down = 1,
		hold = 2,
		up = 3,
	}

	public class Mdl_InGameUI : MonoBehaviour {

		private Ctrlr_InGameUI inGameUICtrlr;

		[HideInInspector]public InGameBtStatus JumpBtStatus = InGameBtStatus.none;
		[HideInInspector]public InGameBtStatus DashBtStatus = InGameBtStatus.none;
		[HideInInspector]public InGameBtStatus AttackBtStatus = InGameBtStatus.none;
		[HideInInspector]public InGameBtStatus ShieldBtStatus = InGameBtStatus.none;

		// Use this for initialization
		void Start () {
			inGameUICtrlr = gameObject.AddComponent(typeof(Ctrlr_InGameUI)) as Ctrlr_InGameUI;
		}

		void FixedUpdate () {
			JumpBtStatus = InGameBtStatus.none;
			DashBtStatus = InGameBtStatus.none;

			if (AttackBtStatus == InGameBtStatus.down) {
				AttackBtStatus = InGameBtStatus.hold;
			} else if (AttackBtStatus == InGameBtStatus.up) {
				AttackBtStatus = InGameBtStatus.none;
			}

			if (ShieldBtStatus == InGameBtStatus.down) {
				ShieldBtStatus = InGameBtStatus.hold;
			} else if (ShieldBtStatus == InGameBtStatus.up) {
				ShieldBtStatus = InGameBtStatus.none;
			}
		}

		public void ClickJump () {
			JumpBtStatus = InGameBtStatus.down;
		}

		public void ClickDash () {
			DashBtStatus = InGameBtStatus.down;
		}

		public void AttackBtDown () {
			AttackBtStatus = InGameBtStatus.down;
		}
		public void AttackBtUp () {
			AttackBtStatus = InGameBtStatus.up;
		}

		public void ShieldBtDown () {
			ShieldBtStatus = InGameBtStatus.down;
		}
		public void ShieldBtUp () {
			ShieldBtStatus = InGameBtStatus.up;
		}
	}
}