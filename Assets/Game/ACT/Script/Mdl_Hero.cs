﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat {

	public class Mdl_Hero : MonoBehaviour {
		public Mdl_Joystick joystickMdl;
		public Mdl_InGameUI inGameUiMdl;
		public Mdl_HeroCamera heroCameraMdl;
		public GameObject heroPrefab;
        public float heroEffectRadius;
		[HideInInspector]public GameObject heroObj;

		[HideInInspector]public bool isMoving = false;
		[HideInInspector]public bool isDashing = false;
		[HideInInspector]public bool isAfterDash = false;
		[HideInInspector]public bool isCharging = false;
		[HideInInspector]public bool isStartAttack = false;
		[HideInInspector]public bool isAttacking = false;
		[HideInInspector]public bool isShield= false;

		[HideInInspector]public int jumpTimes = 0;
		[HideInInspector]public int dashTimes = 0;
		[HideInInspector]public bool isHitTop = false;
		[HideInInspector]public int groundAttackTimes = 0;
		[HideInInspector]public int airAttackTimes = 0;
		[HideInInspector]public int chargeAttackLevel = 0;

		[HideInInspector]public Vector2 heroDir;
		[HideInInspector]public float heroSpeed;
		[HideInInspector]public Vector3 heroGroundPos;

		public float heroGravity = 10f;
		public float heroMaxGravity = -30f;
		public float heroGroundedGravity = -1f;
		public float heroMoveSpeed = 8f;
		public float heroChargeMoveSpeed = 3f;
		public float heroShieldMoveSpeed = 3f;
		public float moveAcceleration = 2f;
		public float heroRotateSpeedForJoystick = 0.1f;
		public float heroRotateSpeedForKeyboard = 0.1f;
		public int heroMaxJumpTimes = 2;
		public float heroJumpForce = 15f;
		public float heroDashSpeed = 30f;
		public int heroMaxDashTimes = 1;
		public float heroDashTime = 0.1f;
		public float heroEndDashTime = 0.3f;
		public float heroEndJumpDashRate = 0.2f;
		public float groundInfoCheckDistance = 2f;
		public float slopeSlideSpeed = 0.2f;
		public float slideAngle = 45f;
		public float maxSlopeAngle = 45f;
		public float slopeLastTime = 1f;
		public int maxGroundCombo = 3;
		public int maxAirCombo = 1;
		public float groundAttackInterval = 1f; // the minimal time to luanch next action
		public float[] groundAttackDurationList; // the whole time of the attack animation
		public float jumpAttackUpForce = 5f;
		public float airAttackDuration = 0.6f; // the whole time of the attack animation
		public float[] chargeTimeList;
		public float[] chargeAttackDurationList;
		public float startAttackTime = 0.2f;
		public int maxChargeAttackLevel = 3;


		[HideInInspector]public Ctrlr_Hero heroCtrlr;
		[HideInInspector]public View_Hero heroView;
		[HideInInspector]public Transform targetEnemy;

		// Use this for initialization
		void Start () {
			heroObj = Tool_Spawn.SpawnObj (true, heroPrefab, gameObject, "HeroObj", Vector3.zero, Vector3.zero, Vector3.one, true);
			heroCameraMdl.hero = heroObj.transform;

			heroCtrlr = gameObject.AddComponent(typeof(Ctrlr_Hero)) as Ctrlr_Hero;
			heroView = gameObject.AddComponent(typeof(View_Hero)) as View_Hero;
		}

		// Update is called once per frame
//		void Update () {
//			
//		}

		public void SetTargetEnemy (Transform i) {
			targetEnemy = i;
		}
	}
}