﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat
{

    public class Ctrlr_HeroCamera : MonoBehaviour
    {

        private Mdl_HeroCamera mdl;

        private float canvasToScreenRate = 1f;
        private float screenToCanvasRate = 1f;
        private Rect rotateArea = new Rect(0, 0, 0, 0);
        private float lastScreenRes;
        private int rotateFingerId;
        private bool HasRotateTouch = false;

        private Vector3 moveTargetPos;
        private Vector3 moveTargetSlerpPos;
        private float heightOffsetWithRotate;
        private Vector3 rotateTargetDir;
        private Vector2 lastMousePos;
        private float zoomTargetDist;
        private float zoomOffsetWithRotate;
        private float heroTargetLastDist;

        private bool isBetweenMode = false;

        //private HeroCameraStatus cameraStatusId = HeroCameraStatus.freeFollow;

        // Use this for initialization
        void Start()
        {
            mdl = GetComponent<Mdl_HeroCamera>() as Mdl_HeroCamera;

            lastMousePos = Input.mousePosition;

            SetResParams();

            rotateTargetDir = mdl.cameraDirAxis.localEulerAngles;
            zoomTargetDist = mdl.cameraDistAxis.localPosition.z;
        }
        
        void Update()
        {
            if (lastScreenRes != Screen.height)
            {
                SetResParams();
            }

            float cameraTan = Mathf.Tan(Mathf.Deg2Rad * mdl.camera.fieldOfView);
            Shader.SetGlobalFloat("_CameraTan", cameraTan);
        }

        public void SetResParams()
        {
            canvasToScreenRate = (float)Screen.height / (float)mdl.uiHeight;
            screenToCanvasRate = (float)mdl.uiHeight / (float)Screen.height;

            rotateArea = new Rect(
                mdl.rotateCamAreaImage.rectTransform.position.x,
                mdl.rotateCamAreaImage.rectTransform.position.y,
                mdl.rotateCamAreaImage.rectTransform.rect.width * canvasToScreenRate,
                mdl.rotateCamAreaImage.rectTransform.rect.height * canvasToScreenRate);

            //print("rotateArea = " + rotateArea);

            lastScreenRes = Screen.height;
        }

        void LateUpdate()
        {
            FreeFollow();
        }

        public void FreeFollow()
        {
            // move 
            CameraMove();
            CameraRotate();
            CameraZoom();
        }

        void CameraMove()
        {
            Vector3 tempOffset = new Vector3(0, heightOffsetWithRotate, 0);
            Vector3 tempHeroGroundPos = mdl.heroMdl.heroGroundPos;

            if (mdl.targetEnemy == null)
            {
                moveTargetPos = tempHeroGroundPos + tempOffset;
            }
            else
            {
                // check lock target mode
                float tempDist = Vector3.Distance(tempHeroGroundPos, mdl.targetEnemy.position);
                if (tempDist < mdl.endLockTargetDist)
                {
                    isBetweenMode = true;
                }
                else
                {
                    isBetweenMode = false;
                }

                if (isBetweenMode == true)
                {
                    moveTargetPos = tempHeroGroundPos * 0.6f + mdl.targetEnemy.position * 0.4f + tempOffset * 0.5f;
                }
                else
                {
                    moveTargetPos = tempHeroGroundPos + tempOffset;
                }
            }
            //			mdl.cameraPosAxis.position = moveTargetPos;

            // height lerp
            float tempHeight = mdl.cameraPosAxis.position.y + (moveTargetPos.y - mdl.cameraPosAxis.position.y) * mdl.moveSlerpSpeed;
            mdl.cameraPosAxis.position = new Vector3(moveTargetPos.x, tempHeight, moveTargetPos.z);

        }

        void CameraRotate()
        {
            // if (SystemInfo.deviceType == DeviceType.Handheld)
            // {
                foreach (Touch touch in Input.touches)
                {
                    // Start
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (rotateArea.Contains(touch.position) && HasRotateTouch == false)
                        {
                            rotateFingerId = touch.fingerId;
                            HasRotateTouch = true;
                        }
                    }
                    // End
                    if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        if (rotateFingerId == touch.fingerId)
                        {
                            HasRotateTouch = false;
                        }
                    }
                    // Move
                    if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                    {
                        if (rotateFingerId == touch.fingerId && HasRotateTouch == true)
                        {
                            rotateTargetDir += new Vector3(touch.deltaPosition.y, -touch.deltaPosition.x, 0) * mdl.rotateSpeed;

                            float tempTouchY = Mathf.Clamp(rotateTargetDir.x, mdl.rotateRangeV.x, mdl.rotateRangeV.y);
                            rotateTargetDir = new Vector3(tempTouchY, rotateTargetDir.y, 0);
                        }
                    }
                }
            // }
            // else if (SystemInfo.deviceType == DeviceType.Desktop)
            // {
                if (Input.GetMouseButtonDown(1))
                {
                    lastMousePos = Input.mousePosition;
                }
                if (Input.GetMouseButton(1))
                {
                    Vector2 mouseDeltaPosition = new Vector2(Input.mousePosition.x - lastMousePos.x, Input.mousePosition.y - lastMousePos.y);

                    rotateTargetDir += new Vector3(mouseDeltaPosition.y, -mouseDeltaPosition.x, 0) * mdl.rotateSpeed;

                    float tempMouseY = Mathf.Clamp(rotateTargetDir.x, mdl.rotateRangeV.x, mdl.rotateRangeV.y);
                    rotateTargetDir = new Vector3(tempMouseY, rotateTargetDir.y, 0);

                    lastMousePos = Input.mousePosition;
                }
            // }

            // game controller
            rotateTargetDir += new Vector3(Input.GetAxis("HorizontalR"), Input.GetAxis("VerticalR"), 0) * mdl.rotateSpeed * 10;
            float tempY = Mathf.Clamp(rotateTargetDir.x, mdl.rotateRangeV.x, mdl.rotateRangeV.y);
            rotateTargetDir = new Vector3(tempY, rotateTargetDir.y, 0);


            // smooth follow
            mdl.cameraDirAxis.rotation = Quaternion.Slerp(mdl.cameraDirAxis.rotation, Quaternion.Euler(rotateTargetDir), Time.deltaTime * mdl.rotateSlerpSpeed);

            // zoom offset with rotate
            float tempA = mdl.cameraDirAxis.eulerAngles.x;
            if (tempA > 180)
                tempA -= 360;
            float tempT = (tempA - mdl.rotateRangeV.x) / (mdl.rotateRangeV.y - mdl.rotateRangeV.x);
            zoomOffsetWithRotate = Mathf.Lerp(mdl.zoomOffsetRangeWithRotate.x, mdl.zoomOffsetRangeWithRotate.y, tempT);
            heightOffsetWithRotate = Mathf.Lerp(mdl.heightOffsetWithRotate, 0f, tempT);
        }

        void CameraZoom()
        {

            if (mdl.targetEnemy == null)
            {

            }
            else
            {
                Vector3 tempHeroScreenPos = mdl.camera.WorldToScreenPoint(mdl.hero.position);
                Vector3 tempTargetScreenPos = mdl.camera.WorldToScreenPoint(mdl.targetEnemy.position);
                float tempCurrentDist = Vector3.Distance(tempHeroScreenPos, tempTargetScreenPos);

                if (isBetweenMode == true)
                {
                    if (tempCurrentDist > Screen.height * 0.4f && tempCurrentDist > heroTargetLastDist)
                    {
                        zoomTargetDist -= mdl.zoomSpeed * Time.deltaTime;
                    }
                    if (tempCurrentDist < Screen.height * 0.4f && tempCurrentDist < heroTargetLastDist)
                    {
                        if (zoomTargetDist < mdl.minFreeZoomDist)
                        {
                            zoomTargetDist += mdl.zoomSpeed * Time.deltaTime;
                        }
                        else
                        {
                            zoomTargetDist = mdl.minFreeZoomDist;
                        }
                    }
                    if (heroTargetLastDist != tempCurrentDist)
                    {
                        heroTargetLastDist = tempCurrentDist;
                    }
                }
                else
                {
                    if (zoomTargetDist < mdl.minFreeZoomDist)
                    {
                        zoomTargetDist += mdl.zoomSpeed * Time.deltaTime;
                    }
                }
            }
            
            Vector3 finalZoomPos = new Vector3(0, 0, zoomTargetDist + zoomOffsetWithRotate);

            // check collider
            if(mdl.hitCollider == true)
            {
                RaycastHit hit;
                Vector3 rayPos = mdl.hero.position + new Vector3(0, 1, 0);
                float rayDist = Mathf.Abs(zoomTargetDist + zoomOffsetWithRotate);
                if (Physics.Raycast(rayPos, -mdl.cameraDistAxis.forward, out hit, rayDist, mdl.hitLayer))
                {
                    Debug.DrawRay(rayPos, -mdl.cameraDistAxis.forward * rayDist, Color.yellow);

                    finalZoomPos = new Vector3(0, 0, -hit.distance);
                }
            }

            mdl.cameraDistAxis.localPosition = Vector3.Slerp(mdl.cameraDistAxis.localPosition, finalZoomPos, Time.deltaTime * mdl.zoomSlerpSpeed);
        }

        public void SelectObject()
        {
            // handhold
            foreach (Touch touch in Input.touches)
            {
                // Start
                if (touch.phase == TouchPhase.Began)
                {
                    ScreenRayHit(mdl.camera, new Vector3(touch.position.x, touch.position.y, 0));
                }

            }
            // desktop 
            if (Input.GetMouseButtonDown(0))
            {
                ScreenRayHit(mdl.camera, Input.mousePosition);
            }
        }

        void ScreenRayHit(Camera cam, Vector3 pos)
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(pos);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    //					print ("hit obj name = " + hit.transform.name + "   tag: " + hit.collider.tag.ToString());

                    if (hit.collider.tag == "Enemy")
                    {
                        mdl.targetEnemy = hit.transform;
                    }
                    else
                    {
                        mdl.targetEnemy = null;
                    }
                }
            }
        }
    }
}