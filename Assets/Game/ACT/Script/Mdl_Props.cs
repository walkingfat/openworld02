﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat {
	public enum Props {
		normalChest = 1,
		stone = 10,
		bomb = 20,

	}

	public class Mdl_Props : MonoBehaviour {

		public int hp = 100;

		public bool canDestroy = true;
		public GameObject hitPrefab;
		public GameObject destroyedPrefab;
		public Props spawnItemId;

		public GameObject propsPrefab;

		[HideInInspector] public GameObject propsObj;
		[HideInInspector] public Ctrlr_Props propsCtrlr;

		// Use this for initialization
		void Start () {

            propsObj = Tool_Spawn.SpawnObj (true, propsPrefab, gameObject, "HeroObj", Vector3.zero, Vector3.zero, Vector3.one, true);

			propsCtrlr = gameObject.AddComponent(typeof(Ctrlr_Props)) as Ctrlr_Props;
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		void OnCollisionEnter(Collision collision) {
			if (collision.collider.tag == "Attack") {
				ContactPoint contact = collision.contacts[0];
				Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
				Vector3 pos = contact.point;
				Instantiate(hitPrefab, pos, rot);

				int attack = collision.collider.gameObject.GetComponent<Mdl_Attack> ().attack;
				hp -= attack;

				if (hp <= 0) {
					BeDestroyed ();
				}
			}
		}

		void BeDestroyed () {
//			Tool_Spawn.SpawnObj ();
			Destroy (gameObject);

		}
	}
}