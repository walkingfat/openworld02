﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat{
	public class Mdl_Attack : MonoBehaviour {
		public int attack = 30;
		public GameObject attackPrefab;

		[HideInInspector] public GameObject attackObj;

		// Use this for initialization
		void Start () {
			attackObj = Tool_Spawn.SpawnObj (true, attackPrefab, gameObject, "HeroObj", Vector3.zero, Vector3.zero, Vector3.one, true);

		}

		// Update is called once per frame
		void Update () {

		}
	}
}