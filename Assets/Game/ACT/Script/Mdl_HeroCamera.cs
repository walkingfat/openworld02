﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WalkingFat;

namespace WalkingFat {

	public enum HeroCameraStatus {
		freeFollow,
		fixedFollow,
		animated,
	}

	public class Mdl_HeroCamera : MonoBehaviour {
		public Transform cameraPosAxis;
		public Transform cameraDirAxis;
		public Transform cameraDistAxis;
		public Animator cameraAnim;
		public Camera camera;
        public bool hitCollider;
        public LayerMask hitLayer;

		public GameObject uiCanvasObj;
        public int uiHeight = 800;
        public Image rotateCamAreaImage;
		public Vector2 rotateRangeV;

		private Ctrlr_HeroCamera heroCameraCtrlr;

		public Transform hero;

		[HideInInspector] public Transform targetEnemy = null;
		//[HideInInspector] public PostProcessingProfile ppp;

		public Mdl_Hero heroMdl;

		public float moveSpeed;
		public float moveSlerpSpeed;
		public float moveTargetOffset;
		public float moveDashSlerpSpeed;
		public float heightOffsetWithRotate;
		public float zoomSpeed;
		public float zoomSlerpSpeed;
		public Vector2 zoomOffsetRangeWithRotate;
		public float rotateSpeed;
		public float rotateSlerpSpeed;
		public float minFreeZoomDist; // min zoom distance in free follow 
		public float endLockTargetDist; // switch camera lock mode distance
        [HideInInspector] public bool isTouching = false;

		void Awake()
		{
#if UNITY_EDITOR
			QualitySettings.vSyncCount = 0;  // VSync must be disabled
			Application.targetFrameRate = 60;
#endif
			// if (SystemInfo.deviceType == DeviceType.Handheld)
			// {
			// 	CanvasScaler uiCanvasScaler = uiCanvasObj.GetComponent<CanvasScaler>();
			// 	uiCanvasScaler.referenceResolution = new Vector2(1000, uiHeight);
			// }
		}

		// Use this for initialization
		void Start () {
			//PostProcessingBehaviour ppb = camera.gameObject.GetComponent<PostProcessingBehaviour> () as PostProcessingBehaviour;
			//ppp = ppb.profile;

			heroCameraCtrlr = gameObject.AddComponent(typeof(Ctrlr_HeroCamera)) as Ctrlr_HeroCamera;

		}

        public void SetTouchStatus(bool i)
        {
            isTouching = i;
        }
    }
}
