﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat{
	public class Mdl_Item : MonoBehaviour {

		public GameObject itemPrefab;

		[HideInInspector] public GameObject itemObj;
		[HideInInspector] public Ctrlr_Item itemCtrlr;

		// Use this for initialization
		void Start () {
			itemObj = Tool_Spawn.SpawnObj (true, itemPrefab, gameObject, "HeroObj", Vector3.zero, Vector3.zero, Vector3.one, true);

			itemCtrlr = gameObject.AddComponent(typeof(Ctrlr_Item)) as Ctrlr_Item;
			
		}
		
		// Update is called once per frame
		void Update () {
			
		}
	}
}