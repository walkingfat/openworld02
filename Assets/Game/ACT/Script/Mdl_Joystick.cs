﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WalkingFat;

namespace WalkingFat{

	public enum JoystickModelStatus {
		disable = 0, // totally can not be use
		enable = 1, // can be normally use
		Locked = 2, // can be use but locked now
	}

	public class Mdl_Joystick : MonoBehaviour {

		public GameObject joystickPrefab;
        public Image joystickAreaImg;
		//public Rect joystickCanvasArea;
		public int uiHeight = 800;
		private Ctrlr_Joystick joystickCtrlr;
		private JoystickModelStatus joystickStatus;
		[HideInInspector]public Vector2 joystickDir = Vector2.zero;
		[HideInInspector]public float joystickAngel = 0f;
		[HideInInspector]public float joystickForce = 0f;

		// Use this for initialization
		void Start () {
			joystickCtrlr = gameObject.AddComponent(typeof(Ctrlr_Joystick)) as Ctrlr_Joystick;

			//joystickCtrlr.Initialize (uiHeight, joystickCanvasArea);

			joystickCtrlr.SpawnJoystick (joystickPrefab, transform);

			joystickCtrlr.SetViewStatusId (JoystickViewStatus.disable);
			joystickCtrlr.SetViewForceValue (0f);

			joystickStatus = JoystickModelStatus.enable;
		}
		
		// Update is called once per frame
		void Update () {
			switch (joystickStatus) {
			case JoystickModelStatus.disable:
				
				break;
			case JoystickModelStatus.enable:
				Vector4 joystickValue = joystickCtrlr.GetJoystickValue ();
				joystickDir = new Vector2 (joystickValue.x, joystickValue.y);
				joystickAngel = joystickValue.z;
				joystickForce = joystickValue.w;
				break;
			case JoystickModelStatus.Locked:
				joystickCtrlr.JoystickLocked ();
				break;
			}

        }
	}
}
