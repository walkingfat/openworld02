﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat {


	public class Ctrlr_Hero: MonoBehaviour {

		private Mdl_Hero mdl;
		private Transform myTransform;

		private GameObject Joystick;
		private Animator heroAnim;
		private CharacterController heroCc;

		private float lastMoveAngle;
		private bool lastIsGrounded;
		private float moveSpeed = 0f;
		private float moveSpeedTime = 0f;
		private float dashTimeGo = 0f;
		private float grivaty = 0f;
		private Vector2 finalHeroDir = new Vector2 (0,0);
		private Vector3 finalHeroVelocity = new Vector3(0,0,0);
		private float slopeAngle = 0f;
		private float slopeSpeedRate = 0f;
		private Vector3 slopeForce = new Vector3 (0,0,0);
		private float slopeForceTimeGo = 0f;

		private float slideTime = 0f;
		private bool isSliding = false;

		private Vector3 dashForce = Vector3.zero;
		private Vector3 currentDashForce = Vector3.zero;
		private Vector3 afterAirDashForce = Vector3.zero;

		private float attackTimeGo;
		private bool canAttack = false;
		private float chargeAttackTimeGo;
		private float startAttackTimeGo;
		private bool hasStartAttack = false;


		void Start () {
			myTransform = transform;
			mdl = GetComponent<Mdl_Hero> () as Mdl_Hero;
			heroCc = GetComponent<CharacterController> () as CharacterController;
			lastIsGrounded = heroCc.isGrounded;
		}

		void Update () {

			if (heroCc.isGrounded == true) {
				grivaty = mdl.heroGroundedGravity; // set gravity on ground
				mdl.jumpTimes = 0;
				mdl.dashTimes = 0;
			} else {
				if (lastIsGrounded == true && mdl.jumpTimes == 0) { // while fall from the edge
					grivaty = 0f; // set grivaty as 0 when start falling

					// lose attack
					mdl.isCharging = false;
					mdl.isStartAttack = false;
					hasStartAttack = false;
					mdl.groundAttackTimes = 0;
				}

				if (mdl.isDashing == false)
					grivaty -= mdl.heroGravity * Time.deltaTime; // add gravity in air
				
				if (grivaty <= mdl.heroMaxGravity) {
					grivaty = mdl.heroMaxGravity;
				}
			}
			lastIsGrounded = heroCc.isGrounded;

			// action
			HeroJump ();
			HeroDash ();
			HeroAttack ();
			HeroShield ();

            Shader.SetGlobalVector("_CharPosWorld", mdl.gameObject.transform.position);
		}

		void FixedUpdate () {
			GetHeroGroundInfo ();

			// Get speed rate on slope
			if (heroCc.isGrounded) {
				slopeSpeedRate = 1f - Mathf.Abs (slopeAngle * 0.01f);
				if (slopeSpeedRate < 0)
					slopeSpeedRate = 0;
			} else {
				// make sure the slope speed rate not affect while jumping
				slopeForce = Vector3.zero;
				slopeSpeedRate = 1f;
				isSliding = false;
			}

			// finally set move velocity
			finalHeroVelocity = new Vector3 (finalHeroDir.x * slopeSpeedRate, grivaty, finalHeroDir.y * slopeSpeedRate) + slopeForce; // set hero move velocity

			if (mdl.isDashing == false) {
				if (heroCc.isGrounded == true) {
					// moveing
					InputDir (); // set move direction while hero is not attacking or dashing
					heroCc.Move (finalHeroVelocity * Time.deltaTime);
				} else { // jumping
					heroCc.Move (finalHeroVelocity * Time.deltaTime);
				}
			} else {
				// dashing
				dashTimeGo += Time.deltaTime;

				if (dashTimeGo < mdl.heroEndDashTime) {
					if (dashTimeGo < mdl.heroDashTime) {
						currentDashForce = dashForce;
					} else {
						float r = 1 - (mdl.heroEndDashTime - dashTimeGo) / (mdl.heroEndDashTime - mdl.heroDashTime);
						currentDashForce = Vector3.Lerp (dashForce, Vector3.zero, r);
					}
					heroCc.Move (currentDashForce * Time.deltaTime);
				} else {
					// end dashing
					mdl.isDashing = false;
					mdl.isAfterDash = true;
					dashTimeGo = 0f;
				}
			}


			if (Input.GetKeyDown(KeyCode.Joystick1Button0))
				print("joystick 1 bt 0");

			if (Input.GetKeyDown(KeyCode.Joystick1Button1))
				print("joystick 1 bt 1");

			if (Input.GetKeyDown(KeyCode.Joystick1Button2))
				print("joystick 1 bt 2");

			if (Input.GetKeyDown(KeyCode.Joystick1Button3))
				print("joystick 1 bt 3");

			if (Input.GetKeyDown(KeyCode.Joystick1Button4))
				print("joystick 1 bt 4");

			if (Input.GetKeyDown(KeyCode.Joystick1Button5))
				print("joystick 1 bt 5");

			if (Input.GetKeyDown(KeyCode.Joystick1Button6))
				print("joystick 1 bt 6");

			if (Input.GetKeyDown(KeyCode.Joystick1Button7))
				print("joystick 1 bt 7");

			if (Input.GetKeyDown(KeyCode.Joystick1Button8))
				print("joystick 1 bt 8");

			if (Input.GetKeyDown(KeyCode.Joystick1Button9))
				print("joystick 1 bt 9");

			if (Input.GetKeyDown(KeyCode.Joystick1Button10))
				print("joystick 1 bt 10");

			if (Input.GetKeyDown(KeyCode.Joystick1Button11))
				print("joystick 1 bt 11");

			if (Input.GetKeyDown(KeyCode.Joystick1Button12))
				print("joystick 1 bt 12");

			if (Input.GetKeyDown(KeyCode.Joystick1Button13))
				print("joystick 1 bt 13");

			if (Input.GetKeyDown(KeyCode.Joystick1Button14))
				print("joystick 1 bt 14");

			if (Input.GetKeyDown(KeyCode.Joystick1Button15))
				print("joystick 1 bt 15");

			if (Input.GetKeyDown(KeyCode.Joystick1Button16))
				print("joystick 1 bt 16");
		}

		void OnGUI () {
			float g = Mathf.FloorToInt (grivaty * 10) * 0.1f;

//			GUI.Label (new Rect(10,10,900,40), "isGrounded = " + heroCc.isGrounded + "    isShield = " + mdl.isShield);
//			GUI.Label (new Rect(10,40,900,40), "moveSpeedTime = " + moveSpeedTime);
//			GUI.Label (new Rect(10,70,900,40), "chargeAttackLevel = " + mdl.chargeAttackLevel + "    chargeAttackTimeGo: " + chargeAttackTimeGo + "    isAttacking: " + mdl.isAttacking);
//			GUI.Label (new Rect(10,100,900,40), "groundAttackTimes = " + mdl.groundAttackTimes + "    isStartAttack: " + mdl.isStartAttack + "    isCharging: " + mdl.isCharging);
//			GUI.Label (new Rect(10,130,900,40), "canAttack = " +canAttack + "    attackTimeGo = " + attackTimeGo);

		}

		void InputDir () {
			if (SystemInfo.deviceType == DeviceType.Handheld) {
				Vector2 tempDir = Vector2.zero;

				if (mdl.isStartAttack == true || mdl.isAttacking == true) { // can not move
					moveSpeedTime -= Time.deltaTime * mdl.moveAcceleration;
				} else { // can move
					// check input
					if (mdl.joystickMdl.joystickForce > 0) {
						mdl.isMoving = true;

						// rotate
						if (mdl.isCharging == false && mdl.isShield == false) {
							Quaternion tempHeroRotation = Quaternion.LookRotation (new Vector3 (mdl.heroDir.x, 0, mdl.heroDir.y));
							mdl.heroObj.transform.rotation = Quaternion.Slerp (mdl.heroObj.transform.rotation, tempHeroRotation, mdl.heroRotateSpeedForJoystick);
						}
					} else {
						mdl.isMoving = false;
					}
					// move
					float tempAngle = mdl.heroCameraMdl.cameraDirAxis.eulerAngles.y - mdl.joystickMdl.joystickAngel;
					float tempDeltaAngle = Mathf.DeltaAngle (lastMoveAngle, tempAngle);
					if (tempDeltaAngle > 150) {
						moveSpeedTime = 0;
					}
					lastMoveAngle = tempAngle;

					mdl.heroDir = new Vector2 (Mathf.Cos (tempAngle * Mathf.Deg2Rad), -Mathf.Sin (tempAngle * Mathf.Deg2Rad));
					tempDir = mdl.heroDir * mdl.joystickMdl.joystickForce;

					// speed
					if (mdl.joystickMdl.joystickForce > 0) {
						moveSpeedTime += Time.deltaTime * mdl.moveAcceleration;
					} else {
						moveSpeedTime -= Time.deltaTime * mdl.moveAcceleration;
					}
				}

				moveSpeedTime = Mathf.Clamp01 (moveSpeedTime);

				if (mdl.isCharging == true) {
					mdl.heroSpeed = Mathf.Lerp (0f, mdl.heroChargeMoveSpeed, moveSpeedTime);
				} else if (mdl.isShield == true) {
					mdl.heroSpeed = Mathf.Lerp (0f, mdl.heroShieldMoveSpeed, moveSpeedTime);
				}  else {
					mdl.heroSpeed = Mathf.Lerp (0f, mdl.heroMoveSpeed, moveSpeedTime);
				}

				finalHeroDir = new Vector3 (tempDir.x * mdl.heroSpeed, tempDir.y * mdl.heroSpeed);

			} else if (SystemInfo.deviceType == DeviceType.Desktop) {

				float tempDirX = Input.GetAxis("Horizontal");
				float tempDirY = Input.GetAxis("Vertical");
				float tempForce = Mathf.Max (Mathf.Abs (tempDirX), Mathf.Abs (tempDirY));

				// speed
				if (mdl.isStartAttack == true || mdl.isAttacking == true) { // can not move
					moveSpeedTime -= Time.deltaTime * mdl.moveAcceleration;
				} else { // can move
					// vector to angle
					if (tempDirX != 0 || tempDirY != 0) {
						mdl.isMoving = true;

						float tempInputAngle = Vector2.Angle (Vector2.right, new Vector2 (tempDirX, tempDirY));
						if (tempDirY < 0)
							tempInputAngle = 360 - tempInputAngle;

						float tempAngle = mdl.heroCameraMdl.cameraDirAxis.eulerAngles.y - tempInputAngle;

						float tempDeltaAngle = Mathf.DeltaAngle (lastMoveAngle, tempAngle);
						if (tempDeltaAngle > 150) {
							moveSpeedTime = 0;
						}
						lastMoveAngle = tempAngle;

						mdl.heroDir = new Vector2 (Mathf.Cos (tempAngle * Mathf.Deg2Rad), -Mathf.Sin (tempAngle * Mathf.Deg2Rad));

						// check run
						if (mdl.isCharging == true) {
							moveSpeed = mdl.heroChargeMoveSpeed;
						} else if (mdl.isShield == true) {
							moveSpeed = mdl.heroShieldMoveSpeed;
						} else {
							if (Input.GetKey (KeyCode.LeftShift) || Input.GetKey(KeyCode.Joystick1Button8)) {
								moveSpeed = mdl.heroMoveSpeed;
							} else {
								moveSpeed = mdl.heroMoveSpeed * 0.6f;
							}
						}

						// move speed acceleration, for break or change dir in running.
						moveSpeedTime += Time.deltaTime * mdl.moveAcceleration;

						// rotate
						if (mdl.isCharging == false && mdl.isShield == false) {
							Quaternion tempHeroRotation = Quaternion.LookRotation (new Vector3 (mdl.heroDir.x, 0, mdl.heroDir.y));
							mdl.heroObj.transform.rotation = Quaternion.Slerp (mdl.heroObj.transform.rotation, tempHeroRotation, mdl.heroRotateSpeedForKeyboard);
						}

					} else {
						mdl.isMoving = false;
						moveSpeedTime -= Time.deltaTime * mdl.moveAcceleration;
					}
				}

				moveSpeedTime = Mathf.Clamp01 (moveSpeedTime);
				mdl.heroSpeed = Mathf.Lerp (0f, moveSpeed, moveSpeedTime);

				finalHeroDir = new Vector2 (mdl.heroDir.x * mdl.heroSpeed * tempForce, mdl.heroDir.y * mdl.heroSpeed * tempForce);
			}
		}

		void HeroShield () {
			if (heroCc.isGrounded == true && mdl.isAttacking == false && mdl.isStartAttack == false && mdl.isDashing == false && mdl.isCharging == false) {
				if (SystemInfo.deviceType == DeviceType.Handheld) {
					if (mdl.inGameUiMdl.ShieldBtStatus == InGameBtStatus.hold) {
						mdl.isShield = true;
					} else {
						mdl.isShield = false;
					}
				} else if (SystemInfo.deviceType == DeviceType.Desktop) {
					if (Input.GetKey (KeyCode.X) || Input.GetKey (KeyCode.Joystick1Button10)) {
						mdl.isShield = true;
					} else {
						mdl.isShield = false;
					}
				}
				// game controller
//				if (Input.GetKey (KeyCode.Joystick1Button10)) {
//					mdl.isShield = true;
//				}
			}
		}

		void HeroAttack () {
			if (canAttack == true) {
				if (SystemInfo.deviceType == DeviceType.Handheld) {
					if (mdl.inGameUiMdl.AttackBtStatus == InGameBtStatus.down) {
						AttackBtDown ();
					}
					if (mdl.inGameUiMdl.AttackBtStatus == InGameBtStatus.hold) {
						HoldAttackBt ();
					}
					if (mdl.inGameUiMdl.AttackBtStatus == InGameBtStatus.up) {
						AttackBtUp ();
					}
				} else if (SystemInfo.deviceType == DeviceType.Desktop) {
					if (Input.GetMouseButtonDown (0)) {
						AttackBtDown ();
					}
					if (Input.GetMouseButton (0)) {
						HoldAttackBt ();
					}
					if (Input.GetMouseButtonUp (0)) {
						AttackBtUp ();
					}
				}

				// game controller
				if (Input.GetKeyDown (KeyCode.Joystick1Button15)) {
					AttackBtDown ();
				}
				if (Input.GetKey (KeyCode.Joystick1Button15)) {
					HoldAttackBt ();
				}
				if (Input.GetKeyUp (KeyCode.Joystick1Button15)) {
					AttackBtUp ();
				}
			}

			// check can attack
			if (mdl.isDashing == false) {
				if (heroCc.isGrounded) {
					if (mdl.airAttackTimes > 0) { // after jump attack
						if (attackTimeGo < mdl.airAttackDuration) {
							attackTimeGo += Time.deltaTime;
						} else {
							mdl.airAttackTimes = 0;
							mdl.groundAttackTimes = 0;
							canAttack = true;
							mdl.isAttacking = false;
						}
					} else { // on ground
						if (mdl.groundAttackTimes > 0) {
							if (mdl.chargeAttackLevel > 0) { // charge attack
								if (mdl.isCharging == false && mdl.isStartAttack == false) {
									if (attackTimeGo < mdl.chargeAttackDurationList[mdl.chargeAttackLevel-1]) {
										attackTimeGo += Time.deltaTime;
									} else {
										mdl.isAttacking = false;
										mdl.groundAttackTimes = 0;
									}
									canAttack = false;
								}
							} else { // combo attack
								if (mdl.groundAttackTimes < mdl.maxGroundCombo) {
									if (mdl.isCharging == false && mdl.isStartAttack == false) {
										if (attackTimeGo < mdl.groundAttackDurationList[mdl.groundAttackTimes-1]) {
											attackTimeGo += Time.deltaTime;
										} else {
											mdl.isAttacking = false;
											mdl.groundAttackTimes = 0;
										}
									}

									if (attackTimeGo > mdl.groundAttackInterval) {
										canAttack = true;
									} else {
										canAttack = false;
									}
								} else { // max combo
									attackTimeGo += Time.deltaTime;
									if (attackTimeGo > mdl.groundAttackDurationList[mdl.groundAttackTimes-1]) {
										canAttack = true;
										mdl.isAttacking = false;
										mdl.groundAttackTimes = 0;
									} else {
										canAttack = false;
									}
								}
							}
						} else {
							canAttack = true;
						}
					}
				} else {
					// in air
					if (mdl.airAttackTimes > 0) {
						canAttack = false;
					} else {
						canAttack = true;
					}
				}
			} else {
				canAttack = false;
			}
		}

		void AttackBtDown () {
			if (heroCc.isGrounded == false) { // air attack
				luanchAttack ();
			} else { // ground attack
				mdl.chargeAttackLevel = 0;
				chargeAttackTimeGo = 0;
				startAttackTimeGo = 0;
				mdl.isCharging = false;
				mdl.isStartAttack = true;
				hasStartAttack = true;
				mdl.isAttacking = false;
			}
		}

		void HoldAttackBt () {
			if (hasStartAttack == true) {
				// start
				if (mdl.isStartAttack == true) {
					startAttackTimeGo += Time.deltaTime;

					if (startAttackTimeGo > mdl.startAttackTime) { // before charging
						mdl.chargeAttackLevel = 0;
						mdl.isStartAttack = false;
						chargeAttackTimeGo = 0;

						if (mdl.maxChargeAttackLevel == 0) { // no charge, launch normal attack
							luanchAttack ();

						} else { // start charging
							mdl.isCharging = true;
						}
					}
				} else if (mdl.isCharging == true) {
					//charge
					chargeAttackTimeGo += Time.deltaTime;

					if (chargeAttackTimeGo < mdl.chargeTimeList [0]) { // start charging
						mdl.chargeAttackLevel = 0;
					} else if (chargeAttackTimeGo >= mdl.chargeTimeList [0] && chargeAttackTimeGo < mdl.chargeTimeList [1] && mdl.maxChargeAttackLevel >= 1) { // combo 1
						mdl.chargeAttackLevel = 1;
					} else if (chargeAttackTimeGo >= mdl.chargeTimeList [1] && chargeAttackTimeGo < mdl.chargeTimeList [2] && mdl.maxChargeAttackLevel >= 2) { // combo 2
						mdl.chargeAttackLevel = 2;
					} else if (chargeAttackTimeGo >= mdl.chargeTimeList [2] && mdl.maxChargeAttackLevel >= 3) { // combo 3
						mdl.chargeAttackLevel = 3;
					}
				}
			}
		}

		void AttackBtUp () {
			if (hasStartAttack == true) {
				luanchAttack ();
			}
		}

		void luanchAttack () {
			mdl.isCharging = false;
			mdl.isStartAttack = false;
			hasStartAttack = false;

			mdl.isAttacking = true;

			if (heroCc.isGrounded) { // charge attack
				if (mdl.chargeAttackLevel > 0) {
					if (mdl.groundAttackTimes < mdl.maxGroundCombo) {
						mdl.groundAttackTimes += 1;
						attackTimeGo = 0;
					}
				} else { // Combo attack
					if (mdl.groundAttackTimes < mdl.maxGroundCombo) {
						mdl.groundAttackTimes += 1;
						attackTimeGo = 0;
					}
				}
			} else { // jump attack
				if (mdl.airAttackTimes == 0) {
					mdl.airAttackTimes += 1;
					attackTimeGo = 0;
					grivaty = mdl.jumpAttackUpForce;
				}
			}
		}

		void HeroJump () {
			if (mdl.jumpTimes < mdl.heroMaxJumpTimes && mdl.airAttackTimes == 0 && mdl.isAttacking == false && mdl.isStartAttack == false && mdl.isHitTop == false) {
				if (SystemInfo.deviceType == DeviceType.Handheld) {
					if (mdl.inGameUiMdl.JumpBtStatus == InGameBtStatus.down) {
						LuanchJump ();
					}
				} else if (SystemInfo.deviceType == DeviceType.Desktop) {
					if (Input.GetKeyDown (KeyCode.Space)) {
						LuanchJump ();
					}
				}

				// game controller
				if (Input.GetKeyDown (KeyCode.Joystick1Button14)) {
					LuanchJump ();
				}
			}
		}

		void LuanchJump () {
			mdl.isCharging = false;
			mdl.isStartAttack = false;
			hasStartAttack = false;
			mdl.groundAttackTimes = 0;

			mdl.isDashing = false;
			mdl.isAfterDash = false;
			dashTimeGo = 0f;

			grivaty = mdl.heroJumpForce;
			mdl.jumpTimes += 1;

			// GetJumpDir
			if (SystemInfo.deviceType == DeviceType.Handheld) {
				if (mdl.joystickMdl.joystickForce > 0) {

					float tempAngle = mdl.heroCameraMdl.cameraDirAxis.eulerAngles.y - mdl.joystickMdl.joystickAngel;
					mdl.heroDir = new Vector2 (Mathf.Cos (tempAngle * Mathf.Deg2Rad), -Mathf.Sin (tempAngle * Mathf.Deg2Rad));
					Vector2 tempDir = mdl.heroDir * mdl.joystickMdl.joystickForce;

					// rotate
					mdl.heroObj.transform.rotation = Quaternion.LookRotation (new Vector3 (mdl.heroDir.x, 0, mdl.heroDir.y));
					// speed
					mdl.heroSpeed = Mathf.Lerp (0f, mdl.heroMoveSpeed, mdl.joystickMdl.joystickForce);

					finalHeroDir = new Vector3 (tempDir.x * mdl.heroSpeed, tempDir.y * mdl.heroSpeed);
				}
			} else if (SystemInfo.deviceType == DeviceType.Desktop) {
				float tempDirX = Input.GetAxis("Horizontal");
				float tempDirY = Input.GetAxis("Vertical");

				if (tempDirX != 0 || tempDirY != 0) {

					float tempInputAngle = Vector2.Angle (Vector2.right, new Vector2 (tempDirX, tempDirY));
					if (tempDirY < 0)
						tempInputAngle = 360 - tempInputAngle;

					float tempAngle = mdl.heroCameraMdl.cameraDirAxis.eulerAngles.y - tempInputAngle;

					lastMoveAngle = tempAngle;

					mdl.heroDir = new Vector2 (Mathf.Cos (tempAngle * Mathf.Deg2Rad), -Mathf.Sin (tempAngle * Mathf.Deg2Rad));

					// check run
					if (Input.GetKey (KeyCode.LeftShift)) {
						moveSpeed = mdl.heroMoveSpeed;
					} else {
						moveSpeed = mdl.heroMoveSpeed * 0.6f;
					}

					// rotate
					mdl.heroObj.transform.rotation = Quaternion.LookRotation (new Vector3 (mdl.heroDir.x, 0, mdl.heroDir.y));
					// speed
					mdl.heroSpeed = moveSpeed;

					finalHeroDir = new Vector2 (mdl.heroDir.x * mdl.heroSpeed, mdl.heroDir.y * mdl.heroSpeed);
				}
			}
		}

		void HeroDash () { 
			if (mdl.isStartAttack == false && mdl.isAttacking == false && mdl.isDashing == false && mdl.dashTimes < mdl.heroMaxDashTimes) {
				if (SystemInfo.deviceType == DeviceType.Handheld) {
					if (mdl.inGameUiMdl.DashBtStatus == InGameBtStatus.down) {
						LuanchDash ();
					}
				} else if (SystemInfo.deviceType == DeviceType.Desktop) {
					if (Input.GetKeyDown (KeyCode.C)) {
						LuanchDash ();
					}
				}

				// game controller
				if (Input.GetKeyDown (KeyCode.Joystick1Button9)) {
					LuanchDash ();
				}
			}
		}

		void LuanchDash () {
			mdl.isCharging = false;
			mdl.isStartAttack = false;
			hasStartAttack = false;
			mdl.groundAttackTimes = 0;

			mdl.dashTimes += 1;
			mdl.isDashing = true;
			mdl.isAfterDash = false;
			grivaty = 0f;

			// get dash dir
			if (SystemInfo.deviceType == DeviceType.Handheld) {
				if (mdl.joystickMdl.joystickForce > 0) {

					float tempAngle = mdl.heroCameraMdl.cameraDirAxis.eulerAngles.y - mdl.joystickMdl.joystickAngel;
					mdl.heroDir = new Vector2 (Mathf.Cos (tempAngle * Mathf.Deg2Rad), -Mathf.Sin (tempAngle * Mathf.Deg2Rad));
					Vector2 tempDir = mdl.heroDir * mdl.joystickMdl.joystickForce;
					// speed
					mdl.heroSpeed = Mathf.Lerp (0f, mdl.heroMoveSpeed, mdl.joystickMdl.joystickForce);

					finalHeroDir = new Vector3 (tempDir.x * mdl.heroSpeed, tempDir.y * mdl.heroSpeed);
				}
			} else if (SystemInfo.deviceType == DeviceType.Desktop) {
				float tempDirX = Input.GetAxis("Horizontal");
				float tempDirY = Input.GetAxis("Vertical");

				if (tempDirX != 0 || tempDirY != 0) {

					float tempInputAngle = Vector2.Angle (Vector2.right, new Vector2 (tempDirX, tempDirY));
					if (tempDirY < 0)
						tempInputAngle = 360 - tempInputAngle;

					float tempAngle = mdl.heroCameraMdl.cameraDirAxis.eulerAngles.y - tempInputAngle;

					mdl.heroDir = new Vector2 (Mathf.Cos (tempAngle * Mathf.Deg2Rad), -Mathf.Sin (tempAngle * Mathf.Deg2Rad));

					// check run
					if (Input.GetKey (KeyCode.LeftShift)) {
						moveSpeed = mdl.heroMoveSpeed;
					} else {
						moveSpeed = mdl.heroMoveSpeed * 0.6f;
					}

					// speed
					mdl.heroSpeed = moveSpeed;

					finalHeroDir = new Vector2 (mdl.heroDir.x * mdl.heroSpeed, mdl.heroDir.y * mdl.heroSpeed);
				}
			}
			// rotate
			mdl.heroObj.transform.rotation = Quaternion.LookRotation (new Vector3 (mdl.heroDir.x, 0, mdl.heroDir.y));

			dashForce = new Vector3 (mdl.heroDir.x * mdl.heroDashSpeed, 0, mdl.heroDir.y * mdl.heroDashSpeed);

		}

		void GetHeroGroundInfo () {
			RaycastHit hitInfo;
			float tempY;
			if (Physics.Raycast (mdl.heroObj.transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, mdl.groundInfoCheckDistance)) {
				tempY = hitInfo.point.y;
//				groundNormal = hitInfo.normal;
			} else {
				tempY = mdl.heroObj.transform.position.y;
//				groundNormal = Vector3.up;
			}
			float tempLerpY = Mathf.MoveTowards (mdl.heroObj.transform.position.y, tempY, 2f);
			mdl.heroGroundPos = new Vector3 (mdl.heroObj.transform.position.x, tempLerpY, mdl.heroObj.transform.position.z);
		}

		void OnControllerColliderHit(ControllerColliderHit hit) {
			// get slide force
			if (heroCc.isGrounded) { // && mdl.isMoving == false) {
				slopeAngle = Vector3.Angle (hit.normal, Vector3.up);
				Vector3 f = new Vector3 (hit.normal.x, 0, hit.normal.z) * mdl.slopeSlideSpeed;

				if (mdl.isMoving == false) { // not moving
					if (slopeAngle > mdl.slideAngle) {
						isSliding = true;
						slopeForce = f;
					} else {
						isSliding = false;
						slopeForce = Vector3.zero;
					}
				} else { // is moving
					isSliding = true;
					slopeForce = f;
				}

				if (mdl.isDashing == false)
					mdl.isAfterDash = false;

				mdl.isHitTop = false;
			} else { // not grounded
				isSliding = false;
				slopeAngle = 0;
				slopeForce = Vector3.zero;

				// get grivaty while hit top collider
				if (heroCc.collisionFlags == CollisionFlags.Above || (int)heroCc.collisionFlags == 3) {
					if (grivaty > 0)
						grivaty = 0f;

					mdl.isHitTop = true;
				}
			}


		}
	}
}
