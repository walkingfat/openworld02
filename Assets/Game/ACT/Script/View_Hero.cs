﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat {
	public class View_Hero : MonoBehaviour {

		enum heroActionStatus {
			idle = 0,
			walk = 1,
			jump = 2,
			secondJump = 3,
			fall = 4,
			dash = 5,
			Hit = 6,
			dead = 7,
			shield = 8,
			shieldWalk = 9,

			// sword
			swordStartAttack1 = 30,
			swordStartAttack2 = 31,
			swordStartAttack3 = 32,
			
			swordAttack1 = 33,
			swordAttack2 = 34,
			swordAttack3 = 35,
				
			swordCharge1 = 36,
			swordCharge2 = 37,
			swordCharge3 = 38,

			swordChargeAttack1 = 39,
			swordChargeAttack2 = 40,
			swordChargeAttack3 = 41,

			swordChargeWalk1 = 42,
			swordChargeWalk2 = 43,
			swordChargeWalk3 = 44,

			swordJumpAttack = 45,
		}

		enum chargeEffectStatus {
			none = 0,
			charge1 = 1,
			charge2 = 2,
			charge3 = 3,
		}


		private Mdl_Hero mdl;
		private Animator heroAnim;
		private CharacterController heroCc;

		public float secondJumpRollDuration;

		private float timeGo;

		// Use this for initialization
		void Start () {
			mdl = GetComponent<Mdl_Hero> () as Mdl_Hero;

			heroAnim = mdl.heroObj.GetComponent<Animator>() as Animator;
			heroCc = GetComponent<CharacterController> () as CharacterController;
		}
		
		// Update is called once per frame
		void FixedUpdate () {
            if (heroAnim != null) {
    			if (mdl.isDashing == true) {
    				heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.dash);
    				
    			} else {
    				if (heroCc.isGrounded == true) {
    					if (mdl.airAttackTimes == 0) { // for after air attack
    						if (mdl.isStartAttack == true) { // before attack
    							if (mdl.groundAttackTimes == 0) { // normal sword attack
    								heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordStartAttack1);
    								heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    							} else if (mdl.groundAttackTimes == 1) {
    								heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordStartAttack2);
    								heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    							} else if (mdl.groundAttackTimes == 2) {
    								heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordStartAttack3);
    								heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    							}
    						} else if (mdl.isCharging == true) { // charge
    							if (mdl.isMoving == true) { // sword charge move
    								if (mdl.groundAttackTimes == 0) {
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordChargeWalk1);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.charge1);
    								} else if (mdl.groundAttackTimes == 1) {
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordChargeWalk2);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.charge2);
    								} else if (mdl.groundAttackTimes == 2) {
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordChargeWalk3);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.charge3);
    								}
    							} else { // sword charge idle
    								if (mdl.groundAttackTimes == 0) {
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordCharge1);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.charge1);
    								} else if (mdl.groundAttackTimes == 1) {
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordCharge2);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.charge2);
    								} else if (mdl.groundAttackTimes == 2) {
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordCharge3);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.charge3);
    								}
    							}
    						} else if (mdl.isAttacking == true) {
    							if (mdl.chargeAttackLevel > 0) { // charge attack
    								if (mdl.groundAttackTimes == 1) { // charge attack 1
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordChargeAttack1);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								} else if (mdl.groundAttackTimes == 2) { // charge attack 2
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordChargeAttack2);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								} else if (mdl.groundAttackTimes == 3) { // charge attack 3
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordChargeAttack3);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								}
    							} else { // normal attack
    								if (mdl.groundAttackTimes == 1) { // attack combo 1
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordAttack1);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								} else if (mdl.groundAttackTimes == 2) { // attack combo 2
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordAttack2);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								} else if (mdl.groundAttackTimes == 3) { // attack combo 3
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordAttack3);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								}
    							}
    						} else {
    							if (mdl.isShield == true) {
    								if (mdl.isMoving == true) { // moving
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.shieldWalk);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								} else {
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.shield);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								}
    							} else {
    								if (mdl.isMoving == true) { // moving
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.walk);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								} else { // idle
    									heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.idle);
    									heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    								}
    							}
    						}
    					}
    				} else {
    					if (mdl.airAttackTimes == 0) {
    						if (mdl.jumpTimes == 0) { // fall from ground edge
    							heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.fall);
    							heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);

    						} else if (mdl.jumpTimes == 1) { // first jump
    							heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.jump);
    							heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);

    						} else if (mdl.jumpTimes == 2) {
    							if (mdl.isHitTop == false && mdl.isAfterDash == false) { // second jump
    								heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.secondJump);
    								heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);

    							} else { // hit top while jumping or is after dashing
    								heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.jump);
    								heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    							}
    						}
    					} else { // jump attack
    						heroAnim.SetInteger ("heroActionId", (int)heroActionStatus.swordJumpAttack);
    						heroAnim.SetInteger ("chargeEffectId", (int)chargeEffectStatus.none);
    					}
    				}
    			}
            }
        }
	}
}
