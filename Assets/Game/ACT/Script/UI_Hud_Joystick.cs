﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WalkingFat;

namespace WalkingFat {
	public enum JoystickViewStatus {
		disable = 0,
		enable = 1,
		show = 2,
		hide = 3,
		beHit = 4,
		flash = 5,
		locked = 6,
	}
	
	public class UI_Hud_Joystick : MonoBehaviour {

		public Animator joystickAnimator;
		public RectTransform stickTransform;
		public RectTransform axisTransform;

		public float joystickRadius;
		public float joystickInnerRadius;

		// Use this for initialization
		void Start () {
			
		}

		public void SetAnimStatusId (JoystickViewStatus id) {
			if (joystickAnimator)
				joystickAnimator.SetInteger ("statusId", (int)id);
			else
				Debug.LogError ("Can not find left joystick animator.");
		}

		public void SetAnimForceValue (float force) {
			if (joystickAnimator)
				joystickAnimator.SetFloat ("force", force);
			else
				Debug.LogError ("Can not find left joystick animator.");
		}

		public void SetStickPos (Vector2 pos) {
			if (stickTransform)
                stickTransform.anchoredPosition = pos;
			else
				Debug.LogError ("Can not find stick transform of joystick.");
		}

		public void SetAxisPos (Vector2 pos) {
            if (axisTransform)
                axisTransform.anchoredPosition = pos;
            else
				Debug.LogError ("Can not find axis transform of joystick.");
		}
	}
}