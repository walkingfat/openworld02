﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat {
	public class UI_TestCharShow : MonoBehaviour {

		public Transform charTransform;
		public int originCharDirY;

		public float charRotateSpeed = 1;

		private Vector3 prevMousePos;
		private Vector3 deltaMousePos;

		private float timeGo = 0;

		public Transform cameraTransform;
		private int cameraPosId = 0;
		public Transform[] cameraPosList;
        public Animator charAnimator;

		// Use this for initialization
		void Start () {
			charRotateSpeed *= 360f / (float)Screen.width;
		}
		
		// Update is called once per frame
		void FixedUpdate () {
			RotateChar ();
		}

		void RotateChar () {
			if (Input.GetMouseButtonDown(0)) {
				prevMousePos = Input.mousePosition;
			}
			if (Input.GetMouseButton(0)) {
				deltaMousePos = Input.mousePosition - prevMousePos;
				prevMousePos = Input.mousePosition;

				charTransform.localEulerAngles += new Vector3 (0, deltaMousePos.x * charRotateSpeed * Time.deltaTime, 0);

			}
		}

		public void switchCameraPos () {
			cameraPosId += 1;
			if (cameraPosId >= cameraPosList.Length)
				cameraPosId = 0;

			cameraTransform.SetParent (cameraPosList[cameraPosId]);
			cameraTransform.localEulerAngles = Vector3.zero;
			cameraTransform.localPosition = Vector3.zero;

			print ("cameraPosId = " + cameraPosId);
		}

        public void SwitchCharAnim () {
            int i = charAnimator.GetInteger("statusId");
            i += 1;
            if (i > 3) i = 0;
            charAnimator.SetInteger("statusId", i);
        }
	}
}