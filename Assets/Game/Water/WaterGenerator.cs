﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor; // for inspector button
using WalkingFat;

namespace WalkingFat
{
    [ExecuteInEditMode]
    public class WaterGenerator : MonoBehaviour
    {
        public Mesh mesh;
        public Material mat;
        public int meshSize;
        public int WaterSize;
        
        // draw mesh instance
        public Camera camera;
        private Matrix4x4 matrix;
        private Matrix4x4[] matrices;
        private MaterialPropertyBlock property;
        public LayerMask drawLayer;
        public bool castShadows = false;
        public bool recieveShadows = false;
        public bool useGpuInstancing = false;

        private Vector3[] waterPosList;

        private int oldMeshSize;
        private int oldWaterSize;

        private void Start()
        {
            InitializeWaterList();
            //Debug.LogError("water start");
        }

        private void Update()
        {
            if (oldMeshSize != meshSize || oldWaterSize != WaterSize)
            {
                InitializeWaterList();

                oldMeshSize = meshSize;
                oldWaterSize = WaterSize;
            }
               
            DrawWaterInstancing();
        }

        private void InitializeWaterList()
        {
            waterPosList = new Vector3[WaterSize * WaterSize];

            for (int h = 0; h < WaterSize; h++)
            {
                for (int v = 0; v < WaterSize; v++)
                {
                    int id = v + h * WaterSize;

                    waterPosList[id] = new Vector3((h - WaterSize * 0.5f + 0.5f) * meshSize, transform.position.y, (v - WaterSize * 0.5f + 0.5f) * meshSize);
                }
            }
        }

        private void DrawWaterInstancing ()
        {
            if (waterPosList.Length > 1023)
            {
                matrices = new Matrix4x4[1023];
            }
            else
            {
                matrices = new Matrix4x4[waterPosList.Length];
            }

            for (int n = 0; n < matrices.Length; n++)
            {
                matrix = Matrix4x4.TRS(waterPosList[n], Quaternion.identity, Vector3.one);

                if (useGpuInstancing)
                {
                    matrices[n] = matrix;
                }
                else
                {
                    Graphics.DrawMesh(mesh, matrix, mat, drawLayer.value, camera, 0, property, castShadows, recieveShadows, false); // otherwise just draw it now
                }
            }

            if (useGpuInstancing)
            {
                UnityEngine.Rendering.ShadowCastingMode castShadowMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                if (castShadows)
                    castShadowMode = UnityEngine.Rendering.ShadowCastingMode.On;

                Graphics.DrawMeshInstanced(mesh, 0, mat, matrices, matrices.Length, property, castShadowMode, recieveShadows, drawLayer.value, camera);

            }
        }
    }
}