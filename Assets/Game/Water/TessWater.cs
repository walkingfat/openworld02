﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WalkingFat;

namespace WalkingFat
{
    public class TessWater : MonoBehaviour
    {
        public Texture2D noiseTex;
        private RenderTexture rt;
        public Material prePassMat;
        public Material waterMat;

        public int size;

        // Start is called before the first frame update
        void Start()
        {
            rt = new RenderTexture(512, 512, 0, RenderTextureFormat.ARGB32);

            if (!noiseTex || !rt || !prePassMat)
            {
                Debug.LogError("lost source file.");
            }
        }

        // Update is called once per frame
        void Update()
        {

            Graphics.Blit(noiseTex, rt, prePassMat);

            waterMat.SetTexture("_RefNoiseTex", rt);
        }

        void InitialzeWater()
        {

        }
    }

}