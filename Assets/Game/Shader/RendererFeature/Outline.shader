﻿Shader "WalkingFat/RendererFeather/Outline"
{
	Properties
	{
		_Color("Color", Color) = (0,0,0,1)
		_Width("Width", Range(0.0, 0.01)) = 0.005
		_EqualWidthRate("Equal Width Rate", Range(0.0, 1.0)) = 0.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300

		Pass
		{
			Name "ReceiveRain"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			//ZTest Greater
			Cull Front

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _Color;
			float _Width;
			float _EqualWidthRate;
			CBUFFER_END

			float _CameraTan;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				input.positionOS.xyz += _Width * input.normalOS;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				half3 normalWS = normalize(normalInput.normalWS);
				half3 positionWS = TransformObjectToWorld(input.positionOS.xyz);

				half distWS = distance(positionWS, GetCameraPositionWS());

				positionWS.xyz += _Width * normalWS * distWS * _CameraTan * _EqualWidthRate;

				output.positionCS = TransformWorldToHClip(positionWS);

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				return _Color;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
