﻿Shader "WalkingFat/RendererFeather/SSGI"
{
	Properties
	{
		[HideInInspector]_MainTex("Main Tex", 2D) = "" {}
		_NoiseTex("Noise Tex", 2D) = "" {}
		[HideInInspector]_RayCount("Ray Count", Float) = 4
		[HideInInspector]_ScreenRes("Screen Res", Vector) = (1,1,1,1)
		[HideInInspector]_TraceRes("Trace Res", Vector) = (1,1,1,1)
		[HideInInspector]_TraceSteps("Trace Steps", Float) = 16
		[HideInInspector]_TraceMaxLevel("Trace Max Level", int) = 10
		[HideInInspector]_TraceStartLevel("Trace Start Level", int) = 1
		[HideInInspector]_TraceEndLevel("Trace End Level", int) = 1
		[HideInInspector]_Thickness("Thickness", Float) = 4
		[HideInInspector]_TemporalScale("Temporal Scale", Float) = 0
		[HideInInspector]_TemporalWeight("Thickness", Float) = 0.99
		[HideInInspector]_RandomOffset("Random Offset", Vector) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 200

		Pass
		{
			Name "CopyDepth"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
			CBUFFER_END

			TEXTURE2D_FLOAT(_MainTex);						SAMPLER(sampler_MainTex);
			//TEXTURE2D(_CameraDepthTexture);				SAMPLER(sampler_CameraDepthTexture);

			// Global

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = input.texcoord;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				return output;
			}


			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float depth = SAMPLE_DEPTH_TEXTURE(_MainTex, sampler_MainTex, input.uv);
				//float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, input.uv);

				return depth;
			}
			ENDHLSL
		}

		Pass
		{
			Name "HiZ"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_Position;
				float4 uv : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
				float4 _ScreenRes;
			CBUFFER_END

			TEXTURE2D_FLOAT(_HiZRT);						SAMPLER(sampler_HiZRT);

			// global
			int _HiZ_PrevLevel;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = input.texcoord;
				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);

				return output;
			}

			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float2 screenPixel = float2(_ScreenRes.z, _ScreenRes.w);

				float4 minDepth = float4(
					SAMPLE_DEPTH_TEXTURE_LOD(_HiZRT, sampler_HiZRT, input.uv + float2(-screenPixel.x, -screenPixel.y), _HiZ_PrevLevel),
					SAMPLE_DEPTH_TEXTURE_LOD(_HiZRT, sampler_HiZRT, input.uv + float2(-screenPixel.x, screenPixel.y), _HiZ_PrevLevel),
					SAMPLE_DEPTH_TEXTURE_LOD(_HiZRT, sampler_HiZRT, input.uv + float2(screenPixel.x, -screenPixel.y), _HiZ_PrevLevel),
					SAMPLE_DEPTH_TEXTURE_LOD(_HiZRT, sampler_HiZRT, input.uv + float2(screenPixel.x, screenPixel.y), _HiZ_PrevLevel)
					);

				return max(max(minDepth.r, minDepth.g), max(minDepth.b, minDepth.a));
				//return EncodeFloatRGB(hiz);
			}
			ENDHLSL
		}

		Pass
		{
			Name "NormalWS"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_Position;
				float4 uv : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
				float4 _ScreenRes;
			CBUFFER_END
			

			TEXTURE2D_FLOAT(_MainTex);							SAMPLER(sampler_MainTex);
			//TEXTURE2D_FLOAT(_HiZRT);							SAMPLER(sampler_HiZRT);
			//TEXTURE2D(_CameraDepthTexture);				SAMPLER(sampler_CameraDepthTexture);

			// global
			int _HiZ_PrevLevel;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = input.texcoord;

				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);

				float4 projPos = output.positionCS * 0.5;
				projPos.xy = projPos.xy + projPos.w;

				output.uv.xy = input.texcoord;
				output.uv.zw = projPos.xy;

				return output;
			}

			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// get normal WS
				float2 screenPixel = float2(_ScreenRes.z, _ScreenRes.w);
				float2 uv0 = input.uv.xy;
				float2 uv1 = input.uv.xy + float2(screenPixel.x, 0); // right 
				float2 uv2 = input.uv.xy + float2(0, screenPixel.y); // top

				float depth0 = SAMPLE_DEPTH_TEXTURE(_MainTex, sampler_MainTex, uv0);
				float depth1 = SAMPLE_DEPTH_TEXTURE(_MainTex, sampler_MainTex, uv1);
				float depth2 = SAMPLE_DEPTH_TEXTURE(_MainTex, sampler_MainTex, uv2);

				float2 ndc0 = input.uv.zw;
				float2 ndc1 = input.uv.zw + float2(screenPixel.x / input.positionCS.w, 0);
#if UNITY_UV_STARTS_AT_TOP
				float2 ndc2 = input.uv.zw + float2(0, -screenPixel.y / input.positionCS.w);
#else
				float2 ndc2 = input.uv.zw + float2(0, screenPixel.y / input.positionCS.w);
#endif
				float3 P0 = GetPosWSFromDepth(ndc0, depth0);
				float3 P1 = GetPosWSFromDepth(ndc1, depth1);
				float3 P2 = GetPosWSFromDepth(ndc2, depth2);

				float3 normalWS = normalize(cross(P2 - P0, P1 - P0));
				//return float4(normalWS, 1);
				return float4((normalWS + 1) * 0.5, 1);
			}
			ENDHLSL
		}

		Pass
		{
			Name "RayTracing"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
				int _RayCount;
				int _TraceSteps;
				float4 _ScreenRes;
				float4 _TraceRes;
				int _TraceMaxLevel;
				int _TraceStartLevel;
				int _TraceEndLevel;
				float _Thickness;
				float4 _RandomOffset;
				float _TemporalWeight;
			CBUFFER_END

			TEXTURE2D(_MainTex);						SAMPLER(sampler_MainTex);
			TEXTURE2D(_NoiseTex);						SAMPLER(sampler_NoiseTex);
			TEXTURE2D(_NormalWS);						SAMPLER(sampler_NormalWS);
			TEXTURE2D_FLOAT(_HiZRT);					SAMPLER(sampler_HiZRT);

			TEXTURE2D(_PrevSSGITraceRT);				SAMPLER(sampler_PrevSSGITraceRT);

			// Global
			float4x4 _Matrix_V;
			float4x4 _Matrix_IV;
			float4x4 _Matrix_P;
			float4x4 _Matrix_IP;
			float4x4 _Matrix_VP;
			float4x4 _Matrix_IVP;
			float4x4 _Matrix_VP_Prev;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				float4 projPos = output.positionCS * 0.5;
				projPos.xy = projPos.xy + projPos.w;

				output.uv.xy = input.texcoord;
				output.uv.zw = projPos.xy;

				return output;
			}

			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// sample depth RT
				float depth = SAMPLE_TEXTURE2D_LOD(_HiZRT, sampler_HiZRT, input.uv.xy, 0);
				float EyeDepth = LinearEyeDepth(depth, _ZBufferParams);

				// current pos VS
				float3 screenPos = float3(input.uv.xy * 2 - 1, depth);
				float4 viewPos = mul(_Matrix_IP, float4(screenPos, 1));
				float3 posVS = viewPos.xyz / viewPos.w;

				// sample normal RT
				float4 normalTex = SAMPLE_TEXTURE2D(_NormalWS, sampler_NormalWS, input.uv.xy);
				float3 normalWS = normalTex.xyz;
				normalWS = normalWS * 2 - 1;

				float3x3 TangentBasis = GetTangentBasis(normalWS);
				TangentBasis = GetTangentSpaceMatrix(normalWS);

				//-----Consten Property-------------------------------------------------------------------------
				float Out_Mask = 0;
				float3 Out_Color = 0;
				float4 Out_Test = float4(0,0,0,1);

				UNITY_LOOP
				for (uint i = 0; i < (uint)_RayCount; i++)
				{
					//-----Trace Dir-----------------------------------------------------------------------------
					float noiseTexSize = float2(1024, 1024);
					float2 noiseUV = (input.uv.xy + sin(i + _TraceRes.zw)) * _ScreenRes.xy / noiseTexSize;
					float2 Hash = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, noiseUV).xy;

					float3 L;
					L.xy = UniformSampleDiskConcentric(Hash);
					L.z = sqrt(1 - dot(L.xy, L.xy));
					float3 World_L = mul(L, TangentBasis);
					float3 View_L = mul((float3x3)(_Matrix_V), World_L);

					float marchDither = abs((Hash.x + Hash.y) % 0.05);

					float3 rayStart = float3(input.uv.xy, screenPos.z);
					float4 rayProj = mul(_Matrix_P, float4(posVS + View_L, 1.0));
					float3 rayDir = normalize((rayProj.xyz / rayProj.w) - screenPos);
					rayDir.xy *= 0.5;

					float4 RayHitData = HiZ_Tracing(_TraceMaxLevel, _TraceStartLevel, _TraceEndLevel, _TraceSteps, _Thickness, 
						_ScreenRes.zw, rayStart, rayDir, marchDither, _HiZRT, sampler_HiZRT);
					
					//Out_Test = float4(RayHitData.xyz, 1);

					float3 SampleColor = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, float4(RayHitData.xy, 0, 0));
					float4 SampleNormal = SAMPLE_TEXTURE2D(_NormalWS, sampler_NormalWS, RayHitData.xy * 2 - 1);

					float Occlusion = 1 - saturate(dot(World_L, SampleNormal.rgb));
					Occlusion *= saturate(dot(World_L, normalWS));
					//float Occlusion = saturate(dot(World_L, normalWS));

					SampleColor *= Occlusion;
					SampleColor *= rcp(1 + Luminance(SampleColor));

					// fix warp edge problem
					float outClip = 1 - saturate(dot(float2(1, 1), step(0.5, abs(RayHitData.xy - 0.5))));
					Out_Color += SampleColor * outClip;

					// screen fade bord
					float borderDist = min(1 - max(RayHitData.xy.x, RayHitData.xy.y), min(RayHitData.xy.x, RayHitData.xy.y));
					float value = 0.05;
					float screenFade = saturate(borderDist > value ? 1 : borderDist / value);
					Out_Mask += pow(RayHitData.a * screenFade, 2);
				}
				Out_Color /= _RayCount;
				Out_Color *= rcp(1 - Luminance(Out_Color));
				Out_Mask /= _RayCount;

				//-----Output-----------------------------------------------------------------------------
				//half4 traceCol float4(Out_Color, EyeDepth);
				half4 traceCol = float4(Out_Color * saturate(Out_Mask * 2), EyeDepth);
				half3 sceneCol = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv.xy).rgb;
				half sceneLum = dot(sceneCol.rgb, half3(0.299, 0.587, 0.114));
				half emissionMask = step(sceneLum, 1);

				traceCol *= emissionMask;
				return traceCol;
			}
			ENDHLSL
		}

		Pass
		{
			Name "Temporal Filter"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
				float4 _ScreenRes;
				float _TemporalWeight;
				float _TemporalScale;
			CBUFFER_END

			TEXTURE2D(_MainTex);						SAMPLER(sampler_MainTex);
			//TEXTURE2D(_NormalWS);						SAMPLER(sampler_NormalWS);
			TEXTURE2D(_PrevSSGITraceRT);				SAMPLER(sampler_PrevSSGITraceRT);
			TEXTURE2D(_HiZRT);							SAMPLER(sampler_HiZRT);

			// global
			float4x4 _Matrix_VP_Prev;
			
			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				float4 projPos = output.positionCS * 0.5;
				projPos.xy = projPos.xy + projPos.w;

				output.uv.xy = input.texcoord;
				output.uv.zw = projPos.xy;

				return output;
			}

			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				//half2 UV = input.uv;
				//half3 WorldNormal = SAMPLE_TEXTURE2D(_NormalWS, sampler_NormalWS, input.uv) * 2 - 1;

				// motion vector
				float depth = SAMPLE_TEXTURE2D_LOD(_HiZRT, sampler_HiZRT, input.uv.xy, 0);
				float3 posWS = GetPosWSFromDepth(input.uv.zw, depth);
				float4 prevPosSS = ComputeScreenPos(mul(_Matrix_VP_Prev, float4(posWS, 1)));

				prevPosSS.xy = prevPosSS.xy / prevPosSS.w;

#if UNITY_UV_STARTS_AT_TOP
				prevPosSS.y = 1 - prevPosSS.y;
				float2 velocity = float2(input.uv.x - prevPosSS.x, prevPosSS.y - input.uv.y);
#else
				float2 velocity = float2(input.uv.x - prevPosSS.x, input.uv.y - prevPosSS.y);
#endif

				/////Get AABB ClipBox
				half SS_Indirect_Variance = 0;
				half4 SS_Indirect_CurrColor = 0;
				half4 SS_Indirect_MinColor, SS_Indirect_MaxColor;
				ResolverAABB(_MainTex, sampler_MainTex, 0, 10, _TemporalScale, input.uv, _ScreenRes, SS_Indirect_Variance, SS_Indirect_MinColor, SS_Indirect_MaxColor, SS_Indirect_CurrColor);

				/////Clamp TemporalColor
				half4 prevTraceTex = SAMPLE_TEXTURE2D(_PrevSSGITraceRT, sampler_PrevSSGITraceRT, prevPosSS.xy);
				prevTraceTex = clamp(prevTraceTex, SS_Indirect_MinColor, SS_Indirect_MaxColor);
				//SS_Indirect_CurrColor = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv);

				//return SS_Indirect_CurrColor;

				/////Combine TemporalColor
				half Temporal_BlendWeight = saturate(_TemporalWeight * (1 - length(velocity) * 2));
				return lerp(SS_Indirect_CurrColor, prevTraceTex, Temporal_BlendWeight);
			}
			ENDHLSL
		}

		Pass
		{
			Name "Bilateralfilter_X"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
				float4 _ScreenRes;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = input.texcoord;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				return output;
			}

			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				const float Radius = 12.0;
				return BilateralBlur(Radius, input.uv, float2(_ScreenRes.z, 0), _MainTex, sampler_MainTex);
			}
			ENDHLSL
		}

		Pass
		{
			Name "Bilateralfilter_Y"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
				float4 _ScreenRes;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = input.texcoord;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				return output;
			}

			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				const float Radius = 12.0;
				return BilateralBlur(Radius, input.uv, float2(0, _ScreenRes.w), _MainTex, sampler_MainTex);
			}
			ENDHLSL
		}

		Pass
		{
			Name "BlendScene"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
				//float4 _ScreenRes;
			CBUFFER_END

			TEXTURE2D(_MainTex);								SAMPLER(sampler_MainTex);
			TEXTURE2D(_SSGIRT);									SAMPLER(sampler_SSGIRT);
			//TEXTURE2D(_SSGITraceRT);							SAMPLER(sampler_SSGITraceRT);
			//TEXTURE2D(_PrevSSGITraceRT);						SAMPLER(sampler_PrevSSGITraceRT);

			// global
			float _SSGIIntensity;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = input.texcoord;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				return output;
			}


			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				//return SAMPLE_TEXTURE2D(_SSGITraceRT, sampler_SSGITraceRT, input.uv);

				//half screenCut0 = step(0.333, input.uv.x);
				//half screenCut1 = step(0.666, input.uv.x);

				half4 ssgiTex = SAMPLE_TEXTURE2D(_SSGIRT, sampler_SSGIRT, input.uv);
				
				half4 sceneTex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv);
				//return sceneTex;

				//half4 traceTex = SAMPLE_TEXTURE2D(_SSGITraceRT, sampler_SSGITraceRT, input.uv);

				//half4 finalCol = half4(sceneTex.xyz * 1.0 + saturate(traceTex.xyz) * _SSGIIntensity, 1);

				//return lerp(lerp(sceneTex, traceTex, screenCut0), finalCol, screenCut1);
				//return ssgiTex;

				//half4 ssgiCol = lerp(traceTex, ssgiTex, step(0.5, input.uv.x));
				return half4(sceneTex.xyz + saturate(ssgiTex.xyz) * _SSGIIntensity, 1);
			}
			ENDHLSL
		}


		Pass
		{
			Name "RayTracing1"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
				int _RayCount;
				int _TraceSteps;
				float4 _ScreenRes;
				float4 _TraceRes;
				int _TraceMaxLevel;
				int _TraceStartLevel;
				int _TraceEndLevel;
				float _Thickness;
				float4 _RandomOffset;
				float _TemporalWeight;
			CBUFFER_END

			TEXTURE2D(_MainTex);						SAMPLER(sampler_MainTex);
			TEXTURE2D(_NoiseTex);						SAMPLER(sampler_NoiseTex);
			TEXTURE2D(_NormalWS);						SAMPLER(sampler_NormalWS);
			TEXTURE2D_FLOAT(_HiZRT);					SAMPLER(sampler_HiZRT);

			TEXTURE2D(_PrevSSGITraceRT);				SAMPLER(sampler_PrevSSGITraceRT);

			// Global
			float4x4 _Matrix_V;
			float4x4 _Matrix_IV;
			float4x4 _Matrix_P;
			float4x4 _Matrix_IP;
			float4x4 _Matrix_VP;
			float4x4 _Matrix_IVP;
			float4x4 _Matrix_VP_Prev;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				float4 projPos = output.positionCS * 0.5;
				projPos.xy = projPos.xy + projPos.w;

				output.uv.xy = input.texcoord;
				output.uv.zw = projPos.xy;

				return output;
			}

			float4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// sample depth RT
				float depth = SAMPLE_TEXTURE2D_LOD(_HiZRT, sampler_HiZRT, input.uv.xy, 0);
				float EyeDepth = LinearEyeDepth(depth, _ZBufferParams);

				// current pos VS
				float3 screenPos = float3(input.uv.xy * 2 - 1, depth);
				float4 viewPos = mul(_Matrix_IP, float4(screenPos, 1));
				float3 posVS = viewPos.xyz / viewPos.w;

				// motion vector
				float3 posWS = GetPosWSFromDepth(input.uv.zw, depth);
				float4 prevPosSS = ComputeScreenPos(mul(_Matrix_VP_Prev, float4(posWS, 1)));

				prevPosSS.xy = prevPosSS.xy / prevPosSS.w;

	#if UNITY_UV_STARTS_AT_TOP
				prevPosSS.y = 1 - prevPosSS.y;
				float2 velocity = float2(input.uv.x - prevPosSS.x, prevPosSS.y - input.uv.y);
	#else
				float2 velocity = float2(input.uv.x - prevPosSS.x, input.uv.y - prevPosSS.y);
	#endif

				// sample normal RT
				float4 normalTex = SAMPLE_TEXTURE2D(_NormalWS, sampler_NormalWS, input.uv.xy);
				float3 normalWS = normalTex.xyz;
				normalWS = normalWS * 2 - 1;

				float3x3 TangentBasis = GetTangentBasis(normalWS);
				TangentBasis = GetTangentSpaceMatrix(normalWS);

				//-----Consten Property-------------------------------------------------------------------------
				float Out_Mask = 0;
				float3 Out_Color = 0;
				float4 Out_Test = float4(0,0,0,1);

				UNITY_LOOP
				for (uint i = 0; i < (uint)_RayCount; i++)
				{
					//-----Trace Dir-----------------------------------------------------------------------------
					float noiseTexSize = float2(1024, 1024);
					float2 noiseUV = (input.uv.xy + sin(i + _TraceRes.zw)) * _ScreenRes.xy / noiseTexSize;
					float2 Hash = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, noiseUV).xy;

					float3 L;
					L.xy = UniformSampleDiskConcentric(Hash);
					L.z = sqrt(1 - dot(L.xy, L.xy));
					float3 World_L = mul(L, TangentBasis);
					float3 View_L = mul((float3x3)(_Matrix_V), World_L);

					float marchDither = abs((Hash.x + Hash.y) % 0.05);

					float3 rayStart = float3(input.uv.xy, screenPos.z);
					float4 rayProj = mul(_Matrix_P, float4(posVS + View_L, 1.0));
					float3 rayDir = normalize((rayProj.xyz / rayProj.w) - screenPos);
					rayDir.xy *= 0.5;

					float4 RayHitData = HiZ_Tracing(_TraceMaxLevel, _TraceStartLevel, _TraceEndLevel, _TraceSteps, _Thickness,
						_ScreenRes.zw, rayStart, rayDir, marchDither, _HiZRT, sampler_HiZRT);

					//Out_Test = float4(RayHitData.xyz, 1);

					float3 SampleColor = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, float4(RayHitData.xy, 0, 0));
					float4 SampleNormal = SAMPLE_TEXTURE2D(_NormalWS, sampler_NormalWS, RayHitData.xy * 2 - 1);

					half mark = step(input.uv.x, 0.5);
					float Occlusion = 1 - saturate(dot(World_L, SampleNormal.rgb));
					Occlusion *= saturate(dot(World_L, normalWS));
					//float Occlusion = saturate(dot(World_L, normalWS));

					SampleColor *= Occlusion;
					SampleColor *= rcp(1 + Luminance(SampleColor));

					// fix warp edge problem
					float outClip = 1 - saturate(dot(float2(1, 1), step(0.5, abs(RayHitData.xy - 0.5))));
					Out_Color += SampleColor * outClip;

					// screen fade bord
					float borderDist = min(1 - max(RayHitData.xy.x, RayHitData.xy.y), min(RayHitData.xy.x, RayHitData.xy.y));
					float value = 0.05;
					float screenFade = saturate(borderDist > value ? 1 : borderDist / value);
					Out_Mask += pow(RayHitData.a * screenFade, 2);
				}
				Out_Color /= _RayCount;
				Out_Color *= rcp(1 - Luminance(Out_Color));
				Out_Mask /= _RayCount;

				//-----Output-----------------------------------------------------------------------------
				//half4 traceCol float4(Out_Color, EyeDepth);
				half4 traceCol = float4(Out_Color * saturate(Out_Mask * 2), EyeDepth);
				//return traceCol;//

				// simple temporal filter
				half4 prevTraceTex = SAMPLE_TEXTURE2D(_PrevSSGITraceRT, sampler_PrevSSGITraceRT, prevPosSS.xy);
				half3 sceneCol = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv.xy).rgb;
				half sceneLum = dot(sceneCol.rgb, half3(0.299, 0.587, 0.114));
				half emissionMask = step(sceneLum, 1);

				half Temporal_BlendWeight = saturate(_TemporalWeight * (1 - length(velocity) * 2));

				return lerp(traceCol, prevTraceTex, Temporal_BlendWeight) * emissionMask;
			}
			ENDHLSL
		}

	}
}
