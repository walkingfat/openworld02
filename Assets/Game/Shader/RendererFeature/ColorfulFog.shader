﻿Shader "WalkingFat/RendererFeather/ColorfulFog"
{
	Properties
	{
		//[HideInInspector]_MainTex("Main Tex", 2D) = "" {}
		_NoiseTexture("Noise Texture", 2D) = "white" {}
		_FogColorTexture("Fog Color Texture", 2D) = "white" {}
		_FogHeight("Fog Height", float) = 10
		_GlobalFogDensity("Global Fog Density", Range(0, 1)) = 1
		_FogFalloff("Fog Falloff", Range(0, 1)) = 1
		_FogStartDensity("Fog Start Density", Range(0, 1)) = 0.5
		_FogGradientDist("Fog Gradient Distance", float) = 200
		//_FogFarFadeDist("Fog Far Fade Distance", float) = 500
		_InscatterExp("Inscatter Exponent", Range(0, 10)) = 1
		_InscatterDist("Inscatter Distance", float) = 150
		_NearFogCol("Near Fog Color", Color) = (1,1,1,1)
		_FogNoiseStrength("Fog Noise Strength", Range(0, 1)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "ColorfulFog"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_Position;
				float4 uv : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float _FogHeight;
			float _GlobalFogDensity;
			float _FogFalloff;
			float _FogStartDensity;
			float _FogGradientDist;
			//float _FogFarFadeDist;
			float _InscatterExp;
			float _InscatterDist;
			half4 _NearFogCol;
			half _FogNoiseStrength;
			CBUFFER_END

			//float4x4 _ViewToWorldMaterx;

			TEXTURE2D(_ColorTex);				SAMPLER(sampler_ColorTex); // use custom color and depth buffer for _CameraOpaqueTexture can not draw transparent
			TEXTURE2D_FLOAT(_DepthTex);						SAMPLER(sampler_DepthTex);
			TEXTURE2D(_NoiseTexture);						SAMPLER(sampler_NoiseTexture); 
			//TEXTURE2D(_CameraOpaqueTexture);		        SAMPLER(sampler_CameraOpaqueTexture);
			//TEXTURE2D(_CameraDepthTexture);				SAMPLER(sampler_CameraDepthTexture);
			TEXTURE2D(_FogColorTexture);					SAMPLER(sampler_FogColorTexture);

			// global
			float4 _WindParams;
			float3 _SkyColor0;
			float _MainLitStrength;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				//output.uv = input.texcoord;
				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);

				float4 projPos = output.positionCS * 0.5;
				projPos.xy = projPos.xy + projPos.w;

				output.uv.xy = input.texcoord;
				output.uv.zw = projPos.xy;
			
				return output;
			}

			half4 VisualizePosition(Varyings input, float3 pos)
			{
				const float grid = 1;
				const float width = 3;

				pos *= grid;

				// Detect borders with using derivatives.
				float3 fw = fwidth(pos);
				half3 bc = saturate(width - abs(1 - 2 * frac(pos)) / fw);

				// Frequency filter
				half3 f1 = smoothstep(1 / grid, 2 / grid, fw);
				half3 f2 = smoothstep(2 / grid, 4 / grid, fw);
				bc = lerp(lerp(bc, 0.5, f1), 0, f2);

				// Blend with the source color.
				//half4 c = SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, input.uv.xy);
				half4 c = half4(lerp(half3(0.5, 0.5, 0.5), bc, 0.8), 1);

				return c;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half3 mainLitCol = GetMainLight().color;
				half4 sceneCol = SAMPLE_TEXTURE2D(_ColorTex, sampler_ColorTex, input.uv.xy);
				//return sceneCol;
				float depth = SAMPLE_TEXTURE2D_X(_DepthTex, sampler_DepthTex, input.uv).r;

				// compute posWS
#if UNITY_REVERSED_Z
				depth = 1.0 - depth;
#endif

				depth = 2.0 * depth - 1.0;

				half3 viewPos = ComputeViewSpacePosition(input.uv.zw, depth, unity_CameraInvProjection);
				half4 worldPos = half4(mul(unity_CameraToWorld, half4(viewPos, 1.0)).xyz, 1.0);
				half distWS = distance(worldPos, _WorldSpaceCameraPos);

				// noise
				float2 noiseUV1 = worldPos.xz * 0.03 + _Time.x * 1.3 * (_WindParams.xy + float2(0.12, -0.25)) * _WindParams.z;
				float2 noiseUV2 = worldPos.xz * 0.02 + _Time.x * 0.6 * (_WindParams.xy + float2(-0.32, -0.15)) * _WindParams.z;
				half fogNoise1 = SAMPLE_TEXTURE2D(_NoiseTexture, sampler_NoiseTexture, noiseUV1);
				half fogNoise2 = SAMPLE_TEXTURE2D(_NoiseTexture, sampler_NoiseTexture, noiseUV2);
				half fogNoise = saturate(fogNoise1 * fogNoise2 * 2);

				half fogNoiseFade = min(1, distWS / lerp(_FogGradientDist, 50, _FogStartDensity));
				//fogNoiseFade = lerp(fogNoiseFade, 1, _FogStartDensity);
				
				fogNoiseFade = fogNoiseFade * fogNoiseFade;
				fogNoiseFade = fogNoise * saturate(1 - fogNoiseFade) + fogNoiseFade;

				//fogDensity = fogDensity * fogNoise;
				//return fogDensity;

				// Exponential Height Fog
				half fogFactor = worldPos.y - _FogHeight - fogNoise;
				half heightFogDensity = saturate(exp2(-fogFactor * _FogFalloff));

				half fogDistValue = saturate(1 - (_FogGradientDist - distWS) / _FogGradientDist);
				//fogDistValue = lerp(fogDistValue, 1, _FogStartDensity);
				half exp2FogRate = saturate(exp2(-(_WorldSpaceCameraPos.y - _FogHeight) * _FogFalloff));
				half exp2FogDensity = saturate(exp2(fogDistValue * fogDistValue)) * exp2FogRate;

				half fogDensity = saturate(heightFogDensity + exp2FogDensity) * _GlobalFogDensity;
				
				// blend sky rim color
				half2 fogDistColUv = half2(fogDistValue, 0.5);
				half4 fogDistCol = SAMPLE_TEXTURE2D(_FogColorTexture, sampler_FogColorTexture, fogDistColUv);

				//half fogFarLerp = saturate((distWS - _FogGradientDist) / (_FogFarFadeDist - _FogGradientDist));
				//fogFarLerp = fogFarLerp * fogFarLerp;

				//fogDistCol.rgb = lerp(fogDistCol.rgb, pow(_SkyColor0, 2.2), fogFarLerp);

				half fogColLerp = saturate((distWS + 30) / lerp(_FogGradientDist, 60, _FogStartDensity));
				//fogColLerp = lerp(fogColLerp, 0, _FogStartDensity * 0.8);
				fogDistCol.rgb = lerp(_NearFogCol.rgb, pow(_SkyColor0, 2.2), fogColLerp);


				// sun inscatter
				half3 viewDir = normalize(worldPos - _WorldSpaceCameraPos);
				half3 lightDir = normalize(_MainLightPosition.xyz);
				half inscatterFog = pow(saturate(dot(viewDir, lightDir)), _InscatterExp);

				half inscatterFogDistFade = max(0,distWS - _InscatterDist) * 0.01;
				inscatterFog *= saturate(pow(inscatterFogDistFade, 0.3));

				half sunHalo = pow(inscatterFog, lerp(20, 5, fogDensity));

				half inscatterFogHalo = sunHalo * fogDensity * fogDistValue * lerp(3, 1, fogDensity);
				
				half inscatterFogHaloFade = max(0, distWS - _FogGradientDist) * 0.01;
				inscatterFogHalo *= saturate(inscatterFogHaloFade * inscatterFogHaloFade);

				half3 inscatterFogHaloCol = mainLitCol * inscatterFogHalo * (1.2 - _FogStartDensity);
				

				// final

				half finalFogDensity = saturate(fogDensity + fogDensity * inscatterFog) * fogColLerp;

				half3 finalFogCol = lerp(fogDistCol.rgb, mainLitCol, inscatterFog * _MainLitStrength);
				
				finalFogCol *= lerp(1, (fogNoiseFade + 1) * 0.5, _FogNoiseStrength);

				//return sceneCol;// half4(finalFogCol, 1);

				half4 finalCol = half4(lerp(sceneCol.xyz, finalFogCol, finalFogDensity) + inscatterFogHaloCol, 1);

				return finalCol;
				

				//return VisualizePosition(input, worldPos.xyz);
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
