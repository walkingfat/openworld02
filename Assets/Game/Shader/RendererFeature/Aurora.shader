﻿Shader "WalkingFat/RendererFeather/Aurora"
{
	Properties
	{
		_NoiseTex("Noise Tex", 2D) = "" {}
		[HideInInspector]_MainTex("Main Tex", 2D) = "" {}
		_Strength("_Strength", Range(0.0, 1.0)) = 0.5
		_TopColor("Top Color", Color) = (1,0,0,1)
		_BottomColor("Bottom Color", Color) = (0,1,0,1)
		[HideInInspector]_AuroraOffset("Aurora Offset", Float) = 0.0
		_Decay("Decay", Range(0.0, 1.0)) = 0.5
		_Exposure("Exposure", Range(0.0, 5.0)) = 0.5
		_BlurRadius("Blur Radius", Range(1, 10)) = 4
		_ColorGradient("Color Gradient", Range(1.0, 3.0)) = 1.0
		[HideInInspector]_BlurParams("Blur Params", Vector) = (0,0,0,0) // x = tex width; y = tex height; y = null; z = null

	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "Draw Line"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 uv1 : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				//UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv1 : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float3 positionWS : TEXCOORD2;
				float4 positionNDC : TEXCOORD3;
			};

			CBUFFER_START(UnityPerMaterial)
				float _Strength;
			//float4 _MainTex_ST;
			CBUFFER_END

			TEXTURE2D(_NoiseTex);							SAMPLER(sampler_NoiseTex);
			//TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);


			// Global
			float4 _ScreenSunPos;
			float _AuroraOffset;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;
				output.positionWS = vertexInput.positionWS;
				output.positionNDC = vertexInput.positionNDC;

				output.uv1 = input.uv1;
				output.uv2 = input.uv2;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half screenBottomFade = input.positionNDC.y / input.positionNDC.w;
				screenBottomFade = min(1, screenBottomFade * 5 - 0.01);

				half dist = 1 - input.uv2.y;
				dist = dist * dist * dist;

				/*
				// draw 3 line
				half auroraDist = input.positionWS.z * 0.01;
				half ditherSpeed = _Time.z * 0.01;
				half auroraRandWidth = (sin(auroraDist * 13 + ditherSpeed) + sin(auroraDist * 17 - ditherSpeed)) * 0.1 + 0.8;
				half speed = _Time.y * 0.3;

				half auroraDistortion0 = sin(auroraDist * 0.5 + speed * 0.2) * 250;
				half auroraDistortion1 = sin(auroraDist * 1.5 + speed * 0.3) * 150;
				half auroraDistortion = (auroraDistortion0 + auroraDistortion1) * 0.5;
				half auroraLine = max(0, auroraRandWidth - abs(input.positionWS.x - _AuroraOffset + auroraDistortion) * 0.02);

				auroraDistortion0 = sin(auroraDist * 0.4 + speed * 0.23) * 300;
				auroraDistortion1 = sin(auroraDist * 1.6 + speed * 0.27) * 170;
				auroraDistortion = (auroraDistortion0 + auroraDistortion1) * 0.5;
				auroraLine += max(0, auroraRandWidth - abs(input.positionWS.x - _AuroraOffset + 200 + auroraDistortion) * 0.02);

				auroraDistortion0 = sin(auroraDist * 0.6 + speed * 0.18) * 270;
				auroraDistortion1 = sin(auroraDist * 1.4 + speed * 0.32) * 140;
				auroraDistortion = (auroraDistortion0 + auroraDistortion1) * 0.5;
				auroraLine += max(0, auroraRandWidth - abs(input.positionWS.x - _AuroraOffset - 200 + auroraDistortion) * 0.02);

				auroraLine = min(0.8, auroraLine);
				auroraLine = auroraLine * auroraLine * auroraLine * auroraLine;
				
				half screenBottomFade = input.positionNDC.y / input.positionNDC.w;
				screenBottomFade = min(1, screenBottomFade * 5 - 0.01);

				half dist = 1 - input.uv2.y;
				dist = dist * dist * dist;

				half4 finalCol = half4(auroraLine * screenBottomFade, dist, 0, 1);
				*/

				// draw by texture
				half2 noiseUV = input.uv1.xy + half2(_Time.y, _Time.y) * 0.01;
				half noise = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, noiseUV).z * 0.3 * (1 - dist);
				
				half2 uvOffset = half2(input.uv1.x * 2 - 0.5 + noise, input.uv1.y + _Time.y * 0.1 + noise);
				
				half4 auroraTex = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uvOffset);
				//return aurora0;
				half auroraLine = lerp(auroraTex.x, 0, abs(_Strength - 0.5) * 2) + lerp(0, auroraTex.y, (_Strength - 0.5) * 2);

				// final

				half4 finalCol = half4(auroraLine * screenBottomFade, dist, 0, 1);

				return finalCol;
			}
			ENDHLSL
		}

		Pass
		{
			Name "Draw Gradient"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 uv0 : TEXCOORD0;
				float4 uv1 : TEXCOORD1;
				float4 uv2 : TEXCOORD2;
				float4 uv3 : TEXCOORD3;
				float4 uv4 : TEXCOORD4;
				float4 uv5 : TEXCOORD5;
				float4 uv6 : TEXCOORD6;
				float4 uv7 : TEXCOORD7;
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _TopColor;
			float4 _BottomColor;
			float _Decay;
			float _Exposure;
			float _ColorGradient;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);

			// Global
			float4 _ScreenTopPos;
			float _AuroraHeight;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				float2 tempUV = input.texcoord.xy;
				float2 deltaUV = tempUV - float2(0.5, 4);// _ScreenTopPos.xy;
				deltaUV *= 0.0625 * _AuroraHeight;;

				output.uv0.xy = tempUV;
				tempUV += deltaUV;
				output.uv0.zw = tempUV;
				tempUV += deltaUV;
				output.uv1.xy = tempUV;
				tempUV += deltaUV;
				output.uv1.zw = tempUV;
				tempUV += deltaUV;
				output.uv2.xy = tempUV;
				tempUV += deltaUV;
				output.uv2.zw = tempUV;
				tempUV += deltaUV;
				output.uv3.xy = tempUV;
				tempUV += deltaUV;
				output.uv3.zw = tempUV;
				tempUV += deltaUV;
				output.uv4.xy = tempUV;
				tempUV += deltaUV;
				output.uv4.zw = tempUV;
				tempUV += deltaUV;
				output.uv5.xy = tempUV;
				tempUV += deltaUV;
				output.uv5.zw = tempUV;
				tempUV += deltaUV;
				output.uv6.xy = tempUV;
				tempUV += deltaUV;
				output.uv6.zw = tempUV;
				tempUV += deltaUV;
				output.uv7.xy = tempUV;
				tempUV += deltaUV;
				output.uv7.zw = tempUV;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half tempDecay = 1.0;
				half colorGradient = 0.0;
				half aurora = 0;
				half3 color = half3(0, 0, 0);
				half3 gradientStep = 0.0625 + _ColorGradient * 0.01;

				half4 tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv0.xy);
				half uvGradientY = tex.y;
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;


				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv0.zw);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv1.xy);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv1.zw);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv2.xy);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv2.zw);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv3.xy);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv3.zw);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv4.xy);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv4.zw);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv5.xy);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv5.zw);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv6.xy);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv6.zw);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv7.xy);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;
				colorGradient += gradientStep + tex.y * 0.1;
				tempDecay *= _Decay - tex.y * 0.5;

				tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv7.zw);
				aurora = tex.x * tempDecay;
				color += aurora * lerp(_BottomColor, _TopColor, colorGradient).rgb;


				color *= 0.0625;

				//return half4(color.a, 0,0,1);
				return half4(color * _Exposure, uvGradientY);
			}
			ENDHLSL
		}

		Pass
		{
			Name "Radial Blur"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 uv0 : TEXCOORD0;
				float4 uv1 : TEXCOORD1;
				float4 uv2 : TEXCOORD2;
				float4 uv3 : TEXCOORD3;
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _TopColor;
			float4 _BottomColor;
			float _Decay;
			float _Exposure;
			float _ColorGradient;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);

			// Global
			float4 _ScreenTopPos;
			float _AuroraHeight;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);

				float2 tempUV = input.texcoord.xy;
				float2 deltaUV = tempUV - float2(0.5, 4);// _ScreenTopPos.xy;
				float dist = distance(float2(0, 0), input.positionOS.xz) * 0.001;
				deltaUV *= 0.125 * _AuroraHeight - dist;

				output.uv0.xy = tempUV;
				tempUV += deltaUV;
				output.uv0.zw = tempUV;
				tempUV += deltaUV;
				output.uv1.xy = tempUV;
				tempUV += deltaUV;
				output.uv1.zw = tempUV;
				tempUV += deltaUV;
				output.uv2.xy = tempUV;
				tempUV += deltaUV;
				output.uv2.zw = tempUV;
				tempUV += deltaUV;
				output.uv3.xy = tempUV;
				tempUV += deltaUV;
				output.uv3.zw = tempUV;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half tempDecay = 1.0;
				half3 color = half3(0, 0, 0);
				color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv0.xy).rgb * tempDecay;
				tempDecay *= _Decay;

				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv0.zw).rgb * tempDecay;
				tempDecay *= _Decay;

				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv1.xy).rgb * tempDecay;
				tempDecay *= _Decay;

				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv1.zw).rgb * tempDecay;
				tempDecay *= _Decay;

				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv2.xy).rgb * tempDecay;
				tempDecay *= _Decay;

				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv2.zw).rgb * tempDecay;
				tempDecay *= _Decay;

				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv3.xy).rgb * tempDecay;
				tempDecay *= _Decay;

				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv3.zw).rgb * tempDecay;

				color *= 0.125;

				return half4(color * _Exposure, 1);
			}
			ENDHLSL
		}

		Pass
		{
			Name "Gaussian Blur"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
			float _BlurRadius;
			float4 _BlurParams;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);

			// Global
			float4 _ScreenSunPos;

			// Gaussian Blur
			float GetGaussianDistribution(float x, float y, float rho) {
				float g = 1.0f / sqrt(2.0f * 3.141592654f * rho * rho);
				return g * exp(-(x * x + y * y) / (2 * rho * rho));
			}

			float4 GetGaussBlurColor(float blurRadius, float texSize, float2 uv)
			{
				float space = texSize;
				float rho = (float)blurRadius * space / 3.0;

				float weightTotal = 0;
				for (int x = -blurRadius; x <= blurRadius; x++)
				{
					for (int y = -blurRadius; y <= blurRadius; y++)
					{
						weightTotal += GetGaussianDistribution(x * space, y * space, rho);
					}
				}

				float4 colorTmp = float4(0, 0, 0, 0);
				for (int x = -blurRadius; x <= blurRadius; x++)
				{
					for (int y = -blurRadius; y <= blurRadius; y++)
					{
						float weight = GetGaussianDistribution(x * space, y * space, rho) / weightTotal;

						float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv + float2(x * space, y * space));
						color = color * weight;
						colorTmp += color;
					}
				}
				return colorTmp;

			}

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = input.texcoord;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				return output;
			}


			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 finalCol = GetGaussBlurColor(_BlurRadius, _BlurParams.x, input.uv);

				return finalCol;
			}
			ENDHLSL
		}

	}
}
