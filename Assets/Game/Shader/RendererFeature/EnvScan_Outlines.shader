﻿Shader "WalkingFat/RendererFeather/EnvScan/Outlines"
{
	Properties
	{
		//[HideInInspector]_ScanParams("", Vector) = (0,0,0,0)
		[HideInInspector]_ScanDist("", Float) = 0
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_PivotRadius("Pivot Radius", Range(1.0, 100.0)) = 10.0
		_HeightOffset("Height Offset", Range(0.0, 2.0)) = 0.0
		_OutlineWidth("Outline Width", Range(0.0, 5.0)) = 1.0
		_DepthThreshold("Depth Threshold", Range(0.0, 10.0)) = 1.0
		_ScanLineInterval("Scan Line Interval", Range(1.0, 5.0)) = 4.0
		_ScanLineWidth("Scan Line Width", Range(1.0, 30.0)) = 4.0
		_ScanWaveWidth("Scan Wave Width", Range(1.0, 100.0)) = 50
		//_ScanWaveStrength("Scan Wave Strength", Range(0.0, 1.0)) = 0.5
		//_ScaneAfterimage("Scan Afterimage", Range(0.0, 1.0)) = 1
		_ScanPointInterval("Scan Point Interval", Range(1.0, 10.0)) = 4.0
		_ScanPointSize("Scan Point Size", Range(1.0, 10.0)) = 4.0
		//_ScanPointDist("Scan Point Distance", Range(50, 100)) = 50
	}
		SubShader
	{
		Tags { "RenderType" = "Overlay" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "CharRim"
			Tags { "LightMode" = "UniversalForward" }

			Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency 
			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			//--------------------------------------

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Assets/Game/Shader/Common/DitherTransparency.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 positionWS : TEXCOORD0;
				float4 positionNDC : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float _ScanDist;
			float4 _Color1;
			float4 _Color2;
			float _PivotRadius;
			float _HeightOffset;
			float _OutlineWidth;
			float _DepthThreshold;
			float _ScanWaveWidth;
			//float _ScanWaveStrength;
			//float _ScaneAfterimage;
			float _ScanPointInterval;
			float _ScanPointSize;
			//float _ScanPointDist;
			float4 _CameraDepthTexture_TexelSize;
			CBUFFER_END

			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);

			// global
			float4 _ScanParams;
			float4x4 _InverseView;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
				
				output.positionWS.xyz = TransformObjectToWorld(input.positionOS);

				// mask
				half toPivotDist = distance(_ScanParams.xyz, output.positionWS.xyz);
				half scanRimFade = saturate(max(0, _ScanDist - toPivotDist) * 0.04);

				half scanTime = _ScanParams.w;
				half scanDist = clamp(scanTime, 0, _ScanDist);

				half outlineField = saturate(_ScanWaveWidth * 5 / (scanDist - toPivotDist));

				half scanFade = saturate((_ScanDist * 2 - scanTime) / _ScanDist);

				half nearMask = saturate((toPivotDist - _PivotRadius) * 0.1);

				output.positionWS.w = outlineField * scanFade * scanRimFade * nearMask;
				
				// height offset
				output.positionWS.y += _HeightOffset * nearMask;

				output.positionCS = TransformWorldToHClip(output.positionWS.xyz);

				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				half normalMask = dot(normalInput.normalWS, half3(0, 1, 0));
				normalMask = min(1, normalMask * 3);

				float4 ndc = output.positionCS * 0.5f;
				output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				output.positionNDC.zw = output.positionCS.zw;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// sample depth ================================================
				half2 uv = input.positionNDC.xy / input.positionNDC.w;

				half2 topRightUV = uv + float2(_CameraDepthTexture_TexelSize.x, _CameraDepthTexture_TexelSize.y) * _OutlineWidth;
				half2 topLeftUV = uv + float2(-_CameraDepthTexture_TexelSize.x * _OutlineWidth, _CameraDepthTexture_TexelSize.y * _OutlineWidth);

				half depthBase = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv), _ZBufferParams);
				half depth0 = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, topRightUV), _ZBufferParams);
				half depth1 = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, topLeftUV), _ZBufferParams);

				// world position from depth
				float3 posWS = mul(_InverseView, float4(uv, depthBase, 1)).xyz;
				return frac(distance(_ScanParams.xyz, posWS));

				// outline =================================================
				half depthThreshold = _DepthThreshold * depthBase;

				half depthFiniteDifference1 = max(0, depth0 - depthBase);
				half depthFiniteDifference3 = max(0, depth1 - depthBase);

				half scanOutline = (depthFiniteDifference1 + depthFiniteDifference3) * 100;
				scanOutline = scanOutline > depthThreshold ? 1 : 0;

				// final scan color
				half finalScanValue = scanOutline * input.positionWS.w;

				half4 finalCol = lerp(_Color2, _Color1, finalScanValue);
				finalCol.a = finalScanValue;

				return scanOutline;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
