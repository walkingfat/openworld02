﻿Shader "WalkingFat/RendererFeather/RainSplash"
{
	Properties
	{
		_NoiseMap("Noise Map", 2D) = "white" {}
		_NoiseScale("Noise Scale", Range(1.0, 10.0)) = 6.0
		_FocusDist("Focus Distance", Range(5.0, 20.0)) = 10.0
		_FocusRate("Focus Rate", Range(0.0, 0.2)) = 0.05
		_SlpashDist("Slpash Distance", Range(0.0, 0.05)) = 0.01
		_EqualWidthRate("Equal Width Rate", Range(0.0, 1.0)) = 0.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300

		Pass{
			Name "ReceiveRain"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			Cull Front

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex PassVertex
			#pragma fragment PassFragment


			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float3 normalWS : TEXCOORD0;
				float3 viewDirWS : TEXCOORD1;
				float3 positionWS : TEXCOORD2;
				float4 positionNDC : TEXCOORD3;
				float4 positionCS : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			half _SlpashDist;
			half _FocusRate;
			half _FocusDist;
			half _NoiseScale;
			half _EqualWidthRate;
			CBUFFER_END

			half _RainRate;
			half _CameraTan;

			TEXTURE2D(_NoiseMap);							SAMPLER(sampler_NoiseMap);

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				input.positionOS.xyz += _SlpashDist * input.normalOS;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				output.normalWS = normalize(normalInput.normalWS);
				output.viewDirWS = normalize(GetCameraPositionWS() - vertexInput.positionWS);
				
				output.positionWS = TransformObjectToWorld(input.positionOS.xyz);

				half distWS = distance(output.positionWS, GetCameraPositionWS());

				output.positionWS.xyz += _SlpashDist * output.normalWS * distWS * _CameraTan * _EqualWidthRate;

				output.positionCS = TransformWorldToHClip(output.positionWS);
				output.positionNDC = ComputeScreenPos(output.positionCS);

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half screenRatio = _ScreenParams.x / _ScreenParams.y;
				half2 uv = half2(input.positionNDC.x * screenRatio, input.positionNDC.y) / input.positionNDC.w * _NoiseScale;

				uv = half2(uv.x + ceil((_Time.w * 6) % 2), uv.y + ceil((_Time.w * 3) % 2)) * 0.5;

				half noise = SAMPLE_TEXTURE2D(_NoiseMap, sampler_NoiseMap, uv).r;

				half fresnel = 1 - dot(input.normalWS, -input.viewDirWS);
				fresnel = fresnel * fresnel;

				half topFace = dot(input.normalWS, half3(0, 1, 0));

				half dist = distance(input.positionWS, GetCameraPositionWS());
				half distFade = 1 - saturate(abs(dist - _FocusDist) * _FocusRate);

				clip(fresnel * topFace * noise * distFade * _RainRate - 0.1);

				return unity_FogColor;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
