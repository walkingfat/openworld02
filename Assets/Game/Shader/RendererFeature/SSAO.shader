﻿Shader "WalkingFat/RendererFeather/SSAO"
{
	Properties
	{
		[HideInInspector]_MainTex("Main Tex", 2D) = "" {}
		_Strength("_Strength", Range(0.0, 1.0)) = 0.5

	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "SSAO"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 uv : TEXCOORD0;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				//UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 positionWS : TEXCOORD1;
				float4 positionNDC : TEXCOORD2;
			};

			CBUFFER_START(UnityPerMaterial)
				float _Strength;
			CBUFFER_END
			
			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);


			// Global

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;
				output.positionWS = vertexInput.positionWS;
				output.positionNDC = vertexInput.positionNDC;

				output.uv = input.uv;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				return 1;
			}
			ENDHLSL
		}

	}
}
