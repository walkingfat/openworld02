﻿Shader "WalkingFat/RendererFeather/CapsuleShadow"
{
	Properties
	{
		[HideInInspector] _MainTex("Main Tex", 2D) = "White" {}
		_ShadowSoftness("Shadow Softness", Range(80, 1000)) = 0.5
		_ShadowIntenstiy("Shadow Intensity", Range(0.99, 0.999)) = 0.99
		_FadeDist("Fade Distance", Range(1.0, 10.0)) = 5.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "CapsuleShadow"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "SRPCore.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_Position;
				float4 uv : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
				float _ShadowSoftness;
				float _ShadowIntenstiy;
				float _FadeDist;
			CBUFFER_END

			//float4x4 _ViewToWorldMaterx;

			//TEXTURE2D(_ColorTex);							SAMPLER(sampler_ColorTex);
			TEXTURE2D(_CameraDepthTexture);							SAMPLER(sampler_CameraDepthTexture);

			// global
			float4 _CapsuleData[10];
			float4x4 _CameraInvProjection;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);

				float4 projPos = output.positionCS * 0.5;
				projPos.xy = projPos.xy + projPos.w;

				output.uv.xy = input.texcoord;
				output.uv.zw = projPos.xy;
			
				return output;
			}

			inline void GetCrossPointOf2Line(half3 a, half3 b, half3 c, half3 d, out half3 p0, out half3 p1)
			{
				half3 r = b - a;
				half3 s = d - c;
				half3 q = a - c;

				half dotqr = dot(q, r);
				half dotqs = dot(q, s);
				half dotrs = dot(r, s);
				half dotrr = dot(r, r);
				half dotss = dot(s, s);

				half denom = dotrr * dotss - dotrs * dotrs;
				half numer = dotqs * dotrs - dotqr * dotss;

				half t = numer / denom;
				half u = (dotqs + t * dotrs) / dotss;

				// The two points of intersection
				p0 = a + t * r;
				p1 = c + u * s;

				// Is the intersection occuring along both line segments and does it intersect
				//onSegment = false;
				//intersects = false;
				//if (0 <= t && t <= 1 && 0 <= u && u <= 1) onSegment = true;
				//if ((p0 - p1).magnitude <= marginOfError) intersects = true;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				//return _CapsuleData[3].w;

				half3 mainLitCol = GetMainLight().color;
				//half4 sceneCol = SAMPLE_TEXTURE2D(_ColorTex, sampler_ColorTex, input.uv.xy);
				//return sceneCol;
				float depth = SAMPLE_TEXTURE2D_X(_CameraDepthTexture, sampler_CameraDepthTexture, input.uv).r;

				half3 worldPos = GetPosWSFromDepth(input.uv.zw, depth);

				//half switchTime = step(_Time.y % 2, 1);

				half3 litPos = GetMainLight().direction * 99999;
				half capsuleShadow = 1;
				
				for (int n = 0; n < 5; n++)
				{
					int id0 = n * 2;
					int id1 = id0 + 1;
					half3 lp = litPos - worldPos; // pos to light dir
					half3 nlp = normalize(lp);
					half3 ab = _CapsuleData[id0].xyz - _CapsuleData[id1].xyz; // capsule dir
					half3 ap = _CapsuleData[id0].xyz - worldPos; // pos to capsule dir
					half3 N = normalize(cross(ab, lp)); // lightDir to capsuleDir
					half d = abs(dot(N, ap)); // distance between lightDir and capsuleDir

					half3 cp0 = half3(0, 0, 0); // cross point on capsule
					half3 cp1 = half3(0, 0, 0); // cross point on lit direction
					GetCrossPointOf2Line(_CapsuleData[id0].xyz, _CapsuleData[id1].xyz, worldPos, litPos, cp0, cp1); // caculate 2 cross point

					//cp0 = clamp(cp0, _CapsuleData[id0].xyz, _CapsuleData[id1].xyz);
					half length = distance(_CapsuleData[id0].xyz, _CapsuleData[id1].xyz);
					half t = saturate(distance(cp0, _CapsuleData[id0].xyz) / length); // distance ratio of capsule
					half radius = lerp(_CapsuleData[id0].w, _CapsuleData[id1].w, t); // caculate current radius of capsule
					half distFade = 1 - min(1, distance(worldPos, cp0) / _FadeDist); // fade distance from capsule to ground
					//radius = lerp(0, radius, distFade);

					cp0 = lerp(_CapsuleData[id0].xyz, _CapsuleData[id1].xyz, t); // make sure cross point in the capsule line
					cp1 = nlp * dot(cp0 - worldPos, nlp) + worldPos; // get real shortest cross point of light dir
					half cpd = distance(cp0, cp1); // distance between two cross points
					half3 cp = cp0 + (cp1 - cp0) * min(radius/cpd, 1); // get cross point on the capsule surface
					
					half s = dot(normalize(cp - worldPos), normalize(lp)); // minimum rate between light dir and direction from posWS to capsule surface

					s = (max(0, s) - _ShadowIntenstiy) * _ShadowSoftness * distFade; // fixed shadow shape
					
					//capsuleShadow *= saturate(1 - s);
					capsuleShadow = min(capsuleShadow, saturate(1 - s)); // blend with last shadow value
				}
				//return VisualizePosition(worldPos);
				return capsuleShadow;
			}
			ENDHLSL
		}

		Pass
		{
			Name "Blend Scene"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
			float _Alpha;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);
			TEXTURE2D(_ColorTex);							SAMPLER(sampler_ColorTex);

			// global


			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				output.uv = input.texcoord.xy;

				return output;
			}


			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 sceneCol = SAMPLE_TEXTURE2D(_ColorTex, sampler_ColorTex, input.uv);
				half capsuleShadow = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv).x;

				half4 finalCol = half4(sceneCol.xyz * capsuleShadow, 1);

				return finalCol;
			}
			ENDHLSL
		}
	}
}
