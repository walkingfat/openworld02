﻿Shader "WalkingFat/RendererFeather/EnvScan/PointMarks"
{
	Properties
	{
		[HideInInspector]_PointMarkParams("", Vector) = (0,0,0,0)
		[HideInInspector]_PointMarkDist("", Float) = 0
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_PivotRadius("Pivot Radius", Range(1.0, 20.0)) = 10.0
		_HeightOffset("Height Offset", Range(0.0, 2.0)) = 0.0
		_ScanSpeed("Scan Speed", Range(1.0, 10.0)) = 5.0
		_ScaneWaveWidth("Scan Wave Width", Range(1.0, 100.0)) = 50
		_ScanPointInterval("Scan Point Interval", Range(1.0, 10.0)) = 4.0
		_ScanPointSize("Scan Point Size", Range(1.0, 10.0)) = 4.0
		_PointMarkDist("Point Matrix Distance", Range(10, 60)) = 30
	}
		SubShader
	{
		Tags { "RenderType" = "Overlay" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100
		Cull Off

		Pass
		{
			Name "CharRim"
			Tags { "LightMode" = "UniversalForward" }

			Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency 
			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			//--------------------------------------

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Assets/Game/Shader/Common/DitherTransparency.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 positionWS : TEXCOORD0;
				float4 positionNDC : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float _PointMarkDist;
			float4 _Color1;
			float4 _Color2;
			float _PivotRadius;
			float _HeightOffset;
			float _ScanSpeed;
			float _ScaneWaveWidth;
			//float _ScaneAfterimage;
			float _ScanPointInterval;
			float _ScanPointSize;
			float4 _CameraDepthTexture_TexelSize;
			CBUFFER_END

			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);

			// global
			float4 _PointMarkParams;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.positionWS.xyz = TransformObjectToWorld(input.positionOS);

				// mask
				half toPivotDist = distance(_PointMarkParams.xyz, output.positionWS.xyz);
				half scanRimFade = saturate(max(0, _PointMarkDist - toPivotDist) * 0.1);

				half scanTime = _PointMarkParams.w;
				half scanDist = clamp(scanTime, 0, _PointMarkDist);

				half pointField = saturate(scanDist - toPivotDist);

				half scanFade = saturate((_PointMarkDist * 6 - scanTime) / _PointMarkDist);

				half nearMask = saturate((toPivotDist - _PivotRadius) * 0.1);

				output.positionWS.w = pointField * scanFade * scanRimFade * nearMask;

				// height offset
				output.positionWS.y += _HeightOffset * nearMask;

				output.positionCS = TransformWorldToHClip(output.positionWS.xyz);

				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				half normalMask = dot(normalInput.normalWS, half3(0, 1, 0));
				normalMask = min(1, normalMask * 3);

				float4 ndc = output.positionCS * 0.5f;
				output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				output.positionNDC.zw = output.positionCS.zw;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				//return input.positionWS.w;

				half toPointDist = distance(_PointMarkParams.xyz, input.positionWS.xyz);

				// scan mark
				half pointMatrixValue = abs(sin(input.positionWS.x * _ScanPointInterval) * sin(input.positionWS.z * _ScanPointInterval));
				half pointThreshold = 1 - toPointDist * 0.0001 * _ScanPointSize;
				half pointMatrix = pointMatrixValue > pointThreshold ? 1 : 0;

				// final scan color
				half scanValue = pointMatrix * input.positionWS.w;

				half4 finalCol = lerp(_Color2, _Color1, scanValue);

				finalCol.a = scanValue;

				return finalCol;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
