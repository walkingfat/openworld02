﻿Shader "WalkingFat/RendererFeather/SunShafts"
{
	Properties
	{
		[HideInInspector]_MainTex("Main Tex", 2D) = "" {}
		//_ColorTex("Color Tex", 2D) = "" {}
		//_DepthTex("Depth Tex", 2D) = "" {}
		_Density("Density", Range(0.0, 1.0)) = 0.5
		_Decay("Decay", Range(0.0, 1.0)) = 0.5
		_Exposure("Exposure", Range(0.0, 2.0)) = 0.5

		_Alpha("_Alpha", Range(0.0, 1.0)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "Setup"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
			//float4 _MainTex_ST;
			CBUFFER_END

			TEXTURE2D_FLOAT(_DepthTex);							SAMPLER(sampler_DepthTex);
			//TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);


			// Global
			float4 _ScreenSunPos;


			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				output.uv = input.texcoord.xy;

				return output;
			}


			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half depth = SAMPLE_TEXTURE2D_X(_DepthTex, sampler_DepthTex, input.uv).r;

#if UNITY_REVERSED_Z
				depth = 1.0 - depth;
#endif
				//depth = 2.0 * depth - 1.0;

				half sunHalo = (1 - min(1, distance(input.uv.xy, _ScreenSunPos.xy) * _ScreenSunPos.z)) * _ScreenSunPos.z;
				//sunHalo = sunHalo * sunHalo * sunHalo;

				half mask = (saturate((1 - depth) * 100)) * _ScreenSunPos.z;

				half4 finalCol = half4(mask, sunHalo, 0, 1);
				
				return finalCol;
			}
			ENDHLSL
		}

		Pass
		{
			Name "Radial Blur"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off
			Cull Off
			ZTest Always

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 uv0 : TEXCOORD0;
				float4 uv1 : TEXCOORD1;
				float4 uv2 : TEXCOORD2;
				float4 uv3 : TEXCOORD3;
			};

			CBUFFER_START(UnityPerMaterial)
			float _Density;
			float _Decay;
			float _Exposure;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);
			//TEXTURE2D(_DepthTex);							SAMPLER(sampler_DepthTex);

			// Global
			float4 _ScreenSunPos;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);

				float2 tempUV = input.texcoord.xy;
				float2 deltaUV = tempUV - _ScreenSunPos.xy;
				deltaUV *= 0.125 * _Density;

				output.uv0.xy = tempUV;
				tempUV -= deltaUV;
				output.uv0.zw = tempUV;
				tempUV -= deltaUV;
				output.uv1.xy = tempUV;
				tempUV -= deltaUV;
				output.uv1.zw = tempUV;
				tempUV -= deltaUV;
				output.uv2.xy = tempUV;
				tempUV -= deltaUV;
				output.uv2.zw = tempUV;
				tempUV -= deltaUV;
				output.uv3.xy = tempUV;
				tempUV -= deltaUV;
				output.uv3.zw = tempUV;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half tempDecay = 1.0f;

				half4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv0.xy) * tempDecay;
				tempDecay *= _Decay;
				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv0.zw) * tempDecay;
				tempDecay *= _Decay;
				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv1.xy) * tempDecay;
				tempDecay *= _Decay;
				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv1.zw) * tempDecay;
				tempDecay *= _Decay;
				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv2.xy) * tempDecay;
				tempDecay *= _Decay;
				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv2.zw) * tempDecay;
				tempDecay *= _Decay;
				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv3.xy) * tempDecay;
				tempDecay *= _Decay;
				color += SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv3.zw) * tempDecay;

				color *= 0.125;

				//return half4(color.a, 0,0,1);
				return half4(color.rgb * _Exposure, color.a);
			}
			ENDHLSL
		}

		Pass
		{
			Name "Blend Scene"
			//Tags { "LightMode" = "UniversalForward" }

			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			CBUFFER_START(UnityPerMaterial)
			float _Alpha;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);
			TEXTURE2D(_ColorTex);							SAMPLER(sampler_ColorTex);

			// global
			float _DayNightTime;


			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				output.positionCS = vertexInput.positionCS;

				output.uv = input.texcoord.xy;

				return output;
			}


			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 sceneCol = SAMPLE_TEXTURE2D(_ColorTex, sampler_ColorTex, input.uv);
				half4 sunShaftsTex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv);


				//return sunShaftsTex.x;
				half3 sunShaftsCol = max(0, sunShaftsTex.y - sunShaftsTex.x) * GetMainLight().color;

				half nightSightFade = _DayNightTime * 0.9 + 0.1;
				sunShaftsCol *= nightSightFade;

				half4 finalCol = half4(sceneCol.rgb + sunShaftsCol * _Alpha, 1);
				
				return finalCol;
			}
			ENDHLSL
		}
	}
}
