﻿#ifndef WALKINGFAT_SRP_CORE_INCLUDED
#define WALKINGFAT_SRP_CORE_INCLUDED

//#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

inline void GetPosFromDepth(in float2 uv, in float depth, out float3 posVS, out float3 posWS)
{
#if UNITY_REVERSED_Z
	depth = 1.0 - depth;
#endif
	depth = 2.0 * depth - 1.0;

	posVS = ComputeViewSpacePosition(uv, depth, unity_CameraInvProjection);
	posWS = mul(unity_CameraToWorld, half4(posVS, 1.0)).xyz;
	//return posWS;
}

inline float3 GetPosWSFromDepth(float2 uv, float depth)
{
#if UNITY_REVERSED_Z
	depth = 1.0 - depth;
#endif
	depth = 2.0 * depth - 1.0;

	float3 posVS = ComputeViewSpacePosition(uv, depth, unity_CameraInvProjection);
	float3 posWS = mul(unity_CameraToWorld, half4(posVS, 1.0)).xyz;
	return posWS;
}

inline half4 VisualizePosition(float3 posWS)
{
	const float grid = 1;
	const float width = 3;

	posWS *= grid;

	// Detect borders with using derivatives.
	float3 fw = fwidth(posWS);
	half3 bc = saturate(width - abs(1 - 2 * frac(posWS)) / fw);

	// Frequency filter
	half3 f1 = smoothstep(1 / grid, 2 / grid, fw);
	half3 f2 = smoothstep(2 / grid, 4 / grid, fw);
	bc = lerp(lerp(bc, 0.5, f1), 0, f2);

	half4 c = half4(lerp(half3(0.5, 0.5, 0.5), bc, 0.8), 1);

	return c;
}

inline float3x3 GetTangentBasis(float3 TangentZ)
{
	float3 UpVector = abs(TangentZ.z) < 0.999 ? float3(0, 0, 1) : float3(1, 0, 0);
	float3 TangentX = normalize(cross(UpVector, TangentZ));
	float3 TangentY = cross(TangentZ, TangentX);
	return float3x3(TangentX, TangentY, TangentZ);
}

inline float3x3 GetTangentSpaceMatrix(float3 normal)
{
	//float3 tangent;

	float3 c1 = cross(normal, float3(0, 0, 1));
	float3 c2 = cross(normal, float3(0, 1, 0));

	// if(len(c1) > len(c2))
	// {
	// 	tangent = c1;
	// }
	// else
	// {
	// 	tangent = c2;
	// }

	float3 tangent = length(c1) > length(c2) ? c1 : c2;

	tangent = normalize(tangent);

	float3 bitangent = normalize(cross(tangent, normal));

	return float3x3(tangent, bitangent, normal);
}

inline float2 UniformSampleDiskConcentric(float2 E)
{
	float2 p = 2 * E - 1;
	float Radius;
	float Phi;
	if (abs(p.x) > abs(p.y))
	{
		Radius = p.x;
		Phi = (PI / 4) * (p.y / p.x);
	}
	else
	{
		Radius = p.y;
		Phi = (PI / 2) - (PI / 4) * (p.x / p.y);
	}
	return float2(Radius * cos(Phi), Radius * sin(Phi));
}

float CrossBilateralWeight(float BLUR_RADIUS, float r, float Depth, float originDepth)
{
	const float BlurSigma = BLUR_RADIUS * 0.5;
	const float BlurFalloff = 1.0 / (2.0 * BlurSigma * BlurSigma);

	float dz = (originDepth - Depth) * _ProjectionParams.z * 0.25;
	return exp2(-r * r * BlurFalloff - dz * dz);
}

void ProcessSample(float4 AO_RO_Depth, float BLUR_RADIUS, float r, float originDepth, inout float3 totalAO_RO, inout float totalWeight)
{
	float weight = CrossBilateralWeight(BLUR_RADIUS, r, originDepth, AO_RO_Depth.w);
	totalWeight += weight;
	totalAO_RO += weight * AO_RO_Depth.xyz;
}

void ProcessRadius(texture2D _SourceTexture, sampler _SourceSampler, float2 uv0, float2 deltaUV, 
	float BLUR_RADIUS, float originDepth, inout float3 totalAO_RO, inout float totalWeight)
{
	float r = 1.0;
	float z = 0.0;
	float2 uv = 0.0;
	float3 AO_RO = 0.0;

	UNITY_UNROLL
	for (; r <= BLUR_RADIUS * 0.5; r += 1.0) {
		uv = uv0 + r * deltaUV;
		float4 SourceColor = SAMPLE_TEXTURE2D_X(_SourceTexture, _SourceSampler, uv);
		z = SourceColor.w;
		AO_RO = SourceColor.xyz;
		ProcessSample(float4(AO_RO, z), BLUR_RADIUS, r, originDepth, totalAO_RO, totalWeight);
	}

	UNITY_UNROLL
	for (; r <= BLUR_RADIUS; r += 2.0) {
		uv = uv0 + (r + 0.5) * deltaUV;
		float4 SourceColor = SAMPLE_TEXTURE2D_X(_SourceTexture, _SourceSampler, uv);
		z = SourceColor.w;
		AO_RO = SourceColor.xyz;
		ProcessSample(float4(AO_RO, z), BLUR_RADIUS, r, originDepth, totalAO_RO, totalWeight);
	}

}

inline float4 BilateralBlur(float BLUR_RADIUS, float2 uv, float2 deltaUV, texture2D _SourceTexture, sampler _SourceSampler)
{
	float totalWeight = 1.0;
	float4 SourceColor = SAMPLE_TEXTURE2D_X(_SourceTexture, _SourceSampler, uv);
	float Depth = SourceColor.w;
	float3 totalAOR = SourceColor.xyz;

	ProcessRadius(_SourceTexture, _SourceSampler, uv, -deltaUV, BLUR_RADIUS, Depth, totalAOR, totalWeight);
	ProcessRadius(_SourceTexture, _SourceSampler, uv, deltaUV, BLUR_RADIUS, Depth, totalAOR, totalWeight);

	totalAOR /= totalWeight;
	return float4(totalAOR, Depth);
}

inline float GetGaussianDistribution(float x, float y, float rho) {
	float g = 1.0f / sqrt(2.0f * 3.141592654f * rho * rho);
	return g * exp(-(x * x + y * y) / (2 * rho * rho));
}
inline float4 GetGaussBlurColor(texture2D SourceTex, sampler texSampler, float2 uv, float2 texSize, float blurRadius)
{
	float2 space = 1.0 / texSize;
	float2 rho = (float)blurRadius * space / 3.0;
	float weightTotal = 0;
	for (int x = -blurRadius; x <= blurRadius; x++)
	{
		for (int y = -blurRadius; y <= blurRadius; y++)
		{
			weightTotal += GetGaussianDistribution(x * space.x, y * space.x, rho.x);
		}
	}
	float4 colorTmp = float4(0, 0, 0, 0);
	for (int x = -blurRadius; x <= blurRadius; x++)
	{
		for (int y = -blurRadius; y <= blurRadius; y++)
		{
			float weight = GetGaussianDistribution(x * space.y, y * space.y, rho.y) / weightTotal;

			float4 color = SAMPLE_TEXTURE2D_X(SourceTex, texSampler, uv + float2(x * space.y, y * space.y));
			color = color * weight;
			colorTmp += color;
		}
	}
	return colorTmp;
}

/////////////////BicubicSampler
inline half Luma4(half3 Color)
{
    return (Color.g * 2) + (Color.r + Color.b);
}

inline half HdrWeight4(half3 Color, half Exposure)
{
    return rcp(Luma4(Color) * Exposure + 4);
}

void Bicubic2DCatmullRom(in float2 UV, in float4 Size, out float2 Sample[3], out float2 Weight[3])
{
    UV *= Size.xy;

    float2 tc = floor(UV - 0.5) + 0.5;
    float2 f = UV - tc;
    float2 f2 = f * f;
    float2 f3 = f2 * f;

    float2 w0 = f2 - 0.5 * (f3 + f);
    float2 w1 = 1.5 * f3 - 2.5 * f2 + 1;
    float2 w3 = 0.5 * (f3 - f2);
    float2 w2 = 1 - w0 - w1 - w3;

    Weight[0] = w0;
    Weight[1] = w1 + w2;
    Weight[2] = w3;

    Sample[0] = tc - 1;
    Sample[1] = tc + w2 / Weight[1];
    Sample[2] = tc + 2;

    Sample[0] *= Size.zw;
    Sample[1] *= Size.zw;
    Sample[2] *= Size.zw;
}

#define BICUBIC_CATMULL_ROM_SAMPLES 5

struct FCatmullRomSamples
{
    uint Count;// Constant number of samples (BICUBIC_CATMULL_ROM_SAMPLES)

    int2 UVDir[BICUBIC_CATMULL_ROM_SAMPLES];// Constant sign of the UV direction from master UV sampling location.

    float2 UV[BICUBIC_CATMULL_ROM_SAMPLES];// Bilinear sampling UV coordinates of the samples

    float Weight[BICUBIC_CATMULL_ROM_SAMPLES];// Weights of the samples

    float FinalMultiplier; // Final multiplier (it is faster to multiply 3 RGB values than reweights the 5 weights)
};

FCatmullRomSamples GetBicubic2DCatmullRomSamples(float2 UV, float4 screenRes)
{
    FCatmullRomSamples Samples;
    Samples.Count = BICUBIC_CATMULL_ROM_SAMPLES;

    float2 Weight[3];
    float2 Sample[3];
    Bicubic2DCatmullRom(UV, screenRes, Sample, Weight);

    // Optimized by removing corner samples
    Samples.UV[0] = float2(Sample[1].x, Sample[0].y);
    Samples.UV[1] = float2(Sample[0].x, Sample[1].y);
    Samples.UV[2] = float2(Sample[1].x, Sample[1].y);
    Samples.UV[3] = float2(Sample[2].x, Sample[1].y);
    Samples.UV[4] = float2(Sample[1].x, Sample[2].y);

    Samples.Weight[0] = Weight[1].x * Weight[0].y;
    Samples.Weight[1] = Weight[0].x * Weight[1].y;
    Samples.Weight[2] = Weight[1].x * Weight[1].y;
    Samples.Weight[3] = Weight[2].x * Weight[1].y;
    Samples.Weight[4] = Weight[1].x * Weight[2].y;

    Samples.UVDir[0] = int2(0, -1);
    Samples.UVDir[1] = int2(-1, 0);
    Samples.UVDir[2] = int2(0, 0);
    Samples.UVDir[3] = int2(1, 0);
    Samples.UVDir[4] = int2(0, 1);

    // Reweight after removing the corners
    float CornerWeights;
    CornerWeights = Samples.Weight[0];
    CornerWeights += Samples.Weight[1];
    CornerWeights += Samples.Weight[2];
    CornerWeights += Samples.Weight[3];
    CornerWeights += Samples.Weight[4];
    Samples.FinalMultiplier = 1 / CornerWeights;

    return Samples;
}

inline half4 Texture2DSampleBicubic(texture2D SourceTex, sampler texSampler, half2 UV, half4 screenRes)
{
	FCatmullRomSamples Samples = GetBicubic2DCatmullRomSamples(UV, screenRes);

	half4 OutColor = 0;
	for (uint i = 0; i < Samples.Count; i++)
	{
		OutColor += SAMPLE_TEXTURE2D_LOD(SourceTex, texSampler, Samples.UV[i], 0) * Samples.Weight[i];
	}
	OutColor *= Samples.FinalMultiplier;

	return OutColor;
}

inline void ResolverAABB(texture2D SourceTex, sampler texSampler, half Sharpness, half ExposureScale, half AABBScale, half2 uv, half4 screenRes, inout half Variance, inout half4 MinColor, inout half4 MaxColor, inout half4 FilterColor)
{
	const int2 SampleOffset[9] = { int2(-1.0, -1.0), int2(0.0, -1.0), int2(1.0, -1.0), int2(-1.0, 0.0), int2(0.0, 0.0), int2(1.0, 0.0), int2(-1.0, 1.0), int2(0.0, 1.0), int2(1.0, 1.0) };
	half4 SampleColors[9];

	for (uint i = 0; i < 9; i++) {
//#if AA_BicubicFilter
		//SampleColors[i] = Texture2DSampleBicubic(SourceTex, texSampler, uv + (SampleOffset[i] * screenRes.zw), screenRes);
//#else
		SampleColors[i] = SAMPLE_TEXTURE2D(SourceTex, texSampler, uv + (SampleOffset[i] * screenRes.zw));
//#endif
	}

	
//#if AA_Filter
	half SampleWeights[9];
	for (uint j = 0; j < 9; j++) {
		SampleWeights[j] = HdrWeight4(SampleColors[j].rgb, ExposureScale);
	}

	half TotalWeight = 0;
	for (uint k = 0; k < 9; k++) {
		TotalWeight += SampleWeights[k];
	}
	SampleColors[4] = (
		SampleColors[0] * SampleWeights[0]
		+ SampleColors[1] * SampleWeights[1]
		+ SampleColors[2] * SampleWeights[2]
		+ SampleColors[3] * SampleWeights[3]
		+ SampleColors[4] * SampleWeights[4]
		+ SampleColors[5] * SampleWeights[5]
		+ SampleColors[6] * SampleWeights[6]
		+ SampleColors[7] * SampleWeights[7]
		+ SampleColors[8] * SampleWeights[8]
		) / TotalWeight;
//#endif
	

	half4 m1 = 0.0; half4 m2 = 0.0;
	for (uint x = 0; x < 9; x++) {
		m1 += SampleColors[x];
		m2 += SampleColors[x] * SampleColors[x];
	}

	half4 mean = m1 / 9.0;
	half4 stddev = sqrt((m2 / 9.0) - mean * mean);

	MinColor = mean - AABBScale * stddev;
	MaxColor = mean + AABBScale * stddev;

	FilterColor = SampleColors[4];
	MinColor = min(MinColor, FilterColor);
	MaxColor = max(MaxColor, FilterColor);

	half4 TotalVariance = 0;
	for (uint z = 0; z < 9; z++) {
		half4 l = Luminance(SampleColors[z]) - Luminance(mean);
		TotalVariance += l * l;
	}
	Variance = saturate((TotalVariance / 9) * 256);
	Variance *= FilterColor.a;
}

float GetMarchSize(float2 start, float2 end, float2 SamplerPos, float dither)
{
	float2 dir = abs(end - start);
	return length(float2(min(dir.x, SamplerPos.x), min(dir.y, SamplerPos.y))) + dither;// * step(_Time.y % 2, 1);
}

float4 HiZ_Tracing(int traceMaxLevel, int traceStartLevel, int traceEndLevel, int traceSteps, float thickness,
	float2 traceRes, float3 rayStart, float3 rayDir, float marchDither, Texture2D depthTex, SamplerState depthSampler)
{
	float samplerSize = GetMarchSize(rayStart.xy, rayStart.xy + rayDir.xy, traceRes, 0);

	float3 samplePos = rayStart + rayDir * samplerSize;
	int level = traceStartLevel;
	float mask = 0.0;

	UNITY_LOOP
		for (int i = 0; i < traceSteps; i++)
		{
			float2 cellCount = traceRes * exp2(level + 1.0);
			float newSamplerSize = GetMarchSize(samplePos.xy, samplePos.xy + rayDir.xy, cellCount, marchDither);
			float3 newSamplePos = samplePos + rayDir * newSamplerSize;
			float sampleMinDepth = SAMPLE_TEXTURE2D_LOD(depthTex, depthSampler, newSamplePos.xy, level);

#if UNITY_REVERSED_Z
			UNITY_FLATTEN
				if (sampleMinDepth < newSamplePos.z) {
					level = min(traceMaxLevel, level + 1.0);
					samplePos = newSamplePos;
				}
				else {
					level--;
				}
#else
			UNITY_FLATTEN
				if (sampleMinDepth > newSamplePos.z) {
					level = min(traceMaxLevel, level + 1.0);
					samplePos = newSamplePos;
				}
				else {
					level--;
				}
#endif

			UNITY_BRANCH
				if (level < traceEndLevel) {
					float delta = (-LinearEyeDepth(sampleMinDepth, _ZBufferParams)) - (-LinearEyeDepth(samplePos.z, _ZBufferParams));
					mask = delta <= thickness && i > 0.0;
					return float4(samplePos, mask);
				}
		}

	return float4(samplePos, mask);
}

#endif