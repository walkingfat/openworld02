﻿Shader "WalkingFat/RendererFeather/CapsuleShadowEditor"
{
	Properties
	{
		[HideInInspector]_RadiusA("Radius A", Float) = 1
		[HideInInspector]_RadiusB("Radius B", Float) = 1
		[HideInInspector]_Length("Length", Float) = 1
		[HideInInspector]w_Color("Color", COLOR) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "CapsuleShadow"
			Tags { "LightMode" = "UniversalForward" }

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 color : COLOR;
				float3 normalOS : NORMAL;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_Position;
				float3 normalWS : TEXCOORD0;
				float4 color : TEXCOORD1;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _Color;
			float _RadiusA;
			float _RadiusB;
			float _Length;
			CBUFFER_END

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				float scale = lerp(_RadiusA, _RadiusB, input.color.x);
				input.positionOS.xyz *= scale;
				input.positionOS.xyz += float3(0, _Length * input.color.x, 0);
				output.color = input.color;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS);
				output.positionCS = vertexInput.positionCS;
				output.normalWS = normalInput.normalWS;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				//return input.color;

				half NdotL = dot(input.normalWS, GetMainLight().direction);
				half diff = (NdotL + 1) * 0.5;
				half4 finalCol = half4(diff * _Color.rgb, 1);

				return finalCol;
			}
			ENDHLSL
		}

	}
}
