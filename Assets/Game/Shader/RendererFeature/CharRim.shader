﻿Shader "WalkingFat/RendererFeather/CharRim"
{
	Properties
	{
		_RimWidth("Rim Width", Range(0.0, 5.0)) = 1.0
		_LitStrength("Lit Strength", Range(1.0, 5.0)) = 2
		_DarkStrength("Dark Strength", Range(0.0, 1.0)) = 2
		_DepthThreshold("Depth Threshold", Range(0.0, 10.0)) = 1.0
		_TransparencyDist("Transparency Distance", Range(0.0, 5.0)) = 2.0
		_UnlitThreshold("Unlit Threshold", Range(0,1)) = 0.1
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "CharRim"
			Tags { "LightMode" = "UniversalForward" }
			
			Stencil
			{
				ref 201
				Comp NotEqual
			}

			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0
			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_fog
			#pragma multi_compile_instancing

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			//#include "ReceiveRainPass.hlsl"

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Assets/Game/Shader/Common/DitherTransparency.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 positionNDC : TEXCOORD0;
				float4 color : TEXCOORD1;
				float4 shadowCoord : TEXCOORD2;
				float4 fogFactorAndVertexLight : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float _RimWidth;
			float _LitStrength;
			float _DarkStrength;
			float _TransparencyDist; 
			float _DepthThreshold;
			float _UnlitThreshold;
			CBUFFER_END

			float4 _CameraDepthTexture_TexelSize;

			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);
			TEXTURE2D(_CameraOpaqueTexture);		        SAMPLER(sampler_CameraOpaqueTexture);

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);


				output.positionCS = vertexInput.positionCS;

				float4 ndc = output.positionCS * 0.5f;
				output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				output.positionNDC.zw = output.positionCS.zw;

				// custom value
				half3 normalWS = normalize(normalInput.normalWS);
				half3 positionWS = vertexInput.positionWS;

				half3 viewDirWS = normalize(_WorldSpaceCameraPos - positionWS);
				half3 lightDir = normalize(_MainLightPosition.xyz);
				half sss = max(0, dot(normalWS, lightDir));
				half rimStrength = 1 - sss * 0.5;

				half3 pivotPosWS = TransformObjectToWorld(half3(0, 0, 0));
				pivotPosWS.y = GetCameraPositionWS().y;
				half clipStrength = distance(pivotPosWS, GetCameraPositionWS());

				half NdotL = max(0, dot(normalWS, lightDir));

				float3 pivotWS = TransformObjectToWorld(float3(0, input.positionOS.y, 0));
				float3 pivotViewDir = normalize(GetCameraPositionWS() - pivotWS);
				half backLit = max(0, dot(pivotViewDir, -lightDir));

				output.color = half4(rimStrength, clipStrength, NdotL, backLit);

				half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);
				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
				output.shadowCoord = GetShadowCoord(vertexInput);

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// light
				Light mainLight = GetMainLight(input.shadowCoord);
				half fogCoord = input.fogFactorAndVertexLight.x;
				
				half attenuation = mainLight.distanceAttenuation * mainLight.shadowAttenuation;
				attenuation = smoothstep(0.0 + _UnlitThreshold, 0.02 + _UnlitThreshold, attenuation);

				// line
				half halfScaleFloor = floor(_RimWidth * 0.5);
				half halfScaleCeil = ceil(_RimWidth * 0.5);

				half2 uv = input.positionNDC.xy / input.positionNDC.w;

				half4 sceneColor = SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, half4(uv, 0, 0));

				half2 bottomLeftUV = uv - float2(_CameraDepthTexture_TexelSize.x, _CameraDepthTexture_TexelSize.y) * halfScaleFloor;
				half2 topRightUV = uv + float2(_CameraDepthTexture_TexelSize.x, _CameraDepthTexture_TexelSize.y) * halfScaleCeil;
				half2 bottomRightUV = uv + float2(_CameraDepthTexture_TexelSize.x * halfScaleCeil, -_CameraDepthTexture_TexelSize.y * halfScaleFloor);
				half2 topLeftUV = uv + float2(-_CameraDepthTexture_TexelSize.x * halfScaleFloor, _CameraDepthTexture_TexelSize.y * halfScaleCeil);

				half depthBase = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv), _ZBufferParams);
				half depth0 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, bottomLeftUV), _ZBufferParams);
				half depth1 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, topRightUV), _ZBufferParams);
				half depth2 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, bottomRightUV), _ZBufferParams);
				half depth3 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, topLeftUV), _ZBufferParams);

				half depthThreshold = _DepthThreshold * depthBase * input.color.x;

				half depthFiniteDifference0 = max(0, depth0 - depthBase);
				half depthFiniteDifference1 = max(0, depth1 - depthBase);
				half depthFiniteDifference2 = max(0, depth2 - depthBase);
				half depthFiniteDifference3 = max(0, depth3 - depthBase);

				half edgeDepth = (depthFiniteDifference0 + depthFiniteDifference1 + depthFiniteDifference2 + depthFiniteDifference3) * 100;// *attenuation;
				edgeDepth = edgeDepth > depthThreshold ? 1 : 0;

				// line 2
				/*
				float2 offsets[9] = {
					float2(-1, 1),
					float2(0, 1),
					float2(1, 1),
					float2(-1, 0),
					float2(0, 0),
					float2(1, 0),
					float2(-1, -1),
					float2(0, -1),
					float2(1, -1)
				};

				const float4 horizontalDiagCoef = float4(-1, -1, 1, 1);
				const float4 horizontalAxialCoef = float4(0, -1, 0, 1);
				const float4 verticalDiagCoeff = float4(1, 1, -1, -1);
				const float verticalAxialCoef = float4(1, 0, -1, 0);
				// boardlands implementation of sobel filter
				// diagonal / axial values
				float4 depthDiag;
				float4 depthAxial;

				float2 distance = _RimWidth * _CameraDepthTexture_TexelSize.xy;

				depthDiag.x = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv + offsets[6] * distance), _ZBufferParams); // (-1, -1)
				depthDiag.y = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv + offsets[0] * distance), _ZBufferParams); // (-1, 1)
				depthDiag.z = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv + offsets[2] * distance), _ZBufferParams);// (1, 1)
				depthDiag.w = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv + offsets[8] * distance), _ZBufferParams); // (1, -1)

				depthAxial.x = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv + offsets[3] * distance), _ZBufferParams); // (-1, 0)
				depthAxial.y = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv + offsets[1] * distance), _ZBufferParams); // (0, 1)
				depthAxial.z = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv + offsets[5] * distance), _ZBufferParams); // (1, 0)
				depthAxial.w = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv + offsets[7] * distance), _ZBufferParams); // (0, -1)

				float centerDepth = Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv), _ZBufferParams);

				depthDiag /= centerDepth;
				depthAxial -= centerDepth;

				float4 sobelHorizontal = horizontalDiagCoef * depthDiag + horizontalAxialCoef * depthAxial;
				float4 sobelVertical = verticalDiagCoeff * depthDiag + verticalAxialCoef * depthAxial;

				float sobelH = max(0, dot(sobelHorizontal, float4(1, 1, 1, 1)));
				float sobelV = max(0, dot(sobelVertical, float4(1, 1, 1, 1)));

				float sobel = sqrt(sobelH * sobelH + sobelV * sobelV);
				sobel = pow(saturate(sobel), 4);

				//clip(sobel - 0.1);
				//return sobel;
				*/

				// clip
				//half4 a = GetDitherTransparencyWithDistance(_TransparencyDist, edgeDepth, input.positionNDC, input.color.y);
				clip(edgeDepth - 0.1);

				half4 finalCol = sceneColor * lerp(_DarkStrength, _LitStrength, attenuation * input.color.z) + attenuation * input.color.w * half4(mainLight.color, 1) * 3;
				finalCol.rgb = MixFog(finalCol.rgb, fogCoord);

				return finalCol;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
