﻿Shader "WalkingFat/Water/TessWater"
{
	Properties
	{
		_RefNoiseTex("Reflection Noise Texture", 2D) = "white" {}
		_NoiseTile("Noise Tile", Range(0.1, 10)) = 1.0
		_FoamTex("Noise Texture", 2D) = "white" {}

		[Space]
		[Header(Tessellation)]
		_TessUniform("Tessellation Uniform", Range(1, 64)) = 1
		_TessDistMin("Tessellation Distance Min", float) = 10
		_TessDistMax("Tessellation Distance Max", float) = 100

		[Space]
		[Header(Effect)]
		_DepthFactor("Depth Factor", Range(0.0, 5.0)) = 1.0
		_WaterColorShallow("Shallow Water Color", Color) = (1,1,1,1)
		_WaterColorDeep("Deep Water Color", Color) = (1,1,1,1)
		//_WaveSpeed("Wave Speed", Range(0,2)) = 0.2
		_ReflDistort("Reflection Distort", Range(0,10)) = 0.2
		_BackgroundDistort("Background Distort", Range(0,1)) = 0.2

		_Specular("Specular", Range(0,10)) = 1.0
		_Gloss("Gloss", Range(0,10)) = 1.0
		_SpecularStrength("Specular Strength", Range(0,1)) = 0.5
		_SssIntensity("SSS Strength", Range(0,1)) = 0.5
		_FresnelStrength("Fresnel Strength", Range(0,1)) = 0.3
		_ReflFadeDist("Reflection Fade Distance", Float) = 100

		_DepthFactor("Depth Factor", float) = 1.0
		_FoamStrength("Foam Strength", Range(0,1)) = 0.5
		_FoamFactor("Foam Factor", float) = 1.0

		[Space]
		[Header(Debug)]
		_WireColor("Wire Color", Color) = (1,0,0,1)
		_WireWidth("Wire Width", Range(0.1, 5.0)) = 1.0
		[Toggle]_HardNormal("Hard Normal", Float) = 1.0

	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Transparent+2" "RenderPipeline" = "UniversalPipeline"}
		LOD 100

		Pass
		{
			Name "TessWater"
			Tags { "LightMode" = "UniversalForward" }

			Stencil
			{
				ref 201
				Comp Always
				Pass Replace
			}
			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x gles
			#pragma target 4.6

			#pragma vertex TessVertex
			#pragma fragment PassFragment
			#pragma hull hull
			#pragma domain domain
			#pragma geometry geom

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Assets/Game/Shader/Common/TessellationCore.hlsl"

			#if defined(SHADER_API_D3D11) || defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE) || defined(SHADER_API_VULKAN) || defined(SHADER_API_METAL) || defined(SHADER_API_PSSL)
			#define UNITY_CAN_COMPILE_TESSELLATION 1
			#   define UNITY_domain                 domain
			#   define UNITY_partitioning           partitioning
			#   define UNITY_outputtopology         outputtopology
			#   define UNITY_patchconstantfunc      patchconstantfunc
			#   define UNITY_outputcontrolpoints    outputcontrolpoints
			#endif

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
			};

			struct TessAttributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_Position;
				float4 positionWS : TEXCOORD1; // w = bitangentWS.x
				float4 normalWS : TEXCOORD2; // w = bitangentWS.y
				float4 tangentWS : TEXCOORD3; // w = bitangentWS.z
				float4 positionNDC : TEXCOORD4;
				float2 texcoord : TEXCOORD5;
				//float fogCoord : TEXCOORD6;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			struct g2f
			{
				float4 positionCS : SV_Position;
				float4 positionWS : TEXCOORD1; // w = bitangentWS.x
				float4 normalWS : TEXCOORD2; // w = bitangentWS.y
				float4 tangentWS : TEXCOORD3; // w = bitangentWS.z
				float4 positionNDC : TEXCOORD4;
				float3 dist : TEXCOORD5;
				float2 texcoord : TEXCOORD6;
			};

			CBUFFER_START(UnityPerMaterial)
			float _NoiseTile;
			float _TessUniform;
			float _TessDistMin;
			float _TessDistMax;
			float4 _CloudColor;
			float4 _WaterColorShallow;
			float4 _WaterColorDeep;
			float _ReflDistort;
			//float _WaveSpeed;
			float _BackgroundDistort;
			float _SpecularStrength;
			float _SssIntensity;
			float _FoamStrength;
			float _FresnelStrength;
			float _ReflFadeDist;
			float _Specular;
			float _Gloss;
			float _DepthFactor;
			float _FoamFactor;
			float4 _WireColor;
			float _WireWidth;
			float _HardNormal;
			float4 _RefNoiseTex_TexelSize;
			float4 _FoamTex_TexelSize;
			CBUFFER_END

			TEXTURE2D(_RefNoiseTex);						SAMPLER(sampler_RefNoiseTex);
			TEXTURE2D(_FoamTex);							SAMPLER(sampler_FoamTex);
			TEXTURE2D(_PlanarReflectionTexture);            SAMPLER(sampler_PlanarReflectionTexture);
			TEXTURE2D(_CameraOpaqueTexture);		        SAMPLER(sampler_CameraOpaqueTexture);
			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);

			// global
			float3 _CharPosWorld;
			float4 _WindParams;
			float4 _SunPos;
			float _HasSunMoon;
			float4 _HaloColor;
			float4 _SkyColor0;
			float4 _SkyColor1;
			float4 _SkyColor2;
			float _SkyColorMTime;
			float _DayNightTime;
			float _CloudyRate;
			float _LitRateTime;

			Varyings PassVertex(TessAttributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.texcoord = input.texcoord * _NoiseTile;


				output.positionWS.xyz = TransformObjectToWorld(input.positionOS.xyz);

				float4 noiseTex = SAMPLE_TEXTURE2D_LOD(_RefNoiseTex, sampler_RefNoiseTex, input.texcoord, 0);

				float noiseHeight = pow(noiseTex.x, 0.3);

				//float3 noiseNormal = UnpackNormal(noiseValue);
				//float3 normalOS = float3(noiseNormal.x, noiseNormal.y, noiseNormal.z);
				//float3 normalWS = TransformTangentToWorld(noiseNormal, half3x3(normalInput.tangentWS, normalInput.bitangentWS, normalInput.normalWS.xyz));

				//noiseValue = SAMPLE_TEXTURE2D_LOD(_RefNoiseTex, sampler_RefNoiseTex, output.positionWS.xz, 0).g;

				//====
				float3 offset;
				float h = asin(noiseHeight - 0.5);
				offset.xz = _WindParams.xy * cos(h * 2);
				offset.y = (noiseHeight - 0.5) * 1;
				output.positionWS.xyz += offset * 1 * _WindParams.z;

				//output.positionWS.y += (noiseHeight - 0.5) * 2;

				//output.positionWS.xyz += normalInput.normalWS * (0.5 - noiseTex.g) * _DeformStrength;

				// char hit
				/*
				float charDist = min(_InteractiveDist, distance(_CharPosWorld.xyz, output.positionWS.xyz));
				float offset = 1 - pow(charDist / _InteractiveDist, 2);
				output.positionWS.xyz -= normalInput.normalWS * offset * _InteractiveStrength;
				*/
				//output.noiseValue = float4(UnpackNormal(noiseValue), noiseHeight);

				output.positionCS = TransformWorldToHClip(output.positionWS.xyz);

				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				output.normalWS = float4(normalInput.normalWS, normalInput.bitangentWS.x);
				output.tangentWS = float4(normalInput.tangentWS, normalInput.bitangentWS.y);
				output.positionWS.w = normalInput.bitangentWS.z;

				float4 ndc = output.positionCS * 0.5f;
				output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				output.positionNDC.zw = output.positionCS.zw;

				//output.fogCoord = ComputeFogFactor(output.positionCS.z);

				return output;
			}

			TessAttributes TessVertex(Attributes input)
			{
				TessAttributes output = (TessAttributes)0;

				output.positionOS = input.positionOS;
				output.normalOS = input.normalOS;
				output.tangentOS = input.tangentOS;
				output.texcoord = input.texcoord;
				return output;
			}

			// tessellation functions
			TessellationFactors patchConstantFunction(InputPatch<TessAttributes, 3> patch)
			{
				TessellationFactors f;

				float4 tess = DistanceBasedTess(patch[0].positionOS, patch[1].positionOS, patch[2].positionOS, _TessDistMin, _TessDistMax, _TessUniform);

				f.edge[0] = tess.x;
				f.edge[1] = tess.y;
				f.edge[2] = tess.z;
				f.inside = tess.w;
				return f;
			}

			[UNITY_domain("tri")]
			[UNITY_outputcontrolpoints(3)]
			[UNITY_outputtopology("triangle_cw")]
			[UNITY_partitioning("integer")]
			[UNITY_patchconstantfunc("patchConstantFunction")]
			TessAttributes hull(InputPatch<TessAttributes, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[UNITY_domain("tri")]
			Varyings domain(TessellationFactors factors, OutputPatch<TessAttributes, 3> patch, float3 bary : SV_DomainLocation)
			{
				Attributes input;

				input.positionOS = patch[0].positionOS * bary.x + patch[1].positionOS * bary.y + patch[2].positionOS * bary.z;
				input.normalOS = patch[0].normalOS * bary.x + patch[1].normalOS * bary.y + patch[2].normalOS * bary.z;
				input.tangentOS = patch[0].tangentOS * bary.x + patch[1].tangentOS * bary.y + patch[2].tangentOS * bary.z;
				input.texcoord = patch[0].texcoord * bary.x + patch[1].texcoord * bary.y + patch[2].texcoord * bary.z;

				Varyings output = PassVertex(input);

				return output;
			}

			[maxvertexcount(3)]
			void geom(triangle Varyings IN[3], inout TriangleStream<g2f> triStream)
			{
				float3 dist = CalculateDistToCenter(IN[0].positionCS, IN[1].positionCS, IN[2].positionCS);

				g2f OUT;
				OUT.positionCS = IN[0].positionCS;
				OUT.positionWS = IN[0].positionWS;
				OUT.normalWS = IN[0].normalWS;
				OUT.tangentWS = IN[0].tangentWS;
				OUT.positionNDC = IN[0].positionNDC;
				OUT.dist = float3(dist.x, 0, 0);
				OUT.texcoord = IN[0].texcoord;
				triStream.Append(OUT);

				OUT.positionCS = IN[1].positionCS;
				OUT.positionWS = IN[1].positionWS;
				OUT.normalWS = IN[1].normalWS;
				OUT.tangentWS = IN[1].tangentWS;
				OUT.positionNDC = IN[1].positionNDC;
				OUT.dist = float3(0, dist.y, 0);
				OUT.texcoord = IN[1].texcoord;
				triStream.Append(OUT);

				OUT.positionCS = IN[2].positionCS;
				OUT.positionWS = IN[2].positionWS;
				OUT.normalWS = IN[2].normalWS;
				OUT.tangentWS = IN[2].tangentWS;
				OUT.positionNDC = IN[2].positionNDC;
				OUT.dist = float3(0, 0, dist.z);
				OUT.texcoord = IN[2].texcoord;
				triStream.Append(OUT);
			}

			half4 PassFragment(g2f input) : SV_Target
			{
				//UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// decode gamma
				half4 skyColor0 = pow(_SkyColor0, 2.2);
				half4 skyColor1 = pow(_SkyColor1, 2.2);
				half4 skyColor2 = pow(_SkyColor2, 2.2);
				half4 haloColor = pow(_HaloColor, 2.2);

				float3 hardNormal = CalcHardNormal(input.positionWS.xyz);
				half hardNdotL = dot(hardNormal, GetMainLight().direction);

				float3 bitangentWS = float3(input.normalWS.w, input.tangentWS.w, input.positionWS.w);
				float3 viewDir = normalize(GetCameraPositionWS() - input.positionWS.xyz);
				//float texelSize = (1.0 / 128.0) / _NoiseTile;

				_WaterColorShallow = lerp(_WaterColorShallow, 0.05, _CloudyRate);
				_WaterColorDeep = lerp(_WaterColorDeep, 0.05, _CloudyRate);

				float3 litDir = _MainLightPosition;
				//float3 viewDirWS = normalize(_WorldSpaceCameraPos.xyz - input.positionWS.xyz);
				half toCamDist = distance(_WorldSpaceCameraPos.xyz, input.positionWS.xyz);
				float viewDist = pow(max(0, 1 - toCamDist * 0.0003), 5);
				float waterDist = 1 - pow(saturate(toCamDist * 0.000125), 0.12);

				float2 uvWS = input.positionWS.xz + _WindParams.xy * _Time.y * _WindParams.z;
				//half4 noiseTex = SAMPLE_TEXTURE2D(_RefNoiseTex, sampler_RefNoiseTex, uvWS * _NoiseTile);


				half4 noiseTex = SAMPLE_TEXTURE2D(_RefNoiseTex, sampler_RefNoiseTex, input.texcoord.xy);

				half4 foamTex = SAMPLE_TEXTURE2D(_FoamTex, sampler_FoamTex, input.texcoord.xy);

				half texelSize = _RefNoiseTex_TexelSize.x;

				half2 uvx0 = input.texcoord.xy + float2(texelSize, 0);
				half2 uvx1 = input.texcoord.xy + float2(-texelSize, 0);
				half2 uvy0 = input.texcoord.xy + float2(0, texelSize);
				half2 uvy1 = input.texcoord.xy + float2(0, -texelSize);

				half4 ntx0 = SAMPLE_TEXTURE2D(_RefNoiseTex, sampler_RefNoiseTex, uvx0);
				half4 ntx1 = SAMPLE_TEXTURE2D(_RefNoiseTex, sampler_RefNoiseTex, uvx1);
				half4 nty0 = SAMPLE_TEXTURE2D(_RefNoiseTex, sampler_RefNoiseTex, uvy0);
				half4 nty1 = SAMPLE_TEXTURE2D(_RefNoiseTex, sampler_RefNoiseTex, uvy1);

				half3 normalFH = normalize(half3(ntx1.x - ntx0.x, texelSize, nty0.x - nty1.x));

				half3 normalWS = TransformTangentToWorld(normalFH, half3x3(input.tangentWS.xyz, bitangentWS, input.normalWS.xyz));

				normalWS = NormalizeNormalPerPixel(normalWS);

				normalWS = lerp(normalWS, hardNdotL, _HardNormal);

				// foam mask
				half foam0 = noiseTex.x * 4 - ntx0.x - ntx1.x - nty0.x - nty1.x;
				half foam1 = saturate(pow(noiseTex.x * _WindParams.z, 3) * 10);
				half foamTS = min(1, foam1 * foam1 * (foam0 + 0.2) * 10);

				//return foam;

				//========

				half NdotL = saturate(dot(normalWS, GetMainLight().direction));
				half NdotV = saturate(dot(normalWS, viewDir));

				// sky color
				half4 skyRimCol = lerp(skyColor0, skyColor1, 0.2);
				half4 skyCol = lerp(skyRimCol, skyColor2, viewDist);
					
				// reflction
				half reflDistort = _ReflDistort + saturate(toCamDist * 0.005) * 5;
				float4 reflUV = input.positionNDC + float4(normalWS * reflDistort, 0);
				half4 reflTex = SAMPLE_TEXTURE2D(_PlanarReflectionTexture, sampler_PlanarReflectionTexture, float4(reflUV.xy / reflUV.w, 0, 0));

				half reflMask01 = step(reflTex.x + reflTex.y + reflTex.z, 0);
				//half reflMask = 1 - min(1, (reflTex.x + reflTex.y + reflTex.z) * lerp(40, 10, _LitRateTime * 0.1667));
				half reflMaskFar = saturate(reflMask01 + noiseTex.x * 0.6); // far Wave
				half reflMask = lerp(reflMask01, reflMaskFar, saturate((toCamDist - 100) * 0.01));

				half4 reflFogCol = lerp(reflTex, unity_FogColor, saturate((toCamDist - 50) * 0.003) * (1 - reflMask));
				half4 reflCol = lerp(reflFogCol, skyCol, reflMask);

				// fresnel
				half nearDistFade = saturate(toCamDist / (100 + distance(input.positionWS.y, _WorldSpaceCameraPos.y) * 5));
				half fresnel = pow(1 - NdotV, 10 * waterDist) * nearDistFade;
				half refFresnel = saturate(fresnel + (1 - reflMask) * fresnel * pow(viewDist, 2) * 1.5);

				half4 fresnelCol = reflCol * refFresnel * _FresnelStrength * lerp(1, 0.5, _CloudyRate);
				//return fresnel;

				// sss
				half3 h = normalize(normalWS + normalize(_SunPos.xyz) * 5);
				half sssValue = pow(saturate(dot(viewDir, -h)), 10) * _SssIntensity;
				half4 sssCol = sssValue * lerp(_WaterColorShallow, _MainLightColor, 0.4);
				//return sssValue;

				// sun halo
				float3 posClip2 = input.positionCS.xyz / input.positionCS.w;
				half4 sunPosClip = TransformWorldToHClip(half4(_SunPos.xyz, 1));
				// Check DX or OpenGL
#if UNITY_UV_STARTS_AT_TOP
				sunPosClip.y = sunPosClip.y;
#endif
				half sunHaloValue = 0;
				half faceSunMoon = step(0, sunPosClip.w);

				sunPosClip.xyz = sunPosClip.xyz / sunPosClip.w;
				sunHaloValue = (1 - distance(posClip2.xy, sunPosClip.xy) * 0.5) * faceSunMoon;
				sunHaloValue = saturate(pow(sunHaloValue, 5));
				half4 sunHaloCol = sunHaloValue * haloColor * fresnel * 5;

				half sunHaloMask = saturate(_LitRateTime * (1.5 - _LitRateTime * 0.8));
				sunHaloCol *= sunHaloMask * reflMask;

				// specular
				half specMask = saturate(_LitRateTime - 0.9) * viewDist * viewDist * reflMask;

				float3 specNormal = normalize(normalWS + input.normalWS * 0.5);

				float3 halfVector = normalize(litDir + viewDir);
				float litSpecular = pow(max(0, dot(halfVector, specNormal)), _Specular);
				litSpecular = smoothstep(0.99, 1, litSpecular) * _Gloss * specMask;
				half4 litSpecCol = litSpecular * _MainLightColor;

				float directSpecular = pow(max(0, dot(halfVector, normalWS)), _Specular);
				float specular = smoothstep(0.8, 1, directSpecular) * specMask;
				half4 specularCol = specular * _MainLightColor * _SpecularStrength;

				// foam
				float4 bgUVoffset = float4(normalWS * _BackgroundDistort, 0);
				float4 bgUV = input.positionNDC + bgUVoffset;

				float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, float4(bgUV.xy / bgUV.w, 0, 0)), _ZBufferParams);

				bgUVoffset *= saturate(depth - input.positionNDC.w) * 1;
				bgUV = (input.positionNDC + bgUVoffset) / input.positionNDC.w;

				depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, float4(bgUV.xy / bgUV.w, 0, 0)), _ZBufferParams);

				float foamLine = 1 - saturate(_FoamFactor * (depth - input.positionNDC.w));
				float foamValue = step(0.1 + toCamDist * 0.002, noiseTex.x * foamLine);
				foamValue = min(1, foamValue + foamTS);//* foamTex.x;

				half4 foamCol = foamValue * _FoamStrength * _MainLightColor * saturate(_DayNightTime + 0.3);

				// distort background
				half4 bgTex = SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, float4(bgUV.xy / bgUV.w, 0, 0));

				half skyGray = dot(skyCol.xyz, float3(0.299, 0.587, 0.114));

				half4 bgColShallow = _WaterColorShallow * skyGray;
				half4 bgColDeep = _WaterColorDeep * skyGray;
				
				float depthLine = 1 - saturate(_DepthFactor * (depth - input.positionNDC.w));
				depthLine = pow(depthLine, 5);

				half4 bgCol = lerp(bgTex, bgColShallow, 1 - depthLine * 0.8);
				bgCol = lerp(bgCol, bgColDeep, saturate(0.5 - depthLine * 2));

				// finalCol
				half4 finalCol = bgCol + fresnelCol * (1 - depthLine);// +sssCol;// +foamCol;

				// apply fog
				//finalCol.xyz = MixFog(finalCol.xyz, input.fogCoord);

				finalCol += litSpecCol + specularCol + sunHaloCol;
				finalCol = lerp(bgTex, finalCol, saturate(viewDist * 50));



				// wireframe
				//distance of frag from triangles center
				float distFT = min(input.dist.x, min(input.dist.y, input.dist.z));
				//fade based on dist from center
				float wireframeValue = exp2((-1 / _WireWidth) * distFT * distFT);
				wireframeValue *= _WireColor.a;

				//finalCol.rgb = lerp(half3(smoothNdotL, smoothNdotL, smoothNdotL), _WireColor.rgb, wireframeValue);// debug

				finalCol.rgb = lerp(finalCol.rgb, _WireColor.rgb, wireframeValue);

				//finalCol.r += foam;

				return finalCol;

			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
