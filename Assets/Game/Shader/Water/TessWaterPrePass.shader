﻿Shader "WalkingFat/Water/TessWaterPrePass"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "" {}
		_RefPannerHeight("Reflection Panner Height", Float) = 0.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Geometry+1" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "TessWaterNoise"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				//float4 positionNDC : TEXCOORD0;
				float4 uv0 : TEXCOORD0;
				float4 uv1 : TEXCOORD1;
				float4 uv2 : TEXCOORD2;
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _MainTex_ST;
			float _RefPannerHeight;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);
			TEXTURE2D(_CameraOpaqueTexture);		        SAMPLER(sampler_CameraOpaqueTexture);
			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);

			// Global
			float4 _WindParams;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);

				float4 ndc = output.positionCS * 0.5f;
				float4 positionNDC;
				positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				positionNDC.zw = output.positionCS.zw;

				float2 windForce = -_WindParams.xy * _Time.y * 0.05;
				float2 windTangent = float2(-windForce.y, windForce.x);

				float2 uv = input.texcoord.xy;// *_MainTex_ST.xy * 1;
				//uv = float2(uv.x, -uv.y);

				output.uv0.xy = uv + windForce + windTangent * 0.55;
				output.uv0.zw = uv + (windForce - windTangent * 0.31) * 1.27;
				output.uv1.xy = uv + (windForce + windTangent * 0.42) * 2.25;
				output.uv1.zw = uv + (windForce - windTangent * 0.55) * 2.16;


				float4 projPos = output.positionCS * 0.5;
				projPos.xy = projPos.xy + projPos.w;

				output.uv2.xy = positionNDC.xy / positionNDC.w;
				output.uv2.zw = projPos.xy;

				return output;
			}

			half4 VisualizePosition(Varyings input, float3 pos)
			{
				const float grid = 1;
				const float width = 3;

				pos *= grid;

				// Detect borders with using derivatives.
				float3 fw = fwidth(pos);
				half3 bc = saturate(width - abs(1 - 2 * frac(pos)) / fw);

				// Frequency filter
				half3 f1 = smoothstep(1 / grid, 2 / grid, fw);
				half3 f2 = smoothstep(2 / grid, 4 / grid, fw);
				bc = lerp(lerp(bc, 0.5, f1), 0, f2);

				// Blend with the source color.
				//half4 c = SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, input.uv.xy);
				half4 c = half4(lerp(half3(0.5, 0.5, 0.5), bc, 0.8), 1);

				return c;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
				
				// Noise ==========================
				half4 noiseTex0 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv0.xy);
				half4 noiseTex1 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv0.zw);
				half4 noiseTex2 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv1.xy);
				half4 noiseTex3 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv1.zw);

				half HM = (noiseTex0.x + noiseTex1.x) * 0.5;
				half HL = (noiseTex2.y + noiseTex3.y) * 0.5;

				half noiseHeight = lerp(HM, HL, _WindParams.z * 0.4 + 0.5);

				noiseHeight = lerp(0.5 + noiseHeight * 0.02, noiseHeight, _WindParams.z);

				// SSPR ==========================
				half2 uvSS = input.uv2.xy;
				half2 uvPS = input.uv2.zw;

				half4 sceneCol = SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, uvSS);
				float depth = SAMPLE_TEXTURE2D_X(_CameraDepthTexture, sampler_CameraDepthTexture, uvSS).r;

				// compute posWS
#if UNITY_REVERSED_Z
				depth = 1.0 - depth;
#endif

				depth = 2.0 * depth - 1.0;

				half3 viewPos = ComputeViewSpacePosition(uvPS, depth, unity_CameraInvProjection);
				half3 worldPos = mul(unity_CameraToWorld, half4(viewPos, 1.0)).xyz;

				half needRef = step(_RefPannerHeight, worldPos.y);
				half3 refWorldPos = half3(worldPos.x, -_RefPannerHeight * 2 + worldPos.y, worldPos.z);
				refWorldPos = lerp(worldPos, refWorldPos, needRef);

				half4 positionCS = TransformWorldToHClip(refWorldPos);

				//return VisualizePosition(input, worldPos.xyz);

				/*
				half4 ndc = positionCS * 0.5f;
				half4 positionNDC;
				positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				positionNDC.zw = positionCS.zw;
				half2 uvR = positionCS.xy / positionCS.w;// / positionNDC.w;
				*/

				half2 ndcR = positionCS.xy / positionCS.w;
				half needNdcR = step(ndcR.x * ndcR.y, 1);
				half2 uvR = ndcR * 0.5 + 0.5;
				uvR = lerp(half2(0,0), uvR, needNdcR);

#if UNITY_REVERSED_Z
				uvR.y = 1.0 - uvR.y;
#endif
				uvR *= 512;
				return half4(noiseHeight, uvR, 1);
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}