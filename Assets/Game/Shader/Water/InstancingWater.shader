﻿Shader "WalkingFat/Water/InstancingWater"
{
	Properties
	{
		_WaveTex("Wave Texture", 2D) = "white" {}
		_TileScale("Tile Scale", float) = 1.0
		_WaterColorShallow("Shallow Water Color", Color) = (1,1,1,1)
		_WaterColorDeep("Deep Water Color", Color) = (1,1,1,1)
		_WaveSpeed("Wave Speed", Range(0,2)) = 0.2
		_ReflDistort("Reflection Distort", Range(0,10)) = 0.2
		_BackgroundDistort("Background Distort", Range(0,1)) = 0.2

		_Specular("Specular", Range(0,1)) = 0.5
		_Gloss("Gloss", Range(0,1)) = 0.5
		_SpecularStrength("Specular Strength", Range(0,1)) = 0.5
		_HaloSpecularStrength("Halo Specular Strength", Range(0,5)) = 1
		_FresnelStrength("Fresnel Strength", Range(0,1)) = 0.3
		_ReflFadeDist("Reflection Fade Distance", Float) = 100

		_DepthFactor("Depth Factor", float) = 1.0
		_FoamStrength("Foam Strength", Range(0,1)) = 0.5
		_FoamFactor("Foam Factor", float) = 1.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Transparent+2" "RenderPipeline" = "UniversalPipeline"}
		LOD 100
		//ZWrite On
		
		Pass {

			Stencil
			{
				ref 201
				Comp Always
				Pass Replace
			}
			
			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile_fog
			#pragma multi_compile_instancing

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 vertex : SV_POSITION;
				float3 positionWS : TEXCOORD0;
				float3 positionVS : TEXCOORD1;
				float4 positionCS : TEXCOORD2;
				float4 positionNDC : TEXCOORD3;
				half4 normalWS : TEXCOORD4;
				half4 tangentWS : TEXCOORD5;
				half4 bitangentWS : TEXCOORD6;
				float fogCoord : TEXCOORD7;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			TEXTURE2D(_WaveTex);							SAMPLER(sampler_WaveTex);
			TEXTURE2D(_PlanarReflectionTexture);            SAMPLER(sampler_PlanarReflectionTexture);
			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);
			TEXTURE2D(_CameraOpaqueTexture);		        SAMPLER(sampler_CameraOpaqueTexture);
			
			CBUFFER_START(UnityPerMaterial)
			float4 _WaveTex_ST;
			float _TileScale;
			float4 _WaterColorShallow, _WaterColorDeep;
			float _ReflDistort, _WaveSpeed, _BackgroundDistort;
			float _SpecularStrength;
			float _HaloSpecularStrength;
			float _FoamStrength;
			float _FresnelStrength;
			float _ReflFadeDist;
			float _Specular, _Gloss, _DepthFactor, _FoamFactor;
			CBUFFER_END

			// global
			float4 _SunPos;
			float _HasSunMoon;
			float4 _HaloColor;
			float4 _SkyColor0;
			float4 _SkyColor1;
			float4 _SkyColor2;
			float _SkyColorMTime;
			float _DayNightTime;
			float _CloudyRate;
			float _LitRateTime;
			

			Varyings vert(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				output.vertex = vertexInput.positionCS;
				output.positionWS = vertexInput.positionWS;
				output.positionVS = vertexInput.positionVS;
				output.positionCS = vertexInput.positionCS;
				output.positionNDC = vertexInput.positionNDC;

				half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
				output.normalWS = half4(normalInput.normalWS, viewDirWS.x);
				output.tangentWS = half4(normalInput.tangentWS, viewDirWS.y);
				output.bitangentWS = half4(normalInput.bitangentWS, viewDirWS.z);

				output.fogCoord = ComputeFogFactor(vertexInput.positionCS.z);

				return output;
			}

			half4 frag(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// decode gamma
				half4 skyColor0 = pow(_SkyColor0, 2.2);
				half4 skyColor1 = pow(_SkyColor1, 2.2);
				half4 skyColor2 = pow(_SkyColor2, 2.2);
				half4 haloColor = pow(_HaloColor, 2.2);

				_WaterColorShallow = lerp(_WaterColorShallow, 0.05, _CloudyRate);
				_WaterColorDeep = lerp(_WaterColorDeep, 0.05, _CloudyRate);

				// sample Texture
				float2 waveUV = input.positionWS.xz * _TileScale * 0.01;
				half4 waveTex1 = SAMPLE_TEXTURE2D(_WaveTex, sampler_WaveTex, waveUV + _Time.x * float2(0.4,1) * _WaveSpeed);
				half4 waveTex2 = SAMPLE_TEXTURE2D(_WaveTex, sampler_WaveTex, waveUV * 1.3 + _Time.x * float2(-0.53, 0.7) * _WaveSpeed);
				half4 waveTex3 = SAMPLE_TEXTURE2D(_WaveTex, sampler_WaveTex, waveUV * 0.06 + _Time.x * float2(0.08, 0.1));
				half waveMask = saturate(pow(waveTex3.x * waveTex3.y, 1) * 2);

				// common
				float3 litDir = _MainLightPosition;
				float3 viewDirWS = normalize(_WorldSpaceCameraPos.xyz - input.positionWS.xyz);
				half toCamDist = distance(_WorldSpaceCameraPos.xyz, input.positionWS.xyz);
				float viewDist = pow(max(0, 1 - toCamDist * 0.0003), 5);
				float waterDist = 1 - pow(saturate(toCamDist * 0.000125), 0.12);

				float3 waveNormal1 = UnpackNormal(waveTex1);
				float3 waveNormal2 = UnpackNormal(waveTex2);
				float3 waveNormal3 = UnpackNormal(waveTex3);

				float3 waveNormal = normalize(waveNormal1 + waveNormal2 + waveNormal3);
				//half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
				half3 normalWS = TransformTangentToWorld(waveNormal,
					half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));
				//return dot(viewDirWS, normalWS);
				// sky color
                half4 skyRimCol = lerp(skyColor0, skyColor1, 0.2);
				half4 skyCol = lerp(skyRimCol, skyColor2, viewDist);

				// reflction
				half reflDistort = _ReflDistort + saturate(toCamDist * 0.005) * 5;
				float4 reflUV = input.positionNDC + float4(waveNormal * reflDistort, 0);
				half4 reflTex = SAMPLE_TEXTURE2D(_PlanarReflectionTexture, sampler_PlanarReflectionTexture, float4(reflUV.xy / reflUV.w, 0, 0));
				//return reflTex;

				half reflMask01 = step(reflTex.x + reflTex.y + reflTex.z, 0);
				//half reflMask = 1 - min(1, (reflTex.x + reflTex.y + reflTex.z) * lerp(40, 10, _LitRateTime * 0.1667));
				half reflMaskFar = saturate(reflMask01 + pow(waveMask, 1) * 0.6); // far Wave
				half reflMask = lerp(reflMask01, reflMaskFar, saturate((toCamDist - 100) * 0.01));

				//return reflMask;

				half4 reflFogCol = lerp(reflTex, unity_FogColor, saturate((toCamDist - 50) * 0.003) * (1 - reflMask));
				half4 reflCol = lerp(reflFogCol, skyCol, reflMask);

				// fresnel
				float nearDistFade = saturate(toCamDist / (100 + distance(input.positionWS.y, _WorldSpaceCameraPos.y) * 5));
				half fresnel = pow(1 - saturate(dot(viewDirWS, normalWS)), 10 * waterDist) * nearDistFade;
				fresnel = saturate(fresnel +(1 - reflMask) * fresnel * pow(viewDist, 2) * 1.5);

				half4 fresnelCol = reflCol * fresnel * _FresnelStrength * lerp(1, 0.5, _CloudyRate);
				//return fresnel;

				// sun halo
				float3 posClip2 = input.positionCS.xyz / input.positionCS.w;
				half4 sunPosClip = TransformWorldToHClip(half4(_SunPos.xyz, 1));
				// Check DX or OpenGL
#if UNITY_UV_STARTS_AT_TOP
				sunPosClip.y = sunPosClip.y;
#endif
				half sunHaloValue = 0;
				half faceSunMoon = step(0, sunPosClip.w);

				sunPosClip.xyz = sunPosClip.xyz / sunPosClip.w;
				sunHaloValue = (1 - distance(posClip2.xy, sunPosClip.xy) * 0.5) * faceSunMoon;
				sunHaloValue = saturate(pow(sunHaloValue, 5));
				half4 sunHaloCol = sunHaloValue *haloColor * fresnel * 5;

				half sunHaloMask = saturate(_LitRateTime * (1.5 - _LitRateTime * 0.8));
				sunHaloCol *= sunHaloMask * reflMask;

				// specular
				half specMask = saturate(_LitRateTime - 0.9) * viewDist * viewDist * reflMask;

				float3 specNormal = normalize(normalWS + input.normalWS * 0.5);

				float3 halfVector = normalize(litDir + viewDirWS);
				float litSpecular = pow(max(0, dot(halfVector, specNormal)), 5);
				litSpecular = smoothstep(0.99, 1, litSpecular) * 4 * specMask;
				half4 litSpecCol = litSpecular * _MainLightColor;

				float directSpecular = pow(max(0, dot(halfVector, normalWS)), 5);
				float specular = smoothstep(0.8, 1, directSpecular) * specMask;
				half4 specularCol = specular * _MainLightColor * 0.2;

				// foam
				float4 bgUVoffset = float4(waveNormal * _BackgroundDistort, 0);
				float4 bgUV = input.positionNDC + bgUVoffset;

				float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, float4(bgUV.xy / bgUV.w, 0, 0)), _ZBufferParams);
				
				bgUVoffset *= saturate(depth - input.positionNDC.w) * 1;
				bgUV = (input.positionNDC + bgUVoffset) / input.positionNDC.w;

				depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, float4(bgUV.xy / bgUV.w, 0, 0)), _ZBufferParams);

				float foamLine = 1 - saturate(_FoamFactor * (depth - input.positionNDC.w));
				float foamValue = step(0.1 + toCamDist * 0.002, waveTex1.x * waveTex2.y * foamLine);

				half4 foamCol = foamValue * _FoamStrength * _MainLightColor * saturate(_DayNightTime + 0.3);

				// distort background
				half4 bgTex = SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, float4(bgUV.xy / bgUV.w, 0, 0));

				half skyGray = dot(skyCol.xyz, float3(0.299, 0.587, 0.114));

				half4 bgColShallow = _WaterColorShallow * skyGray;
				half4 bgColDeep = _WaterColorDeep * skyGray;

				float depthLine = 1 - saturate(_DepthFactor * (depth - input.positionNDC.w));
				depthLine = pow(depthLine , 5);

				half4 bgCol = lerp(bgTex, bgColShallow, 1 - depthLine * 0.8);
				bgCol = lerp(bgCol, bgColDeep, saturate(0.5 - depthLine * 2));
				
				// finalCol
				half4 finalCol = bgCol + fresnelCol * (1 - depthLine) + foamCol;
				
				// apply fog
				finalCol.xyz = MixFog(finalCol.xyz, input.fogCoord);

				finalCol += litSpecCol + specularCol + sunHaloCol;
				finalCol = lerp(bgTex, finalCol, saturate(viewDist * 50));

				return finalCol;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}