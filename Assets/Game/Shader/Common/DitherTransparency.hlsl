﻿#ifndef WALKINGFAT_Dither_Transparency_INCLUDED
#define WALKINGFAT_Dither_Transparency_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

half4 GetDitherTransparencyWithDistance(half clipDist, half alpha, half4 posScreen, half distWS)
{
	float4x4 thresholdMatrix =
    {  1.0 / 17.0,  9.0 / 17.0,  3.0 / 17.0, 11.0 / 17.0,
      13.0 / 17.0,  5.0 / 17.0, 15.0 / 17.0,  7.0 / 17.0,
       4.0 / 17.0, 12.0 / 17.0,  2.0 / 17.0, 10.0 / 17.0,
      16.0 / 17.0,  8.0 / 17.0, 14.0 / 17.0,  6.0 / 17.0
    };

    float4x4 _RowAccess = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };
    float2 pos = posScreen.xy / posScreen.w;
    pos *= _ScreenParams.xy;

    // Discard pixel if below threshold.
    half d = max(0, distWS - 1.5) / (clipDist - 1.5);
	half4 c = d * alpha - float4(thresholdMatrix[fmod(pos.x, 4)] * _RowAccess[fmod(pos.y, 4)]);
    
    return c;
}

#endif
