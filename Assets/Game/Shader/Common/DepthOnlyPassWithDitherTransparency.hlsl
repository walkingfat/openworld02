﻿#ifndef WALKINGFAT_DEPTH_ONLY_PASS_WITH_DITHER_TRANSPARENCY_INCLUDED
#define WALKINGFAT_DEPTH_ONLY_PASS_WITH_DITHER_TRANSPARENCY_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Assets/Game/Shader/Common/DitherTransparency.hlsl"

CBUFFER_START(UnityPerMaterial)
float4 _BaseMap_ST;
half _TransparencyDist;
half _Cutoff;
CBUFFER_END

TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);

struct Attributes
{
	float4 position     : POSITION;
	float2 texcoord     : TEXCOORD0;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
	float3 uv           : TEXCOORD0;
	float4 positionCS   : SV_POSITION;
	float4 positionNDC  : TEXCOORD1;
	UNITY_VERTEX_INPUT_INSTANCE_ID
	UNITY_VERTEX_OUTPUT_STEREO
};

Varyings DepthOnlyVertex(Attributes input)
{
	Varyings output = (Varyings)0;
	UNITY_SETUP_INSTANCE_ID(input);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

	output.uv.xy = TRANSFORM_TEX(input.texcoord, _BaseMap);
	output.positionCS = TransformObjectToHClip(input.position.xyz);

	float4 ndc = output.positionCS * 0.5f;
	output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
	output.positionNDC.zw = output.positionCS.zw;

	// custom value
	half3 pivotPosWS = TransformObjectToWorld(half3(0, 0, 0));
	pivotPosWS.y = GetCameraPositionWS().y;
	output.uv.z = distance(pivotPosWS, GetCameraPositionWS());

	return output;
}

half4 DepthOnlyFragment(Varyings input) : SV_TARGET
{
	UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
	
	half alpha = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv.xy).a;

	half4 a = GetDitherTransparencyWithDistance(_TransparencyDist, alpha * _Cutoff, input.positionNDC, input.uv.z);
	clip(a);

	return 0;
}
#endif
