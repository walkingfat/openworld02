﻿#ifndef WALKINGFAT_TESSELLATION_CORE_INCLUDED
#define WALKINGFAT_TESSELLATION_CORE_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

float CalcDistanceTessFactor(float4 positionOS, float minDist, float maxDist, float tess)
{
	float3 wpos = TransformObjectToWorld(positionOS).xyz;
	float dist = distance(wpos, _WorldSpaceCameraPos);
	float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
	return f;
}

float4 CalcTriEdgeTessFactors(float3 triVertexFactors)
{
	float4 tess;
	tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
	tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
	tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
	tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
	return tess;
}

float CalcEdgeTessFactor(float3 wpos0, float3 wpos1, float edgeLen)
{
	// distance to edge center
	float dist = distance(0.5 * (wpos0 + wpos1), _WorldSpaceCameraPos);
	// length of the edge
	float len = distance(wpos0, wpos1);
	// edgeLen is approximate desired size in pixels
	float f = max(len * _ScreenParams.y / (edgeLen * dist), 1.0);
	return f;
}

float DistanceFromPlane(float3 pos, float4 plane)
{
	float d = dot(float4(pos, 1.0f), plane);
	return d;
}


// Returns true if triangle with given 3 world positions is outside of camera's view frustum.
// cullEps is distance outside of frustum that is still considered to be inside (i.e. max displacement)
bool WorldViewFrustumCull(float3 wpos0, float3 wpos1, float3 wpos2, float cullEps)
{
	float4 planeTest;

	// left
	planeTest.x = ((DistanceFromPlane(wpos0, unity_CameraWorldClipPlanes[0]) > -cullEps) ? 1.0f : 0.0f) +
		((DistanceFromPlane(wpos1, unity_CameraWorldClipPlanes[0]) > -cullEps) ? 1.0f : 0.0f) +
		((DistanceFromPlane(wpos2, unity_CameraWorldClipPlanes[0]) > -cullEps) ? 1.0f : 0.0f);
	// right
	planeTest.y = ((DistanceFromPlane(wpos0, unity_CameraWorldClipPlanes[1]) > -cullEps) ? 1.0f : 0.0f) +
		((DistanceFromPlane(wpos1, unity_CameraWorldClipPlanes[1]) > -cullEps) ? 1.0f : 0.0f) +
		((DistanceFromPlane(wpos2, unity_CameraWorldClipPlanes[1]) > -cullEps) ? 1.0f : 0.0f);
	// top
	planeTest.z = ((DistanceFromPlane(wpos0, unity_CameraWorldClipPlanes[2]) > -cullEps) ? 1.0f : 0.0f) +
		((DistanceFromPlane(wpos1, unity_CameraWorldClipPlanes[2]) > -cullEps) ? 1.0f : 0.0f) +
		((DistanceFromPlane(wpos2, unity_CameraWorldClipPlanes[2]) > -cullEps) ? 1.0f : 0.0f);
	// bottom
	planeTest.w = ((DistanceFromPlane(wpos0, unity_CameraWorldClipPlanes[3]) > -cullEps) ? 1.0f : 0.0f) +
		((DistanceFromPlane(wpos1, unity_CameraWorldClipPlanes[3]) > -cullEps) ? 1.0f : 0.0f) +
		((DistanceFromPlane(wpos2, unity_CameraWorldClipPlanes[3]) > -cullEps) ? 1.0f : 0.0f);

	// has to pass all 4 plane tests to be visible
	return !all(planeTest);
}



// ---- functions that compute tessellation factors


// Distance based tessellation:
// Tessellation level is "tess" before "minDist" from camera, and linearly decreases to 1
// up to "maxDist" from camera.
float4 DistanceBasedTess(float4 v0, float4 v1, float4 v2, float minDist, float maxDist, float tess)
{
	float3 f;
	f.x = CalcDistanceTessFactor(v0, minDist, maxDist, tess);
	f.y = CalcDistanceTessFactor(v1, minDist, maxDist, tess);
	f.z = CalcDistanceTessFactor(v2, minDist, maxDist, tess);

	return CalcTriEdgeTessFactors(f);
}

// Desired edge length based tessellation:
// Approximate resulting edge length in pixels is "edgeLength".
// Does not take viewing FOV into account, just flat out divides factor by distance.
float4 EdgeLengthBasedTess(float4 v0, float4 v1, float4 v2, float edgeLength)
{
	float3 pos0 = TransformObjectToWorld(v0).xyz;
	float3 pos1 = TransformObjectToWorld(v1).xyz;
	float3 pos2 = TransformObjectToWorld(v2).xyz;
	float4 tess;
	tess.x = CalcEdgeTessFactor(pos1, pos2, edgeLength);
	tess.y = CalcEdgeTessFactor(pos2, pos0, edgeLength);
	tess.z = CalcEdgeTessFactor(pos0, pos1, edgeLength);
	tess.w = (tess.x + tess.y + tess.z) / 3.0f;
	return tess;
}


// Same as EdgeLengthBasedTess, but also does patch frustum culling:
// patches outside of camera's view are culled before GPU tessellation. Saves some wasted work.
float4 EdgeLengthBasedTessCull(float4 v0, float4 v1, float4 v2, float edgeLength, float maxDisplacement)
{
	float3 pos0 = TransformObjectToWorld(v0).xyz;
	float3 pos1 = TransformObjectToWorld(v1).xyz;
	float3 pos2 = TransformObjectToWorld(v2).xyz;
	float4 tess;

	if (WorldViewFrustumCull(pos0, pos1, pos2, maxDisplacement))
	{
		tess = 0.0f;
	}
	else
	{
		tess.x = CalcEdgeTessFactor(pos1, pos2, edgeLength);
		tess.y = CalcEdgeTessFactor(pos2, pos0, edgeLength);
		tess.z = CalcEdgeTessFactor(pos0, pos1, edgeLength);
		tess.w = (tess.x + tess.y + tess.z) / 3.0f;
	}
	return tess;
}

//=========== Debug framewire ==============

float3 CalcHardNormal(float3 posWS)
{
	float3 dpdx = ddx(posWS);
	float3 dpdy = ddy(posWS);
	float3 hardNormal = normalize(cross(dpdy, dpdx));

	return hardNormal;
}

float3 CalculateDistToCenter(float4 v0, float4 v1, float4 v2) {
	// points in screen space
	float2 ss0 = _ScreenParams.xy * v0.xy / v0.w;
	float2 ss1 = _ScreenParams.xy * v1.xy / v1.w;
	float2 ss2 = _ScreenParams.xy * v2.xy / v2.w;

	// edge vectors
	float2 e0 = ss2 - ss1;
	float2 e1 = ss2 - ss0;
	float2 e2 = ss1 - ss0;

	// area of the triangle
	float area = abs(e1.x * e2.y - e1.y * e2.x);

	// values based on distance to the center of the triangle
	float dist0 = area / length(e0);
	float dist1 = area / length(e1);
	float dist2 = area / length(e2);

	return float3(dist0, dist1, dist2);
}



#endif // TESSELLATION_CGINC_INCLUDED