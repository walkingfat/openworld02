﻿Shader "WalkingFat/Test/PaintOutlineWeight"
{
	Properties
	{
		_ShowVertexColor("Show Vertex Color", range(0,1)) = 0
		_BaseMap("Base Map", 2D) = "white" {}

		_Tint("Tint", Color) = (1,1,1,1)
		_Cutoff("Cutoff", Range(0.0, 1.0)) = 0.5

		_ShadowColor("Shadow Color", Color) = (0.5,0.5,0.5,1)
		_UnlitThreshold("Unlit Threshold", Range(0,1)) = 0.1
		_ShadowThreshold("Shadow Threshold", Range(0,1)) = 0.1

		_Specular("Specular", Range(0,1)) = 0.5
		_SpecularRamp("Specular Ramp", 2D) = "white"{}

		_OutlineColor("Outline Color", Color) = (0,0,0,1)
		_OutlineWidth("Outline Width", Range(0.0, 0.2)) = 0.02
	}

	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True" }
		LOD 300

		Pass{
			Name "Outline"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			Cull Front

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment


			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float3 normalWS : TEXCOORD0;
				//float3 viewDirWS : TEXCOORD1;
				//float3 positionWS : TEXCOORD2;
				//float4 positionNDC : TEXCOORD3;
				float4 positionCS : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
				float4 _OutlineColor;
				float _OutlineWidth;
			CBUFFER_END

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				input.positionOS.xyz += _OutlineWidth * input.normalOS * input.color.r;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

				//output.normalWS = normalize(normalInput.normalWS);
				//output.viewDirWS = normalize(GetCameraPositionWS() - vertexInput.positionWS);

				//output.positionWS = TransformObjectToWorld(input.positionOS.xyz);

				//output.positionWS.xyz += _SlpashDist * output.normalWS * distWS * _CameraTan * _EqualWidthRate;

				output.positionCS = TransformWorldToHClip(vertexInput.positionWS);
				//output.positionNDC = ComputeScreenPos(output.positionCS);

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);


				return _OutlineColor;
			}
			ENDHLSL
		}

		Pass{
			Name "ToonChar"
			//Tags { "LightMode" = "UniversalForward" }
			
			ZWrite On

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ToonPassVertex
			#pragma fragment ToonPassFragment

			//#include "ReceiveRainPass.hlsl"

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Assets/Game/Shader/Common/DitherTransparency.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float3 normalWS : TEXCOORD0;
				float4 positionNDC : TEXCOORD1;
				float4 uv : TEXCOORD2;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 3);
				float4 fogFactorAndVertexLight : TEXCOORD4;
				float4 shadowCoord : TEXCOORD5;
				float3 positionWS : TEXCOORD6;
				float4 vertexColor: TEXCOORD7;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float _Specular;
			float4 _Tint;
			float4 _ShadowColor;
			float _UnlitThreshold;
			float _ShadowThreshold;
			float _TransparencyDist;
			float4 _BaseMap_ST;
			float4 _BumpMap_ST;
			float _Cutoff;
			float _ShowVertexColor;
			CBUFFER_END

			// global
			half4 _AmbientColor;
			float3 _CharPosWorld;

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);

			Varyings ToonPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS);
				//half3 viewDirWS = normalize(GetCameraPositionWS() - vertexInput.positionWS);
				half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				output.uv.xy = TRANSFORM_TEX(input.texcoord, _BaseMap);

				output.normalWS = normalInput.normalWS;

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
				output.shadowCoord = GetShadowCoord(vertexInput);

				output.positionNDC = vertexInput.positionNDC;
				output.positionCS = vertexInput.positionCS;
				output.positionWS = vertexInput.positionWS;
				output.vertexColor = input.color;

				return output;
			}

			void InitializeInputData(Varyings input, out InputData inputData)
			{
				inputData = (InputData)0;

				inputData.normalWS = NormalizeNormalPerPixel(input.normalWS);

				inputData.viewDirectionWS.xyz = normalize(GetCameraPositionWS() - input.positionWS); 

				inputData.shadowCoord = input.shadowCoord;

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 ToonPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 baseTex = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv.xy);

				InputData inputData;
				InitializeInputData(input, inputData);

				// dir
				half3 viewDir = inputData.viewDirectionWS;

				Light mainLight = GetMainLight(inputData.shadowCoord);
				MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

				half3 lightDir = mainLight.direction;

				// Common
				half2 screenPos = input.positionNDC.xy / input.positionNDC.w;

				half NdotL = max(0.0, dot(input.normalWS, lightDir));
				half NWdotL = max(0.0, dot(inputData.normalWS, lightDir));

				half NdotV = max(0.0, dot(input.normalWS, inputData.viewDirectionWS));
				half NWdotV = max(0.0, dot(inputData.normalWS, inputData.viewDirectionWS));

				half screenRatio = _ScreenParams.x / _ScreenParams.y;

				// attenuation
				half attenuation = mainLight.distanceAttenuation * mainLight.shadowAttenuation;
				attenuation = smoothstep(0.0 + _UnlitThreshold, 0.3 + _UnlitThreshold, attenuation);

				// side rim
				half falloff = clamp(1.0 - abs(NdotV), 0.02, 0.98);

				half rimDot = saturate(0.5 * (dot(input.normalWS, -lightDir + half3(1, 0, 0)) + 1.5));
				falloff = saturate(rimDot * falloff);
				half rimValue = falloff;// smoothstep(0.4, 1, falloff);


				// Cel
				half celValue = smoothstep(0 + _UnlitThreshold, 0.1 + _UnlitThreshold, NWdotL);
				half litValue = celValue * attenuation;
				half3 litCol = lerp(_ShadowColor.xyz, 1, litValue) * mainLight.color + _AmbientColor * 0.2;

				half4 color = half4(baseTex.xyz * litCol * 3, 1.0);

				//half4 a = GetDitherTransparencyWithDistance(_TransparencyDist, baseTex.a * _Cutoff, input.positionNDC, input.color.x);
				//clip(a);


				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				color.a = 1;


				half4 finalCol = lerp(color, input.vertexColor, _ShowVertexColor);

				return finalCol;
			}
			ENDHLSL
		}

		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
