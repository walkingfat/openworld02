﻿Shader "WalkingFat/Foliage/FloatPlants"
{
	Properties
	{
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_Smoothness("Smoothness", Range(0,1)) = 0.5
		_Specular("Specular", Range(0,1)) = 0.0
		_SSSRate("SSS Rate", Range(0,1)) = 0.0
		_Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5

		// grass hit obstacle
		_ObstacleEffectHeightRange("Obstacle Effect Height Range", Range(0.0, 4.0)) = 2
		_ObstacleEffectStrength("Obstacle Effect Strength", range(0.1, 2.0)) = 2

		// shake with wind
		_ShakeSpeed("Shake speed", Float) = 1
		_ShakeRate("Shake Rate", Float) = 1

		// fade
		_FadeOutRange("Fade Out Range", Range(0.0, 5.0)) = 3
		_FadeOutDist("Fade Out Distance", Float) = 15
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300

		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			//#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			//#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			//#pragma multi_compile _ _SHADOWS_SOFT
			//#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			//#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			//#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "FoliageCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv                       : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float4 positionWS : TEXCOORD2;
				float3 normalWS : TEXCOORD3;
				float3 viewDir : TEXCOORD4;
				half4 fogFactorAndVertexLight : TEXCOORD5;
				float4 shadowCoord : TEXCOORD6;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _BaseMap_ST;
			float _Cutoff;
			// obstacles
			float _ObstacleCount;
			float4 _ObstacleParams[10];
			// grass bend
			float _ObstacleEffectHeightRange, _ObstacleEffectStrength;
			// wind
			float _ShakeSpeed, _ShakeRate;
			// fade
			float _FadeOutDist, _FadeOutRange;
			// light
			float _Specular, _Smoothness, _SSSRate;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;
			float3 _LightDirection;

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

				half4 posAndFade = GetWaveOffsetWSAndFadeScale(input.positionOS, _ObstacleCount, _ObstacleParams,
					_ObstacleEffectHeightRange, _ObstacleEffectStrength, _ShakeSpeed, _ShakeRate, 
					_FadeOutDist, _FadeOutRange, _CharPosWorld);

				half3 positionWS = posAndFade.xyz;
				half fadeScale = posAndFade.w;

				// initialize input data
				VertexPositionInputs vertexInput;

				vertexInput.positionWS = positionWS;
				vertexInput.positionVS = TransformWorldToView(vertexInput.positionWS);
				vertexInput.positionCS = TransformWorldToHClip(vertexInput.positionWS);

				float4 ndc = vertexInput.positionCS * 0.5f;
				vertexInput.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				vertexInput.positionNDC.zw = vertexInput.positionCS.zw;

				output.positionWS.xyz = vertexInput.positionWS;
				output.positionWS.w = fadeScale;
				output.positionCS = vertexInput.positionCS;

				half3 normalOS = normalize(input.normalOS * 0.0 + half3(0, 1, 0));
				VertexNormalInputs normalInput = GetVertexNormalInputs(normalOS, input.tangentOS);
				output.normalWS = normalInput.normalWS;

				output.viewDir = GetCameraPositionWS() - vertexInput.positionWS;

				half3 vertexLight = VertexLighting(output.positionWS, output.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);

				output.shadowCoord = GetShadowCoord(vertexInput);

				return output;
			}

			void InitializeInputData(Varyings input,  out InputData inputData)
			{
				inputData = (InputData)0;
				inputData.positionWS = input.positionWS;
				inputData.normalWS = NormalizeNormalPerPixel(input.normalWS);
				inputData.viewDirectionWS = SafeNormalize(input.viewDir);

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float2 uv = input.uv;
				half4 baseCol = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);

				half3 diffuse = baseCol.rgb;
				half specular = _Specular;
				half smoothness = _Smoothness;
				half sss = _SSSRate;
				half3 emission = half3(0, 0, 0);

				half alpha = baseCol.a;
				clip(alpha * input.positionWS.w - _Cutoff);

				InputData inputData;
				InitializeInputData(input, inputData);

				half4 color = GrassLighting(inputData, diffuse, specular, smoothness, sss, emission, alpha);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				return color;
			}
			ENDHLSL
		}
		
		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/CommonMaterial.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
			#include "FoliageCore.hlsl"

			CBUFFER_START(UnityPerMaterial)
			float4 _BaseMap_ST;
			float _Cutoff;
			// obstacles
			float _ObstacleCount;
			float4 _ObstacleParams[10];
			// grass bend
			float _ObstacleEffectHeightRange, _ObstacleEffectStrength;
			// wind
			float _ShakeSpeed, _ShakeRate;
			// fade
			float _FadeOutDist, _FadeOutRange;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;
			float3 _LightDirection;

			struct Attributes
			{
				float4 positionOS   : POSITION;
				float3 normalOS     : NORMAL;
				float2 texcoord     : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv           : TEXCOORD0;
				float4 positionCS   : SV_POSITION;
			};

			Varyings ShadowPassVertex(Attributes input)
			{
				Varyings output;
				UNITY_SETUP_INSTANCE_ID(input);

				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

				half4 posAndFade = GetWaveOffsetWSAndFadeScale(input.positionOS, _ObstacleCount, _ObstacleParams,
					_ObstacleEffectHeightRange, _ObstacleEffectStrength, _ShakeSpeed, _ShakeRate,
					_FadeOutDist, _FadeOutRange, _CharPosWorld);

				half3 positionWS = posAndFade.xyz;

				//--------------------------------
				float3 normalWS = TransformObjectToWorldNormal(input.normalOS);

				float4 positionCS = TransformWorldToHClip(ApplyShadowBias(positionWS, normalWS, _LightDirection));
#if UNITY_REVERSED_Z
				positionCS.z = min(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
#else
				positionCS.z = max(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
#endif

				output.positionCS = positionCS;

				return output;
			}

			half4 ShadowPassFragment(Varyings input) : SV_TARGET
			{
				half alpha = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv).a;
				clip(alpha - _Cutoff);

				return 0;
			}

			ENDHLSL
		}


		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			Cull Off

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#pragma multi_compile_instancing

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "FoliageCore.hlsl"

			struct Attributes
			{
				float4 positionOS     : POSITION;
				float2 texcoord     : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv           : TEXCOORD0;
				float4 positionCS   : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
				float4 _BaseMap_ST;
			float _Cutoff;
			// obstacles
			float _ObstacleCount;
			float4 _ObstacleParams[10];
			// grass bend
			float _ObstacleEffectHeightRange, _ObstacleEffectStrength;
			// wind
			float _ShakeSpeed, _ShakeRate;
			// fade
			float _FadeOutDist, _FadeOutRange;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;

			Varyings DepthOnlyVertex(Attributes input)
			{
				Varyings output = (Varyings)0;
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

				half4 posAndFade = GetWaveOffsetWSAndFadeScale(input.positionOS, _ObstacleCount, _ObstacleParams,
					_ObstacleEffectHeightRange, _ObstacleEffectStrength, _ShakeSpeed, _ShakeRate,
					_FadeOutDist, _FadeOutRange, _CharPosWorld);

				half3 positionWS = posAndFade.xyz;

				//--------------------------------
				output.positionCS = TransformWorldToHClip(positionWS);


				return output;
			}

			half4 DepthOnlyFragment(Varyings input) : SV_TARGET
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half alpha = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv).a;

				clip(alpha - _Cutoff);

				return 0;
			}
			ENDHLSL
		}
	}
		FallBack "Hidden/InternalErrorShader"
}
