﻿Shader "WalkingFat/Foliage/TreeLeaves"
{
	Properties
	{
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_Smoothness("Smoothness", Range(0,1)) = 0.5
		_Specular("Specular", Range(0,1)) = 0.0
		_Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5

		_Tint("Tint Color", Color) = (1 ,1 ,1 ,1)

		// shake with wind
		_ShakeSpeed("Shake speed", Float) = 1
		_ShakeRate("Shake Rate", Float) = 1
		_WindDirRate("Wind Direction Rate", float) = 0.5
		
		_RainNoiseMap("Rain Noise Map)", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 200

		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			//#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			//#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			//#pragma multi_compile _ _SHADOWS_SOFT
			//#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			//#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			//#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "FoliageCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float4 color : COLOR;
				float4 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 waveUV : TEXCOORD1;
				float2 lightmapUV : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD2;
				float3 normalWS : TEXCOORD3;
				float3 viewDir : TEXCOORD4;
				half4 fogFactorAndVertexLight : TEXCOORD5;
				float4 shadowCoord : TEXCOORD6;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			
			CBUFFER_START(UnityPerMaterial)
			// basic
			float4 _BaseMap_ST;
			float4 _Tint;
			float _Cutoff;
			// wind
			float _ShakeSpeed, _ShakeRate, _WindDirRate;
			// light
			float _Specular, _Smoothness;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			TEXTURE2D(_RainNoiseMap);						SAMPLER(sampler_RainNoiseMap);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;
			float _RainRate;

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

				// bend with Wind
				half3 posWorld = TransformObjectToWorld(input.positionOS);
				half3 axisPosWorld = TransformObjectToWorld(half3(0, input.positionOS.y, 0));
				half axisWorldY = TransformObjectToWorld(half3(0, 0, 0)).y;
				half axisDistWorldY = max(0, axisPosWorld.y - axisWorldY);
				half bendRateY = pow(input.waveUV.y, 2);
				half3 offsetValue = half3(0, 0, 0);

				// wind wave
				half bendTime = _Time.x - bendRateY * 0.01;
				half randomTime = bendTime + axisPosWorld.x + axisPosWorld.z;

				half waveValue = sin(randomTime * 34.15 + input.waveUV.x) * sin(randomTime * 22.45 + input.waveUV.x);
				waveValue = waveValue * 0.5 + 0.5;
				waveValue = lerp(1, waveValue, _WindParams.z);

				// random Wave
				const half4 shakeXSize = half4 (0.048, 0.06, 0.24, 0.096);
				const half4 shakeZSize = half4 (0.024, 0.08, 0.08, 0.2);

				half4 shakeXmove = half4 (0.024, 0.04, -0.12, 0.096);
				half4 shakeZmove = half4 (0.006, 0.02, -0.02, 0.1);

				half4 shake;
				shake = axisPosWorld.x * shakeXSize;
				shake += axisPosWorld.z * shakeZSize;

				half shakeSpeed = _ShakeSpeed + _WindDirRate * 10;
				shake += bendTime * shakeSpeed + (axisPosWorld.x + axisPosWorld.z + axisWorldY) * 100;

				half4 s, c;
				shake = frac(shake);
				FastSinCos(shake, s, c);

				half3 shakeOffset = half3 (0, 0, 0);

				half waveStrength = _WindParams.z * waveValue;
				half windDirX = _WindParams.x * waveStrength;
				half windDirZ = _WindParams.y * waveStrength;

				shakeOffset.x = dot(s * _ShakeRate, shakeXmove * windDirX);
				shakeOffset.z = dot(c * _ShakeRate, shakeZmove * windDirZ);

				// random shake
				shake += posWorld.x + posWorld.y + posWorld.z;
				shake = frac(shake);
				FastSinCos(shake, s, c);
				shakeOffset.x += dot(s * _ShakeRate * (1 + _WindParams.z), shakeXmove * windDirX) * input.color.x;
				shakeOffset.z += dot(c * _ShakeRate * (1 + _WindParams.z), shakeZmove * windDirZ) * input.color.x;

				half3 windDirOffset = half3 (windDirX * _WindDirRate, 0, windDirZ * _WindDirRate);

				half3 waveForce = shakeOffset + windDirOffset;

				offsetValue.xz -= waveForce.xz * bendRateY;

				// caculate gravity
				half offsetDist = distance(half2(0, 0), offsetValue.xz);
				half tempL = pow(offsetDist * offsetDist + axisDistWorldY * axisDistWorldY, 0.5) + 0.0001;
				half tempR = axisDistWorldY / tempL;
				offsetValue.xz *= tempR;
				offsetValue.y = axisDistWorldY * (tempR - 1);

				half3 positionWS = TransformObjectToWorld(input.positionOS);
				positionWS += offsetValue;

				// initialize input data
				VertexPositionInputs vertexInput;

				vertexInput.positionWS = positionWS;
				vertexInput.positionVS = TransformWorldToView(vertexInput.positionWS);
				vertexInput.positionCS = TransformWorldToHClip(vertexInput.positionWS);

				float4 ndc = vertexInput.positionCS * 0.5f;
				vertexInput.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				vertexInput.positionNDC.zw = vertexInput.positionCS.zw;

				output.positionWS = vertexInput.positionWS;
				output.positionCS = vertexInput.positionCS;

				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);
				output.normalWS = normalInput.normalWS;

				output.viewDir = GetCameraPositionWS() - vertexInput.positionWS;

				half3 vertexLight = VertexLighting(output.positionWS.xyz, output.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);

				output.shadowCoord = GetShadowCoord(vertexInput);

				return output;
			}

			void InitializeInputData(Varyings input, out InputData inputData)
			{
				inputData = (InputData)0;
				inputData.positionWS = input.positionWS.xyz;
				inputData.normalWS = NormalizeNormalPerPixel(input.normalWS.xyz);
				inputData.viewDirectionWS = SafeNormalize(input.viewDir);

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 LitPassFragment(Varyings input, half facing : VFACE) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				//return facing > 0 ? 0 : 1;
				//input.normalWS.xyz *= facing * 2 - 1;

				//input.normalWS.xyz = normalize(input.normalWS.xyz + half3(0,0.5,0));

				//return saturate(dot(input.viewDir, input.normalWS));

				float2 uv = input.uv.xy;

				// rain noise
				float2 rainUv = uv * 4 + float2(_Time.z, _Time.z);
				half rainNoiseValue = SAMPLE_TEXTURE2D(_RainNoiseMap, sampler_RainNoiseMap, rainUv).r;
				half2 rainData = half2(rainNoiseValue, _RainRate);

				// data
				half4 baseCol = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);
				half heightGradient = uv.y * uv.y * uv.y;

				half3 diffuse = _Tint.rgb * baseCol.rgb;
				half specular = 0;//_Specular * baseCol.y + heightGradient * 0.2;
				half smoothness = _Smoothness;
				half sss = 1;//heightGradient * 0.4;
				half3 emission = half3(0,0,0);

				half alpha = baseCol.a;
				clip(alpha - _Cutoff);

				InputData inputData;
				InitializeInputData(input, inputData);

				half4 color = FoliageLighting(inputData, diffuse, specular, smoothness, sss, emission, alpha);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				return color;
			}
			ENDHLSL
		}
		
		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			Cull Off

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#pragma multi_compile_instancing

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "FoliageCore.hlsl"

			struct Attributes
			{
				float4 positionOS     : POSITION;
				float4 color : COLOR;
				float2 texcoord     : TEXCOORD0;
				float2 waveUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv           : TEXCOORD0;
				float4 positionCS   : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};


			CBUFFER_START(UnityPerMaterial)
			// basic
			float4 _BaseMap_ST;
			float _Cutoff;
			// wind
			float _ShakeSpeed, _ShakeRate, _WindDirRate;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;

			Varyings DepthOnlyVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
				
				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

				// bend with Wind
				half3 posWorld = TransformObjectToWorld(input.positionOS);
				half3 axisPosWorld = TransformObjectToWorld(half3(0, input.positionOS.y, 0));
				half axisWorldY = TransformObjectToWorld(half3(0, 0, 0)).y;
				half axisDistWorldY = max(0, axisPosWorld.y - axisWorldY);
				half bendRateY = pow(input.waveUV.y, 2);
				half3 offsetValue = half3(0, 0, 0);
				
				// wind wave
				half bendTime = _Time.x - bendRateY * 0.01;
				half randomTime = bendTime + axisPosWorld.x + axisPosWorld.z;
				
				half waveValue = sin(randomTime * 34.15 + input.waveUV.x) * sin(randomTime * 22.45 + input.waveUV.x);
				waveValue = waveValue * 0.5 + 0.5;
				waveValue = lerp(1, waveValue, _WindParams.z);
				
				// random Wave
				const half4 shakeXSize = half4 (0.048, 0.06, 0.24, 0.096);
				const half4 shakeZSize = half4 (0.024, 0.08, 0.08, 0.2);

				half4 shakeXmove = half4 (0.024, 0.04, -0.12, 0.096);
				half4 shakeZmove = half4 (0.006, 0.02, -0.02, 0.1);

				half4 shake;
				shake = axisPosWorld.x * shakeXSize;
				shake += axisPosWorld.z * shakeZSize;

				half shakeSpeed = _ShakeSpeed + _WindDirRate * 10;
				shake += bendTime * shakeSpeed + (axisPosWorld.x + axisPosWorld.z + axisWorldY) * 100;

				half4 s, c;
				shake = frac(shake);
				FastSinCos(shake, s, c);

				half3 shakeOffset = half3 (0, 0, 0);

				half waveStrength = _WindParams.z * waveValue;
				half windDirX = _WindParams.x * waveStrength;
				half windDirZ = _WindParams.y * waveStrength;

				shakeOffset.x = dot(s * _ShakeRate, shakeXmove * windDirX);
				shakeOffset.z = dot(c * _ShakeRate, shakeZmove * windDirZ);

				// random shake
				shake += posWorld.x + posWorld.y + posWorld.z;
				shake = frac(shake);
				FastSinCos(shake, s, c);
				shakeOffset.x += dot(s * _ShakeRate * (1 + _WindParams.z), shakeXmove * windDirX) * input.color.x;
				shakeOffset.z += dot(c * _ShakeRate * (1 + _WindParams.z), shakeZmove * windDirZ) * input.color.x;

				half3 windDirOffset = half3 (windDirX * _WindDirRate, 0, windDirZ * _WindDirRate);

				half3 waveForce = shakeOffset + windDirOffset;

				offsetValue.xz -= waveForce.xz * bendRateY;

				// caculate gravity
				half offsetDist = distance(half2(0, 0), offsetValue.xz);
				half tempL = pow(offsetDist * offsetDist + axisDistWorldY * axisDistWorldY, 0.5) + 0.0001;
				half tempR = axisDistWorldY / tempL;
				offsetValue.xz *= tempR;
				offsetValue.y = axisDistWorldY * (tempR - 1);

				half3 positionWS = TransformObjectToWorld(input.positionOS);
				positionWS += offsetValue;

				// initialize input data
				VertexPositionInputs vertexInput;

				vertexInput.positionWS = positionWS;
				vertexInput.positionCS = TransformWorldToHClip(positionWS);

				output.positionCS = vertexInput.positionCS;

				return output;
			}

			half4 DepthOnlyFragment(Varyings input) : SV_TARGET
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float2 uv = input.uv.xy;
				half alpha = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv).a;
				clip(alpha - _Cutoff);

				return 0;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
