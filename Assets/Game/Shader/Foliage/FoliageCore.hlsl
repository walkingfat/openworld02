﻿#ifndef WALKINGFAT_FOLIAGE_CORE_INCLUDED
#define WALKINGFAT_FOLIAGE_CORE_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

void FastSinCos(half4 val, out half4 s, out half4 c)
{
	val = val * 6.408849 - 3.1415927;
	// powers for taylor series
	half4 r5 = val * val;
	half4 r6 = r5 * r5;
	half4 r7 = r6 * r5;
	half4 r8 = r6 * r5;
	half4 r1 = r5 * val;
	half4 r2 = r1 * r5;
	half4 r3 = r2 * r5;
	//Vectors for taylor's series expansion of sin and cos
	half4 sin7 = { 1, -0.16161616, 0.0083333, -0.00019841 };
	half4 cos8 = { -0.5, 0.041666666, -0.0013888889, 0.000024801587 };
	// sin
	s = val + r1 * sin7.y + r2 * sin7.z + r3 * sin7.w;
	// cos
	c = 1 + r5 * cos8.x + r6 * cos8.y + r7 * cos8.z + r8 * cos8.w;
}

// for float plants
half4 GetWaveOffsetWSAndFadeScale(half3 positionOS, half obstacleCount, float4 obstacleParams[10], 
	half offsetHeightRange, half offsetStrength, half shakeSpeed, half shakeRate, 
	half fadeOutDist, half fadeOutRange, half3 charPosWorld)
{
	half3 axisPosWorld = TransformObjectToWorld(half3(0, positionOS.y, 0));
	half axisWorldY = TransformObjectToWorld(half3(0, 0, 0)).y;
	half axisDistWorldY = max(0, axisPosWorld.y - axisWorldY);
	//half bendRateY = pow(axisDistWorldY, 1.05);
	//bendRateY = lerp(1, bendRateY, offsetWithHeight);
	half3 offsetValue = half3(0, 0, 0);

	// shake by wind ==========================================================

	const half4 shakeXSize = half4 (0.048, 0.06, 0.24, 0.096);
	const half4 shakeZSize = half4 (0.024, 0.08, 0.08, 0.2);

	half4 shakeXmove = half4 (0.024, 0.04, -0.12, 0.096);
	half4 shakeZmove = half4 (0.006, 0.02, -0.02, 0.1);

	half4 shake;
	shake = axisPosWorld.x * shakeXSize;
	shake += axisPosWorld.z * shakeZSize;

	shake += _Time.x * shakeSpeed + axisPosWorld.x + axisPosWorld.z + axisWorldY * 100;

	half4 s, c;
	shake = frac(shake);
	FastSinCos(shake, s, c);

	half3 shakeOffset = half3 (0, 0, 0);

	shakeOffset.x = dot(s * shakeRate, shakeXmove);
	shakeOffset.z = dot(c * shakeRate, shakeZmove);

	offsetValue.xz -= shakeOffset.xz;

	// ===========================================================================

	for (int n = 0; n < obstacleCount; n++) {

		// get char top and bottom pos as effect range in Y.
		half charTopY = obstacleParams[n].y + offsetHeightRange;
		half charBottomY = obstacleParams[n].y - offsetHeightRange;
		half charY = clamp(axisPosWorld.y, charBottomY, charTopY); // when Y is within the Range (charTopY to charBottomY)

		// get bend force by distance --------------------------
		half dist = distance(half3(obstacleParams[n].x, charY, obstacleParams[n].z), axisPosWorld.xyz); // get distance of char and vertices
		half effectRate = clamp(obstacleParams[n].w - dist, 0, obstacleParams[n].w); // get bend rate within 0 ~ max
		half3 bendDir = normalize(axisPosWorld.xyz - half3 (obstacleParams[n].x, axisPosWorld.y, obstacleParams[n].z)); // get bend dir
		half3 bendForce = bendDir * effectRate;

		// get final bend force
		half3 finalBendForce = mul((half3x3)unity_WorldToObject, bendForce * offsetStrength);

		// set bend force to vertices offset ================================================
		offsetValue.xz += finalBendForce.xz;// *bendRateY;
	}

	// fade out
	half distWorldXZ = distance(axisPosWorld.xz, charPosWorld.xz);
	half fadeScale = saturate((fadeOutDist - distWorldXZ) / fadeOutRange);

	// ---------------finish vertex offset------------------
	half3 positionWS = TransformObjectToWorld(positionOS);
	positionWS += offsetValue;

	return half4(positionWS, fadeScale);
}


// for near grass
half4 GetWaveOffsetWSAndFadeScale(half3 positionOS, half obstacleCount, float4 obstacleParams[10],
	half obstacleEffectHeightRange, half obstacleEffectStrength,
	texture2D waveTex , sampler sampler_GrassWaveTex,
	half shakeSpeed, half shakeRate, half windDirRate, half waveSpeed,
	half fadeOutDist, half fadeOutRange, half3 charPosWorld, half4 windParams)
{
	half3 axisPosWorld = TransformObjectToWorld(half3(0, positionOS.y, 0));
	half axisWorldY = TransformObjectToWorld(half3(0, 0, 0)).y;
	half axisDistWorldY = max(0, axisPosWorld.y - axisWorldY);
	half bendRateY = pow(axisDistWorldY, 1.05);
	half3 offsetValue = half3(0, 0, 0);

	// grass wave
	half2 pivot = float2(0.5, 0.5);

	half cosAngle = cos(-windParams.w);
	half sinAngle = sin(-windParams.w);
	half2x2 rot = half2x2(cosAngle, -sinAngle, sinAngle, cosAngle);

	half2 waveUV = axisPosWorld.xz * 0.015 - pivot;
	waveUV += windParams.xy * _Time.x * waveSpeed;
	waveUV = mul(rot, waveUV);
	waveUV += pivot;

	half waveValue = SAMPLE_TEXTURE2D_LOD(waveTex, sampler_GrassWaveTex, half4(waveUV, 1, 1), 0).r;
	waveValue = waveValue * 1.0 + 0.5;
	waveValue = lerp(1, waveValue, windParams.z);

	// shake by wind ==========================================================

	const half4 shakeXSize = half4 (0.048, 0.06, 0.24, 0.096);
	const half4 shakeZSize = half4 (0.024, 0.08, 0.08, 0.2);

	half4 shakeXmove = half4 (0.024, 0.04, -0.12, 0.096);
	half4 shakeZmove = half4 (0.006, 0.02, -0.02, 0.1);

	half4 shake;
	shake = axisPosWorld.x * shakeXSize;
	shake += axisPosWorld.z * shakeZSize;

	shake += _Time.x * shakeSpeed + axisPosWorld.x + axisPosWorld.z + axisWorldY * 100;

	half4 s, c;
	shake = frac(shake);
	FastSinCos(shake, s, c);

	half3 shakeOffset = half3 (0, 0, 0);

	half waveStrength = waveValue * windParams.z;
	half windDirX = windParams.x * waveStrength;
	half windDirZ = windParams.y * waveStrength;

	shakeOffset.x = dot(s * shakeRate, shakeXmove * windDirX);
	shakeOffset.z = dot(c * shakeRate, shakeZmove * windDirZ);

	half3 windDirOffset = half3 (windDirX * windDirRate, 0, windDirZ * windDirRate);

	half3 waveForce = shakeOffset + windDirOffset;

	offsetValue.xz -= waveForce.xz * bendRateY;

	// ===========================================================================

	for (int n = 0; n < obstacleCount; n++) {

		// get char top and bottom pos as effect range in Y.
		half charTopY = obstacleParams[n].y + obstacleEffectHeightRange;
		half charBottomY = obstacleParams[n].y - obstacleEffectHeightRange;
		half charY = clamp(axisPosWorld.y, charBottomY, charTopY); // when Y is within the Range (charTopY to charBottomY)

		// get bend force by distance --------------------------
		half dist = distance(half3(obstacleParams[n].x, charY, obstacleParams[n].z), axisPosWorld.xyz); // get distance of char and vertices
		half effectRate = clamp(obstacleParams[n].w - dist, 0, obstacleParams[n].w); // get bend rate within 0 ~ max
		half3 bendDir = normalize(axisPosWorld.xyz - half3 (obstacleParams[n].x, axisPosWorld.y, obstacleParams[n].z)); // get bend dir
		half3 bendForce = bendDir * effectRate;

		// get final bend force
		half3 finalBendForce = mul((half3x3)unity_WorldToObject, bendForce * obstacleEffectStrength);

		// set bend force to vertices offset ================================================
		offsetValue.xz += finalBendForce.xz * bendRateY;
	}

	// caculate gravity
	half offsetDist = distance(half2(0, 0), offsetValue.xz);
	half tempL = pow(offsetDist * offsetDist + axisDistWorldY * axisDistWorldY, 0.5) + 0.0001;
	half tempR = axisDistWorldY / tempL;
	offsetValue.xz *= tempR;
	offsetValue.y = axisDistWorldY * (tempR - 1);

	// fade out
	half distWorldXZ = distance(axisPosWorld.xz, charPosWorld.xz);
	half fadeScale = saturate((fadeOutDist - distWorldXZ) / fadeOutRange);

	// ---------------finish vertex offset------------------
	positionOS.xz *= fadeScale;

	half3 positionWS = TransformObjectToWorld(positionOS);
	positionWS += offsetValue;

	return half4(positionWS, fadeScale);
}

// for far grass
half4 GetWaveOffsetWSAndFadeScale(half3 positionOS,
	texture2D waveTex, sampler sampler_GrassWaveTex,
	half shakeSpeed, half shakeRate, half windDirRate, half waveSpeed,
	half fadeInDist, half fadeInRange, half fadeOutDist, half fadeOutRange,
	half3 charPosWorld, half4 windParams)
{
	half3 axisPosWorld = TransformObjectToWorld(half3(0, positionOS.y, 0));
	half axisWorldY = TransformObjectToWorld(half3(0, 0, 0)).y;
	half axisDistWorldY = max(0, axisPosWorld.y - axisWorldY);
	half bendRateY = pow(axisDistWorldY, 1.05);
	half3 offsetValue = half3(0, 0, 0);

	// grass wave
	half2 pivot = float2(0.5, 0.5);

	half cosAngle = cos(-windParams.w);
	half sinAngle = sin(-windParams.w);
	half2x2 rot = half2x2(cosAngle, -sinAngle, sinAngle, cosAngle);

	half2 waveUV = axisPosWorld.xz * 0.015 - pivot;
	waveUV += windParams.xy * _Time.x * waveSpeed;
	waveUV = mul(rot, waveUV);
	waveUV += pivot;

	half waveValue = SAMPLE_TEXTURE2D_LOD(waveTex, sampler_GrassWaveTex, half4(waveUV, 1, 1), 0).r;
	waveValue = waveValue * 1.0 + 0.5;
	waveValue = lerp(1, waveValue, windParams.z);

	// shake by wind ==========================================================

	const half4 shakeXSize = half4 (0.048, 0.06, 0.24, 0.096);
	const half4 shakeZSize = half4 (0.024, 0.08, 0.08, 0.2);

	half4 shakeXmove = half4 (0.024, 0.04, -0.12, 0.096);
	half4 shakeZmove = half4 (0.006, 0.02, -0.02, 0.1);

	half4 shake;
	shake = axisPosWorld.x * shakeXSize;
	shake += axisPosWorld.z * shakeZSize;

	shake += _Time.x * shakeSpeed + axisPosWorld.x + axisPosWorld.z + axisWorldY * 100;

	half4 s, c;
	shake = frac(shake);
	FastSinCos(shake, s, c);

	half3 shakeOffset = half3 (0, 0, 0);

	half waveStrength = waveValue * windParams.z;
	half windDirX = windParams.x * waveStrength;
	half windDirZ = windParams.y * waveStrength;

	shakeOffset.x = dot(s * shakeRate, shakeXmove * windDirX);
	shakeOffset.z = dot(c * shakeRate, shakeZmove * windDirZ);

	half3 windDirOffset = half3 (windDirX * windDirRate, 0, windDirZ * windDirRate);

	half3 waveForce = shakeOffset + windDirOffset;

	offsetValue.xz -= waveForce.xz * bendRateY;

	// caculate gravity
	half offsetDist = distance(half2(0, 0), offsetValue.xz);
	half tempL = pow(offsetDist * offsetDist + axisDistWorldY * axisDistWorldY, 0.5) + 0.0001;
	half tempR = axisDistWorldY / tempL;
	offsetValue.xz *= tempR;
	offsetValue.y = axisDistWorldY * (tempR - 1);

	// fade in and out
	half distWorldXZ = distance(axisPosWorld.xz, charPosWorld.xz);
	half fadeInRate = 1 - saturate((distWorldXZ - fadeInDist) / fadeInRange);

	float fadeOutRate = saturate((fadeOutDist - distWorldXZ) / fadeOutRange);
	fadeOutRate = pow(fadeOutRate, 0.7);
	fadeOutRate *= pow(1 - fadeInRate, 0.7);
	half fadeScale = 1 - fadeOutRate;

	// ---------------finish vertex offset------------------
	half3 positionWS = TransformObjectToWorld(positionOS);
	positionWS += offsetValue;

	return half4(positionWS, fadeScale);
}

half4 FoliageLighting(InputData inputData, half3 diffuse, half specular, half smoothness, half sss, half3 emission, half alpha)
{
	Light mainLight = GetMainLight(inputData.shadowCoord);
	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

	half atten = mainLight.distanceAttenuation * mainLight.shadowAttenuation;

	half NdotL = saturate(dot(mainLight.direction, inputData.normalWS));
	half NdotV = saturate(dot(inputData.viewDirectionWS, inputData.normalWS));

	half diff = NdotL* saturate(dot(mainLight.direction, normalize(inputData.normalWS + inputData.viewDirectionWS)));
	
	half NdotV2 = saturate(dot(-inputData.viewDirectionWS, normalize(inputData.normalWS + inputData.viewDirectionWS * 0.4)));

	half fresnel = 1.0 - max(0, NdotV);

	half3 diffuseColor = inputData.bakedGI + mainLight.color * diff * atten;
	half luminace = dot(mainLight.color, float3(0.299, 0.587, 0.114));

	// specular ----
	float specularPow = exp2((smoothness) * 5.0 + 1.0);
	float3 halfVec = SafeNormalize(float3(mainLight.direction) + float3(inputData.viewDirectionWS));
	half NdotH = saturate(dot(inputData.normalWS, halfVec));
	half specValue = pow(NdotH, specularPow) * specular;
	half3 specularColor = specValue * lerp(diffuse, mainLight.color, 0.1) * atten * luminace;
	// -------------

	// SSS
	half3 backLitDir = inputData.normalWS + mainLight.direction;
	half backSSS = saturate(dot(inputData.viewDirectionWS, -backLitDir));
	backSSS = saturate(pow(backSSS, 3) - NdotV2 * 10);
	half3 sssCol = backSSS * atten * lerp(diffuse, mainLight.color, 0.1) * sss * luminace;

	half3 finalColor = diffuseColor * diffuse;
	finalColor += specularColor;
	finalColor += sssCol;

	return half4(finalColor, alpha);
}

half4 GrassLighting(InputData inputData, half3 diffuse, half specular, half smoothness, half sss, half3 emission, half alpha)
{
	Light mainLight = GetMainLight(inputData.shadowCoord);
	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

	half atten = mainLight.distanceAttenuation * mainLight.shadowAttenuation;

	half NdotL = saturate(dot(mainLight.direction, inputData.normalWS));
	//half NdotV = saturate(dot(inputData.viewDirectionWS, inputData.normalWS));

	half diff = NdotL;

	//half NdotV2 = saturate(dot(-inputData.viewDirectionWS, normalize(inputData.normalWS + inputData.viewDirectionWS * 0.4)));

	//half fresnel = 1.0 - max(0, NdotV);

	half3 diffuseColor = inputData.bakedGI + mainLight.color * diff * atten;
	half luminace = dot(mainLight.color, float3(0.299, 0.587, 0.114));

	// specular ----
	float specularPow = exp2((smoothness) * 5.0 + 1.0);
	float3 halfVec = SafeNormalize(float3(mainLight.direction) + float3(inputData.viewDirectionWS));
	half NdotH = saturate(dot(inputData.normalWS, halfVec));
	half specValue = pow(NdotH, specularPow) * specular;
	half3 specularColor = specValue * lerp(diffuse, mainLight.color, 0.1) * atten * luminace;
	// -------------

	
	// SSS
	float3 backLitDir = inputData.normalWS + mainLight.direction;
	float backSSS = saturate(dot(inputData.viewDirectionWS, -backLitDir));
	//backSSS = pow(backSSS, 3);// saturate(pow(backSSS, 3) - NdotV2 * 10);
	half3 sssCol = backSSS * atten * lerp(diffuse, mainLight.color, 0.1) * sss * luminace;
	

	half3 finalColor = diffuseColor * diffuse;
	finalColor += specularColor;
	finalColor += sssCol;

	return half4(finalColor, alpha);
}

#endif