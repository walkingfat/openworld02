﻿Shader "WalkingFat/Foliage/DynamicGrassNear"
{
    Properties
    {
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_Smoothness("Smoothness", Range(0,1)) = 0.5
        _Specular("Specular", Range(0,1)) = 0.0

		_GrassWaveTex("Grass Wave Texture", 2D) = "white" {}

		_GrassColorTop("Grass Color Top", Color) = (1 ,1 ,1 ,1)
		_GrassColorBottom("Grass Color Bottom", Color) = (1 ,1 ,1 ,1)

		// grass hit obstacle
		_ObstacleEffectHeightRange("Obstacle Effect Height Range", Range(0.0, 4.0)) = 2.0
		_ObstacleEffectStrength("Obstacle Effect Strength", range(0.1, 2.0)) = 2

		// shake with wind
		_ShakeSpeed("Shake speed", Float) = 1
		_ShakeRate("Shake Rate", Float) = 1
		_WindDirRate("Wind Direction Rate", Float) = 0.5
		_WaveSpeed("Wave Speed", Float) = 6

		// fade
		_FadeOutRange("Fade Out Range", Range(0.0, 5.0)) = 3
		[HideInInspector]_FadeOutDist("Fade Out Distance", Float) = 15
    }
    SubShader
    {
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
        LOD 300
		
		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }
			
			ZWrite On
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			//#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			//#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			//#pragma multi_compile _ _SHADOWS_SOFT
			//#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			//#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			//#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "FoliageCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv                       : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD2;
				float3 normalWS : TEXCOORD3;
				float3 viewDir : TEXCOORD4;
				half4 fogFactorAndVertexLight : TEXCOORD5;
				float4 shadowCoord : TEXCOORD6;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			// basic
			float4 _GrassColorTop, _GrassColorBottom;
			float4 _BaseMap_ST;
			// obstacles
			float _ObstacleCount;
			float4 _ObstacleParams[10];
			// grass bend
			float _BendAmount, _EffectTopOffset, _ObstacleEffectHeightRange, _ObstacleEffectStrength;
			// wind
			float _ShakeSpeed, _ShakeRate, _WindDirRate, _WaveSpeed;
			// light
			float _Specular, _Smoothness;
			// fade
			float _FadeOutDist, _FadeOutRange;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			TEXTURE2D(_GrassWaveTex);						SAMPLER(sampler_GrassWaveTex);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
				//input.positionOS.x += output.uv.y * 0.1;

				half4 posAndFade = GetWaveOffsetWSAndFadeScale(input.positionOS, _ObstacleCount, _ObstacleParams,
					_ObstacleEffectHeightRange, _ObstacleEffectStrength,
					_GrassWaveTex, sampler_GrassWaveTex,
					_ShakeSpeed, _ShakeRate, _WindDirRate, _WaveSpeed,
					_FadeOutDist, _FadeOutRange, _CharPosWorld, _WindParams);

				half3 positionWS = posAndFade.xyz;

				// initialize input data
				VertexPositionInputs vertexInput;

				vertexInput.positionWS = positionWS;
				vertexInput.positionVS = TransformWorldToView(vertexInput.positionWS);
				vertexInput.positionCS = TransformWorldToHClip(vertexInput.positionWS);
				
				half4 ndc = vertexInput.positionCS * 0.5f;
				vertexInput.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				vertexInput.positionNDC.zw = vertexInput.positionCS.zw;
				
				output.positionWS = vertexInput.positionWS;
				output.positionCS = vertexInput.positionCS;

				half3 normalOS = half3(0, 1, 0);// normalize(input.normalOS * 0.0 + half3(0, 1, 0));
				VertexNormalInputs normalInput = GetVertexNormalInputs(normalOS, input.tangentOS);
				output.normalWS = normalInput.normalWS;

				output.viewDir = GetCameraPositionWS() - vertexInput.positionWS;

				half3 vertexLight = VertexLighting(output.positionWS, output.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);

				output.shadowCoord = GetShadowCoord(vertexInput);

				return output;
			}

			void InitializeInputData(Varyings input,  out InputData inputData)
			{
				inputData = (InputData)0;
				inputData.positionWS = input.positionWS;
				inputData.normalWS = NormalizeNormalPerPixel(input.normalWS);
				inputData.viewDirectionWS = SafeNormalize(input.viewDir);

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half spec = saturate(input.uv.x * 2 + input.uv.y * 0.5 - 1);
				spec = min(1, spec * spec * spec * 6) * input.uv.y;
				half heightGradient = input.uv.y * input.uv.y * input.uv.y;

				half3 albedo = lerp(_GrassColorBottom, _GrassColorTop, input.uv.y).rgb;
				half specular = spec * _Specular + heightGradient * 0.2;
				half smoothness = _Smoothness * heightGradient;
				half sss = heightGradient * 0.4;
				
				half alpha = 1;

				InputData inputData;
				InitializeInputData(input, inputData);

				Light mainLight = GetMainLight(inputData.shadowCoord);
				half3 emission = lerp(mainLight.color, albedo, 0.9) * heightGradient * 0.1;

				half4 color = UniversalFragmentPBR(inputData, albedo, 0, 0, smoothness, 1, emission, alpha);
				//half4 color = GrassLighting(inputData, albedo, specular, smoothness, sss, emission, alpha);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				return color;
			}
			ENDHLSL
		}
		
		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			Cull Off

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#pragma multi_compile_instancing

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "FoliageCore.hlsl"

			struct Attributes
			{
				float4 positionOS     : POSITION;
				//float2 texcoord     : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				//float2 uv           : TEXCOORD0;
				float4 positionCS   : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			// basic
			//float4 _BaseMap_ST;
			//float _Cutoff;
			// obstacles
			float _ObstacleCount;
			float4 _ObstacleParams[10];
			// grass bend
			float _BendAmount, _EffectTopOffset, _ObstacleEffectHeightRange, _ObstacleEffectStrength;
			// wind
			float _ShakeSpeed, _ShakeRate, _WindDirRate, _WaveSpeed;
			// fade
			float _FadeOutDist, _FadeOutRange;
			CBUFFER_END

			//TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			TEXTURE2D(_GrassWaveTex);						SAMPLER(sampler_GrassWaveTex);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;

			Varyings DepthOnlyVertex(Attributes input)
			{
				Varyings output = (Varyings)0;
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				//half2 uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
				//input.positionOS.x += uv.y * 0.1;

				half4 posAndFade = GetWaveOffsetWSAndFadeScale(input.positionOS, _ObstacleCount, _ObstacleParams,
					_ObstacleEffectHeightRange, _ObstacleEffectStrength,
					_GrassWaveTex, sampler_GrassWaveTex,
					_ShakeSpeed, _ShakeRate, _WindDirRate, _WaveSpeed,
					_FadeOutDist, _FadeOutRange, _CharPosWorld, _WindParams);

				half3 positionWS = posAndFade.xyz;

				output.positionCS = TransformWorldToHClip(positionWS);

				return output;
			}

			half4 DepthOnlyFragment(Varyings input) : SV_TARGET
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				return 0;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
