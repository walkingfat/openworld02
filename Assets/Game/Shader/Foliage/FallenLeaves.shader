﻿Shader "WalkingFat/Foliage/FallenLeaves"
{
	Properties
	{
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_Smoothness("Smoothness", Range(0,1)) = 0.5
		_Specular("Specular", Range(0,1)) = 0.0
		_SSSRate("SSS Rate", Range(0,1)) = 0.0
		_Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5

		// fade
		_FadeOutRange("Fade Out Range", Range(0.0, 5.0)) = 3
		[HideInInspector]_FadeOutDist("Fade Out Distance", Float) = 15
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300

		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			//#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			//#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			//#pragma multi_compile _ _SHADOWS_SOFT
			//#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			//#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			//#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "FoliageCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv                       : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD2;
				float3 normalWS : TEXCOORD3;
				float3 viewDir : TEXCOORD4;
				half4 fogFactorAndVertexLight : TEXCOORD5;
				float4 shadowCoord : TEXCOORD6;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			float4 _BaseMap_ST;

			// light
			float _Specular, _Smoothness, _SSSRate, _Cutoff;
			// fade
			float _FadeOutDist, _FadeOutRange;
			// global
			float3 _CharPosWorld;


			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

				half3 axisPosWorld = TransformObjectToWorld(half3(0, input.positionOS.y, 0));

				// fade out
				half distWorldXZ = distance(axisPosWorld.xz, _CharPosWorld.xz);
				half fadeScale = saturate((_FadeOutDist - distWorldXZ) / _FadeOutRange);

				// ---------------finish vertex offset------------------
				input.positionOS.xyz *= fadeScale;
				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

				output.positionWS = vertexInput.positionWS;
				//output.positionWS.w = fadeScale;
				output.positionCS = vertexInput.positionCS;

				half3 normalOS = normalize(input.normalOS * 0.0 + half3(0, 1, 0));
				VertexNormalInputs normalInput = GetVertexNormalInputs(normalOS, input.tangentOS);
				output.normalWS = normalInput.normalWS;

				output.viewDir = GetCameraPositionWS() - vertexInput.positionWS;

				half3 vertexLight = VertexLighting(output.positionWS, output.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);

				output.shadowCoord = GetShadowCoord(vertexInput);

				return output;
			}

			void InitializeInputData(Varyings input,  out InputData inputData)
			{
				inputData = (InputData)0;
				inputData.positionWS = input.positionWS;
				inputData.normalWS = NormalizeNormalPerPixel(input.normalWS);
				inputData.viewDirectionWS = SafeNormalize(input.viewDir);

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float2 uv = input.uv;
				half4 baseCol = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);

				half3 diffuse = baseCol.rgb;
				half specular = _Specular;
				half smoothness = _Smoothness;
				half sss = _SSSRate;
				half3 emission = half3(0, 0, 0);

				half alpha = baseCol.a;
				clip(alpha - _Cutoff);

				InputData inputData;
				InitializeInputData(input, inputData);

				half4 color = GrassLighting(inputData, diffuse, specular, smoothness, sss, emission, alpha);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				return color;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
