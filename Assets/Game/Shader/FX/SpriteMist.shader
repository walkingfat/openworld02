﻿Shader "WalkingFat/FX/SpriteMist"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_DepthFactor("Depth Factor", float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent+3" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
        LOD 100

        Pass
        {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			Blend SrcAlpha OneMinusSrcAlpha

			//ZWrite On

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			//#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			//#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			//#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			//#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			//#pragma multi_compile _ _SHADOWS_SOFT
			//#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			//#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			//#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "FXCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv : TEXCOORD0;
				float4 positionCS : SV_POSITION;
				float4 positionWS : TEXCOORD1;
				float4 positionNDC : TEXCOORD2;
				float4 normalWS : TEXCOORD3;
				float3 viewDirWS : TEXCOORD4;
				//float4 lit : TEXCOORD4;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 5);
				float4 fogFactorAndVertexLight : TEXCOORD6;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
            float4 _MainTex_ST;
			float _DepthFactor;
			CBUFFER_END

			TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);
			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);

			// global
			float _DayNightTime;
			float _CloudyRate;

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				// billboard mesh towards camera
				float3 vpos = mul((float3x3)unity_ObjectToWorld, input.positionOS.xyz);
				float4 worldCoord = float4(unity_ObjectToWorld._m03, unity_ObjectToWorld._m13, unity_ObjectToWorld._m23, 1);
				float4 viewPos = mul(UNITY_MATRIX_V, worldCoord) + float4(vpos, 0);
				float4 outPos = mul(UNITY_MATRIX_P, viewPos);

				output.uv = TRANSFORM_TEX(input.texcoord, _MainTex);

				half3 camPosWS = GetCameraPositionWS();

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

				output.positionWS.xyz = TransformObjectToWorld(half3(0, 0, 0)); // center pos Y
				output.positionWS.w = saturate(pow(distance(camPosWS, output.positionWS) * 0.02, 2) - 1); // fog distance fade
				output.normalWS.xyz = normalize(camPosWS - output.positionWS + half3(0, 50, 0));
				output.positionCS = outPos;
				
				float4 ndc = output.positionCS * 0.5f;
				output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				output.positionNDC.zw = output.positionCS.zw;

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				half3 vertexLight = VertexLighting(vertexInput.positionWS, output.normalWS.xyz);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);
				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);

				// back SSS
				Light mainLight = GetMainLight();
				half backSSS = saturate(dot(output.normalWS.xyz, -mainLight.direction));
				backSSS = lerp(0.3, 1.2, backSSS);
				output.normalWS.w = pow(backSSS, 0.5);
				/*
				// light
				Light mainLight = GetMainLight();
				half3 worldNormal = normalize(camPosWS - output.positionWS + half3(0, 1, 0));

				float NdotL = saturate(dot(worldNormal, mainLight.direction));
				float backSSS = 1 - NdotL;

				NdotL = lerp(0.3, 1.2, NdotL);
				backSSS = pow(backSSS, 0.5) * 4;

				// distance fade
				half distFade = saturate(pow(distance(camPosWS, output.positionWS) * 0.02, 2) - 1);

				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				output.lit = float4(NdotL, backSSS, distFade, fogFactor);
				*/

                return output;
            }

			void InitializeInputData(Varyings input, out InputData inputData)
			{
				inputData = (InputData)0;

				inputData.normalWS = NormalizeNormalPerPixel(input.normalWS.xyz);
				inputData.viewDirectionWS = SafeNormalize(input.viewDirWS);

				inputData.shadowCoord = float4(0, 0, 0, 0);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				Light mainLight = GetMainLight();
                // sample the texture
				half2 uv = (input.uv + half2(_Time.x, -_Time.x * 0.6) * 0.5) * 0.5;
				half flowValue = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv).z;

				half2 distoredUV = input.uv + half2(sin(flowValue * 3), cos(flowValue * 3)) * 0.02;
                half4 tex = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, distoredUV);

				half4 bgUV = half4(input.positionNDC.xy / input.positionNDC.w, 0, 0);
				half sceneDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, bgUV), _ZBufferParams).x;
				half transparentLine = saturate(_DepthFactor * (sceneDepth - input.positionNDC.w));
				
				half mistValue = tex.x * flowValue + tex.x;

				half alpha = tex.w * transparentLine;
				alpha = saturate(pow(alpha, 3) + alpha * flowValue);
				alpha *= input.positionWS.w;

				/*
				half3 mistCol = lerp(unity_FogColor.rgb * 0.7, mainLight.color.rgb, mistValue) * input.lit.x;

				mistCol += mainLight.color * input.lit.y * tex.y; // back SSS

				//mistCol = lerp(mistValue * unity_FogColor.rgb * 6, mistCol, mainLight.color.a); // no sun no moon

				mistCol = lerp(mistCol, unity_FogColor, _CloudyRate); // cloudy


				mistCol *= lerp(0.5, 1, _DayNightTime);

				half4 finalCol = half4(mistCol, alphaValue);
				//finalCol.rgb *= alphaValue;

				finalCol.rgb = MixFog(finalCol.rgb, input.lit.w);
				*/

				half3 albedo = half3(mistValue, mistValue, mistValue);

				half backSSS = tex.y * input.normalWS.w + albedo * 0.1;// *lerp(1, 0.3, _CloudyRate); // back SSS
				half3 emission = half3(0,0,0);

				InputData inputData;
				InitializeInputData(input, inputData);

				half4 color = DustLighting(inputData, albedo, emission, backSSS, alpha);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				return color;
            }
			ENDHLSL
        }
    }
	FallBack "Hidden/InternalErrorShader"
}
