﻿Shader "WalkingFat/FX/HologramBoard"
{
	Properties
	{
		_MainTex("Main Texture",2D) = "white" {}
		_PointCol("Point Color", Color) = (1,1,1,1)
		_BloomScale("Bloom Scale", Range(1.0, 3.0)) = 1.0
		_FadeDistance("Fade Distance", Range(5.0, 30.0)) = 10.0
		_FadeRange("Fade Range", Range(1.0, 10.0)) = 5.0
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "EnvScanLines"
			Tags { "LightMode" = "UniversalForward" }

			Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency 
			//ZWrite Off
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0
			// -------------------------------------
			// Universal Pipeline keywords
			//#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			//#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE

			//--------------------------------------
			// GPU Instancing
			//#pragma multi_compile_fog
			#pragma multi_compile_instancing

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float3 uv : TEXCOORD0;
				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD1;
				float4 positionNDC : TEXCOORD2;
				//float3 normalWS : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _PointCol;
			float _BloomScale;
			float _FadeDistance;
			float _FadeRange;
			float4 _MainTex_ST;
			CBUFFER_END

			TEXTURE2D(_MainTex);					SAMPLER(sampler_MainTex);

			// global
			float3 _CharPosWorld;

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS);

				output.positionWS = vertexInput.positionWS;

				output.positionCS = vertexInput.positionCS;

				float4 ndc = output.positionCS * 0.5f;
				output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				output.positionNDC.zw = output.positionCS.zw;

				// uv
				float objScaleX = length(float3(unity_ObjectToWorld[0].x, unity_ObjectToWorld[1].x, unity_ObjectToWorld[2].x));
				float objScaleY = length(float3(unity_ObjectToWorld[0].y, unity_ObjectToWorld[1].y, unity_ObjectToWorld[2].y));
				float scaleU = objScaleX / objScaleY;

				float2 uv = TRANSFORM_TEX(input.texcoord, _MainTex);

				float3 pivotWS = TransformObjectToWorld(float3(0,0,0));

				float3 viewDirWS =  _CharPosWorld - pivotWS;

				float2 tanXZ = float2(-normalInput.normalWS.z, normalInput.normalWS.x) * objScaleX * 0.5;

				float viewOffset = dot(tanXZ, viewDirWS.xz) / pow(tanXZ.x * tanXZ.x + tanXZ.y * tanXZ.y, 0.5) / objScaleY;

				float limitX = scaleU * 0.5 - 0.5;
				viewOffset = clamp(viewOffset, -limitX, limitX) + 0.5 * (scaleU - 1);

				output.uv.xy = float2(uv.x * scaleU - viewOffset, uv.y);

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// point matrix
				half scanline = sin(input.positionWS.x * 6) * sin(input.positionWS.y * 6);
				half pointMatrix = step(0.99, abs(scanline));
				half pointMask = 1 - saturate(abs(0.5 - input.uv.x));
				//half4 pointCol = _PointCol * pointMatrix * pointMask;

				half4 mainTexCol = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv.xy);

				half4 finalCol = half4(mainTexCol.rgb, 1);

				// get pixel matrix and caculate screen position
				float4x4 thresholdMatrix =
				{ 1.0 / 17.0,  9.0 / 17.0,  3.0 / 17.0, 11.0 / 17.0,
				  13.0 / 17.0,  5.0 / 17.0, 15.0 / 17.0,  7.0 / 17.0,
				   4.0 / 17.0, 12.0 / 17.0,  2.0 / 17.0, 10.0 / 17.0,
				  16.0 / 17.0,  8.0 / 17.0, 14.0 / 17.0,  6.0 / 17.0
				};

				float4x4 _RowAccess = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };
				float2 pos = input.positionNDC.xy / input.positionNDC.w;
				pos *= _ScreenParams.xy; // pixel position

				// fade distance
				half distFade = 1 - saturate((distance(_CharPosWorld, input.positionWS) - _FadeDistance) / _FadeRange);

				// Discard pixel if below threshold.
				half alpha = (mainTexCol.a + pointMatrix * pointMask) * distFade;
				clip(alpha - thresholdMatrix[fmod(pos.x, 4)] * _RowAccess[fmod(pos.y, 4)]);

				return finalCol * _BloomScale;
			}
			ENDHLSL
		}
	}
		FallBack "Hidden/InternalErrorShader"
}
