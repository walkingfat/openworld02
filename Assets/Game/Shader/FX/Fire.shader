﻿Shader "WalkingFat/FX/Fire"
{
	Properties
	{
		_NoiseMap("Noise Map", 2D) = "white" {}
		_MainColor("Main Color", Color) = (1,1,1,1)
		_LitStrength("Lit Strength",Range(1.0, 10.0)) = 3.0

	}

	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 200

		Pass
		{
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			//Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency 
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv : TEXCOORD0;
				float4 positionCS : SV_POSITION;
				//float4 positionWS : TEXCOORD1;
				float4 positionNDC : TEXCOORD1;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _NoiseMap_ST;
			half4 _MainColor;
			half _LitStrength;
			CBUFFER_END

			TEXTURE2D(_NoiseMap);					SAMPLER(sampler_NoiseMap);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;
			float _RainRate;

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _NoiseMap);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

				//output.positionWS = vertexInput.positionWS;
				output.positionCS = vertexInput.positionCS;
				//output.positionNDC = vertexInput.positionNDC;

				return output;
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float4 uv = float4(input.uv, 0, 0);
				half4 noiseCol = SAMPLE_TEXTURE2D(_NoiseMap, sampler_NoiseMap, uv);

				half4 finalCol = _MainColor * noiseCol.r * _LitStrength;

				return finalCol;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}