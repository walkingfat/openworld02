﻿#ifndef WALKINGFAT_FX_CORE_INCLUDED
#define WALKINGFAT_FX_CORE_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

inline void InitializeBRDFDataForDust(half3 albedo, half alpha, out BRDFData outBRDFData)
{
	half oneMinusReflectivity = OneMinusReflectivityMetallic(0);
	half reflectivity = 1.0 - oneMinusReflectivity;

	outBRDFData.diffuse = albedo;
	outBRDFData.specular = 0;
    outBRDFData.reflectivity = reflectivity;

	outBRDFData.grazingTerm = 0;// saturate(smoothness + reflectivity);
	outBRDFData.perceptualRoughness = PerceptualSmoothnessToPerceptualRoughness(0);
	outBRDFData.roughness = max(PerceptualRoughnessToRoughness(outBRDFData.perceptualRoughness), HALF_MIN);
	outBRDFData.roughness2 = outBRDFData.roughness * outBRDFData.roughness;

	outBRDFData.normalizationTerm = outBRDFData.roughness * 4.0h + 2.0h;
	outBRDFData.roughness2MinusOne = outBRDFData.roughness2 - 1.0h;
}

half4 DustLighting(InputData inputData, half3 albedo, half3 emission, half backSSS, half alpha)
{
	BRDFData brdfData;
	InitializeBRDFDataForDust(albedo, alpha, brdfData);

	Light mainLight = GetMainLight(inputData.shadowCoord);
	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

	half3 color = GlobalIllumination(brdfData, inputData.bakedGI, 1, inputData.normalWS, inputData.viewDirectionWS);
	color += LightingPhysicallyBased(brdfData, mainLight, inputData.normalWS, inputData.viewDirectionWS);

	color += backSSS * mainLight.color;
	color += emission;
	return half4(color, alpha);
}

#endif