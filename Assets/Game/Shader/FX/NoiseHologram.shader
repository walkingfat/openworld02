﻿Shader "WalkingFat/FX/NoiseHologram"
{
	Properties
	{
		_BaseMap("Base Map", 2D) = "white" {}
		_BumpMap("Bump Map", 2D) = "normal" {}
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_Cutoff("Cutoff", Range(0.0, 1.0)) = 0.5
		_Brightness("Brightness", Range(0.0, 5.0)) = 1.0
		_FresnelPow("Fresnel Power", Range(0.0, 10.0)) = 3.0
		_FresnelStrength("Fresnel Strength", Range(0.0, 1)) = 0.5
		_FallDown("Fall Down", Range(0.0, 1.0)) = 1.0
		_FallHeight("Fall Height", Float) = 3.0
		_BottomOffset("BottomOffset", Float) = 0.0

	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100
		Cull Off

		Pass
		{
			Name "CharRim"
			Tags { "LightMode" = "UniversalForward" }


			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord1 : TEXCOORD0;
				float2 texcoord2 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 normalWS : TEXCOORD0;
				float4 color : TEXCOORD1;
				float4 positionNDC : TEXCOORD2;
				float4 uv : TEXCOORD3;
				float4 tangentWS : TEXCOORD4;
				float4 bitangentWS : TEXCOORD5;
				//DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 5);
				//float4 fogFactorAndVertexLight : TEXCOORD6;
				//float4 shadowCoord : TEXCOORD7;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _Color1;
			float4 _Color2;
			float _Cutoff;
			float _Brightness;
			float _FresnelPow;
			float _FresnelStrength;
			float _PointSize;
			float _FallDown;
			float _FallHeight;
			float _BottomOffset;
			float4 _BaseMap_ST;
			float4 _BumpMap_ST;
			CBUFFER_END

			TEXTURE2D(_BaseMap);					SAMPLER(sampler_BaseMap);
			TEXTURE2D(_BumpMap);					SAMPLER(sampler_BumpMap);

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				half3 posWorld = TransformObjectToWorld(input.positionOS.xyz);
				half NdotUp = saturate(dot(normalInput.normalWS, half3(0, -1, 0)));

				//half fallDown1 = _FallDown * 0.6;

				half posBottom = input.positionOS.y - _BottomOffset;
				half fallDown1 = min(0, ((_FallDown - 0.667) * 3 * _FallHeight - posBottom) * _FallHeight) * NdotUp;
				half fallDown2 = min(0, ((_FallDown - 0.334) * 3 * _FallHeight - posBottom) * _FallHeight) * (1 - NdotUp);
				input.positionOS.y = max(_BottomOffset, input.positionOS.y + fallDown1 + fallDown2 + (_FallDown - 1) * 0.3 *( posBottom + 1));// *NdotUp;


				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

				half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
				half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				output.uv.xy = TRANSFORM_TEX(input.texcoord1, _BaseMap);
				output.uv.zw = TRANSFORM_TEX(input.texcoord2, _BumpMap);

				output.normalWS = half4(normalInput.normalWS, viewDirWS.x);
				output.tangentWS = half4(normalInput.tangentWS, viewDirWS.y);
				output.bitangentWS = half4(normalInput.bitangentWS, viewDirWS.z);

				//OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				//OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				//output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
				//output.shadowCoord = GetShadowCoord(vertexInput);

				half3 axisPosWorld = TransformObjectToWorld(half3(0, 0, 0));
				half distWS = distance(axisPosWorld, GetCameraPositionWS());
				half distLerpRate = saturate(distWS * 0.1);

				output.color.x = distLerpRate;
				output.color.y = vertexInput.positionWS.y;
				output.color.z = 0;
				output.color.w = fogFactor;
				output.positionCS = vertexInput.positionCS;
				output.positionNDC = vertexInput.positionNDC;

				return output;
			}

			half4 PassFragment(Varyings input, half facing : VFACE) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// light
				Light mainLight = GetMainLight();
				half fogCoord = input.color.w;

				half4 baseTex = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv.xy);
				half3 normalTS = UnpackNormal(SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, input.uv.zw));

				half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
				half3 normalWS = TransformTangentToWorld(normalTS, half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));

				normalWS = NormalizeNormalPerPixel(normalWS);
				viewDirWS = SafeNormalize(viewDirWS);

				half luminace = dot(baseTex.rgb, float3(0.299, 0.587, 0.114));

				half attenuation = mainLight.distanceAttenuation * mainLight.shadowAttenuation;

				half NdotL = saturate(dot(input.normalWS, mainLight.direction));
				half NdotV = saturate(dot(input.normalWS, viewDirWS));

				half fresnel = pow(1.0 - NdotV, _FresnelPow);
				fresnel *= lerp(_FresnelStrength * 0.8, _FresnelStrength, saturate(dot(mainLight.direction, -viewDirWS)));

				half lit = saturate(pow(NdotL, 2) + fresnel + 0.3);

				// noise =====================================
				half2 screenUV = input.positionNDC.xy / input.positionNDC.w;
				half2 ScreenPos = screenUV * _ScreenParams.xy;

				// uv noise
				half noiseUV = frac(sin(dot(input.uv.xy, half2(12.9898, 78.233))) * 43758.5453);

				// world pos noise
				half m = lerp(800, 200, input.color.x);
				half noiseWS = sin(input.color.y * m - _Time.w * 3) - 0.1;

				/*
				// screen pos noise
				half pixelSize = 2;
				half interval = ceil(pixelSize * 0.5);

				half screenNoiseX = ScreenPos.x % pixelSize > interval;
				half screenNoiseY = ScreenPos.y % pixelSize > interval;
				half screenNoise = screenNoiseX * screenNoiseY;
				*/

				luminace = pow(luminace, 5);

				half lr = saturate(lit * luminace + fresnel);

				half4 finalCol = lerp(_Color2, _Color1, lr) * _Brightness;
				half4 backFaceCol = lerp(_Color2, finalCol, lr * 0.05);
				finalCol = facing > 0 ? finalCol : backFaceCol;

				half noiseAll = noiseWS * noiseUV;
				half finalNoise = noiseAll > 0.95 - lit;
				half backNoise = 0;// noiseAll > 1.1 - (lit + 1) * 0.3;
				finalNoise = facing > 0 ? finalNoise : backNoise;

				half fallDownNoise = noiseUV > 2.1 - _FallDown * 0.5 - lit;
				fallDownNoise = facing > 0 ? fallDownNoise : backNoise;
				finalNoise = lerp(fallDownNoise, finalNoise, (_FallDown - 0.8) * 5);
				finalCol = lerp(_Color2, finalCol, _FallDown - 0.3);
				
				clip(finalNoise - _Cutoff);

				finalCol.rgb = MixFog(finalCol.rgb, fogCoord);

				return finalCol;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}