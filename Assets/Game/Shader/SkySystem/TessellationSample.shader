Shader "WalkingFat/TessellationSample"
{
	Properties
	{
		[Space]
		[Header(Tessellation)]
		_TessUniform("Tessellation Uniform", Range(1, 64)) = 1
		_TessDistMin("Tessellation Distance Min", float) = 10
		_TessDistMax("Tessellation Distance Max", float) = 100

		[Space]
		[Header(Debug)]
		_WireColor("Wire Color", Color) = (1,0,0,1)
		_WireWidth("Wire Width", Range(0.1, 5.0)) = 1.0
		[Toggle]_HardNormal("Hard Normal", Float) = 1.0

	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Geometry" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "Tessellation Sample"
			Tags { "LightMode" = "UniversalForward" }

			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite On

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 4.6

			#pragma vertex TessVertex
			#pragma fragment PassFragment
			#pragma hull hull
			#pragma domain domain
			#pragma geometry geom

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Assets/Game/Shader/Common/TessellationCore.hlsl"

			#if defined(SHADER_API_D3D11) || defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE) || defined(SHADER_API_VULKAN) || defined(SHADER_API_METAL) || defined(SHADER_API_PSSL)
			#define UNITY_CAN_COMPILE_TESSELLATION 1
			#   define UNITY_domain                 domain
			#   define UNITY_partitioning           partitioning
			#   define UNITY_outputtopology         outputtopology
			#   define UNITY_patchconstantfunc      patchconstantfunc
			#   define UNITY_outputcontrolpoints    outputcontrolpoints
			#endif

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				//UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessAttributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
			};

			struct Varyings
			{
				float4 positionCS : SV_Position;
				float4 positionWS : TEXCOORD1; // w = bitangentWS.x
				float4 normalWS : TEXCOORD2; // w = bitangentWS.y
				float4 tangentWS : TEXCOORD3; // w = bitangentWS.z
				float4 positionNDC : TEXCOORD4;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			struct g2f
			{
				float4 positionCS : SV_Position;
				float4 positionWS : TEXCOORD1; // w = bitangentWS.x
				float4 normalWS : TEXCOORD2; // w = bitangentWS.y
				float4 tangentWS : TEXCOORD3; // w = bitangentWS.z
				float4 positionNDC : TEXCOORD4;
				float3 dist : TEXCOORD5;
			};

			CBUFFER_START(UnityPerMaterial)
			float _TessUniform;
			float _TessDistMin;
			float _TessDistMax;
			float4 _WireColor;
			float _WireWidth;
			float _HardNormal;
			CBUFFER_END


			TessAttributes TessVertex(Attributes input)
			{
				TessAttributes output = (TessAttributes)0;

				output.positionOS = input.positionOS;
				output.normalOS = input.normalOS;
				output.tangentOS = input.tangentOS;
				output.texcoord = input.texcoord;
				return output;
			}

			Varyings PassVertex(TessAttributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				//output.uv = input.texcoord;

				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS);

				output.positionWS.xyz = TransformObjectToWorld(input.positionOS.xyz);

				output.positionCS = TransformWorldToHClip(output.positionWS.xyz);

				output.normalWS = float4(normalInput.normalWS, normalInput.bitangentWS.x);
				output.tangentWS = float4(normalInput.tangentWS, normalInput.bitangentWS.y);
				output.positionWS.w = normalInput.bitangentWS.z;

				float4 ndc = output.positionCS * 0.5f;
				output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				output.positionNDC.zw = output.positionCS.zw;
				return output;
			}

			// tessellation functions
			TessellationFactors patchConstantFunction(InputPatch<TessAttributes, 3> patch)
			{
				TessellationFactors f;

				float4 tess = DistanceBasedTess(patch[0].positionOS, patch[1].positionOS, patch[2].positionOS, _TessDistMin, _TessDistMax, _TessUniform);

				f.edge[0] = tess.x;
				f.edge[1] = tess.y;
				f.edge[2] = tess.z;
				f.inside = tess.w;
				return f;
			}

			[UNITY_domain("tri")]
			[UNITY_outputcontrolpoints(3)]
			[UNITY_outputtopology("triangle_cw")]
			[UNITY_partitioning("integer")]
			[UNITY_patchconstantfunc("patchConstantFunction")]
			TessAttributes hull(InputPatch<TessAttributes, 3> patch, uint id : SV_OutputControlPointID)
			{
				return patch[id];
			}

			[UNITY_domain("tri")]
			Varyings domain(TessellationFactors factors, OutputPatch<TessAttributes, 3> patch, float3 bary : SV_DomainLocation)
			{
				Attributes input;

				input.positionOS = patch[0].positionOS * bary.x + patch[1].positionOS * bary.y + patch[2].positionOS * bary.z;
				input.normalOS = patch[0].normalOS * bary.x + patch[1].normalOS * bary.y + patch[2].normalOS * bary.z;
				input.tangentOS = patch[0].tangentOS * bary.x + patch[1].tangentOS * bary.y + patch[2].tangentOS * bary.z;
				input.texcoord = patch[0].texcoord * bary.x + patch[1].texcoord * bary.y + patch[2].texcoord * bary.z;

				Varyings output = PassVertex(input);

				return output;
			}

			[maxvertexcount(3)]
			void geom(triangle Varyings IN[3], inout TriangleStream<g2f> triStream)
			{
				float3 dist = CalculateDistToCenter(IN[0].positionCS, IN[1].positionCS, IN[2].positionCS);

				g2f OUT;
				OUT.positionCS = IN[0].positionCS;
				OUT.positionWS = IN[0].positionWS;
				OUT.normalWS = IN[0].normalWS;
				OUT.tangentWS = IN[0].tangentWS;
				OUT.positionNDC = IN[0].positionNDC;
				OUT.dist = float3(dist.x, 0, 0);
				triStream.Append(OUT);

				OUT.positionCS = IN[1].positionCS;
				OUT.positionWS = IN[1].positionWS;
				OUT.normalWS = IN[1].normalWS;
				OUT.tangentWS = IN[1].tangentWS;
				OUT.positionNDC = IN[1].positionNDC;
				OUT.dist = float3(0, dist.y, 0);
				triStream.Append(OUT);

				OUT.positionCS = IN[2].positionCS;
				OUT.positionWS = IN[2].positionWS;
				OUT.normalWS = IN[2].normalWS;
				OUT.tangentWS = IN[2].tangentWS;
				OUT.positionNDC = IN[2].positionNDC;
				OUT.dist = float3(0, 0, dist.z);
				triStream.Append(OUT);
			}

			half4 PassFragment(g2f input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);


				//float3 hardNormal = CalcHardNormal(input.positionWS.xyz);
				//half hardNdotL = dot(hardNormal, GetMainLight().direction);

				// wireframe
				//distance of frag from triangles center
				float distFT = min(input.dist.x, min(input.dist.y, input.dist.z));
				//fade based on dist from center
				float wireframeValue = exp2((-1 / _WireWidth) * distFT * distFT);

				//half3 wireCol = half3(wireframeValue, wireframeValue, wireframeValue);
				
				half4 finalCol = half4(_WireColor.rgb * wireframeValue, 1);
				
				//half4 finalCol = half4(lerp(_WireColor.rgb * hardNdotL, wireCol, _WireColor.a), 1);

				return finalCol;

			}
			ENDHLSL
		}
		/*
		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

			ENDHLSL
		}
		
		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
			ENDHLSL
		}
		*/
	}		
	FallBack "Hidden/InternalErrorShader"
}
