﻿Shader "WalkingFat/RendererFeather/MarchingMeshCloud"
{
	Properties
	{
		_NoiseTexture("Noise Texture", 3D) = "white" {}
		_NoiseTile("Noise Tile", Range(0.001, 0.1)) = 0.01
		_MarchingStep("Marching Step", Range(0.0, 2.0)) = 0.2
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Geometry" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			Name "DrawCloud"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 3.0

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 texcoord : TEXCOORD0;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float4 positionOS : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_Position;
				float4 positionWS : TEXCOORD0; // w = bitangentWS.x
				float4 normalWS : TEXCOORD1; // w = bitangentWS.y
				float4 tangentWS : TEXCOORD2; // w = bitangentWS.z
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float _MarchingStep;
			float _NoiseTile;
			CBUFFER_END

			float4x4 _ViewToWorldMaterx;

			//TEXTURE2D(_MainTex);							SAMPLER(sampler_MainTex);
			//TEXTURE2D(_DepthTex);							SAMPLER(sampler_DepthTex);
			TEXTURE3D(_NoiseTexture);						SAMPLER(sampler_NoiseTexture);
			//TEXTURE2D(_CameraOpaqueTexture);		        SAMPLER(sampler_CameraOpaqueTexture);
			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);

			// global

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				//output.uv = input.texcoord;

				output.positionWS.xyz = TransformObjectToWorld(input.positionOS.xyz);
				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);

				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS);

				//float3 viewDirWS = normalize(GetCameraPositionWS() - posWS);

				output.normalWS = half4(normalInput.normalWS, normalInput.bitangentWS.x);
				output.tangentWS = half4(normalInput.tangentWS, normalInput.bitangentWS.y);
				output.positionWS.w = normalInput.bitangentWS.z;

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half3 mainLitCol = GetMainLight().color;
				float3 viewDir = normalize(GetCameraPositionWS() - input.positionWS);
				float3 bitangentWS = float3(input.normalWS.w, input.tangentWS.w, input.positionWS.w);

				half3 noiseCol;
				half3 diff;
				half3 normalWS;
				float3 uvWS = input.positionWS + float3(1, 1, 1) * _Time.y * 0.3;
				float nr = 1.0 / 128.0;

				for (int n = 0; n < 20; n++)
				{
					float3 uv = uvWS + viewDir * _MarchingStep * n;

					half4 noiseTex = SAMPLE_TEXTURE2D(_NoiseTexture, sampler_NoiseTexture, uv * _NoiseTile);
					noiseCol += (1 - noiseTex.g) * 0.05;

					float3 uvt = uv + input.tangentWS.xyz * nr;
					half4 ntt = SAMPLE_TEXTURE2D(_NoiseTexture, sampler_NoiseTexture, uvt * _NoiseTile);
					float3 uvb = uv + bitangentWS.xyz * nr;
					half4 ntb = SAMPLE_TEXTURE2D(_NoiseTexture, sampler_NoiseTexture, uvb * _NoiseTile);

					float3 normalFH = normalize(float3(-noiseTex.g + ntt.g, -noiseTex.g + ntb.g, nr));

					normalWS += TransformTangentToWorld(normalFH, half3x3(input.tangentWS.xyz, bitangentWS, input.normalWS.xyz)) * 0.05;
					
				}
				//half4 sceneCol = SAMPLE_TEXTURE2D(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, uv);
				//float depth = SAMPLE_TEXTURE2D_X(_CameraDepthTexture, sampler_CameraDepthTexture, uv).r;


				normalWS = NormalizeNormalPerPixel(normalWS);
				diff = pow(saturate(dot(normalWS, GetMainLight().direction)), 2);


				half4 finalCol = half4(noiseCol * diff, 1);
				
				return finalCol;// sceneCol * 0.2;
			}
			ENDHLSL
		}

		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
			ENDHLSL
		}
	}		
	FallBack "Hidden/InternalErrorShader"
}
