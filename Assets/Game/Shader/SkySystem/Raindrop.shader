﻿Shader "WalkingFat/SkySystem/Raindrop"
{
	Properties
	{
		_IlluminationRT("Illumination RT", 2D) = "white" {}
		_BlurTexSize("Blur Texture Size",Float) = 256
		_BlurRadius("Blur Radius",Range(1, 25)) = 1
		_BasicLitStrength("Basic Lit Strength",Range(0.0, 1.0)) = 0.2
		_MainLitStrength("Main Lit Strength",Range(0.0, 1.0)) = 0.5
		_IlluminationLitStrength("Illumination Lit Strength",Range(0.0, 100.0)) = 0.5
		
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Transparent+3" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 200

		Pass 
		{
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			Blend One One // additive
			ZWrite On
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			//#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			//#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			//#pragma multi_compile _ _SHADOWS_SOFT
			//#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			//#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			//#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float4 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float3 uv : TEXCOORD0;
				float4 positionCS : SV_POSITION;
				//float4 positionWS : TEXCOORD1;
				float4 positionNDC : TEXCOORD1;
				//float4 normalWS : TEXCOORD2;
				//float3 viewDir : TEXCOORD3;
				//float4 shadowCoord : TEXCOORD4;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			half _BlurTexSize;
			half _BlurRadius;
			half _BasicLitStrength, _MainLitStrength, _IlluminationLitStrength;
			CBUFFER_END

			TEXTURE2D(_IlluminationRT);					SAMPLER(sampler_IlluminationRT);

			// global
			float4 _WindParams;
			float3 _CharPosWorld;
			float _RainRate;

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv.xy = input.texcoord;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				//output.normalWS.xyz = normalInput.normalWS;
				half3 viewDir = GetCameraPositionWS() - vertexInput.positionWS;

				//output.shadowCoord = GetShadowCoord(vertexInput);

				//output.positionWS = vertexInput.positionWS;
				output.positionCS = vertexInput.positionCS;
				output.positionNDC = vertexInput.positionNDC;

				Light mainLight = GetMainLight();
				half3 backLitDir = normalInput.normalWS + mainLight.direction;
				half backSSS = saturate(dot(viewDir, -backLitDir));
				output.uv.z = backSSS;

				return output;
			}
			
			// blur -------------------
			float4 GetBlurColor(texture2D blurTex, sampler texSampler, half4 uv)
			{

				half space = 1.0 / _BlurTexSize;
				int count = _BlurRadius * 2 + 1;
				count *= count;

				half4 colorTmp = half4(0, 0, 0, 0);
				for (int x = -_BlurRadius; x <= _BlurRadius; x++)
				{
					for (int y = -_BlurRadius; y <= _BlurRadius; y++)
					{
						half4 color = SAMPLE_TEXTURE2D(blurTex, texSampler, uv + half4(x * space, y * space, 0, 0));
						colorTmp += color;
					}
				}
				return colorTmp / count;
			}
			/*
			float4 GetBlurDepth(texture2D blurTex, sampler texSampler, half4 uv)
			{

				half space = 1.0 / _BlurTexSize;
				int count = _BlurRadius * 2 + 1;
				count *= count;

				half depthTmp = 0;
				for (int x = -_BlurRadius; x <= _BlurRadius; x++)
				{
					for (int y = -_BlurRadius; y <= _BlurRadius; y++)
					{
						half depth = SAMPLE_DEPTH_TEXTURE(blurTex, texSampler, uv + half4(x * space, y * space, 0, 0));
						depthTmp += depth;
					}
				}
				return depthTmp / count;
			}
			*/
			//-----------------------------
			

			half4 LitPassFragment(Varyings input, half facing : VFACE) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float2 uv = input.uv.xy;
				Light mainLight = GetMainLight();

				// scene color and depth
				half4 bgUV = half4(input.positionNDC.xy / input.positionNDC.w, 0, 0);

				//half depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, bgUV), _ZBufferParams);

				//half4 illuminationRT = SAMPLE_TEXTURE2D(_IlluminationRT, sampler_IlluminationRT, float4(bgUV.xy / bgUV.w, 0, 0));

				half4 illuminationRT = GetBlurColor(_IlluminationRT, sampler_IlluminationRT, bgUV);
				//half depth = LinearEyeDepth(GetBlurDepth(_CameraDepthTexture, sampler_CameraDepthTexture, bgUV), _ZBufferParams);

				half atten = mainLight.distanceAttenuation * mainLight.shadowAttenuation;

				//half depthMask = max(0, 1 - depth * 0.02);
				half gradient = max(0, sin(uv.x * 4));

				half3 basicCol = _BasicLitStrength;

				half3 mainCol = input.uv.z * _MainLitStrength * (mainLight.color + 0.05);

				half3 illuminationCol = illuminationRT.rgb * _IlluminationLitStrength;// *depthMask;

				//return half4(illuminationCol, 1);

				half4 finalCol = half4((basicCol + mainCol + illuminationCol) * gradient, 1);

				return finalCol;
			}
			ENDHLSL
		}

		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
			ENDHLSL
		}
	}
		FallBack "Hidden/InternalErrorShader"
}
