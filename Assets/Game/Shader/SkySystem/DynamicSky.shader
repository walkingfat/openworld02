﻿Shader "WalkingFat/SkySystem/DynamicSky"
{
	Properties
	{
		//_BaseMap("Texture", 2D) = "white" {}
		//_BaseColor("Color", Color) = (1, 1, 1, 1)
		//_Cutoff("AlphaCutout", Range(0.0, 1.0)) = 0.5

		// sky color 
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_MoonTex("Moon Texture", 2D) = "white" {}
		_MoonScale("Moon Scale", range(1.0,20.0)) = 10.0
		_StarColor("Star Color", COLOR) = (1,1,1,1)
		_GalaxyCloudColor1("Galaxy Cloud Color 1", COLOR) = (1,0,0,1)
		_GalaxyCloudColor2("Galaxy Cloud Color 2", COLOR) = (0,0,1,1)

		// cloud
		_CloudSpeed("Cloud Speed", range(0.0, 30.0)) = 3
		_CloudSize("Cloud Size", range(1, 10)) = 10

		_CloudFadeDist("Cloud Fade Distance", float) = 10000
		_CloudFadeRate("Cloud Fade Rate", float) = 6

		_OffsetDistance("Offset Distance", range(0,1)) = 0.1
		_BackLitStrength("BackLight Strength", range(0.1,20)) = 5
		_EdgeLitPower("Edge Lit Power", range(0.1,10)) = 1
		_EdgeLitStrength("Edge Light Strength", range(0.1,20)) = 1

	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Geometry" "IgnoreProjector" = "True" "RenderPipeline" = "universalPipeline" }
		LOD 100

		//Blend[_SrcBlend][_DstBlend]
		Cull Off
		ZWrite On
		ZTest Less

		Pass
		{
			Name "Unlit"
			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag
			//#pragma shader_feature _ALPHATEST_ON
			//#pragma shader_feature _ALPHAPREMULTIPLY_ON

			// -------------------------------------
			// Unity defined keywords
			//#pragma multi_compile_fog
			//#pragma multi_compile_instancing
			#pragma target 3.0

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 uv1 : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				//UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 vertex : SV_POSITION;
				float2 uv1 : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float3 positionWS : TEXCOORD2;
				float3 positionVS : TEXCOORD3;
				float4 positionCS : TEXCOORD4;
				float4 positionNDC : TEXCOORD5;
				float3 viewDirWS  : TEXCOORD6;
				float3 normalWS : TEXCOORD7;
				//float3 lightDir : TEXCOORD8;

				//UNITY_VERTEX_INPUT_INSTANCE_ID
				//UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _NoiseTex_ST;
			float _MoonScale;
			float4 _StarColor;
			float4 _GalaxyCloudColor1;
			float4 _GalaxyCloudColor2;

			float _CloudSpeed, _CloudSize;
			float _CloudFadeDist, _CloudFadeRate;
			float _OffsetDistance, _BackLitStrength, _EdgeLitPower, _EdgeLitStrength;
			CBUFFER_END

			TEXTURE2D(_NoiseTex);            SAMPLER(sampler_NoiseTex);
			TEXTURE2D(_MoonTex);             SAMPLER(sampler_MoonTex);
			//TEXTURE2D(_NoiseTex);           SAMPLER(sampler_NoiseTex);

			//TEXTURE2D(_AuroraTex);           SAMPLER(sampler_AuroraTex);
			
			
			// Global
			float4 _SunPos;
			float _Timecycle;
			float _HasSunMoon;
			float _LitRateTime;
			float4 _SkyColor0;
			float4 _SkyColor1;
			float4 _SkyColor2;
			float4 _HaloColor;
			float _SkyColorMTime;
			float _HaloRadius;
			float _SunRadius;
			float _DayNightTime;
			float _CloudyRate;
			float4 _CloudParams;
			float4 _CloudColor0, _CloudColor1, _CloudColor2, _CloudEdgeColor;
			float _CloudColorMTime;

			Varyings vert(Attributes input)
			{
				Varyings output = (Varyings)0;

				//UNITY_SETUP_INSTANCE_ID(input);
				//UNITY_TRANSFER_INSTANCE_ID(input, output);
				//UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				output.vertex = vertexInput.positionCS;
				output.uv1 = TRANSFORM_TEX(input.uv1, _NoiseTex);
				output.uv2 = input.uv2;

				output.positionWS = vertexInput.positionWS;
				output.positionVS = vertexInput.positionVS;
				output.positionCS = vertexInput.positionCS;
				output.positionNDC = vertexInput.positionNDC;

				output.viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
				output.normalWS = NormalizeNormalPerPixel(normalInput.normalWS);

				//output.lightDir = _MainLightPosition;// lightInput.direction;

				return output;
			}

			half4 frag(Varyings input) : SV_Target
			{
				//UNITY_SETUP_INSTANCE_ID(input);
				//UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				Light mainLight = GetMainLight();

				_SkyColor0 = pow(_SkyColor0, 2.2);
				_SkyColor1 = pow(_SkyColor1, 2.2);
				_SkyColor2 = pow(_SkyColor2, 2.2);
				_HaloColor = pow(_HaloColor, 2.2);

				_CloudColor0 = pow(_CloudColor0, 2.2);
				_CloudColor1 = pow(_CloudColor1, 2.2);
				_CloudColor2 = pow(_CloudColor2, 2.2);
				_CloudEdgeColor = pow(_CloudEdgeColor, 2.2);

				half edgeFade = distance(input.uv1.xy, half2(0.5, 0.5)) * 1.9;
				half screenRatio = _ScreenParams.x / _ScreenParams.y;
				half cloudySightFade = saturate(1 - _CloudyRate);
				half nightSightFade = saturate(1 - _DayNightTime);
				
				// sky background color
				half4 skyColBottom = lerp(_SkyColor0, _SkyColor1, input.uv2.y / _SkyColorMTime);
				half4 skyColUpper = lerp(_SkyColor1, _SkyColor2, (input.uv2.y - _SkyColorMTime) / (1 - _SkyColorMTime));
				half4 skyCol = lerp(skyColBottom, skyColUpper, step(_SkyColorMTime, input.uv2.y));

				
				// sun halo
				half3 posClip2 = input.positionCS.xyz / input.positionCS.w;
				half4 sunPosClip = TransformWorldToHClip(half4(_SunPos.xyz, 1));
				// Check DX or OpenGL

//#if UNITY_UV_STARTS_AT_TOP
				//sunPosClip.y = 1 - sunPosClip.y; // OpenGL
//#endif
				half sunHaloValue = 0;
				half faceSunMoon = step(0, sunPosClip.w);

				sunPosClip.xyz = sunPosClip.xyz / sunPosClip.w;
				posClip2.x *= screenRatio;
				sunPosClip.x *= screenRatio;
				sunHaloValue = saturate(1 - distance(posClip2.xy, sunPosClip.xy) / _HaloRadius) * faceSunMoon;

				half cloudyEffectHalo = saturate(_CloudyRate - 0.5) + 0.5;
				sunHaloValue = (sunHaloValue + pow(sunHaloValue, 5) * 3)* (1.5 - cloudyEffectHalo);
				half4 sunHaloCol = _HaloColor * sunHaloValue * (1 - cloudyEffectHalo);
				
				// sun
				half sunValue = saturate(1 - distance(posClip2.xy, sunPosClip.xy) / _SunRadius);
				sunValue = smoothstep(0.5, 0.6, sunValue) * (1 - _CloudyRate);
				sunValue = pow(sunValue, 0.5);
				half4 sunCol = half4(sunValue * mainLight.color * 10 * _HasSunMoon * faceSunMoon, 1);

				//return half4(input.positionNDC.xy / input.positionNDC.w, 0, 1);
				// moom 
				half2 moonUV = half2(input.positionNDC.x * screenRatio, input.positionNDC.y) / input.positionNDC.w + 0.5 / _MoonScale; // get moon quad uv
				
				moonUV = moonUV * _MoonScale - half2(sunPosClip.x + screenRatio, 1 + sunPosClip.y) * _MoonScale * 0.5; // OpenGL

				//moonUV = moonUV * _MoonScale - half2(sunPosClip.x + screenRatio, 1 - sunPosClip.y) * _MoonScale * 0.5; // Dx Metal and Vulkan


				half4 moonTex = SAMPLE_TEXTURE2D(_MoonTex, sampler_MoonTex, moonUV);

				half4 moonCol = lerp(moonTex.x, moonTex.y, _CloudyRate) * (1 - _HasSunMoon) * faceSunMoon;
				

				// star
				half4 starCol0 = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, input.uv1 * 7);
				half4 starCol1 = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, input.uv1 * 35);
				half4 starCol2 = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, input.uv1 * 22);
				half starValue = (starCol1.y + starCol2.y) * starCol0.x;
				half4 starCol = starValue * starValue * _StarColor * 4;
				starCol *= nightSightFade * cloudySightFade;
				//return starCol0.x;

				/*
				half skyRimValue = saturate((input.uv2.y - 0.3) * 3); // sky rim value
				half4 starCol0 = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, input.uv1);
				half4 starCol1 = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, input.uv1 * 35);
				half4 starCol2 = SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, input.uv1 * 22);
				half starAll = starCol1.x + starCol2.x;
				half starValue = starAll * (starCol1.z * starCol2.z) * 10; // background star

				half galaxyCloudValue = (starCol0.z + starCol1.z + starCol2.z) * starCol0.y * 1.1;
				half4 galaxyCloudCol = skyCol * starCol0.y * _GalaxyCloudColor2 + _GalaxyCloudColor1 * pow(galaxyCloudValue * 2.6, 3) * starCol0.y;
				half galaxyStarValue = saturate(pow(starAll * starCol0.y * 35, 3)) * 2;
				galaxyStarValue = saturate(galaxyStarValue - sunHaloValue * 0.6 - sunValue); // no star in halo and sun/moon
				half4 galaxyStarCol = (galaxyStarValue * _GalaxyCloudColor1 * starCol1.z + galaxyStarValue * _GalaxyCloudColor2 * starCol2.z) * 22;

				starValue = pow(saturate(starValue * 8 * skyRimValue), 2) * 0.8;

				starValue -= galaxyCloudValue; // no star in galaxy cloud
				starValue = saturate(starValue - sunHaloValue * 0.3 - 0); // no star in halo and sun/moon
				half4 starCol = starValue * _StarColor + galaxyStarCol + galaxyCloudCol;

				starCol *= nightSightFade * cloudySightFade;
				*/

				// Cloud ==================================================================================================================

				// get uv panner
				half2 panner = -_CloudParams.xy * _CloudSpeed;
				half2 uvPanner1 = input.uv1 + panner;
				half2 uvPanner2 = input.uv1 * 1.7 + panner;

				// get uv 
				half2 uv1 = uvPanner1 * (_CloudSize + 0.23);
				half2 uv2 = uvPanner2 * (_CloudSize);

				half cloudLitOffset = clamp(abs(6 - (_Timecycle * 24) % 12), 0, 1);
				half3 litOffset = mainLight.direction * _OffsetDistance * cloudLitOffset;
				half cloudThick = lerp(0, 2.5, _CloudyRate);

				half2 uvOffset1 = uv1 + half2(litOffset.x, litOffset.z);
				half2 uvOffset2 = uv1 - half2(litOffset.x, litOffset.z);
				half2 uvOffset3 = uv2 + half2(litOffset.x, litOffset.z);
				half2 uvOffset4 = uv2 - half2(litOffset.x, litOffset.z);

				half texValue = saturate(SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uv1).r * SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uv2).r * cloudThick);
				half texValue1 = saturate(SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uvOffset1).r * SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uvOffset3).r * cloudThick);
				half texValue2 = saturate(SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uvOffset2).r * SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uvOffset4).r * cloudThick);

				half edgeLight = pow((1 - texValue), _EdgeLitPower) * _EdgeLitStrength * saturate(0.6 + sunHaloValue);

				half litValue = saturate(texValue1 - texValue2 + edgeLight);


				// get cloud alpha
				half cloudValue = saturate(pow(texValue, 1.5) * 3); // cloud edge
				cloudValue = smoothstep(0.6, 1, cloudValue);
				half finalCloudValue = saturate(cloudValue * (1 - edgeFade * edgeFade * 1.3));
				//return finalCloudValue;

				// get cloud color
				half distFadeValue = pow(1 - saturate(distance(0, input.positionWS.xyz) / _CloudFadeDist), _CloudFadeRate);// saturate(1 - distance(0, input.posWorld.xyz) / (_CloudFadeDist + _CloudFadeRange));

				half4 cloudColBottom = lerp(_CloudColor0, _CloudColor1, litValue / _CloudColorMTime);
				half4 cloudColUpper = lerp(_CloudColor1, _CloudColor2, (litValue - _CloudColorMTime) / (1 - _CloudColorMTime));
				half4 finalCloudCol = lerp(cloudColBottom, cloudColUpper, step(_CloudColorMTime, litValue));
				
				finalCloudCol = lerp(skyCol, finalCloudCol, (1 - edgeFade) * 0.4);
				finalCloudCol = saturate(finalCloudCol + (_CloudColor2 * sunHaloValue * litValue * _BackLitStrength)); // add back light of sun/moon

				// edge cloud 
				half edgeCloudValue = edgeFade * 0.5 + texValue * 1.5;
				edgeCloudValue = saturate(pow(edgeCloudValue, 5) + pow(edgeFade, 10));
				sunCol = lerp(sunCol, 0, edgeCloudValue);
				moonCol = lerp(moonCol, 0, edgeCloudValue);

/*
				// aurora color
				half2 screenUV = input.positionNDC.xy / input.positionNDC.w;
				half4 auroraCol = SAMPLE_TEXTURE2D(_AuroraTex, sampler_AuroraTex, screenUV);
				auroraCol *= max(0, 1 - finalCloudValue - edgeCloudValue - edgeFade);
				auroraCol *= cloudySightFade;
				//return auroraCol;
*/

				// final sky color
				half4 finalSkyCol = skyCol + sunHaloCol + (sunCol + moonCol) * step(0.9, _LitRateTime) + starCol;// + auroraCol;

				// dither for banding
				half skyLum = saturate(finalSkyCol.x + finalSkyCol.y + finalSkyCol.z);
				//return skyLum;
				half codeNoise = sin(10000 * (input.positionNDC.x + input.positionNDC.y) * (_Time.x % 1));
				codeNoise = (codeNoise * 2 - 0.5) / 255 * skyLum;
				finalSkyCol += codeNoise;

				return half4(lerp(finalSkyCol.rgb, finalCloudCol.rgb, finalCloudValue), 1);
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}