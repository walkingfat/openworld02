﻿Shader "WalkingFat/Env/Rock"
{
	Properties
	{
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_MixMap("Mix Map", 2D) = "white" {} // R = Smoothness; G = Occlusion; B = Height; A = ???;
		//_EmissionMap("Emission", 2D) = "black" {}
		//_Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5

		_RainNoiseMap("Rain Noise Map)", 2D) = "white" {}

		_CoverBaseMap ("Albedo (RGB)", 2D) = "white" {}
		_CoverBumpMap("Normal Map", 2D) = "bump" {}
		_CoverMixMap("Mix Map", 2D) = "white" {} // R = Smoothness; G = Occlusion; B = Height; A = ???;
			
		_CoverDensity("Cover Density", Range(0.0, 2.0)) = 1
		_RainColor("Rain Color", Color) = (1,1,1,1)
		_SlpashDist("Slpash Distance", Range(0.0, 1.0)) = 0.2

		_BlendDepthFactor("Blend Depth Factor", Range(0.1, 5.0)) = 1.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300
		
		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			//Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Material Keywords
			#pragma shader_feature _NORMALMAP
			#pragma shader_feature _ALPHATEST_ON
			#pragma shader_feature _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _EMISSION
			#pragma shader_feature _METALLICSPECGLOSSMAP
			#pragma shader_feature _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
			#pragma shader_feature _OCCLUSIONMAP

			#pragma shader_feature _SPECULARHIGHLIGHTS_OFF
			#pragma shader_feature _ENVIRONMENTREFLECTIONS_OFF
			#pragma shader_feature _SPECULAR_SETUP
			#pragma shader_feature _RECEIVE_SHADOWS_OFF

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "EnvCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD2;
				float4 normalWS : TEXCOORD3;
				float4 tangentWS : TEXCOORD4;    // xyz: tangent, w: viewDir.y
				float4 bitangentWS : TEXCOORD5;    // xyz: bitangent, w: viewDir.z
				half4 fogFactorAndVertexLight : TEXCOORD6;
				float4 shadowCoord : TEXCOORD7;
				float4 positionNDC : TEXCOORD8;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			//float _BumpScale, _Cutoff;
			float _CoverDensity;
			float4 _BaseMap_ST;
			float4 _MixMap_ST;
			float4 _BumpMap_ST;
			//float4 _EmissionMap_ST;
			float4 _CoverBaseMap_ST;
			float4 _CoverMixMap_ST;
			float4 _CoverBumpMap_ST;
			//float4 _CoverEmissionMap_ST;
			//float4 _RainNoiseMap_ST;
			float _BlendDepthFactor;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			TEXTURE2D(_MixMap);								SAMPLER(sampler_MixMap);
			TEXTURE2D(_BumpMap);							SAMPLER(sampler_BumpMap);
			//TEXTURE2D(_EmissionMap);						SAMPLER(sampler_EmissionMap);

			TEXTURE2D(_CoverBaseMap);						SAMPLER(sampler_CoverBaseMap);
			TEXTURE2D(_CoverMixMap);						SAMPLER(sampler_CoverMixMap);
			TEXTURE2D(_CoverBumpMap);						SAMPLER(sampler_CoverBumpMap);
			//TEXTURE2D(_EmissionMap);						SAMPLER(sampler_EmissionMap);

			TEXTURE2D(_RainNoiseMap);						SAMPLER(sampler_RainNoiseMap);

			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);
			TEXTURE2D(_CameraOpaqueTexture);		        SAMPLER(sampler_CameraOpaqueTexture);

			//global
			float _RainRate;

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);
				half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
				half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

				output.normalWS = half4(normalInput.normalWS, viewDirWS.x);
				output.tangentWS = half4(normalInput.tangentWS, viewDirWS.y);
				output.bitangentWS = half4(normalInput.bitangentWS, viewDirWS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
				output.shadowCoord = GetShadowCoord(vertexInput);

				output.positionWS = vertexInput.positionWS;
				output.positionCS = vertexInput.positionCS;
				output.positionNDC = vertexInput.positionNDC;

				return output;
			}

			void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
			{
				inputData = (InputData)0;

				inputData.positionWS = input.positionWS;

				half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
				inputData.normalWS = TransformTangentToWorld(normalTS,
					half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));

				inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				viewDirWS = SafeNormalize(viewDirWS);

				inputData.viewDirectionWS = viewDirWS;

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float2 uv = input.uv;
				half4 baseCol = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);
				half3 normalTS = UnpackNormal(SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, uv));
				half4 mixCol = SAMPLE_TEXTURE2D(_MixMap, sampler_MixMap, uv);

				half4 coverBaseCol = SAMPLE_TEXTURE2D(_CoverBaseMap, sampler_CoverBaseMap, uv);
				half3 coverNormalTS = UnpackNormal(SAMPLE_TEXTURE2D(_CoverBumpMap, sampler_CoverBumpMap, uv));
				half4 coverMixCol = SAMPLE_TEXTURE2D(_CoverMixMap, sampler_CoverMixMap, uv);

				float2 rainUv = uv * 4 + float2(_Time.z, _Time.z);
				half rainNoiseValue = SAMPLE_TEXTURE2D(_RainNoiseMap, sampler_RainNoiseMap, rainUv).r;
				half2 rainData = half2(rainNoiseValue, _RainRate);

				//half4 EmissionCol = SAMPLE_TEXTURE2D(_EmissionMap, sampler_EmissionMap, uv);

				half coverValue = dot(input.normalWS, half3(0, 1, 0));
				coverValue = saturate((coverMixCol.b + coverValue) * _CoverDensity - mixCol.b);

				half3 blendNormalTS = lerp(normalTS, coverNormalTS, coverValue * 0.5);

				//Blend Scene
				//float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, float4(input.positionNDC.xy / input.positionNDC.w, 0, 0)), _ZBufferParams);
				//float blendValue = 1 - saturate((depth - input.positionNDC.w) / _BlendDepthFactor);
				//return blendValue;

				half3 albedo = lerp(baseCol.rgb, coverBaseCol, coverValue);
				//albedo = lerp(baseCol, coverBaseCol, blendValue);

				half metallic = 0;

				half3 blendMixCol = lerp(mixCol, coverMixCol, coverValue);
				half smoothness = min(0.6, blendMixCol.r + _RainRate);
				half occlusion = blendMixCol.g;
				half height = blendMixCol.b;
				//half3 specular = half3(0, 0, 0);
				half3 emission = half3(0,0,0);// EmissionCol.rgb;

				half alpha = 1;

				InputData inputData;
				InitializeInputData(input, blendNormalTS, inputData);

				//inputData.normalWS = lerp(inputData.normalWS, input.normalWS, blendValue);
				
				half4 color = EnvLighting(inputData, albedo, metallic, smoothness, occlusion, emission, alpha, rainData);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				
				return color;
			}
			ENDHLSL
		}

		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
