﻿#ifndef WALKINGFAT_Env_CORE_INCLUDED
#define WALKINGFAT_Env_CORE_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

inline void InitializeBRDFDataNoSpecular(half3 albedo, half metallic, half smoothness, half alpha, half2 rain, out BRDFData outBRDFData)
{
	half oneMinusReflectivity = OneMinusReflectivityMetallic(metallic);
	half reflectivity = 1.0 - oneMinusReflectivity;

	outBRDFData.diffuse = albedo * oneMinusReflectivity;
	half3 specular = lerp(kDieletricSpec.rgb, albedo, metallic);
	specular = min(1, specular * rain.x + specular + rain.x * max(0, rain.y * rain.y - 0.3) * 0.5);
	outBRDFData.specular = specular;
    outBRDFData.reflectivity = reflectivity;

	outBRDFData.grazingTerm = saturate(smoothness + reflectivity);
	outBRDFData.perceptualRoughness = PerceptualSmoothnessToPerceptualRoughness(smoothness);
	outBRDFData.roughness = max(PerceptualRoughnessToRoughness(outBRDFData.perceptualRoughness), HALF_MIN);
	outBRDFData.roughness2 = outBRDFData.roughness * outBRDFData.roughness;

	outBRDFData.normalizationTerm = outBRDFData.roughness * 4.0h + 2.0h;
	outBRDFData.roughness2MinusOne = outBRDFData.roughness2 - 1.0h;
}

half4 EnvLighting(InputData inputData, half3 albedo, half metallic, half smoothness, half occlusion, half3 emission, half alpha, half2 rain)
{
	BRDFData brdfData;
	InitializeBRDFDataNoSpecular(albedo, metallic, smoothness, alpha, rain, brdfData);

	Light mainLight = GetMainLight(inputData.shadowCoord);
	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

	half3 color = GlobalIllumination(brdfData, inputData.bakedGI, occlusion, inputData.normalWS, inputData.viewDirectionWS);
	color += LightingPhysicallyBased(brdfData, mainLight, inputData.normalWS, inputData.viewDirectionWS);

	color += emission;
	return half4(color, alpha);
}

#endif