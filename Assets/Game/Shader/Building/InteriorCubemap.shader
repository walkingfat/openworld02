﻿Shader "WalkingFat/Building/InteriorCubemap"
{
	Properties
	{
		_RoomTex("Room Atlas RGB (A - back wall fraction)", 2D) = "white" {}
		_Rooms("Room Atlas Rows&Cols (XY)", Vector) = (1,1,0,0)
	}
	SubShader
	{
		Tags { "RenderType" = "Overlay" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 100

		Pass
		{
			//Name "CharRim"
			Tags { "LightMode" = "UniversalForward" }

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			//#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			//#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			//#pragma multi_compile _ _SHADOWS_SOFT
			//#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			//#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			//#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv                       : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD2;
				float3 normalWS : TEXCOORD3;
				float3 viewDir : TEXCOORD4;
				half4 fogFactorAndVertexLight : TEXCOORD5;
				float4 shadowCoord : TEXCOORD6;
				float3 tangentViewDir : TEXCOORD7;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _RoomTex_ST;
			float2 _Rooms;
			CBUFFER_END

			TEXTURE2D(_RoomTex);					SAMPLER(sampler_RoomTex);

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _RoomTex);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				output.positionCS = vertexInput.positionCS;
				output.positionWS = vertexInput.positionWS;

				output.normalWS = normalInput.normalWS;

				// get tangent space camera vector
				float3 objCam = TransformWorldToObject(GetCameraPositionWS());
				float3 viewDir = input.positionOS.xyz - objCam;
				float tangentSign = input.tangentOS.w * unity_WorldTransformParams.w;
				float3 bitangent = cross(input.normalOS.xyz, input.tangentOS.xyz) * tangentSign;
				output.tangentViewDir = float3(
					dot(viewDir, input.tangentOS.xyz),
					dot(viewDir, bitangent),
					dot(viewDir, input.normalOS)
					);
				output.tangentViewDir *= _RoomTex_ST.xyx;

				return output;
			}

			// psuedo random
			float2 rand2(float co) {
				return frac(sin(co * float2(12.9898, 78.233)) * 43758.5453);
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				// room uvs
				half2 roomUV = frac(input.uv);
				half2 roomIndexUV = floor(input.uv);

				// randomize the room
				half2 n = floor(rand2(roomIndexUV.x + roomIndexUV.y * (roomIndexUV.x + 1)) * _Rooms.xy);
				roomIndexUV += n;

				// get room depth from room atlas alpha
				half farFrac = SAMPLE_TEXTURE2D(_RoomTex, sampler_RoomTex, (roomIndexUV + 0.5) / _Rooms).a;
				half depthScale = 1.0 / (1.0 - farFrac) - 1.0;

				// raytrace box from view dir
				half3 pos = half3(roomUV * 2 - 1, -1);
				// pos.xy *= 1.05;
				input.tangentViewDir.z *= -depthScale;
				half3 id = 1.0 / input.tangentViewDir;
				half3 k = abs(id) - pos * id;
				half kMin = min(min(k.x, k.y), k.z);
				pos += kMin * input.tangentViewDir;

				// 0.0 - 1.0 room depth
				half interp = pos.z * 0.5 + 0.5;

				// account for perspective in "room" textures
				// assumes camera with an fov of 53.13 degrees (atan(0.5))
				half realZ = saturate(interp) / depthScale + 1;
				interp = 1.0 - (1.0 / realZ);
				interp *= depthScale + 1.0;

				// iterpolate from wall back to near wall
				half2 interiorUV = pos.xy * lerp(1.0, farFrac, interp);
				interiorUV = interiorUV * 0.5 + 0.5;

				// sample room atlas texture
				half4 room = SAMPLE_TEXTURE2D(_RoomTex, sampler_RoomTex, (roomIndexUV + interiorUV.xy) / _Rooms);

				half4 finalCol = half4(room.rgb, 1.0);

				return finalCol;
			}
		ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
