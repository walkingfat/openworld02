﻿#ifndef WALKINGFAT_Env_CORE_INCLUDED
#define WALKINGFAT_Env_CORE_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

inline void InitializeBRDFDataMetallic(half3 albedo, half metallic, half smoothness, half alpha, out BRDFData outBRDFData)
{
	half oneMinusReflectivity = OneMinusReflectivityMetallic(metallic);
	half reflectivity = 1.0 - oneMinusReflectivity;

	outBRDFData.diffuse = albedo * oneMinusReflectivity;
	half3 specular = lerp(kDieletricSpec.rgb, albedo, metallic);
	outBRDFData.specular = specular;
    outBRDFData.reflectivity = reflectivity;

	outBRDFData.grazingTerm = saturate(smoothness + reflectivity);
	outBRDFData.perceptualRoughness = PerceptualSmoothnessToPerceptualRoughness(smoothness);
	outBRDFData.roughness = max(PerceptualRoughnessToRoughness(outBRDFData.perceptualRoughness), HALF_MIN);
	outBRDFData.roughness2 = outBRDFData.roughness * outBRDFData.roughness;

	outBRDFData.normalizationTerm = outBRDFData.roughness * 4.0h + 2.0h;
	outBRDFData.roughness2MinusOne = outBRDFData.roughness2 - 1.0h;
}

inline void InitializeBRDFDataSpecular(half3 albedo, half3 specular, half smoothness, half alpha, out BRDFData outBRDFData)
{
    half reflectivity = ReflectivitySpecular(specular);
    half oneMinusReflectivity = 1.0 - reflectivity;

    outBRDFData.diffuse = albedo * (half3(1.0h, 1.0h, 1.0h) - specular);
    outBRDFData.specular = specular;

    outBRDFData.grazingTerm = saturate(smoothness + reflectivity);
    outBRDFData.perceptualRoughness = PerceptualSmoothnessToPerceptualRoughness(smoothness);
    outBRDFData.roughness = max(PerceptualRoughnessToRoughness(outBRDFData.perceptualRoughness), HALF_MIN);
    outBRDFData.roughness2 = outBRDFData.roughness * outBRDFData.roughness;

    outBRDFData.normalizationTerm = outBRDFData.roughness * 4.0h + 2.0h;
    outBRDFData.roughness2MinusOne = outBRDFData.roughness2 - 1.0h;

    //outBRDFData.diffuse *= alpha;
    //alpha = alpha * oneMinusReflectivity + reflectivity;
}

inline float D_Ashikhmin(float roughness, float nh) {
    float a2 = roughness * roughness;
    float cos2h = nh * nh;
    float sin2h = max(1.0 - cos2h, 0.0078125); // 2^(-14/2), so sin2h^2 > 0 in fp16
    float sin4h = sin2h * sin2h;
    float cot2 = -cos2h / (a2 * sin2h);
    return 1.0 / (PI * (4.0 * a2 + 1.0) * sin4h) * (4.0 * exp(cot2) + sin4h);

}

inline float DistributionCloth(float roughness, float nh) {

    float invAlpha = 1.0 / roughness;
    float cos2h = nh * nh;
    float sin2h = max(1.0 - cos2h, 0.0078125); // 2^(-14/2), so sin2h^2 > 0 in fp16
    return (2.0 + invAlpha) * pow(sin2h, invAlpha * 0.5) / (2.0 * PI);
}

inline float VisibilityCloth(float nl, float nv) {
    return  1 / (4 * (nl + nv - nl * nv) + 1e-5f);
}

inline half Pow5(half x)
{
    return x * x * x * x * x;
}

inline half DisneyDiffuse0(half NdotV, half NdotL, half LdotH, half perceptualRoughness)
{
    half fd90 = 0.5 + 2 * LdotH * LdotH * perceptualRoughness;
    // Two schlick fresnel term
    half lightScatter = (1 + (fd90 - 1) * Pow5(1 - NdotL));
    half viewScatter = (1 + (fd90 - 1) * Pow5(1 - NdotV));

    return lightScatter * viewScatter;
}

inline half3 FabricBDRF(BRDFData brdfData, Light light, half3 normalWS, half3 viewDirectionWS)
{
    float NdotL = saturate(dot(normalWS, light.direction));
    float NdotV = saturate(dot(normalWS, viewDirectionWS));

    float3 halfDir = SafeNormalize(float3(light.direction) + float3(viewDirectionWS));

    float NdotH = saturate(dot(normalWS, halfDir));
    half LdotH = saturate(dot(light.direction, halfDir));

    half V = VisibilityCloth(NdotL, NdotV);
    half D = DistributionCloth(brdfData.roughness, NdotH);

    half luminance = dot(brdfData.diffuse, half3(0.299, 0.587, 0.114));
    half3 F = half3(luminance, luminance, luminance);

    F = half3(0.04, 0.04, 0.04);// sheenColor;
    //F = lerp(F, F1, step(_Time.y % 2, 1));

    half3 Fr = (D * V) * F;

    // diffuse BRDF
    half diffuse = DisneyDiffuse0(NdotV, NdotL, LdotH, brdfData.roughness);

    // energy conservative wrap diffuse
    diffuse *= saturate((dot(normalWS, light.direction) + 0.5) / 2.25);

    half3 Fd = diffuse * brdfData.diffuse;

    // cheap subsurface scatter
    Fd *= saturate(half3(1, 0.5, 0) + NdotL);
    half3 radiance = light.color * (light.distanceAttenuation * light.shadowAttenuation);
    half3 color = (Fd + Fr * NdotL) * radiance;
    
    //half3 radiance = light.color * (light.distanceAttenuation * light.shadowAttenuation * NdotL);
    //half3 color = (Fd + Fr) * radiance;

    return color;
}

half4 ToonLighting(InputData inputData, half3 albedo, half metallic, half smoothness, half occlusion, half3 emission, half alpha)
{
	BRDFData brdfData;
	InitializeBRDFDataMetallic(albedo, metallic, smoothness, alpha, brdfData);

	Light mainLight = GetMainLight(inputData.shadowCoord);
	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

	half3 color = GlobalIllumination(brdfData, inputData.bakedGI, occlusion, inputData.normalWS, inputData.viewDirectionWS);
	color += LightingPhysicallyBased(brdfData, mainLight, inputData.normalWS, inputData.viewDirectionWS);

    color += emission;
	return half4(color, alpha);
}

half4 FurLighting(InputData inputData, half3 albedo, half metallic, half smoothness, half occlusion, half3 radiance, half3 emission, half alpha)
{
	BRDFData brdfData;
	InitializeBRDFDataMetallic(albedo, metallic, smoothness, alpha, brdfData);

	Light mainLight = GetMainLight(inputData.shadowCoord);

	MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

    half occ = lerp(occlusion, 1, 0.3);

	half3 color = GlobalIllumination(brdfData, inputData.bakedGI, occ, inputData.normalWS, inputData.viewDirectionWS);

    color += DirectBDRF(brdfData, inputData.normalWS, mainLight.direction, inputData.viewDirectionWS) * radiance;

    //half backLit = furFresnel * (1 - radiance);
    //color.rgb += backLit * inputData.bakedGI * 1 + furRim * 2 * mainLight.color;
	color += emission;

    //clip(0.15 - furFresnel);

	return half4(color, alpha);
}


half4 ClothLighting(InputData inputData, half3 albedo, half3 specular, half smoothness, half occlusion, half3 emission, half alpha)
{
    BRDFData brdfData;
    InitializeBRDFData(albedo, 0, specular, smoothness, alpha, brdfData);
    
    Light mainLight = GetMainLight(inputData.shadowCoord);
    MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

    half3 color = GlobalIllumination(brdfData, inputData.bakedGI, occlusion, inputData.normalWS, inputData.viewDirectionWS);
    color += FabricBDRF(brdfData, mainLight, inputData.normalWS, inputData.viewDirectionWS);

    color += emission;
    return half4(color, alpha);

    /*
    BRDFData brdfData;
    InitializeBRDFDataMetallic(albedo, specular, smoothness, alpha, brdfData);

    Light mainLight = GetMainLight(inputData.shadowCoord);
    MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

    float3 halfDir = SafeNormalize(float3(mainLight.direction) + inputData.viewDirectionWS);

    half nv = abs(dot(inputData.normalWS, inputData.viewDirectionWS));
    half nl = saturate(dot(inputData.normalWS, mainLight.direction));
    float nh = saturate(dot(inputData.normalWS, halfDir));

    half lv = saturate(dot(mainLight.direction, inputData.viewDirectionWS));
    half lh = saturate(dot(mainLight.direction, halfDir));
    //diffuse term
    half diffuseTerm = DisneyDiffuse01(nv, nl, lh, brdfData.perceptualRoughness);

    //specular term
    float roughness = max(brdfData.roughness, 0.002);

    half V = VisibilityCloth(nl, nv);
    half D = DistributionCloth(roughness, nh);

    //half luminance = dot(albedo, half3(0.299, 0.587, 0.114));
    //half3 F = luminance;

    half3 F = half3(0.04, 0.04, 0.04);


    half specularTerm = (V * D) * F;
    specularTerm = max(0, specularTerm * nl);


    //half surfaceReduction;
    //surfaceReduction = 1.0 / (roughness * roughness + 1.0);

    //specularTerm *= any(specular) ? 1.0 : 0.0;

    //half grazingTerm = saturate(smoothness + (1-oneMinusReflectivity));


    half3 Fd = albedo * (inputData.bakedGI + mainLight.color * diffuseTerm * nl);
    half3 Fr = specularTerm * mainLight.color;
    half3 radiance = mainLight.distanceAttenuation * mainLight.shadowAttenuation * nl;

    half3 color = GlobalIllumination(brdfData, inputData.bakedGI, occlusion, inputData.normalWS, inputData.viewDirectionWS);
    //color = (Fd + Fr);

    return half4(color, 1);
    */
}

#endif