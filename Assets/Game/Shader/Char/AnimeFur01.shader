﻿Shader "WalkingFat/Char/AnimeFur01"
{
	Properties
	{
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		//_MixMap("Mix Map", 2D) = "white" {} // R = Smoothness; G = Occlusion; B = Height; A = ???;
		_SSSMap("SSS Map", 2D) = "white" {} // RGB = Interior Color; A = Specular
		//_EmissionMap("Emission", 2D) = "black" {}
		//_Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5

		[Space]
		[Header(Effect)]
		_RimHardness("Rim Hardness", Range(2.0, 10.0)) = 3.0
		_RimThreshold("Rim Threshold", Range(0.0, 2.0)) = 1.0
		_RimStrength("Rim Strength", Range(1.0, 5.0)) = 1.0

		[Space]
		[Header(Fur)]
		_FurMap("Fur Map", 2D) = "white" {}
		_FurLength("Fur Length", Range(0.0, 2.0)) = 1.0
		_FurScale("Fur Scale", Range(1.0, 20.0)) = 10.0
		_FurSoftness("Fur Softness", Range(0.0, 1.0)) = 0.5
		_FurDir("Fur Direction", Range(0.0, 3.0)) = 0.2
		_FurGravity("Fur Gravity", Range(0.0, 1.0)) = 0.1
		_FurAtten("Fur Atten", Range(0.1, 5.0)) = 1.0
		_FurShake("Fur Shake", Range(0.0, 1.0)) = 0.2
		_FurRandShake("Fur Random Shake", Range(0.0, 0.2)) = 0.1
		[IntRange]_FurLayer("Fur layer", Range(2, 12)) = 8
		[Toggle] _FurTrigger("Fur Trigger", int) = 0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300

		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			//Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "CharCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 uv : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD2;
				float4 normalWS : TEXCOORD3;
				float4 tangentWS : TEXCOORD4;    // xyz: tangent, w: viewDir.y
				float4 bitangentWS : TEXCOORD5;    // xyz: bitangent, w: viewDir.z
				half4 fogFactorAndVertexLight : TEXCOORD6;
				float4 shadowCoord : TEXCOORD7;
				float3 gravityDir_tangent : TEXCOORD8;
				float3 viewDir_tangent : TEXCOORD9;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			//float _BumpScale, _Cutoff;
			float _RimHardness;
			float _RimThreshold;
			float _RimStrength;
			float _FurLength;
			float _FurScale;
			float _FurSoftness;
			float _FurDir;
			float _FurGravity;
			float _FurAtten;
			float _FurShake;
			float _FurRandShake;
			float _FurLayer;
			float _FurTrigger;
			float4 _BaseMap_ST;
			float4 _SSSMap_ST;
			float4 _BumpMap_ST;
			//float4 _EmissionMap_ST;
			float4 _FurMap_ST;
			float4 _CameraDepthTexture_TexelSize;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			TEXTURE2D(_SSSMap);				SAMPLER(sampler_SSSMap);
			TEXTURE2D(_BumpMap);							SAMPLER(sampler_BumpMap);
			//TEXTURE2D(_EmissionMap);						SAMPLER(sampler_EmissionMap);
			TEXTURE2D(_FurMap);								SAMPLER(sampler_FurMap);

			TEXTURE2D(_CameraDepthTexture);					SAMPLER(sampler_CameraDepthTexture);

			//global

			// caculate parallax uv offset
			inline half2 CaculateParallaxUV(Varyings input, half heightMulti, half2 furDir)
			{
				half height = 0.01 * heightMulti;
				//float3 viewDir = normalize(input.viewDir_tangent);
				half2 offsetH = input.viewDir_tangent.xy * height;
				half2 offsetG = input.gravityDir_tangent.xy * height;
				half2 offsetD = furDir * height;
				//offsetG = pow(offsetG, 2);
				return offsetH + offsetG * _FurGravity + offsetD;
			}

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				input.positionOS.xyz += input.normalOS.xyz * _FurLength * 0.01;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);
				half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
				half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				output.uv.xy = TRANSFORM_TEX(input.texcoord, _BaseMap);
				output.uv.z = cos((input.positionOS.x + input.positionOS.y) * 10 + _Time.z);
				output.uv.w = cos((input.positionOS.z + input.positionOS.y) * 13 + _Time.z);

				output.normalWS = half4(normalInput.normalWS, viewDirWS.x);
				output.tangentWS = half4(normalInput.tangentWS, viewDirWS.y);
				output.bitangentWS = half4(normalInput.bitangentWS, viewDirWS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
				output.shadowCoord = GetShadowCoord(vertexInput);

				output.positionWS = vertexInput.positionWS;
				output.positionCS = vertexInput.positionCS;

				float3 binormal = cross(input.normalOS, input.tangentOS.xyz ) * input.tangentOS.w;
				float3x3 rotation = float3x3(input.tangentOS.xyz, binormal, input.normalOS );

				Light mainLight = GetMainLight();
				float3 gravityPosOS = TransformWorldToObject(half3(0, 99999, 0));
				float3 cameraPosOS = TransformWorldToObject(_WorldSpaceCameraPos.xyz);

				float3 gravityDirOS = gravityPosOS - input.positionOS.xyz;
				float3 viewDirOS = cameraPosOS - input.positionOS.xyz;

				output.gravityDir_tangent = normalize(mul(rotation, gravityDirOS));
				output.viewDir_tangent = normalize(mul(rotation, viewDirOS));

				return output;
			}

			void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
			{
				inputData = (InputData)0;

				inputData.positionWS = input.positionWS;

				half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
				inputData.normalWS = TransformTangentToWorld(normalTS,
					half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));

				inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				viewDirWS = SafeNormalize(viewDirWS);

				inputData.viewDirectionWS = viewDirWS;

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float2 uv = input.uv.xy;
				half4 baseCol = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);
				half3 normalTS = UnpackNormal(SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, uv));

				//half4 EmissionCol = SAMPLE_TEXTURE2D(_EmissionMap, sampler_EmissionMap, uv);
				
				half3 furCol;
				half furDarkFactor = 0.5 / _FurLayer;
				half furFactor = _FurLength / _FurLayer;
				half furValue = 0;

				half2 uvOffset = CaculateParallaxUV(input, _FurLength, half2(0,0));
				uv -= uvOffset;
				half4 baseTex = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);
				half4 furNoiseTex = SAMPLE_TEXTURE2D(_FurMap, sampler_FurMap, uv * _FurScale * _FurMap_ST.xy);
				half4 furDirTex = SAMPLE_TEXTURE2D(_FurMap, sampler_FurMap, input.uv.xy);
				half2 furDir = pow(furDirTex.xy, 0.5) * 2.0f - 1.0f;
				// shake
				half wave0 = cos(_Time.y) * sin(_Time.y * 1.7) * 0.3 * _FurRandShake;
				half2 randShake = sin(furNoiseTex.z * 40 + (_Time.z + wave0) * 5) * (_FurRandShake + wave0);
				half2 furShake = (input.uv.zw * _FurShake + randShake) * _FurLength * 0.0003;

				furCol = baseTex.rgb;
				furValue = furNoiseTex.a;

				//return max(0, half4(furDir.x,0,0,1));
				
				for (int n = _FurLayer - 1; n > -1; n--)
				{
					uvOffset = CaculateParallaxUV(input, n * furFactor, furDir * _FurDir);
					uvOffset += furShake * (_FurLayer - n - 1);
					baseTex = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv - uvOffset);
					furNoiseTex = SAMPLE_TEXTURE2D(_FurMap, sampler_FurMap, (uv - uvOffset) * _FurScale * _FurMap_ST.xy).a;
					half furTip = step(0.9 - furDarkFactor * n, furNoiseTex.a * furNoiseTex.a);
					furCol = lerp(furCol, baseTex.rgb, furTip);
					furValue = lerp(furValue, furNoiseTex.a, furTip);
				}

				//half furAtten = pow(furValue, _FurAtten);// *furValue;// *furValue;
				//half furAO = furValue * furValue * furValue * furValue;
				//return furAO;

				half3 albedo = lerp(baseCol.rgb, furCol, _FurTrigger);
				half metallic = 0;
				half occlusion = furValue * furValue * furValue * furValue;

				//half3 sss = sssAndSmoothnessMapCol.rgb;
				half smoothness = 0.3;// sssAndSmoothnessMapCol.a;
				half3 emission = half3(0, 0, 0);// sss + half3(0, 0, 0);

				half alpha = 1;

				InputData inputData;
				InitializeInputData(input, normalTS, inputData);

				// radiance
				Light mainLight = GetMainLight(inputData.shadowCoord);

				half NdotL = saturate(dot(inputData.normalWS, mainLight.direction));
				half NdotV = saturate(dot(inputData.normalWS, inputData.viewDirectionWS));

				half furNdotL = pow(NdotL, 2.3 - occlusion);
				half viewAO = min(1, occlusion + 1.4 - NdotV);
				//return viewAO;

				half3 rimDir = inputData.normalWS + mainLight.direction;
				half rim = saturate(dot(inputData.viewDirectionWS, -rimDir));
				half furRim = pow(rim, _FurSoftness + (2 - occlusion));
				//return furRim;

				half furFresnel = pow(1 - NdotV, (2 - occlusion) * 1) * 0.5;
				//return step(0.85, 1 - furFresnel);

				half buttomFade = max(0, dot(inputData.normalWS, half3(0, 1, 0)));
				furRim *= min(1, buttomFade + 0.2);
				//wreturn rim;
				//return min(1, NdotL * mainLight.distanceAttenuation * mainLight.shadowAttenuation + rim + furFresnel);

				half furAtten = saturate(mainLight.distanceAttenuation * mainLight.shadowAttenuation * furNdotL + furRim) * viewAO;
				half4 sssCol = SAMPLE_TEXTURE2D(_SSSMap, sampler_SSSMap, half2(furAtten, 0.5));
				//return furAtten;

				//half3 radiance = mainLight.color * lerp(sssCol.rgb, half3(furAtten, furAtten, furAtten), step(_Time.y % 2, 1));
				//return half4(radiance, 1);
				half3 radiance = mainLight.color * sssCol.rgb;

				half4 color = FurLighting(inputData, albedo, metallic, smoothness, occlusion, radiance, emission, alpha);

				

				/*
				half NdotV = max(0.0, dot(inputData.normalWS, inputData.viewDirectionWS));
				half rim0 = saturate(pow(1 - NdotV, 3) * 1);
				half rim1 = min(1, rim0 * 3);

				half clipValue = min(1, (1 - rim0) * furValue * furValue * furValue * furValue + (1 - rim1) * 2);
				*/
				//return clipValue;

				//color.rgb += rimCol;

				//clip(clipValue - 0.9);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				return color;
			}
			ENDHLSL
		}

		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment


			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			CBUFFER_START(UnityPerMaterial)
			float4 _BaseMap_ST;
			half _Cutoff;
			half _FurLength;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);

			struct Attributes
			{
				float4 positionOS     : POSITION;
				float3 normalOS : NORMAL;
				float2 texcoord     : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv           : TEXCOORD0;
				float4 positionCS   : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
					UNITY_VERTEX_OUTPUT_STEREO
			};

			Varyings DepthOnlyVertex(Attributes input)
			{
				Varyings output = (Varyings)0;
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
				input.positionOS.xyz += input.normalOS.xyz * _FurLength * 0.01;
				output.positionCS = TransformObjectToHClip(input.positionOS.xyz);
				return output;
			}

			half4 DepthOnlyFragment(Varyings input) : SV_TARGET
			{
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half alpha = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv).a;

				clip(alpha - _Cutoff);

				return 0;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
