﻿Shader "WalkingFat/Char/ComicStyle"
{
	Properties
	{
		_BaseMap("Base Map", 2D) = "white" {}
		_BumpMap("Bump Map", 2D) = "normal" {}
		_MixMap("Mix Color", 2D) = "black" {}

		_Tint("Tint", Color) = (1,1,1,1)
		_Cutoff("Cutoff", Range(0.0, 1.0)) = 0.5

		_ShadowColor("Shadow Color", Color) = (0.5,0.5,0.5,1)
		_UnlitThreshold("Unlit Threshold", Range(0,1)) = 0.1
		_ShadowThreshold("Shadow Threshold", Range(0,1)) = 0.1

		_Specular("Specular", Range(0,1)) = 0.5
		_SpecularRamp("Specular Ramp", 2D) = "white"{}

		_RimBrightness("Rim Brightness", Range(1.0, 10.0)) = 5.0
		_RimWidth("Rim Width", Range(1.0, 0.05)) = 0.02

		_TransparencyDist("Transparency Distance", Range(0.0, 5.0)) = 2.0

		_SketchScale("Sketch Scale", Range(0.0, 20.0)) = 2
		_SketchDensity("Sketch Density", Range(0.0, 5.0)) = 0.5
		_SketchStrength("Sketch Strength", Range(0.0, 5.0)) = 1
		_SketchWidth("Sketch Width", Range(1.0, 10.0)) = 2
		_SketchRimStrength("Sketch Rim Strength", Range(0.0, 3.0)) = 0.5

		_DotScale("Dot Scale", Range(0.0, 20.0)) = 2
		_DotDensity("Dot Density", Range(1.0, 5.0)) = 0.5
		_DotStrength("Dot Strength", Range(0.0, 5.0)) = 1
		_DotWidth("Dot Width", Range(1.0, 10.0)) = 2
	}

	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True" }
		LOD 300

		Pass{
			Name "ToonChar"
			Tags { "LightMode" = "UniversalForward" }
			
			ZWrite On

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ToonPassVertex
			#pragma fragment ToonPassFragment

			//#include "ReceiveRainPass.hlsl"

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Assets/Game/Shader/Common/DitherTransparency.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord1 : TEXCOORD0;
				float2 texcoord2 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float4 normalWS : TEXCOORD0;
				float4 positionNDC : TEXCOORD1;
				float4 uv : TEXCOORD2;
				float4 tangentWS : TEXCOORD3;
				float4 bitangentWS : TEXCOORD4;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 5);
				float4 fogFactorAndVertexLight : TEXCOORD6;
				float4 shadowCoord : TEXCOORD7;
				float4 color : TEXCOORD8;
				float3 positionWS : TEXCOORD9;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			half _Specular;
			half4 _Tint;
			half4 _ShadowColor;
			half _UnlitThreshold;
			half _ShadowThreshold;
			half _TransparencyDist;
			float4 _BaseMap_ST;
			float4 _BumpMap_ST;
			half _Cutoff;
			half _SketchStrength;
			half _SketchDensity;
			half _SketchScale;
			half _SketchWidth;
			half _SketchRimStrength;
			half _DotScale;
			half _DotStrength;
			half _DotDensity;
			half _DotWidth;
			CBUFFER_END

			// global
			half4 _AmbientColor;
			float3 _CharPosWorld;

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			TEXTURE2D(_BumpMap);							SAMPLER(sampler_BumpMap);
			TEXTURE2D(_MixMap);								SAMPLER(sampler_MixMap);
			TEXTURE2D(_SpecularRamp);						SAMPLER(sampler_SpecularRamp);

			Varyings ToonPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);
				half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
				half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				output.uv.xy = TRANSFORM_TEX(input.texcoord1, _BaseMap);
				output.uv.zw = TRANSFORM_TEX(input.texcoord2, _BumpMap);

				output.normalWS = half4(normalInput.normalWS, viewDirWS.x);
				output.tangentWS = half4(normalInput.tangentWS, viewDirWS.y);
				output.bitangentWS = half4(normalInput.bitangentWS, viewDirWS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
				output.shadowCoord = GetShadowCoord(vertexInput);

				output.positionNDC = vertexInput.positionNDC;
				output.positionCS = vertexInput.positionCS;
				output.positionWS = vertexInput.positionWS;

				// custom value
				half3 pivotPosWS = TransformObjectToWorld(half3(0, 0, 0));
				pivotPosWS.y = GetCameraPositionWS().y;
				output.color.x = distance(pivotPosWS, GetCameraPositionWS());
				output.color.yzw = half3(0, 0, 0);

				return output;
			}

			void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
			{
				inputData = (InputData)0;

				inputData.positionWS = input.positionWS;

				half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
				inputData.normalWS = TransformTangentToWorld(normalTS,
					half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));

				inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				viewDirWS = SafeNormalize(viewDirWS);

				inputData.viewDirectionWS = viewDirWS;

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 ToonPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 baseTex = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv.xy);
				half3 normalTS = UnpackNormal(SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, input.uv.zw));
				half4 mixTex = SAMPLE_TEXTURE2D(_MixMap, sampler_MixMap, input.uv.zw);

				InputData inputData;
				InitializeInputData(input, normalTS, inputData);

				// dir
				half3 viewDir = inputData.viewDirectionWS;

				Light mainLight = GetMainLight(inputData.shadowCoord);
				MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

				half3 lightDir = mainLight.direction;

				// Common
				half2 screenPos = input.positionNDC.xy / input.positionNDC.w;

				half NdotL = max(0.0, dot(input.normalWS.xyz, lightDir.xyz));
				half NWdotL = max(0.0, dot(inputData.normalWS.xyz, lightDir.xyz));

				half NdotV = max(0.0, dot(input.normalWS.xyz, inputData.viewDirectionWS.xyz));
				half NWdotV = max(0.0, dot(inputData.normalWS.xyz, inputData.viewDirectionWS.xyz));

				half screenRatio = _ScreenParams.x / _ScreenParams.y;

				// specular
				half specularPow = exp2((1 - _Specular) * 10.0 + 1.0);
				half3 halfVector = normalize(lightDir + viewDir);

				half specular = max(0, dot(halfVector, inputData.normalWS));
				half4 specularTex = SAMPLE_TEXTURE2D(_SpecularRamp, sampler_SpecularRamp, half2(specular, 0.5));
				half specularValue = specularTex.x;

				// attenuation
				half attenuation = mainLight.distanceAttenuation * mainLight.shadowAttenuation;
				attenuation = smoothstep(0.0 + _UnlitThreshold, 0.3 + _UnlitThreshold, attenuation);

				// side rim
				half falloff = clamp(1.0 - abs(NdotV), 0.02, 0.98);

				half rimDot = saturate(0.5 * (dot(input.normalWS.xyz, -lightDir.xyz + half3(1, 0, 0)) + 1.5));
				falloff = saturate(rimDot * falloff);
				half rimValue = falloff;// smoothstep(0.4, 1, falloff);

				// sketch
				half sketchLinesUL = abs(sin((screenPos.x + screenPos.y) * 50 * _SketchWidth));
				half sketchLinesDL = abs(sin((screenPos.x - screenPos.y) * 50 * _SketchWidth));

				half sketchMask = smoothstep(0 + _UnlitThreshold, 0.8 + _UnlitThreshold, NWdotL);
				sketchMask += rimValue * _SketchRimStrength;

				half sketchNdotL = 1 - sketchMask;

				half sketchScale = _SketchScale / _SketchDensity;
				half sketchDiff = sketchNdotL * (sketchScale + 1);

				half sketchStrength = min(1, sketchDiff * _SketchDensity - sketchScale);
				half sketchUL = smoothstep(sketchStrength, sketchStrength + 0.3, sketchLinesUL);

				sketchScale *= 0.4;
				sketchDiff = sketchNdotL * (sketchScale + 1);
				sketchStrength = min(1, sketchDiff * _SketchDensity - sketchScale);
				half sketchDL = smoothstep(sketchStrength, sketchStrength + 0.3, sketchLinesDL);

				half corssSketchValue = sketchDL;// sketchUL * sketchDL;

				// Polka dot
				//half2 dotUV = input.uv.xy;// input.positionNDC.xy / input.positionNDC.z;
				half dotLinesUL = abs(sin((screenPos.x * screenRatio + screenPos.y) * 50 * _DotWidth));
				half dotLinesDL = abs(sin((screenPos.x * screenRatio - screenPos.y) * 50 * _DotWidth));
				half dotNdotL = 1 - rimValue;

				half dotScale = _DotScale / _DotDensity;
				half dotDiff = dotNdotL * (dotScale + 1);

				half dotStrength = min(1, dotDiff * _DotDensity - dotScale);
				half dotValue = smoothstep(dotStrength, dotStrength + 0.3, dotLinesUL * dotLinesDL);


				// Cel
				half celValue = smoothstep(0 + _UnlitThreshold, 0.1 + _UnlitThreshold, NWdotL);
				half litValue = lerp(celValue, specularValue, _Specular) * attenuation;
				half3 litCol = lerp(_ShadowColor.xyz, 1, litValue) * mainLight.color.xyz + _AmbientColor.xyz * 0.2;

				half4 color = half4(baseTex.xyz * litCol * 3, 1.0);
				color.rgb *= min(1, corssSketchValue + _SketchStrength);
				color.rgb += dotValue * color.rgb * _DotStrength;

				//half4 a = GetDitherTransparencyWithDistance(_TransparencyDist, baseTex.a * _Cutoff, input.positionNDC, input.color.x);
				//clip(a);


				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				color.a = 1;

				return color;
			}
			ENDHLSL
		}

		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
