﻿Shader "WalkingFat/Char/AnimePBR01"
{
	Properties
	{
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_MixMap("Mix Map", 2D) = "white" {} // R = Smoothness; G = Occlusion; B = Height; A = ???;
		_EmissionMap("Emission Map", 2D) = "black" {}
		_InnerMap("Inner Map", 2D) = "white" {}
		_Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5

		_InnerDepth("Inner Depth", Range(0.0, 2.0)) = 2.0
		_InnerColor("Inner Color", Color) = (1,0,0,1)
		_InnerRimMask("Inner Rim Mask", Range(1.0, 10.0)) = 4.0

		_SssStrength("SSS Strength", Range(0.0, 1)) = 0.01
		_SssDistortion("SSS Distortion", Range(0.0, 1)) = 0.01
		_SssBasic("SSS Basic", Range(0.0, 1)) = 0.01
		_Smoothness("Smoothness", Range(0.0, 1.0)) = 0.5

		_RimRampMap("Rim Ramp Map", 2D) = "white" {}
		_RimStrength("Rim Strength", Range(0.0, 2)) = 0.01

		_DetailDist("Detail Distance", Range(1.0, 100.0)) = 10

		_SketchDensity("Sketch Density", Range(0.0, 1.0)) = 0.5
		_SketchWidth("Sketch Width", Range(1.0, 10.0)) = 2
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300

		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "CharCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float4 positionWS : TEXCOORD2; // xyz: pos, w: to camera dist 
				float4 normalWS : TEXCOORD3;
				float4 tangentWS : TEXCOORD4;    // xyz: tangent, w: viewDir.y
				float4 bitangentWS : TEXCOORD5;    // xyz: bitangent, w: viewDir.z
				half4 fogFactorAndVertexLight : TEXCOORD6;
				float4 shadowCoord : TEXCOORD7;
				float3 viewDir_tangent : TEXCOORD8;
				float4 positionNDC : TEXCOORD9;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
				float _InnerDepth;
				float4 _InnerColor;
				float _InnerRimMask;
				float _SssStrength;
				float _SssDistortion;
				float _SssBasic;
				float _Smoothness;
				float _RimStrength;
				float _DetailDist;
				half _SketchDensity;
				half _SketchWidth;
				float4 _BaseMap_ST;
				float4 _BumpMap_ST;
				float4 _MixMap_ST;
				float _Cutoff;
				//float4 _RainNoiseMap_ST;
				CBUFFER_END

				TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
				TEXTURE2D(_BumpMap);							SAMPLER(sampler_BumpMap);
				TEXTURE2D(_MixMap);								SAMPLER(sampler_MixMap);
				TEXTURE2D(_EmissionMap);						SAMPLER(sampler_EmissionMap);
				TEXTURE2D(_InnerMap);							SAMPLER(sampler_InnerMap);
				TEXTURE2D(_RimRampMap);							SAMPLER(sampler_RimRampMap);
				

				//global
				float4 _AmbientColor;
				float _LitRateTime;

				Varyings LitPassVertex(Attributes input)
				{
					Varyings output = (Varyings)0;

					UNITY_SETUP_INSTANCE_ID(input);
					UNITY_TRANSFER_INSTANCE_ID(input, output);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

					VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
					VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);
					half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
					half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
					half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

					output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

					output.normalWS = half4(normalInput.normalWS, viewDirWS.x);
					output.tangentWS = half4(normalInput.tangentWS, viewDirWS.y);
					output.bitangentWS = half4(normalInput.bitangentWS, viewDirWS.z);

					OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
					OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

					output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
					output.shadowCoord = GetShadowCoord(vertexInput);

					output.positionWS.xyz = vertexInput.positionWS;
					output.positionWS.w = distance(vertexInput.positionWS, _WorldSpaceCameraPos.xyz);
					output.positionCS = vertexInput.positionCS;

					float3 binormal = cross(input.normalOS, input.tangentOS.xyz) * input.tangentOS.w;
					float3x3 rotation = float3x3(input.tangentOS.xyz, binormal, input.normalOS);

					float3 cameraPosOS = TransformWorldToObject(_WorldSpaceCameraPos.xyz);
					float3 viewDirOS = cameraPosOS - input.positionOS.xyz;

					output.viewDir_tangent = normalize(mul(rotation, viewDirOS));
					output.positionNDC = vertexInput.positionNDC;

					return output;
				}

				// caculate parallax uv offset
				inline half2 CaculateParallaxUV(Varyings input, half heightMulti)
				{
					half height = 0.01 * heightMulti;
					half2 offset = input.viewDir_tangent.xy * height;
					return offset;
				}

				void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
				{
					inputData = (InputData)0;

					inputData.positionWS = input.positionWS.xyz;

					half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
					inputData.normalWS = TransformTangentToWorld(normalTS,
						half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));

					inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
					viewDirWS = SafeNormalize(viewDirWS);

					inputData.viewDirectionWS = viewDirWS;

					inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

					inputData.fogCoord = input.fogFactorAndVertexLight.x;
					inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
					inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
				}

				half4 LitPassFragment(Varyings input) : SV_Target
				{
					UNITY_SETUP_INSTANCE_ID(input);
					UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
					return half4((input.normalWS.xyz + 1) * 0.5, 1);
					float2 uv = input.uv;
					half4 baseCol = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);
					half3 normalTS = UnpackNormal(SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, uv));
					half4 mixTex = SAMPLE_TEXTURE2D(_MixMap, sampler_MixMap, uv);

					half3 albedo = baseCol.rgb;
					float alpha = baseCol.a;
					half metallic = mixTex.x;
					half smoothness = mixTex.y;
					half occlusion = mixTex.z;
					//half3 sss = mixTex.w * _SssStrength;// , _SssDistortion, _SssBasic);
					//return mixTex.w;
					half3 emission = half3(0,0,0);

					InputData inputData;
					InitializeInputData(input, normalTS, inputData);
					
					half chagneTrigger = step(2, _Time.y % 4);

					half2 screenPos = input.positionNDC.xy / input.positionNDC.w;
					half detail = saturate((input.positionWS.w - _DetailDist) / _DetailDist); // 0: close, 1: far
					Light mainLight = GetMainLight(inputData.shadowCoord);
					half NdotL = dot(inputData.normalWS, mainLight.direction);
					half NdotV = dot(inputData.normalWS, inputData.viewDirectionWS);

					half atten = saturate(mainLight.distanceAttenuation * mainLight.shadowAttenuation * NdotL);

					//return mainLight.distanceAttenuation * mainLight.shadowAttenuation;

					// SSS
					half3 sssLitDir = inputData.normalWS * _SssDistortion + mainLight.direction;
					half VdotNL = dot(inputData.viewDirectionWS, -sssLitDir);
					half sssLitMask = pow(saturate(VdotNL * 0.5 + 0.5), 2);
					half sssLitValue = sssLitMask * mixTex.w * _SssStrength;
					sssLitValue = max(sssLitValue, _SssBasic);
					half3 sss = sssLitValue * albedo * mainLight.color;

					half rimMask = saturate(sssLitMask * 20 + 0.6);
					//return rimMask;

					// Back Lit
					half NdotFL = dot(inputData.normalWS, normalize(half3(mainLight.direction.x, 0, mainLight.direction.z)));
					half backLitValue = max(0, -0.1 - NdotFL);
					backLitValue = backLitValue * backLitValue * backLitValue * backLitValue;
					half3 backLitCol = (_AmbientColor.xyz + albedo.xyz * 0.5) * backLitValue * _LitRateTime * 0.03;
					//return backLitValue;

					// Inner color
					half2 innerUvOffset = CaculateParallaxUV(input, _InnerDepth);
					half4 innerTex = SAMPLE_TEXTURE2D(_InnerMap, sampler_InnerMap, input.uv.xy - innerUvOffset);
					half innerMask = innerTex.a;
					half innerRimMask = 1 - min(1, pow(1.0 - max(0, NdotV), _InnerRimMask) * 10);
					innerRimMask *= 1 - atten;
					innerRimMask *= cos(_Time.y * 3) * 0.5 + 0.5;
					half3 innerCol = innerTex.rgb * _InnerColor.rgb * innerMask * innerRimMask * _InnerColor.a;

					//return half4(innerCol, 1);

					// Rim light
					half falloffU = 1.0 - abs(NdotV);

					half rimlightDot = saturate(0.5 * (dot(inputData.normalWS, mainLight.direction + half3(-1, 1, 0)) + 1.5));
					falloffU = saturate(rimlightDot * falloffU * rimMask);
					falloffU = SAMPLE_TEXTURE2D(_RimRampMap, sampler_RimRampMap, half2(falloffU, 0.5f)).r;

					half rimLitValue = falloffU * min(1, mainLight.distanceAttenuation * mainLight.shadowAttenuation + 0.2);
					half3 rimCol = rimLitValue * _RimStrength * mainLight.color;

					// Rim Dark
					half rimDark = saturate(NdotV + 0.6);
					rimDark = smoothstep(0, 0.5, rimDark * rimDark * rimDark + NdotL * 0.5);

					half viewRim = saturate(NdotV + 0.4);
					viewRim = viewRim * viewRim;
					half rimEdgeDark = 1 - (1 - viewRim) * atten;
					//return rimEdgeDark;

					// sketch
					half screenRatio = _ScreenParams.y / _ScreenParams.x;
					half sketchLinesUL = abs(sin((screenPos.x + screenPos.y * screenRatio) * 100 * _SketchWidth));
					half sketchLinesDL = abs(sin((screenPos.x - screenPos.y * screenRatio) * 100 * _SketchWidth));

					half attenEdge = atten + min(1, max(0, -0.1 - NdotFL) * 2);
					half attenEdgeSketch = min(1, sketchLinesDL * sketchLinesDL + pow(attenEdge, 0.5) + 0.7);
					//return attenEdgeSketch;
					attenEdgeSketch = lerp(attenEdgeSketch, 1, detail);

					half sketch = smoothstep(_SketchDensity, _SketchDensity + 0.3, sketchLinesDL);
					sketch = min(1, sketch + 0.5);
					sketch = lerp(sketch, 1, detail);


					half4 color = ToonLighting(inputData, albedo, metallic, smoothness, occlusion, emission, alpha);

					color.rgb += backLitCol * sketch + sss;
					color.rgb *= rimEdgeDark * rimDark * attenEdgeSketch;
					color.rgb = lerp(color.rgb, rimCol, rimLitValue);

					clip(alpha - _Cutoff);

					color.rgb = MixFog(color.rgb, inputData.fogCoord);
					return color;
				}
				ENDHLSL
			}

			Pass
			{
				Name "ShadowCaster"
				Tags{"LightMode" = "ShadowCaster"}

				ZWrite On
				ZTest LEqual
				Cull Off

				HLSLPROGRAM
				// Required to compile gles 2.0 with standard srp library
				#pragma prefer_hlslcc gles
				#pragma exclude_renderers d3d11_9x
				#pragma target 2.0

				// GPU Instancing
				#pragma multi_compile_instancing

				#pragma vertex ShadowPassVertex
				#pragma fragment ShadowPassFragment

				#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

				ENDHLSL
			}

			Pass
			{
				Name "DepthOnly"
				Tags{"LightMode" = "DepthOnly"}

				ZWrite On
				ColorMask 0
				Cull Off

				HLSLPROGRAM
				// Required to compile gles 2.0 with standard srp library
				#pragma prefer_hlslcc gles
				#pragma exclude_renderers d3d11_9x
				#pragma target 2.0

				// GPU Instancing
				#pragma multi_compile_instancing

				#pragma vertex DepthOnlyVertex
				#pragma fragment DepthOnlyFragment

				#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
				ENDHLSL
			}
	}
	FallBack "Hidden/InternalErrorShader"
}
