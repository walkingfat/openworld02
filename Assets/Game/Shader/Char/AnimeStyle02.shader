﻿Shader "WalkingFat/Char/AnimeStyle02"
{
	Properties
	{
		_BaseMap("Base Map", 2D) = "white" {}
		_BumpMap("Bump Map", 2D) = "normal" {}
		_MixMap("Mix Color", 2D) = "black" {}
		_SpecularRamp("Specular Ramp", 2D) = "white"{}

		_Tint("Tint", Color) = (1,1,1,1)
		_Cutoff("Cutoff", Range(0.0, 1.0)) = 0.5
		_HeightOffset("Height Offset", Range(0.0, 2.0)) = 1.0

		_ShadowColor("Shadow Color", Color) = (0.5,0.5,0.5,1)

		_CelHardness("Cel Hardness", Range(0.0, 1.0)) = 0.02
		_UnlitThreshold("Unlit Threshold", Range(0.0, 1.0)) = 0.4

		_RimHardness("Rim Hardness", Range(0.0, 1.0)) = 0.02

		_HairSpecularStrength("Hair Specular Strength", Range(0, 5)) = 1
		_HairSpecularPow("Hair Specular Power", Range(0.1, 5.0)) = 2.0
		_MetalSpecularStrength("Metal Specular Strength", Range(0, 5)) = 1
		_MetalSpecularPow("Metal Specular Power", Range(0.1, 5.0)) = 2.0

		_RimThreshold("Rim Threshold", Range(0.0, 1.0)) = 0.5
		_RimStrength("Rim Strength", Range(0.0, 30.0)) = 2.0

		_EmissionStrength("Emission Strength", Range(0, 5)) = 1

        //_TransparencyDist("Transparency Distance", Range(0.0, 5.0)) = 2.0
	}

	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True" }
		LOD 300

		Pass{
			Name "ToonChar"
			Tags { "LightMode" = "UniversalForward" }
			
			Stencil
			{
				ref 200
				Comp Always
				Pass Replace
			}

			//ColorMask 0
			ZWrite On
			//Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ToonPassVertex
			#pragma fragment ToonPassFragment

			//#include "ReceiveRainPass.hlsl"

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            //#include "Assets/Game/Shader/Common/DitherTransparency.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord1 : TEXCOORD0;
				float2 texcoord2 : TEXCOORD1;
				float4 vertexColor: COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
                float4 positionCS : SV_POSITION;
				float4 normalWS : TEXCOORD0;
                float4 positionNDC : TEXCOORD1;
				float4 uv : TEXCOORD2;
				float4 tangentWS : TEXCOORD3;
				float4 bitangentWS : TEXCOORD4;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 5);
				float4 fogFactorAndVertexLight : TEXCOORD6;
				float4 shadowCoord : TEXCOORD7;
				float4 color : TEXCOORD8;
				float3 positionWS : TEXCOORD9;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			half _Specular;
			half4 _Tint;
			float _HeightOffset;
			half4 _ShadowColor;
			half _CelHardness;
			half _UnlitThreshold;
			half _RimHardness;
			half _EmissionStrength;
			half _HairSpecularPow;
			half _HairSpecularStrength;
			half _MetalSpecularPow;
			half _MetalSpecularStrength;
			half _RimThreshold;
			half _RimStrength;
			float4 _BaseMap_ST;
			float4 _BumpMap_ST;
			half _Cutoff;
			CBUFFER_END

			// global
			half4 _AmbientColor;
			float3 _CharPosWorld;

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			TEXTURE2D(_BumpMap);							SAMPLER(sampler_BumpMap);
			TEXTURE2D(_MixMap);								SAMPLER(sampler_MixMap);
			TEXTURE2D(_SpecularRamp);						SAMPLER(sampler_SpecularRamp);

			Varyings ToonPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);
				float3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
				float3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
				float fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				output.uv.xy = TRANSFORM_TEX(input.texcoord1, _BaseMap);
				output.uv.zw = TRANSFORM_TEX(input.texcoord2, _BumpMap);

				output.normalWS = float4(normalInput.normalWS, viewDirWS.x);
				output.tangentWS = float4(normalInput.tangentWS, viewDirWS.y);
				output.bitangentWS = float4(normalInput.bitangentWS, viewDirWS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = float4(fogFactor, vertexLight);
				output.shadowCoord = GetShadowCoord(vertexInput);
                
                output.positionNDC = vertexInput.positionNDC;
				output.positionCS = vertexInput.positionCS;
				output.positionWS = vertexInput.positionWS;

				float3 pivotWS = TransformObjectToWorld(float3(0, input.positionOS.y, 0)); //input.positionOS.y / _HeightOffset
				float3 pivotViewDir = normalize(GetCameraPositionWS() - pivotWS);
				float3 litDir = GetMainLight().direction;
				float LdotV = max(0, dot(pivotViewDir, litDir));
				LdotV = LdotV * LdotV * LdotV;
				
				float backLit = max(0, dot(pivotViewDir, -litDir));
				output.color = float4(input.vertexColor.xy, LdotV, backLit);

				return output;
			}

			void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
			{
				inputData = (InputData)0;

				inputData.positionWS = input.positionWS;

				half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
				inputData.normalWS = TransformTangentToWorld(normalTS,
					half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));

				inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				viewDirWS = SafeNormalize(viewDirWS);

				inputData.viewDirectionWS = viewDirWS;

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 ToonPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 baseTex = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv.xy);
				half3 normalTS = UnpackNormal(SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, input.uv.zw));
				half4 mixTex = SAMPLE_TEXTURE2D(_MixMap, sampler_MixMap, input.uv.zw);

				InputData inputData;
				InitializeInputData(input, normalTS, inputData);

				// dir
				half3 viewDir = inputData.viewDirectionWS;

				Light mainLight = GetMainLight(inputData.shadowCoord);
				MixRealtimeAndBakedGI(mainLight, inputData.normalWS, inputData.bakedGI, half4(0, 0, 0, 0));

				half3 lightDir = mainLight.direction;

				// attenuation
				half attenuation = mainLight.distanceAttenuation * mainLight.shadowAttenuation;


				// brush
				//half brushValue = abs(sin((input.uv.z + input.uv.w) * 500));
				//return mixTex.g;

				// Cel
				half NdotL = max(0.0, dot(inputData.normalWS, lightDir));
				half NdotV = max(0.0, dot(inputData.normalWS, viewDir));

				half celValue = smoothstep(_UnlitThreshold, _CelHardness + _UnlitThreshold, NdotL);

				// Back light
				half3 rimLitDir = normalize(viewDir - half3(0, 0.5, 0));
				half NdotR = max(0.0, dot(inputData.normalWS, rimLitDir));
				half falloffU = clamp(1.0 - NdotR, 0.02, 0.98);
				half rimValue = saturate(0.5 * (NdotL + 1.5));
				rimValue = saturate(rimValue * falloffU);
				rimValue = smoothstep(_RimThreshold, _RimHardness + _RimThreshold, rimValue) * _RimStrength;
				half3 rimCol = rimValue * attenuation * mainLight.color;

				// emission
				half3 emissionCol = mixTex.rgb * _EmissionStrength;

				// final lit
				half litValue = min(1, celValue * attenuation * NdotV); // base lit
				litValue = litValue * input.color.z; // main lit
				litValue += NdotV * NdotV * NdotV * input.color.w * 0.4; // back lit

				//half maskedLit = (litValue + 1) * 0.5;
				//litValue = lerp(litValue, maskedLit, input.color.r);// *mixTex.g; // lit mask
				litValue = lerp(litValue, 1, mixTex.b);
				litValue *= mixTex.g;

				// specular
				half3 halfVector = normalize(lightDir + viewDir);
				half specular = max(0, dot(halfVector, inputData.normalWS));

				// Metal Specular
				//half metalSpecularValue = pow(specular, _MetalSpecularPow);
				half4 metalSpecularTex = SAMPLE_TEXTURE2D(_SpecularRamp, sampler_SpecularRamp, half2(specular, 0.5));
				half metalSpecularValue = metalSpecularTex.x * input.color.g * _MetalSpecularStrength;
				metalSpecularValue = pow(abs(metalSpecularValue), _MetalSpecularPow);

				// Hair Specular
				half hairSpecularValue = pow(specular, _HairSpecularPow) + NdotV * 0.3;
				hairSpecularValue *= mixTex.r * _HairSpecularStrength;

				half4 metalSpecularCol = half4(metalSpecularValue * baseTex.rgb * mainLight.color, 1);
				half4 hairSpecularCol = half4(hairSpecularValue * mainLight.color, 1);

				half4 specularCol = metalSpecularCol + hairSpecularCol;

				//return litValue + hairSpecularValue + metalSpecularValue + rimValue;

				// final col
				half3 litCol = baseTex.rgb * NdotL * attenuation * mainLight.color;// +inputData.bakedGI * 0.2;

				half4 color = half4(litCol + emissionCol + rimCol + inputData.bakedGI * 0.2, 1);

				//color = color * min(1, 1.2 - input.color.w);
				//color.rgb += inputData.bakedGI * 0.2;

				//half4 a = GetDitherTransparencyWithDistance(_TransparencyDist, baseTex.a * _Cutoff, input.positionNDC, input.color.x);
				//clip(a);
				clip(_Cutoff);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);

				return color;
			}
			ENDHLSL
		}
		/*
		Pass
		{
			Name "CharOutlineRim"

			ZWrite On
			Cull Front

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0
			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex PassVertex
			#pragma fragment PassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Assets/Game/Shader/Common/DitherTransparency.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0
					;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 uv : TEXCOORD0;
                float4 positionNDC : TEXCOORD1;
				float4 positionCS : SV_POSITION;
				float4 shadowCoord : TEXCOORD2;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _BaseMap_ST;
			float _RimBrightness;
			float _RimWidth;
			float _Specular; 
            float _TransparencyDist;
			float _Cutoff;
			half _UnlitThreshold;
			half _ShadowThreshold;
			half _HeightOffset;
			CBUFFER_END

			float4 _AmbientColor;
			float _CameraTan;
            float3 _CharPosWorld;

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);

			Varyings PassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv.xy = TRANSFORM_TEX(input.texcoord, _BaseMap);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

				half3 normalWS = normalize(normalInput.normalWS);
				half3 positionWS = vertexInput.positionWS;

				half3 viewDirWS = _WorldSpaceCameraPos - positionWS;
				half3 lightDir = normalize(_MainLightPosition.xyz);
				half sss = max(0, dot(normalWS, lightDir));

				half distWS = distance(positionWS, GetCameraPositionWS());
				half width = min(_RimWidth, distWS * _CameraTan);
				half3 offset = width * normalWS * sss;

				half luminace = dot(_MainLightColor.xyz, float3(0.299, 0.587, 0.114));
				luminace = min(1, luminace * 5);

				positionWS.xyz += offset * luminace;

                output.positionCS = TransformWorldToHClip(positionWS);

				float4 ndc = output.positionCS * 0.5f;
				output.positionNDC.xy = float2(ndc.x, ndc.y * _ProjectionParams.x) + ndc.w;
				output.positionNDC.zw = output.positionCS.zw;

				// custom value
				half3 pivotPosWS = TransformObjectToWorld(half3(0, 0, 0));
				pivotPosWS.y = GetCameraPositionWS().y;
				output.uv.z = distance(pivotPosWS, GetCameraPositionWS());
				output.uv.w = 0;

				output.shadowCoord = GetShadowCoord(vertexInput);

				return output;
			}

			half4 PassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 baseTex = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv);

				Light mainLight = GetMainLight(input.shadowCoord);

				// attenuation
				half attenuation = mainLight.distanceAttenuation * mainLight.shadowAttenuation;
				attenuation = smoothstep(0.0 + _UnlitThreshold, 0.02 + _UnlitThreshold, attenuation);

				half3 pivotViewDirWS = normalize(GetCameraPositionWS() - (_CharPosWorld + half3(0, _HeightOffset, 0)));
				half attenAdd = 1 - max(0, dot(pivotViewDirWS, mainLight.direction));
				attenAdd = clamp(attenAdd * _ShadowThreshold, 0, 0.4);

				attenuation += attenAdd + 0.3; // change atten strength by lightDir dot viewDir(char center pivot)

				half4 color = half4(baseTex.xyz * (_MainLightColor.xyz * _RimBrightness + _AmbientColor) * attenuation, 1);

				half4 a = GetDitherTransparencyWithDistance(_TransparencyDist, baseTex.a * _Cutoff, input.positionNDC, input.uv.z);
				clip(a);

				return color;
			}
			ENDHLSL
		}
		*/
		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
