﻿Shader "WalkingFat/Char/Cloth01"
{
	Properties
	{
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_MixMap("Mix Map", 2D) = "white" {} // R = Smoothness; G = Occlusion; B = Height; A = ???;
		//_EmissionMap("Emission", 2D) = "black" {}
		//_Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5

		_Sheen("Specular", Range(0.0, 1.0)) = 0.02
		_Smoothness("Smoothness", Range(0.0, 1.0)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300

		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "CharCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD2;
				float4 normalWS : TEXCOORD3;
				float4 tangentWS : TEXCOORD4;    // xyz: tangent, w: viewDir.y
				float4 bitangentWS : TEXCOORD5;    // xyz: bitangent, w: viewDir.z
				half4 fogFactorAndVertexLight : TEXCOORD6;
				float4 shadowCoord : TEXCOORD7;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
				//float _BumpScale, _Cutoff;
				float _Sheen;
				float _Smoothness;
				float _RimStrength;
				float4 _BaseMap_ST;
				float4 _BumpMap_ST;
				float4 _MixMap_ST;
				//float4 _RainNoiseMap_ST;
				CBUFFER_END

				TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
				TEXTURE2D(_BumpMap);							SAMPLER(sampler_BumpMap);
				TEXTURE2D(_MixMap);								SAMPLER(sampler_MixMap);

				//global

				Varyings LitPassVertex(Attributes input)
				{
					Varyings output = (Varyings)0;

					UNITY_SETUP_INSTANCE_ID(input);
					UNITY_TRANSFER_INSTANCE_ID(input, output);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

					VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
					VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);
					half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
					half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
					half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

					output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);

					output.normalWS = half4(normalInput.normalWS, viewDirWS.x);
					output.tangentWS = half4(normalInput.tangentWS, viewDirWS.y);
					output.bitangentWS = half4(normalInput.bitangentWS, viewDirWS.z);

					OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
					OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

					output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
					output.shadowCoord = GetShadowCoord(vertexInput);

					output.positionWS = vertexInput.positionWS;
					output.positionCS = vertexInput.positionCS;

					return output;
				}

				void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
				{
					inputData = (InputData)0;

					inputData.positionWS = input.positionWS;

					half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
					inputData.normalWS = TransformTangentToWorld(normalTS,
						half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));

					inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
					viewDirWS = SafeNormalize(viewDirWS);

					inputData.viewDirectionWS = viewDirWS;

					inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

					inputData.fogCoord = input.fogFactorAndVertexLight.x;
					inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
					inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
				}

				half4 LitPassFragment(Varyings input) : SV_Target
				{
					UNITY_SETUP_INSTANCE_ID(input);
					UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

					float2 uv = input.uv;
					half4 baseCol = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);
					half3 normalTS = UnpackNormal(SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, uv));
					//half4 mixTex = SAMPLE_TEXTURE2D(_MixMap, sampler_MixMap, uv);

					half3 albedo = baseCol.rgb;
					half occlusion = 1.0;// mixTex.y;
					half sheen = _Sheen;

					half smoothness = _Smoothness;// *mixTex.x;
					half3 emission = half3(0,0,0);

					half alpha = 1;

					InputData inputData;
					InitializeInputData(input, normalTS, inputData);

					half4 color = ClothLighting(inputData, albedo, sheen, smoothness, occlusion, emission, alpha);


					color.rgb = MixFog(color.rgb, inputData.fogCoord);
					return color;
				}
				ENDHLSL
			}

			Pass
			{
				Name "ShadowCaster"
				Tags{"LightMode" = "ShadowCaster"}

				ZWrite On
				ZTest LEqual
				Cull Off

				HLSLPROGRAM
				// Required to compile gles 2.0 with standard srp library
				#pragma prefer_hlslcc gles
				#pragma exclude_renderers d3d11_9x
				#pragma target 2.0

				// GPU Instancing
				#pragma multi_compile_instancing

				#pragma vertex ShadowPassVertex
				#pragma fragment ShadowPassFragment

				#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

				ENDHLSL
			}

			Pass
			{
				Name "DepthOnly"
				Tags{"LightMode" = "DepthOnly"}

				ZWrite On
				ColorMask 0
				Cull Off

				HLSLPROGRAM
				// Required to compile gles 2.0 with standard srp library
				#pragma prefer_hlslcc gles
				#pragma exclude_renderers d3d11_9x
				#pragma target 2.0

				// GPU Instancing
				#pragma multi_compile_instancing

				#pragma vertex DepthOnlyVertex
				#pragma fragment DepthOnlyFragment

				#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
				ENDHLSL
			}
	}
	FallBack "Hidden/InternalErrorShader"
}
