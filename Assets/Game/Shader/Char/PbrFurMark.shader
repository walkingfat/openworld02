﻿Shader "WalkingFat/Char/AnimeFur01"
{
	Properties
	{
		_BaseMap("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_MixMap("Mix Map", 2D) = "white" {} // R = Smoothness; G = Metallic; B = Occlusion; A = FurMark;
		_SSSMap("SSS Map", 2D) = "white" {}
		//_EmissionMap("Emission", 2D) = "black" {}
		//_Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 300

		Pass {
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			ZWrite On

			Stencil {
				Ref 202
				Comp always
				Pass replace
			}

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// -------------------------------------
			// Universal Pipeline keywords
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			// -------------------------------------
			// Unity defined keywords
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "CharCore.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float2 texcoord : TEXCOORD0;
				float2 lightmapUV : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float4 uv : TEXCOORD0;
				DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);

				float4 positionCS : SV_POSITION;
				float3 positionWS : TEXCOORD2;
				float4 normalWS : TEXCOORD3;
				float4 tangentWS : TEXCOORD4;    // xyz: tangent, w: viewDir.y
				float4 bitangentWS : TEXCOORD5;    // xyz: bitangent, w: viewDir.z
				half4 fogFactorAndVertexLight : TEXCOORD6;
				float4 shadowCoord : TEXCOORD7;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
				float4 _BaseMap_ST;
				float4 _BumpMap_ST;
				//float4 _EmissionMap_ST;
				float4 _MixMap_ST;
			CBUFFER_END

			TEXTURE2D(_BaseMap);							SAMPLER(sampler_BaseMap);
			TEXTURE2D(_BumpMap);							SAMPLER(sampler_BumpMap);
			TEXTURE2D(_MixMap);								SAMPLER(sampler_MixMap);
			TEXTURE2D(_SSSMap);								SAMPLER(sampler_SSSMap);

			//global

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
				VertexNormalInputs normalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);
				half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
				half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
				half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

				output.uv.xy = TRANSFORM_TEX(input.texcoord, _BaseMap);

				output.normalWS = half4(normalInput.normalWS, viewDirWS.x);
				output.tangentWS = half4(normalInput.tangentWS, viewDirWS.y);
				output.bitangentWS = half4(normalInput.bitangentWS, viewDirWS.z);

				OUTPUT_LIGHTMAP_UV(input.lightmapUV, unity_LightmapST, output.lightmapUV);
				OUTPUT_SH(output.normalWS.xyz, output.vertexSH);

				output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
				output.shadowCoord = GetShadowCoord(vertexInput);

				output.positionWS = vertexInput.positionWS;
				output.positionCS = vertexInput.positionCS;

				return output;
			}

			void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
			{
				inputData = (InputData)0;

				inputData.positionWS = input.positionWS;

				half3 viewDirWS = half3(input.normalWS.w, input.tangentWS.w, input.bitangentWS.w);
				inputData.normalWS = TransformTangentToWorld(normalTS,
					half3x3(input.tangentWS.xyz, input.bitangentWS.xyz, input.normalWS.xyz));

				inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
				viewDirWS = SafeNormalize(viewDirWS);

				inputData.viewDirectionWS = viewDirWS;

				inputData.shadowCoord = TransformWorldToShadowCoord(inputData.positionWS);

				inputData.fogCoord = input.fogFactorAndVertexLight.x;
				inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
				inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				float2 uv = input.uv.xy;
				half4 baseTex = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);
				half3 normalTS = UnpackNormal(SAMPLE_TEXTURE2D(_BumpMap, sampler_BumpMap, uv));
				half4 mixTex = SAMPLE_TEXTURE2D(_MixMap, sampler_MixMap, uv);
				
				half3 albedo = baseTex.rgb;
				half alpha = baseTex.a;
				half smoothness = mixTex.r;
				half metallic = mixTex.g;
				half occlusion = mixTex.b;

				half3 emission = half3(0, 0, 0);

				InputData inputData;
				InitializeInputData(input, normalTS, inputData);

				// radiance
				Light mainLight = GetMainLight(inputData.shadowCoord);

				half NdotL = saturate(dot(inputData.normalWS, mainLight.direction));
				half NdotV = saturate(dot(inputData.normalWS, inputData.viewDirectionWS));

				half furNdotL = NdotL * NdotL;
				half viewAO = min(1, 1.4 - NdotV);
				//return viewAO;

				half3 rimDir = inputData.normalWS + mainLight.direction;
				half rim = saturate(dot(inputData.viewDirectionWS, -rimDir));
				half furRim = rim * rim;
				//return furRim;

				half furFresnel = pow(1 - NdotV, 2) * 0.5;
				//return step(0.85, 1 - furFresnel);

				half buttomFade = max(0, dot(inputData.normalWS, half3(0, 1, 0)));
				furRim *= min(1, buttomFade + 0.2);
				//wreturn rim;
				//return min(1, NdotL * mainLight.distanceAttenuation * mainLight.shadowAttenuation + rim + furFresnel);

				half furAtten = saturate(mainLight.distanceAttenuation * mainLight.shadowAttenuation * furNdotL + furRim) * viewAO;
				half4 sssCol = SAMPLE_TEXTURE2D(_SSSMap, sampler_SSSMap, half2(furAtten, 0.5));
				//return furAtten;

				//half3 radiance = mainLight.color * lerp(sssCol.rgb, half3(furAtten, furAtten, furAtten), step(_Time.y % 2, 1));
				//return half4(radiance, 1);
				half3 radiance = mainLight.color * sssCol.rgb;

				half4 color = FurLighting(inputData, albedo, metallic, smoothness, occlusion, radiance, emission, alpha);

				

				/*
				half NdotV = max(0.0, dot(inputData.normalWS, inputData.viewDirectionWS));
				half rim0 = saturate(pow(1 - NdotV, 3) * 1);
				half rim1 = min(1, rim0 * 3);

				half clipValue = min(1, (1 - rim0) * furValue * furValue * furValue * furValue + (1 - rim1) * 2);
				*/
				//return clipValue;

				//color.rgb += rimCol;

				//clip(clipValue - 0.9);

				color.rgb = MixFog(color.rgb, inputData.fogCoord);
				return color;
			}
			ENDHLSL
		}

		Pass
		{
			Name "ShadowCaster"
			Tags{"LightMode" = "ShadowCaster"}

			ZWrite On
			ZTest LEqual
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex ShadowPassVertex
			#pragma fragment ShadowPassFragment

			#include "Assets/Game/Shader/Common/ShadowCasterPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "DepthOnly"
			Tags{"LightMode" = "DepthOnly"}

			ZWrite On
			ColorMask 0
			//Cull[_Cull]

			HLSLPROGRAM
			// Required to compile gles 2.0 with standard srp library
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex DepthOnlyVertex
			#pragma fragment DepthOnlyFragment

			#include "Assets/Game/Shader/Common/DepthOnlyPass.hlsl"
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}
