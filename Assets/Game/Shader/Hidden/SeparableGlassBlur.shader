﻿Shader "Hidden/SeparableGlassBlur"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "" {}

	}

	SubShader
	{
		Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		LOD 200

		Pass
		{
			Name "ForwardLit"
			Tags { "LightMode" = "UniversalForward" }

			//Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency 
			Cull Off

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0

			#pragma multi_compile_fog

			//--------------------------------------
			// GPU Instancing
			#pragma multi_compile_instancing

			#pragma vertex LitPassVertex
			#pragma fragment LitPassFragment

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct Attributes
			{
				float4 positionOS : POSITION;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct Varyings
			{
				float2 uv : TEXCOORD0;
				float4 uv01 : TEXCOORD1;
				float4 uv23 : TEXCOORD2;
				float4 uv45 : TEXCOORD3;
				float4 positionCS : SV_POSITION;

				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _MainTex_ST;
			CBUFFER_END

			TEXTURE2D(_MainTex);					SAMPLER(sampler_MainTex);

			// global
			float4 offsets;

			Varyings LitPassVertex(Attributes input)
			{
				Varyings output = (Varyings)0;

				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_TRANSFER_INSTANCE_ID(input, output);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

				output.uv = TRANSFORM_TEX(input.texcoord, _MainTex);

				output.uv01 = input.texcoord.xyxy + offsets.xyxy * float4(1, 1, -1, -1);
				output.uv23 = input.texcoord.xyxy + offsets.xyxy * float4(1, 1, -1, -1) * 2.0;
				output.uv45 = input.texcoord.xyxy + offsets.xyxy * float4(1, 1, -1, -1) * 3.0;

				VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

				//output.positionWS = vertexInput.positionWS;
				output.positionCS = vertexInput.positionCS;
				//output.positionNDC = vertexInput.positionNDC;

				return output;
			}

			half4 LitPassFragment(Varyings input) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(input);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

				half4 color = float4 (0, 0, 0, 0);
				color += 0.40 * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv);
				color += 0.15 * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv01.xy);
				color += 0.15 * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv01.zw);
				color += 0.10 * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv23.xy);
				color += 0.10 * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv23.zw);
				color += 0.05 * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv45.xy);
				color += 0.05 * SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.uv45.zw);

				return color;
			}
			ENDHLSL
		}
	}
	FallBack "Hidden/InternalErrorShader"
}